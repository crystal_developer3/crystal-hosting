<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Terms_condition extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();			
		}

		function index()
		{
			$data['terms_data'] = $this->Production_model->get_all_with_where('web_settings','','',array('id'=>1));
			$this->load->view('terms_condition',$data);	
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>