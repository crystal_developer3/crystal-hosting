<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Post_management extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();			
		}

		function details($id)
		{
			/*Post add in ip-address start*/
			$data['ip_address'] = get_client_ip(); 
			$data['post_id'] = $id; 
			// echo"<pre>"; print_r($data); exit;

			$get_post = $this->Production_model->get_all_with_where('post_visitor','', '', array('post_id' => $id,'ip_address'=>$data['ip_address']));
			// echo"<pre>"; echo $this->db->last_query(); print_r($get_post); exit;
            if ($get_post == null) {
                $record = $this->Production_model->insert_record('post_visitor',$data);
            }
			/*Post add in ip-address end*/

			// echo"<pre>"; print_r($data); exit;
			$where['post_management.post_id'] = $id;

			$join[0]['table_name'] = 'category';
			$join[0]['column_name'] = 'category.category_id = post_management.category_id';
			$join[0]['type'] = 'left';

			$join[1]['table_name'] = 'sub_category';
			$join[1]['column_name'] = 'sub_category.sub_category_id = post_management.sub_category_id';
			$join[1]['type'] = 'left';

			$join[2]['table_name'] = 'user_register';
			$join[2]['column_name'] = 'user_register.user_id = post_management.user_id';
			$join[2]['type'] = 'left';

			$join[3]['table_name'] = 'country';
			$join[3]['column_name'] = 'country.id = user_register.id_country';
			$join[3]['type'] = 'left';
			
			$join[4]['table_name'] = 'state';
			$join[4]['column_name'] = 'state.id = user_register.id_state';
			$join[4]['type'] = 'left';

			$join[5]['table_name'] = 'city';
			$join[5]['column_name'] = 'city.id = user_register.id_city';
			$join[5]['type'] = 'left';

			$join[6]['table_name'] = 'package_management';
			$join[6]['column_name'] = 'package_management.id = post_management.package_id';
			$join[6]['type'] = 'left';
			
			$data['post_details'] = $this->Production_model->jointable_descending(array('post_management.*','category.category_name','category.cat_image','sub_category.sub_category_name','user_register.name','user_register.user_email','user_register.id_country','user_register.id_state','user_register.id_city','user_register.address','user_register.gender','user_register.mobile_no','user_register.mobile_no_display','country.country_name','state.state_name','city.city_name','package_management.name as package_name','package_management.price as package_price','package_management.currency'),'post_management','',$join,'post_id','desc',$where);
			// echo"<pre>"; print_r($data); exit;
			$data['post_images'] = $this->Production_model->get_all_with_where('post_similer_image','image_id','desc',array('post_id'=>$id));
			$this->load->view('post_details',$data);	
		}

		function edit($id) {
			$data['category_details'] = $this->Production_model->get_all_with_where('category', 'category_id', 'asc', array('status' => '1'));
	        $data['post_details'] = $this->Production_model->get_all_with_where('post_management', '', '', array('post_id' => $id));
	        $data['post_images'] = $this->Production_model->get_all_with_where('post_similer_image', '', '', array('post_id' => $id));
	        // echo"<pre>";echo $this->db->last_query(); print_r($data); exit;
	        $this->load->view('post_add',$data);
	    }

	    function update_post() {

	        $post_id = $this->input->post('post_id');
	        $data = $this->input->post();
	        unset($data['package_id']);
	        // echo"<pre>"; print_r($data); exit;

	        $record = $this->Production_model->update_record('post_management', $data,array('post_id'=>$post_id));

            //=========================================================================//
            //======================= multiple image upload start =====================//
            //=========================================================================//
            
            if (!is_dir(POST_NEED_IMG)) {
                mkdir(POST_NEED_IMG);
                mkdir(POST_NEED_IMG . 'thumbnail/');
                @chmod(POST_NEED_IMG, 0777);
            }

            $config['upload_path'] = POST_NEED_IMG;
            $config['allowed_types'] = '*';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            $files = $_FILES;
            $cpt = count($_FILES['similar_image']['name']);

            if ($files['similar_image']['name'][0] != "") {
                for ($i = 0; $i < $cpt; $i++) {
                    $_FILES['similar_image']['name'] = $files['similar_image']['name'][$i];
                    $_FILES['similar_image']['type'] = $files['similar_image']['type'][$i];
                    $_FILES['similar_image']['tmp_name'] = $files['similar_image']['tmp_name'][$i];
                    $_FILES['similar_image']['error'] = $files['similar_image']['error'][$i];
                    $_FILES['similar_image']['size'] = $files['similar_image']['size'][$i];

                    // $this->load->library('upload',$config);
                    $this->upload->do_upload('similar_image');
                    $image_name = $this->upload->data();
                    $image_name = $image_name['file_name'];

                    $this->Production_model->generate_thumbnail(POST_NEED_IMG, $image_name);

                    $imagedata = array(
                        'post_id' => $post_id,
                        'user_id' => $this->session->userdata('login_id'),
                        'similar_image' => $image_name,
                        'create_date' => date('Y-m-d H:i:s')
                    );

                    // echo "<pre>";print_r($imagedata);
                    $image_id = $this->Production_model->insert_record('post_similer_image', $imagedata);
                }
            }
            // exit;
            //=========================================================================//
            //======================= multiple image upload end =======================//
            //=========================================================================//
           
            $this->session->set_flashdata('success', 'Post is updated successfully...!');
            redirect($_SERVER['HTTP_REFERER']);
	    }

		function delete_post($id)
	    {
	        /*Old image delete*/
	        $get_image = $this->Production_model->get_all_with_where('post_similer_image','','',array('post_id'=>$id));
	        // echo"<pre>"; print_r($get_image); exit;
	        
	        if ($get_image !=null && $get_image[0]['similer_image'] !=null && !empty($get_image[0]['similer_image']))
	        {
	           @unlink(POST_NEED_IMG.$get_image[0]['similer_image']);
	           @unlink(POST_NEED_IMG.'thumbnail/'.$get_image[0]['similer_image']);
	           // @unlink(CAT_MEDIUM_IMAGE.$get_image[0]['similer_image']);
	        }
	        $record = $this->Production_model->delete_record('post_management',array('post_id'=>$id));
	        $record = $this->Production_model->delete_record('post_similer_image',array('post_id'=>$id));
	        $record = $this->Production_model->delete_record('product_like',array('product_id'=>$id,'user_id'=>$this->session->userdata('login_id')));
	       
	        if ($record != 0) {
	            $this->session->set_flashdata('success', 'Post Deleted Successfully....!');
	            redirect($_SERVER['HTTP_REFERER']);
	        }
	        else
	        {
	            $this->session->set_flashdata('error', 'Post Not Deleted....!');
	            redirect($_SERVER['HTTP_REFERER']);
	        }
	    }

	    function close_post($id)
	    {
	    	$data = array('order_status'=>'1');
	        $record = $this->Production_model->update_record('post_management',$data,array('post_id'=>$id));
	       
	        if ($record == 1) {
	            $this->session->set_flashdata('success', 'Post Closed Successfully....!');
	            redirect($_SERVER['HTTP_REFERER']);
	        }
	        else
	        {
	            $this->session->set_flashdata('error', 'Post Not Closed....!');
	            redirect($_SERVER['HTTP_REFERER']);
	        }
	    }

	    function delete_image($id)
	    {
	        /*Old image delete*/
	        $get_image = $this->Production_model->get_all_with_where('post_similer_image','','',array('image_id'=>$id));
	        // echo"<pre>"; print_r($get_image); exit;
	        
	        if ($get_image !=null && $get_image[0]['similar_image'] !=null && !empty($get_image[0]['similar_image']))
	        {
	           @unlink(POST_NEED_IMG.$get_image[0]['similar_image']);
	           @unlink(POST_NEED_IMG.'thumbnail/'.$get_image[0]['similar_image']);
	        }
	        // echo POST_NEED_IMG.'thumbnail/'.$get_image[0]['similar_image']; exit;
	        $record = $this->Production_model->delete_record('post_similer_image',array('image_id'=>$id));
	       
	        if ($record != 0) {
	            $this->session->set_flashdata('success', 'Image Deleted Successfully....!');
	            redirect($_SERVER['HTTP_REFERER']);
	        }
	        else
	        {
	            $this->session->set_flashdata('error', 'Image Not Deleted....!');
	            redirect($_SERVER['HTTP_REFERER']);
	        }
	    }

	    public function change_post_status(){
			if (!$this->input->is_ajax_request()) {
			   exit('No direct script access allowed');
			}
			$response_array = array('success'=>false);
			if ($this->input->post('current_status') == '0') {
				$status = '1';
			}
			if ($this->input->post('current_status') == '1') {
				$status = '0';
			}
			$records = array(
				'status' => $status,
			);
			$conditions = array(
				"where" => array("post_id" => $this->input->post('id')),
			);
			// echo"<pre>"; print_r($records); exit;
			$this->common_model->update_data($this->input->post('table'), $records, $conditions); 
			$response_array['success'] = true;
			echo json_encode($response_array);exit;
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>