<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        // header('Content-Type: application/json');
    }
	
    function login() {
    	if ($this->input->is_ajax_request()) { // for web
            $response_array = login($this->input->post());
            // echo "<pre>";print_r($response_array);
			// echo $this->session->login_id;
            
            echo json_encode($response_array);
            exit;
        }else if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					login($data);            
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	/*function registration() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!decodeJson(array('firstName','lastName','dob','email','password','address','city','states','country','contactNumber','zipCode'),$data)){
				$first_name=$data['firstName'];
				$last_name=$data['lastName'];
				$dob=$data['dob'];
				$email=$data['email'];
				$password=$data['password'];
				$address=$data['address'];
				$city=$data['city'];
				$state=$data['states'];
				$country=$data['country'];
				$phone_number=$data['contactNumber'];
				$zip_code=$data['zipCode'];

				if(!checkEmpty($is_api_call)){ // for api
					if(checkHeader()){
					registration($first_name ,$last_name ,$dob ,$email ,$password ,	$address ,	$city ,	$state ,$country ,$phone_number ,$zip_code ,$is_api_call); 		
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
	
				}
			}
		}else {
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}*/

	function registration() {

		if ($this->input->is_ajax_request()) { // for web
            $response_array = registration($this->input->post());
            echo json_encode($response_array);
            exit;
        }else if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!decodeJson(array('firstName','lastName','dob','email','password','address','city','states','country','contactNumber','zipCode'),$data)){
				$data['first_name']	  = $data['firstName'];
				$data['last_name']	  = $data['lastName'];
				$data['id_country']	  = $data['country'];
				$data['id_state']	  = $data['states'];
				$data['id_city']	  = $data['city'];
				$data['phone_number'] = $data['contactNumber'];
				$data['zip_code']	  = $data['zipCode'];

				if(!checkEmpty($is_api_call)){ // for api
					if(checkHeader()){
					registration($data); 		
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
	
				}
			}
		}else {
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}
	
	function forgotPassword() {
		if ($this->input->is_ajax_request()) { // for web
			$data = $this->input->post();
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
            $response_array = forgotPassword($this->input->post('email'),$is_api_call);
            echo json_encode($response_array);
            exit;
        }else if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!decodeJson(array("email"),$data)){
				$email = $data['email'];
				if(!checkEmpty($is_api_call)){ // for api
					header('Content-Type: application/json');
					if(checkHeader()){
						forgotPassword($email,$is_api_call);            
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
	
				}
			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function changePassword() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!decodeJson(array('id','old_password','new_password'),$data)){
				$id=$data['id'];
				$oldPassword=$data['old_password'];
				$newPassword=$data['new_password'];
								
				if(!checkEmpty($is_api_call)){ // for api
					if(checkHeader()){
						changePassword($id,$oldPassword,$newPassword,$is_api_call); 		
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
					
				}
			}
		}else {
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function resetPassword() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!decodeJson(array('id','new_password'),$data)){
				$id = $data['id'];
				$newPassword = $data['new_password'];
				if(!checkEmpty($is_api_call)){ // for api
					if(checkHeader()){
						resetPassword($id,$newPassword,$is_api_call); 		
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
					
				}
			}
		}else {
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}
	
	function getProfile() {
		if ($this->input->is_ajax_request()) { // for web
			// echo "<pre>";print_r($this->input->post('id'));
            $response_array = getProfile($this->input->post('id'),'');
            echo json_encode($response_array);
            exit;
        }else if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!decodeJson(array("id"),$data)){
				$user_id = $data['id'];
				if(!checkEmpty($is_api_call)){ // for api
					header('Content-Type: application/json');
					if(checkHeader()){
						getProfile($user_id,$is_api_call);            
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
	
				}
			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	/*function updateProfile() {	
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!decodeJson(array('id','firstName','lastName','dob','email','address','city','states','country','contactNumber','zipCode'),$data)){
				$id=$data['id'];
				$first_name=$data['firstName'];
				$last_name=$data['lastName'];
				$dob=$data['dob'];
				$email=$data['email'];
				$address=$data['address'];
				$city=$data['city'];
				$state=$data['states'];
				$country=$data['country'];
				$phone_number=$data['contactNumber'];
				$zip_code=$data['zipCode'];
				
				if(!checkEmpty($is_api_call)){ // for api
					if(checkHeader()){
						updateProfile($id,$first_name,$last_name,$dob,$email,$address,$city,$state,$country,$phone_number,$zip_code,$is_api_call); 		
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
					
				}
			}
		}else {
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}*/

	function updateProfile() {

		if ($this->input->is_ajax_request()) { // for web
            $response_array = updateProfile($this->input->post());
            echo json_encode($response_array);
            exit;
        }else if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!decodeJson(array('id','firstName','lastName','dob','email','address','city','states','country','contactNumber','zipCode'),$data)){
				$data['first_name']	  = $data['firstName'];
				$data['last_name']	  = $data['lastName'];
				$data['id_country']	  = $data['country'];
				$data['id_state']	  = $data['states'];
				$data['id_city']	  = $data['city'];
				$data['phone_number'] = $data['contactNumber'];
				$data['zip_code']	  = $data['zipCode'];

				if(!checkEmpty($is_api_call)){ // for api
					if(checkHeader()){
						updateProfile($data); 		
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
	
				}
			}
		}else {
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function feedback() {
		if ($this->input->is_ajax_request()) { // for web
            $response_array = feedback($this->input->post());
            echo json_encode($response_array);
            exit;
        }else if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
				$data['phone_number'] =  $data['contactNumber'];				

				if(!checkEmpty($is_api_call)){ // for api
					if(checkHeader()){
						feedback($data); 		
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ // for web
	
				}
			
		}else {
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}
	function add_contact_us() {
		if ($this->input->is_ajax_request()) {
            $response_array = add_contact_us($this->input->post());
            echo json_encode($response_array);
            exit;
        }else if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';

			if(!checkEmpty($is_api_call)){ // for api
				if(checkHeader()){
					add_contact_us($data); 		
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web
				
			}
			
		}else {
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}
	function add_subscriber() {
		if ($this->input->is_ajax_request()) {
            $response_array = add_subscriber($this->input->post());
            echo json_encode($response_array);
            exit;
        }else if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!checkEmpty($is_api_call)){ // for api
				if(checkHeader()){
					add_subscriber($data); 		
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web
				
			}
		}else {
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	/*function getPlans() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					getPlans($is_api_call);            
					
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}*/

	
	/*function generate_ticket() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';

			if(!decodeJson(array('user_id','name','email','subject','message','priority'),$data)){

				$user_id			=  $data['user_id'];
				$name				=  $data['name'];
				$email				=  $data['email'];				
				$subject			=  $data['subject'];
				$message			=  $data['message'];
				$department			=  $data['department'];
				$related_service	=  $data['related_service'];
				$priority			=  $data['priority'];
				
				if (!empty($_FILES['user_file'])) {
					$filePath = TICKET_FILE;
					if (!is_dir(TICKET_FILE)) {
						mkdir(TICKET_FILE);
						@chmod(TICKET_FILE,0777);
					}
					$new_file_name = 'TICKET-'.$user_id.'-'.rand(1000,100000)."-".str_replace(' ', '-',$_FILES['user_file']['name']);

					$destFile = $filePath . $new_file_name; 
					$filename = $_FILES["user_file"]["tmp_name"];       
					move_uploaded_file($filename,  $destFile);
				}
				else{
					$new_file_name = '';
				}

				if(!checkEmpty($is_api_call)){ // for api
					if(checkHeader()){
						generate_ticket($user_id,$name ,$email ,$subject ,$message,$department,$related_service,$priority ,$new_file_name,$is_api_call); 		
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
	
				}
			}
		}else {
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}*/

	/*function getHostingDetails() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					getHostingDetails($is_api_call);            
					
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}*/
	/*function getCustomerSupport() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					getCustomerSupport($is_api_call);            
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}*/

	/*function getDepartments() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					getDepartments($is_api_call);            
					
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}*/
	/*function getMyPlans() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			$user_id = isset($data->user_id) ? $data->user_id :'';
			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					getMyPlans($user_id,$is_api_call);            
					
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}*/
	function add_search_domain() {

		if ($this->input->is_ajax_request()) { // for web
			$data = $this->input->post();
			// echo "<pre>";print_r($data);exit;
            $response_array = add_search_domain($this->input->post());
            echo json_encode($response_array);
            exit;
        }else if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!decodeJson(array('name','email','domain_type','domain_name','mobile_no'),$data)){
				if(!checkEmpty($is_api_call)){ // for api
					if(checkHeader()){
					add_search_domain($data); 		
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
	
				}
			}
		}else {
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}
	function confirm_register($user_id) {
		if(!empty($user_id) && $user_id != "") {
			$data = array('status'=>'1');
			$record = $this->Production_model->update_record('user_register',$data,array('id'=>$user_id));
			redirect(base_url('login'),'refresh');
		}else {
			redirect(base_url('login'),'refresh');
		}
	}

}
?>
