<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        // header('Content-Type: application/json');
    }

	function checkHeader(){
        $headers = $this->input->get_request_header('Authenticate');
        if (isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')) {
            return true;
        }else{
            return false;
        }
	}
	
	function checkEmpty($value){
		if(isset($value) && !empty($value) && $value !=null){
			return false;
		}else{
			return true;
		}
	}

	function returnResponse($result,$message,$data=null){
		$return =array();
		$return['result'] = $result;
		$return['message'] = $message;
		$return['data'] = $data;
		return $return;
	}

    function login() {
		if(!$this->checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			$email = isset($data->email) ? $data->email :'';
			$password = isset($data->password) ? $data->password :'';
			if(!$this->checkEmpty($is_api_call)){ // for api
				if($this->checkHeader()){
					if($this->checkEmpty($email)){
						echo json_encode($this->returnResponse("Fail","Email address is empty",null));exit;
					}else if($this->checkEmpty($password)){
						echo json_encode($this->returnResponse("Fail","Password is empty",null));exit;
					}else{
						login($email,$password,$is_api_call);            
					}
				}else {
					$return['result'] = "fail";
					$return['message'] = "You are not authorized user...";
					echo json_encode($return);exit;
				}
			}else{ //for web

			}
		}
	}
	
	/*
		Registration
	*/
	function registration() {
		
		if(!$this->checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			$first_name = isset($data->first_name) ? $data->first_name :'';
			$last_name = isset($data->last_name) ? $data->last_name :'';
			$dob = isset($data->dob) ? $data->dob :'';
			$email = isset($data->email) ? $data->email :'';
			$password = isset($data->password) ? $data->password :'';
			$address = isset($data->address) ? $data->address :'';
			$city = isset($data->city) ? $data->city :'';
			$state = isset($data->state) ? $data->state :'';
			$country = isset($data->country) ? $data->country :'';
			$phone_number = isset($data->phone_number) ? $data->phone_number :'';
			$zip_code = isset($data->zip_code) ? $data->zip_code :'';

			if(!$this->checkEmpty($is_api_call)){ // for api
				if($this->checkHeader()){
					if($this->checkEmpty($first_name) || $this->checkEmpty($last_name)|| $this->checkEmpty($dob) || $this->checkEmpty($email) || $this->checkEmpty($password) || $this->checkEmpty($address) || $this->checkEmpty($city) ||$this->checkEmpty($state) || $this->checkEmpty($country) || $this->checkEmpty($phone_number) || $this->checkEmpty($zip_code)){
						echo json_encode($this->returnResponse("Fail","Email address is empty",null));exit;
					}else{
						registration($first_name ,$last_name ,$dob ,$email ,$password ,	$address ,	$city ,	$state ,$country ,$phone_number ,	$zip_code ,$is_api_call); 
					}
				}else {
					$return['result'] = "fail";
					$return['message'] = "You are not authorized user...";
					echo json_encode($return);exit;
				}
			}else{ //for web

			}
		}
    }
}
?>
