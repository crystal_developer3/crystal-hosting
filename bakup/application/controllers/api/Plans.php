<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Plans extends CI_Controller {

    function __construct() {
        parent::__construct();
        // header('Content-Type: application/json');
    }
	
	function getPlans() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					getPlans($is_api_call);            
					
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function generate_ticket() {
		$ticket_data = $this->input->post();
		// echo "<pre>";print_r($ticket_data);exit;
		if ($this->input->is_ajax_request()) { // for web
			if (!empty($_FILES['user_file']['name'])) {
				$filePath = TICKET_FILE;
				if (!is_dir(TICKET_FILE)) {
					mkdir(TICKET_FILE);
					@chmod(TICKET_FILE,0777);
				}
				$new_file_name = 'TICKET-'.$this->session->login_id.'-'.rand(1000,9999)."-".str_replace(' ', '-',$_FILES['user_file']['name']);

				$destFile = $filePath . $new_file_name; 
				$filename = $_FILES["user_file"]["tmp_name"];       
				move_uploaded_file($filename,  $destFile);
			}
			else{
				$new_file_name = '';
			}
			$ticket_data['new_file_name'] = $new_file_name;
			$ticket_data['user_id'] = $this->session->login_id;
            // echo "<pre>";print_r($ticket_data);exit;

            $response_array = generate_ticket($ticket_data);
            // echo "<pre>";print_r($response_array);exit;
            echo json_encode($response_array);
            exit;

        }else if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';

			if(!decodeJson(array('user_id','name','email','subject','message','priority'),$data)){

				$user_id			=  $data['user_id'];
				$name				=  $data['name'];
				$email				=  $data['email'];				
				$subject			=  $data['subject'];
				$message			=  $data['message'];
				$department			=  $data['department'];
				$related_service	=  $data['related_service'];
				$priority			=  $data['priority'];
				
				if (!empty($_FILES['user_file'])) {
					$filePath = TICKET_FILE;
					if (!is_dir(TICKET_FILE)) {
						mkdir(TICKET_FILE);
						@chmod(TICKET_FILE,0777);
					}
					$new_file_name = 'TICKET-'.$user_id.'-'.rand(1000,9999)."-".str_replace(' ', '-',$_FILES['user_file']['name']);

					$destFile = $filePath . $new_file_name; 
					$filename = $_FILES["user_file"]["tmp_name"];       
					move_uploaded_file($filename,  $destFile);
				}
				else{
					$new_file_name = '';
				}

				if(!checkEmpty($is_api_call)){ // for api
					if(checkHeader()){
						generate_ticket($user_id,$name ,$email ,$subject ,$message,$department,$related_service,$priority ,$new_file_name,$is_api_call); 		
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
	
				}
			}
		}else {
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function getMyPlans() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			$user_id = isset($data->user_id) ? $data->user_id :'';
			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					getMyPlans($user_id,$is_api_call);
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function add_ticket_reply() {
		$ticket_data = $this->input->post();
		// echo "<pre>";print_r($ticket_data);exit;
		if ($this->input->is_ajax_request()) { // for web
			if (!empty($_FILES['ticket_document']['name'])) {
				$filePath = TICKET_REPLY_FILE;
				if (!is_dir(TICKET_REPLY_FILE)) {
					mkdir(TICKET_REPLY_FILE);
					@chmod(TICKET_REPLY_FILE,0777);
				}
				$new_file_name = 'REPLY-'.$this->session->login_id.'-'.rand(1000,9999)."-".str_replace(' ', '-',$_FILES['ticket_document']['name']);

				$destFile = $filePath . $new_file_name; 
				$filename = $_FILES["ticket_document"]["tmp_name"];       
				move_uploaded_file($filename,  $destFile);
			}
			else{
				$new_file_name = '';
			}
			$ticket_data['ticket_document'] = $new_file_name;
            // echo "<pre>";print_r($ticket_data);exit;

            $response_array = add_ticket_reply($ticket_data);
            // echo "<pre>";print_r($response_array);exit;
            echo json_encode($response_array);
            exit;

        }else {
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}
}
?>
