<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Security extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();			
		}

		function index()
		{
			$data['security_data'] = $this->Production_model->get_all_with_where('security','','',array('status'=>'1'));
			$data['features_details'] = $this->Production_model->get_all_with_where('security_feature','','',array('status'=>'1'));
			$this->load->view('security',$data);	
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>