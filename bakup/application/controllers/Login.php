<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$data = array();
		/* FOR CHECKING REMEMBER ME DETAILS */
        if (get_cookie("REMEMBER")) {
            if (decrypt_cookie(get_cookie("REMEMBER")) == "yes") {
                $data['user_email'] = decrypt_cookie(get_cookie("USER"));
                $data['password'] = decrypt_cookie(get_cookie("PWD"));
                $data['remember_me'] = "yes";
            }
        }   
        // echo"<pre>"; print_r($data); exit;
		$this->load->view('login',$data);
	}

	public function dologin()
	{	
		$user_email = $this->input->post('user_email');
		$password = $this->input->post('password');	
		
		$this->form_validation->set_rules('user_email', 'Please Enter Useremail', 'required');
		$this->form_validation->set_rules('password', 'Please Enter Password', 'required');
		$user = '';

		if ($this->form_validation->run() == FALSE)
        {
        	$this->load->view('login');
        	// $this->session->set_flashdata('error','Please Enter Useremail and Password...!');
			// redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
			// echo"<pre>"; print_r($_POST); exit;
			$password = $this->input->post('password');	
			// $admnlogin = $this->Production_model->get_all_with_where('tbl_admin','','',array('admin_email'=>$user_email)); 
			
			// if (count($admnlogin) == 1){				
			// 	$dbpass = $this->encryption->decrypt($admnlogin[0]['admin_password']);
			// 	// echo "<pre>"; print_r($dbpass); print_r($admnlogin); exit;			 
			// 	if($password == $dbpass)
			// 	{ 
			// 		$session = array(
			// 			'loginid' => $admnlogin[0]['admin_id'],
			// 			'username' => $admnlogin[0]['admin_name'],
			// 			'useremail' => $admnlogin[0]['admin_email']
			// 		);	
			// 		$this->session->set_userdata($session);
			// 		$user = 'admin';
			// 		// echo "<pre>"; print_r($dbpass); exit;					 
			// 	}		
			// }
			// else{
				$usrlogin = $this->Production_model->email_or_mobile_login('user_register',$user_email);
				// $usrlogin = $this->Production_model->get_all_with_where('user_register','','',array('user_email'=>$user_email));
				// echo"<pre>"; echo $this->db->last_query(); print_r($usrlogin); exit;
				
				if ($usrlogin !=null) {
					$usrdbpass = $this->encryption->decrypt($usrlogin[0]['password']);

					if($usrlogin[0]['status'] == '1'){							
						if($password == $usrdbpass)
						{ 
							// cookie start //
							if ($this->input->post("remember_me") == "yes") {
	                            $time = time() + 60 * 60 * 24;
	                            $cookie = array(
	                                'name' => 'USER',
	                                'value' => encrypt_cookie($user_email),
	                                'expire' => $time,
	                            );
	                            set_cookie($cookie);
	                            $cookie = array(
	                                'name' => 'PWD',
	                                'value' => encrypt_cookie($password),
	                                'expire' => $time,
	                            );
	                            set_cookie($cookie);
	                            $cookie = array(
	                                'name' => 'REMEMBER',
	                                'value' => encrypt_cookie("yes"),
	                                'expire' => $time,
	                            );
	                            set_cookie($cookie);
	                        } else {
	                            delete_cookie("USER");
	                            delete_cookie("PWD");
	                            delete_cookie("REMEMBER");
	                        }

							// cookie end //

							$session = array(
								'login_id' => $usrlogin[0]['user_id'],
								'username' => $usrlogin[0]['name'],
								'useremail' => $usrlogin[0]['user_email'],
								'useremobile' => $usrlogin[0]['mobile_no'],
								'profile_picture' => $usrlogin[0]['profile_picture'],
							);		
							$this->session->set_userdata($session);
							// echo "<pre>"; exit;				
							$user = 'user';						
						}	
						else{
							$this->session->set_flashdata('error','Your password is incorrect...!');
							redirect('login');
						}						
					}
					else{
						$this->session->set_flashdata('error','Your account is not active please contact admin...!');
						redirect('login');
					}					 
				} 
				else{
					$this->session->set_flashdata('error','Email or phone is not valid...!');
					redirect('login');
				}		
			// }
		}

		// if($user == 'admin'){
		// 	$this->session->set_flashdata('success', 'Admin Login Successfully...!');
		// 	redirect(base_url('admin/dashboard'));
		// // echo "string"; exit;	
		// }
		if($user == 'user'){
			// $this->session->set_flashdata('success', 'User Login Successfully...!');
			redirect(base_url('home'));
		}		
		else{    
			$this->session->set_flashdata('error','User email and Password Not Match...!');
			redirect(base_url('login'));	
		}
	}

	public function Logout()
	{
		$array_items = array('login_id', 'username', 'useremail');
		$this->session->unset_userdata($array_items);
		$this->session->sess_destroy();
		redirect(base_url());		
	}

	public function forgot_password()
	{
		$email = $this->input->post('UserEmail');
		
		$this->form_validation->set_rules('UserEmail', 'UserEmail', 'required');		
		if ($this->form_validation->run() == TRUE)
		{
			$get_record = $this->Production_model->get_all_with_where('user_register','','',array('user_email'=>$email));
			// echo $pass = $this->encryption->encrypt('123456'); exit;

			// echo"<pre>"; print_r($get_record); exit; 

			if ($get_record !=null){				
				if($email == $get_record[0]['user_email'])
				{			        
					// $otp = rand(111111,999999);
					// $data = array('otp'=>$otp);
					$tocken = rand(111111,999999);
					$data = array('tocken'=>$tocken);
					$data['password'] = $this->encryption->decrypt($get_record[0]['password']); 
					
					/*call reset password link*/
					$update_tocken = array('forgot_pwd_tocken'=>$tocken);
					$record = $this->Production_model->update_record('user_register',$update_tocken,array('user_email'=>$email));

					// Add Mail Code Here....

					// $send_mail = $this->Production_model->send_email('Online-cart Forgotpassword',$email,'Your password is : "'.$password.'"','','',''); // $email;
					// if ($send_mail == 1) {
					// 	//echo "send";
					// 	$this->session->set_flashdata('success','Please check your mail-id');
					// 	redirect($_SERVER['HTTP_REFERER']);
					// }
					// else{
					// 	// echo "not send";
					// 	echo $this->email->print_debugger();
					// }

					// $this->load->view('mail_form/forgot_password/forgot_email', $data);
					$send_mail = $this->Production_model->mail_send('Forgotpassword',$email,'','mail_form/forgot_password/forgot_email',$data,'');
					// echo"<pre>"; print_r($send_mail); exit;

					if ($send_mail == 1) {
			            $this->session->set_flashdata('success','Please check your mail-id');
			            redirect($_SERVER['HTTP_REFERER']);
			        }
				}
				else
				{
					$this->session->set_flashdata('error','Your Email'.$email.' is not in our record...!');
					redirect($_SERVER['HTTP_REFERER']);
				}
			}
			else
			{
				$this->session->set_flashdata('error','Your Email'.$email.' is not in our record...!');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
		else
		{
			$this->session->set_flashdata('error','Please Enter Email First...!');
			redirect($_SERVER['HTTP_REFERER']);
		}	
	}

	function reset_password($tocken){
		$data = $this->input->post();
		$data['password'] = isset($data['password']) ? $this->encryption->encrypt($data['password']) : '';
		$data['forgot_pwd_tocken'] = '';
		unset($data['confirm_password']);

		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('confirm_password', 'confirm_password', 'required|matches[password]');

		if ($this->form_validation->run() == FALSE)
        {
        	$this->load->view('reset-password');
            // $this->session->set_flashdata('error','Please All The Fields Required Data...!');
			// redirect($_SERVER['HTTP_REFERER']);
        }
        else{
        	$record = $this->Production_model->update_record('user_register',$data,array('forgot_pwd_tocken'=>$tocken));
	        // echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;

	        if ($record == 1) {
	            $this->session->set_flashdata('success', 'Password reset done successfully...!');
	            redirect($_SERVER['HTTP_REFERER']);
	        }
	        else
	        {
	            $this->session->set_flashdata('error', 'Please Try After Some time...!');
	            redirect($_SERVER['HTTP_REFERER']);
	        }
        }
	}

	function add_subscribe()
	{
		// $data = $this->input->post();
		// echo"<pre>"; print_r($data); exit;

		$this->form_validation->set_rules('subscribe_email', 'subscribe email', 'required');
		if ($this->form_validation->run() == FALSE)
        {
        	echo"validation_error";
        }
        else
        {
        	$data = $this->input->post();        	
        	$response_array = array('success'=>false);

          	$get_user_email = $this->Production_model->get_all_with_where('subscribe','','',array('subscribe_email'=>$data['subscribe_email']));

			if (count($get_user_email) > 0) {
				$response_array['email_allredy'] = true;
			}
			else
			{
				$record = $this->Production_model->insert_record('subscribe',$data);
				if ($record !='') {
					$response_array['success'] = true;
				}
				else
				{
					$response_array['error'] = error;
				}
			}
			echo json_encode($response_array);	
		}	
	}
}
?>
