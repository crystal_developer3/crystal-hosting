<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Getdata extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function get_dependent_record() {

        $action = $this->input->post('action');
        $next_data = $this->input->post('next_data');
        $current_id = $this->input->post('current_id');
        $current_name = $this->input->post('current_name');

        if (isset($action) && $action == "get-next-data" && isset($next_data) && isset($current_id)) {

            $response_array = array(
                'success' => false,
                'data' => '',
            );

            $info = $this->common_model->select_data($next_data, array('where' => array($current_name => $current_id)));
            $data = "<option value=''>Select " . $next_data . "</option>";

            if ($info['row_count'] > 0) {

                foreach ($info['data'] as $key => $value) {

                    $data .= "<option value='" . $value['id'] . "'>" . $value[$next_data . '_name'] . "</option>";
                }
            }

            $response_array['data'] = $data;

            $response_array['success'] = true;



            echo json_encode($response_array);

            exit();
        }
    }

}
