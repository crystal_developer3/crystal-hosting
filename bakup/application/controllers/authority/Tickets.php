<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Tickets extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index($page_number='') {
        $settings = array(
            "url" => site_url() . "authority/tickets/index/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select"=>"tickets.*,user_register.first_name","join"=>array("table"=>"user_register","join_type"=>"LEFT","join_conditions"=>"tickets.user_id=user_register.id"),"ORDER BY"=>array('id'=>'DESC'));
        $data = $this->common_model->get_pagination("tickets", $conditions, $settings);
        if (isset($this->session->ticket_msg) && $this->session->ticket_msg != '') {
            $data = array_merge($data, array("success" => $this->session->ticket_msg));
            $this->session->plan_msg = '';
        }
        $records = array(
            'notification' => '1',
        );
        $conditions = array(
            "where" => array("notification" => '0'),
        );
        $this->common_model->update_data("tickets", $records, $conditions); 
        // echo"<pre>".$this->db->last_query(); exit;
        unset($settings, $conditions);        
        $this->load->view('authority/tickets/view', $data);
    }
    public function details($id) {
        $conditions = array("where"=>array('tickets.id'=>$id),"select"=>"tickets.*,user_register.first_name,user_register.id as user_id","join"=>array("table"=>"user_register","join_type"=>"LEFT","join_conditions"=>"tickets.user_id=user_register.id"),"ORDER BY"=>array('id'=>'DESC'));
        $data = $this->common_model->select_data("tickets", $conditions);
        $data = $data['data'][0];

        $where['tickets.id'] = $id;
        
        $join1[0]['table_name'] = 'tickets';
        $join1[0]['column_name'] = 'ticket_reply.ticket_id = tickets.id';
        $join1[0]['type'] = 'left';

        $join1[1]['table_name'] = 'user_register';
        $join1[1]['column_name'] = 'ticket_reply.reply_from = user_register.id or ticket_reply.reply_to = user_register.id';
        $join1[1]['type'] = 'left';
        
        $data['tickets_details'] = $this->Production_model->jointable_descending(array('ticket_reply.*','ticket_reply.ticket_reply','ticket_reply.reply_type','ticket_reply.reply_from','ticket_reply.reply_to','user_register.first_name','ticket_reply.reply_date','ticket_reply.reply_time'), 'ticket_reply', '', $join1, 'tickets.id', 'desc', $where,'',array('ticket_reply.id'));

        if (isset($this->session->ticket_msg) && $this->session->ticket_msg != '') {
            $data = array_merge($data, array("success" => $this->session->ticket_msg));
            $this->session->plan_msg = '';
        }
        // echo "<pre>";print_r($data);exit;
        unset($settings, $conditions);        
        $this->load->view('authority/tickets/details', $data);
    }
    /*function add()
    {
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data['features_details'] = $this->common_model->select_data("ticket_feature", $conditions);
        if (isset($this->session->ticket_msg) && $this->session->ticket_msg != '') {
            $data = array_merge($data, array("success" => $this->session->ticket_msg));
            $this->session->plan_msg = '';
        }
        $data['ticket_details'] = array();
        unset($settings, $conditions);
        $this->load->view('authority/tickets/add-edit',$data);
    }    */
    /*function insert_ticket()
    {
        if($this->input->post()){    
            $data = $this->input->post();        
            $create_date = date('Y-m-d H:i:s');
            $resultSet = Array();                 
            $get_sub_title = $this->Production_model->get_all_with_where('tickets','','',array('title'=> $title));
            $data = array(
                'title' => $data['title'],
                'create_date' => $create_date
            );
            $record = $this->Production_model->insert_record('tickets',$data);
            $chkbox_id = $this->input->post('feature_id');
            foreach ($chkbox_id as $key => $value) {
                $data = array(
                    "ticket_id"=>$record,
                    "feature_id"=>$value
                );
                $record1 = $this->Production_model->insert_record('ticket_related_feature',$data);
            }
            if($record !='') {
                $this->session->set_flashdata('success', 'Ticket Add Successfully....!');
                redirect(base_url('authority/ticket'));
            }else{
                $this->session->set_flashdata('error', 'Ticket Not Added....!');
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }*/
    /*function edit($id)
    {
        
        // echo "<pre>";print_r($data);exit;
        $data['user_details'] = $this->Production_model->get_all_with_where('user_register','','',array('status'=> '1')); 
        $this->load->view('authority/tickets/add-edit',$data);
    }*/
    /*function update_ticket()
    {
        $data = $this->input->post();
        $id = $data['id'];
        $modified_date = date('Y-m-d H:i:s');
        $id = $this->input->post('id');        
        $data = array(
            'title' => $data['title'],
            'modified_date' => $modified_date
        );
        $record = $this->Production_model->update_record('tickets',$data,array('id'=>$id));
        if ($record == 1) {
            $chkbox_id = $this->input->post('feature_id');
            foreach ($chkbox_id as $key => $value) {
                $alreadyt_exists = $this->Production_model->get_all_with_where('ticket_related_feature','','',array('ticket_id'=>$id,'feature_id'=>$value));
                // echo $id." ".$value."<br>";
                if(empty($alreadyt_exists)){
                    $data = array(
                        "ticket_id"=>$id,
                        "feature_id"=>$value
                    );
                    $record1 = $this->Production_model->insert_record('ticket_related_feature',$data);
                }
            }
            $this->session->set_flashdata('success', 'Ticket Update Successfully....');
            redirect(base_url('authority/ticket'));
        }else{
            $this->session->set_flashdata('error', 'Ticket Not Updated....');
            redirect($_SERVER['HTTP_REFERER']);
        }        
    }*/
    function delete_ticket($id)
    {        
        $record = $this->Production_model->delete_record('tickets',array('id'=>$id));
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Ticket Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Ticket Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $record = $this->Production_model->delete_record('tickets',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Ticket Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Ticket Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>