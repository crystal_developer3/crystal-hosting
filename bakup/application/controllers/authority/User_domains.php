<?php defined('BASEPATH') OR exit('No direct script access allowed');
class User_domains extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index($page_number='') {
        $settings = array(
            "url" => site_url() . "authority/user_domains/index/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data = $this->common_model->get_pagination("user_domain", $conditions, $settings);
        if (isset($this->session->invoice_msg) && $this->session->invoice_msg != '') {
            $data = array_merge($data, array("success" => $this->session->invoice_msg));
            $this->session->invoice_msg = '';
        }
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data['user_details'] = $this->common_model->select_data("user_register", $conditions);
        unset($settings, $conditions);        
        $this->load->view('authority/user_domains/view', $data);
    }
    function add()
    {
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data['user_details'] = $this->common_model->select_data("user_register", $conditions);
        if (isset($this->session->invoice_msg) && $this->session->invoice_msg != '') {
            $data = array_merge($data, array("success" => $this->session->invoice_msg));
            $this->session->invoice_msg = '';
        }
        $conditions1 = array("select" => "*",'ORDER BY'=>array('id'=>'ASC'),"where"=>array('status'=>'1'));
        $data['currency_data'] = $this->common_model->select_data("currency", $conditions1);

        $conditions2 = array("select" => "*",'ORDER BY'=>array('id'=>'ASC'),"where"=>array('status'=>'1'));
        $data['domain_type_data'] = $this->common_model->select_data("domain", $conditions2);
        
        $data['user_domains_details'] = array();
        // echo "<pre>";print_r($data);exit;
        unset($settings, $conditions);
        $this->load->view('authority/user_domains/add-edit',$data);
    }    
    function insert_invoice()
    {
        if($this->input->post()){    
            $data = $this->input->post();
            $create_date = date('Y-m-d H:i:s');
            $data = array(
                'domain_name' => $data['domain_name'],
                'book_date' => format_date_ymd($data['book_date']),
                'expiry_date' => add_year_date_ymd($data['book_date'],1),
                'user_id' => $data['user_id'],
                'price' => $data['price'],
                'domain_type_id' => $data['domain_type_id'],
                'currency_id' => $data['currency_id'],
                'create_date' => $create_date
            );
            // echo "<pre>";print_r($data);exit;        
            $record = $this->Production_model->insert_record('user_domain',$data);
            if($record !='') {

                // $where['invoice.id'] = $record;
                // $join[0]['table_name'] = 'user_register';
                // $join[0]['column_name'] = 'user_register.id = invoice.user_id';
                // $join[0]['type'] = 'left';
                
                // $user_details = $this->Production_model->jointable_descending(array('user_register.first_name','user_register.last_name', 'user_register.address', 'invoice.id'), 'user_domain', '', $join, 'invoice.id', 'desc', $where);
                // $data = array_merge($user_details[0], $data);
          
                // $data['order_details'] = $this->Production_model->jointable_descending(array('invoice.*','invoice_item.description','invoice_item.price','invoice_item.quantity','invoice_item.total'), 'user_domain', '', $join1, 'invoice.id', 'desc', $where);
                // // echo "<pre>".$this->db->last_query();print_r($data);exit;

                // require_once 'dompdf/autoload.inc.php';
                // $dompdf = new Dompdf\Dompdf(array('enable_remote' => true));
                // $html = ($this->load->view('order_genrate/invoice', $data, true));
                // echo $html;
                // $dompdf->loadHtml($html);
                // $dompdf->setPaper('A4', 'landscape');
                // $dompdf->render();
                // // $pdf = $dompdf->output();     
                // // $dompdf->stream();
                // $file_name = str_replace(' ', '', $data['invoice_no']) . '.pdf';
                // $update_data['file_name'] = $file_name;
                // file_put_contents('generated_invoices/' . $file_name, $dompdf->output()); // file save in folder
                // $this->session->set_flashdata('success', 'User Domain Add Successfully....!');
                // $this->Production_model->update_record('user_domain',$update_data,array('id'=>$record));

                redirect(base_url('authority/user_domains'));
            }else{
                $this->session->set_flashdata('error', 'User Domain Not Added....!');
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }
    function edit($id)
    {
        $data['user_domains_details'] = $this->Production_model->get_all_with_where('user_domain','','',array('id'=>$id));
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data['user_details'] = $this->common_model->select_data("user_register", $conditions);
        if (isset($this->session->invoice_msg) && $this->session->invoice_msg != '') {
            $data = array_merge($data, array("success" => $this->session->invoice_msg));
            $this->session->invoice_msg = '';
        }
        $data['invoice_item_details'] = $this->Production_model->get_all_with_where('invoice_item','','',array('status'=> '1','invoice_id'=>$id));
        $conditions1 = array("select" => "*",'ORDER BY'=>array('id'=>'ASC'),"where"=>array('status'=>'1'));
        $data['currency_data'] = $this->common_model->select_data("currency", $conditions1); 

        $conditions2 = array("select" => "*",'ORDER BY'=>array('id'=>'ASC'),"where"=>array('status'=>'1'));
        $data['domain_type_data'] = $this->common_model->select_data("domain", $conditions2);
        // echo "<pre>";print_r($data);exit;
        $this->load->view('authority/user_domains/add-edit',$data);
    }
    function update_invoice()
    {
        $data = $this->input->post();
        $id = $data['id'];
        $modified_date = date('Y-m-d H:i:s');
        $data = array(
            'domain_name' => $data['domain_name'],
            'book_date' => format_date_ymd($data['book_date']),
            'expiry_date' => add_year_date_ymd($data['book_date'],1),
            'user_id' => $data['user_id'],
            'price' => $data['price'],
            'domain_type_id' => $data['domain_type_id'],
            'currency_id' => $data['currency_id'],
            'modified_date' => $modified_date
        );
        $record = $this->Production_model->update_record('user_domain',$data,array('id'=>$id));
        if ($record) {
            $this->session->set_flashdata('success', 'User Domain Update Successfully....');
            redirect(base_url('authority/user_domains'));
        }else{
            $this->session->set_flashdata('error', 'User Domain Not Updated....');
            redirect($_SERVER['HTTP_REFERER']);
        }        
    }
    function delete_invoice($id)
    {        
        $record = $this->Production_model->delete_record('user_domain',array('id'=>$id));
        if ($record != 0) {
            $this->session->set_flashdata('success', 'User Domain Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'User Domain Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $record = $this->Production_model->delete_record('user_domain',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'User Domain Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'User Domain Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>