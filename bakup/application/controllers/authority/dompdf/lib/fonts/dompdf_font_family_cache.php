<?php return array (
  'sans-serif' => array(
    'normal' => $fontDir . '\Helvetica',
    'bold' => $fontDir . '\Helvetica-Bold',
    'italic' => $fontDir . '\Helvetica-Oblique',
    'bold_italic' => $fontDir . '\Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $fontDir . '\Helvetica',
    'bold' => $fontDir . '\Helvetica-Bold',
    'italic' => $fontDir . '\Helvetica-Oblique',
    'bold_italic' => $fontDir . '\Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $fontDir . '\ZapfDingbats',
    'bold' => $fontDir . '\ZapfDingbats',
    'italic' => $fontDir . '\ZapfDingbats',
    'bold_italic' => $fontDir . '\ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $fontDir . '\Symbol',
    'bold' => $fontDir . '\Symbol',
    'italic' => $fontDir . '\Symbol',
    'bold_italic' => $fontDir . '\Symbol',
  ),
  'serif' => array(
    'normal' => $fontDir . '\Times-Roman',
    'bold' => $fontDir . '\Times-Bold',
    'italic' => $fontDir . '\Times-Italic',
    'bold_italic' => $fontDir . '\Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $fontDir . '\Courier',
    'bold' => $fontDir . '\Courier-Bold',
    'italic' => $fontDir . '\Courier-Oblique',
    'bold_italic' => $fontDir . '\Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $fontDir . '\DejaVuSans-Bold',
    'bold_italic' => $fontDir . '\DejaVuSans-BoldOblique',
    'italic' => $fontDir . '\DejaVuSans-Oblique',
    'normal' => $fontDir . '\DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $fontDir . '\DejaVuSansMono-Bold',
    'bold_italic' => $fontDir . '\DejaVuSansMono-BoldOblique',
    'italic' => $fontDir . '\DejaVuSansMono-Oblique',
    'normal' => $fontDir . '\DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $fontDir . '\DejaVuSerif-Bold',
    'bold_italic' => $fontDir . '\DejaVuSerif-BoldItalic',
    'italic' => $fontDir . '\DejaVuSerif-Italic',
    'normal' => $fontDir . '\DejaVuSerif',
  ),
  'open_regular' => array(
    'normal' => $fontDir . '\bff3694adddc86ce9c21c6bac233df2a',
  ),
  'open_light' => array(
    'normal' => $fontDir . '\9925578bd97074400aff0a94d6e09f49',
  ),
  'open_semi' => array(
    'normal' => $fontDir . '\2a37df9c9014eeeecda63e5f1128a152',
  ),
) ?>