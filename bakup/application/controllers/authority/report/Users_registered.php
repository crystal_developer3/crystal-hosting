<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users_registered extends CI_Controller {
    private $_search_array = array('full_name'=>'Full name','user_name'=>'User name');
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index() {
        $data = array();       
        // ==================== pagination start ======================== //
        $total_records = $this->Production_model->get_total('user_register');
        if ($total_records !=null) 
        {    
            // ==================== pagination start ======================== //
            $tmp_data = $this->Production_model->get_all_with_where('user_register','','',array());
            $tmp_array['total_record'] = count($tmp_data);
            $tmp_array['url'] = base_url('authority/users_registered/');
            $tmp_array['per_page'] = RECORDS_PER_PAGE;
            $record = $this->Production_model->only_pagination($tmp_array);
            $data['user_details'] = $this->Production_model->get_all_with_where_limit('user_register','user_id','desc',array(),$record['limit'],$record['start']); 
            $data['pagination'] = $record['pagination']; 
            $data['no'] = $record['no']; 
            // echo"<pre>"; print_r($data); exit;
            // ==================== pagination end ======================== //

            //====================== user filter start ======================//
            $start_date=$this->input->post('start_date');
            $end_date=$this->input->post('end_date');
            if($start_date==""){$data['start_date']='';}else{$data['start_date']=$start_date;}
            if($end_date==""){$data['end_date']='';}else{$data['end_date']=$end_date;}
            if($start_date == '' || $end_date == ''){
                $data['user_details'] = $this->Production_model->get_all_with_where_limit('user_register','user_id','desc',array(),$record['limit'],$record['start']); 
            }else{
                $data['user_details'] = $this->Production_model->get_all_with_where_limit_between('user_register','user_id','desc',date('Y-m-d', strtotime($start_date)),date('Y-m-d',strtotime($end_date)),$record['limit'],$record['start']); 
                // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
            }
            //====================== user filter end ======================//
        }
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
        $this->load->view('authority/report/users-registered', $data);
    }

    function excel_genrate_report()
    {       
        $tmp = [];
        // ==================== pagination start ======================== //
        $total_records = $this->Production_model->get_total('user_register');
        if ($total_records !=null) 
        {    
            $config = array();
            $config["base_url"] = base_url() . 'authority/users_registered/';
            $config["total_rows"] = $total_records;
            $config["per_page"] = RECORDS_PER_PAGE;
            $config['use_page_numbers'] = TRUE;
            $config["uri_segment"] = 4;
            $config['num_links'] = $total_records;
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Previous'; 
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li class="prev">';
            $config['prev_tag_close'] = '</li>';
            $config['next_tag_open'] = '<li class="end">';
            $config['next_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $this->pagination->initialize($config);
            $page_number = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            if ($page_number > ceil(($config['total_rows'] / $config['per_page']))) {
                /* Redirect when page limit exceeded */
                redirect($config['base_url']);
            }
            /* GETTING A OFFSET */
            $page = ($page_number == 0) ? 0 : ($page_number * $config['per_page']) - $config['per_page'];
            //echo"<pre>"; print_r($page); exit;
            //====================== user filter start ======================//
            $start_date=$this->input->post('start_date');
            $end_date=$this->input->post('end_date');
            if($start_date==""){$data['start_date']='';}else{$data['start_date']=$start_date;}
            if($end_date==""){$data['end_date']='';}else{$data['end_date']=$end_date;}
            if($start_date == '' || $end_date == ''){
                $data['user_details'] = $this->Production_model->get_all_with_where_limit('user_register','user_id','asc',array(),$config["per_page"], $page); 
            }else{
                $data['user_details'] = $this->Production_model->get_all_with_where_limit_between('user_register','user_id','asc',date('Y-m-d', strtotime($start_date)),date('Y-m-d',strtotime($end_date)),$config["per_page"], $page); 
                // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
            }
            //====================== user filter end ======================//
            // $data["user_details"] = $this->Production_model->get_all_with_where_limit('user_register','user_id','asc',array(),$config["per_page"], $page); // list for all users ....
            $data['links'] = $this->pagination->create_links();  
            $data['no'] = $page+1;
        }
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
        // ==================== pagination end ======================== //
        $tmp = array_merge($tmp,$data['user_details']);
        // excel export data strart //
        $excel_data = [];
        foreach ($tmp as $key => $value) {
            // if ($value['user_type'] == '1') {
            //     $user_type = "Buyer";
            // }
            // if ($value['user_type'] == '2') {
            //     $user_type = "Selle";
            // }
            // if ($value['user_type'] == '3') {
            //     $user_type = "Both";
            // }
            $tmp = array(
                'Name' => $value['name'],
                // 'User Type' => $user_type,
                'User Email' => $value['user_email'],
                'Mobile No' => $value['mobile_no'],
                'Create Date' => date('d-m-Y',strtotime($value['create_date'])),
            );          
            array_push($excel_data,$tmp);
        }
        // echo"<pre>"; print_r($excel_data); exit;
        function filterData(&$str)
        {
            $str = preg_replace("/\t/", "\\t", $str);
            $str = preg_replace("/\r?\n/", "\\n", $str);
            if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
        }
        // file name for download
        $fileName = "User-report" . date('d-m-Y') . ".xls";
        // headers for download
        header("Content-Disposition: attachment; filename=\"$fileName\"");
        header("Content-Type: application/vnd.ms-excel");
        $flag = false;
        foreach($excel_data as $row) {
            if(!$flag) {
                // display column names as first row
                echo implode("\t", array_keys($row)) . "\n";
                $flag = true;
            }
            // filter data
            array_walk($row, 'filterData');
            echo implode("\t", array_values($row)) . "\n";
        }
        exit;
        // excel export data end //
    }

    public function delete($id = "") {
        if ($id != "") {
            $conditions = array("select" => "user_id", "where" => array("user_id" => intval($id)));
            $user = $this->common_model->select_data("user_register", $conditions);
           
            $conditions = array(
                "where" => array("user_id" => $id),
            );
            $this->common_model->delete_data("user_register", $conditions);
            $this->session->set_flashdata('success', 'Record Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect("authority/users_registered/view");
        }
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $get_record = $this->Production_model->get_all_with_where('user_register','','',array('user_id'=>$value));
            // if ($get_record !=null && $value['category_image'] !=null && !empty($value['category_image']))   
            // {
            //     unlink(CATEGORY_IMAGE.$value['category_image']);
            // }
            $record = $this->Production_model->delete_record('user_register',array('user_id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Record deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Record Not deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
}
?>