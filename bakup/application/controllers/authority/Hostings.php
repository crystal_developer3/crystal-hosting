<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Hostings extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index($page_number='') {
        $settings = array(
            "url" => site_url() . "authority/hostings/index/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data = $this->common_model->get_pagination("hosting_plan", $conditions, $settings);
        // echo"<pre>"; print_r($data); exit;
        if (isset($this->session->tutorial_msg) && $this->session->tutorial_msg != '') {
            $data = array_merge($data, array("success" => $this->session->tutorial_msg));
            $this->session->tutorial_msg = '';
        }
        unset($settings, $conditions);        
        $this->load->view('authority/hostings/view', $data);
    }
    function add()
    {
        $data['hosting_details'] = array();       
        $this->load->view('authority/hostings/add-edit',$data);
    }    
    function insert_hosting()
    {
        $data = $this->input->post();
        $name = $data['name']; 
        $details = $data['details']; 
        $count_hosting = count($data['name']); 
        $create_date = date('Y-m-d H:i:s');
        $resultSet = Array();                 
        if(isset($count_hosting)) {     
            for($i = 0; $i < $count_hosting; $i++){
                $get_sub_name = $this->Production_model->get_all_with_where('hosting_plan','','',array('name'=> $name[$i]));

                if(!empty($get_sub_name)) 
                {
                    $resultSet[] = $name[$i];
                }
            }
            if(!empty($resultSet)) {
                $error = implode(', ', $resultSet);
                $this->session->set_flashdata('error',"$error Hostings name is allredy exist...!");
                redirect(base_url('authority/hostings/add'));
            }else{                           
                for($i = 0; $i < $count_hosting; $i++) {
                    if(!is_dir(HOSTING_IMAGE)){
                        mkdir(HOSTING_IMAGE);                
                        @chmod(HOSTING_IMAGE,0777);
                    }
                    // tutorial icon
                    if($_FILES['host_image']['name'][$i]==""){ $img_name='';}
                    else{
                        $iconPath = HOSTING_IMAGE;  
                        $img_name = substr(md5(uniqid(rand(), true)), 0, 16) . '.' . pathinfo($_FILES['host_image']['name'][$i], PATHINFO_EXTENSION);
                        $destFile = $iconPath . $img_name; 
                        $filename = $_FILES["host_image"]["tmp_name"][$i];       
                        move_uploaded_file($filename,  $destFile);   
                        $data['host_image'] = $img_name;
                    }
                    $data = array(
                        'name' => $name[$i],
                        'details' => $details[$i],
                        'seo_slug' => $name[$i],
                        'seo_slug' => create_slug($name[$i]),
                        'create_date' => $create_date
                    );
                    if($name[$i] !=null) {
                        $record = $this->Production_model->insert_record('hosting_plan',$data);
                    }
                }
                if($record !='') {
                    $this->session->set_flashdata('success', 'Hostings Add Successfully....!');
                    redirect(base_url('authority/tutorial'));
                }else{
                    $this->session->set_flashdata('error', 'Hostings Not Added....!');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
           // exit;
        }
    }

    function edit($id)
    {
        $data['hosting_details'] = $this->Production_model->get_all_with_where('hosting_plan','','',array('id'=>$id));
        $this->load->view('authority/hostings/add-edit',$data);
    }

    function update_hosting()
    {
        $data = $this->input->post();
        $name = $data['name'];
        $details = $data['details']; 
        $modified_date = date('Y-m-d H:i:s');
        $id = $this->input->post('id');
        
        $get_image = $this->Production_model->get_all_with_where('hosting_plan','','',array('id'=>$id));
        // echo"<pre>"; print_r($get_image); exit;
        $data = array(
            'name' => $name[0],
            'details' => $details[0],
            'seo_slug' => create_slug($name[0]),
            'modified_date' => $modified_date
        );

        /*tutorial icon */
        if($_FILES['host_image']['name'][0] !='')
        {
            $imagePath = HOSTING_IMAGE;              
            if(!is_dir($imagePath)){
                mkdir($imagePath);                
                @chmod($imagePath,0777);
            }
            // Old image delete
            if ($get_image !=null && $get_image[0]['host_image'] !=null && !empty($get_image[0]['host_image']))
            {
                @unlink(HOSTING_IMAGE.$get_image[0]['host_image']);
            }
            $img_name = substr(md5(uniqid(rand(), true)), 0, 16) . '.' . pathinfo($_FILES['host_image']['name'][0], PATHINFO_EXTENSION);
            $destFile = $imagePath . $img_name; 
            $filename = $_FILES["host_image"]["tmp_name"][0];       
            move_uploaded_file($filename,  $destFile);
            $data['host_image'] = $img_name;
        }
        $record = $this->Production_model->update_record('hosting_plan',$data,array('id'=>$id));
        if ($record == 1) {
            $this->session->set_flashdata('success', 'Hostings Update Successfully....');
            redirect(base_url('authority/hostings'));
        }else{
            $this->session->set_flashdata('error', 'Hostings Not Updated....');
            redirect($_SERVER['HTTP_REFERER']);
        }        
    }

    function delete_hosting($id)
    {
        /*Old image delete*/
        $get_image = $this->Production_model->get_all_with_where('hosting_plan','','',array('id'=>$id));
        if ($get_image !=null && $get_image[0]['host_image'] !=null && !empty($get_image[0]['host_image']))
        {
           @unlink(HOSTING_IMAGE.$get_image[0]['host_image']);
        }
        $record = $this->Production_model->delete_record('hosting_plan',array('id'=>$id));
      
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Hostings Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Hostings Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $get_image = $this->Production_model->get_all_with_where('hosting_plan','','',array('id'=>$value));
            
            if ($get_image !=null && $get_image[0]['host_image'] !=null && !empty($get_image[0]['host_image']))
            {
               @unlink(HOSTING_IMAGE.$get_image[0]['host_image']);
            }
            $record = $this->Production_model->delete_record('hosting_plan',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Hostings Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Hostings Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>