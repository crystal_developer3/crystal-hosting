<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function index() {
        $data['count_slider'] = $this->Production_model->count_num_of_rows('','home_slider',array());
        $data['count_reg'] = $this->Production_model->count_num_of_rows('','user_register',array());
        $data['count_feedback'] = $this->Production_model->count_num_of_rows('','feedback',array());
        $data['count_tickets'] = $this->Production_model->count_num_of_rows('','tickets',array());
        // echo"<pre>"; print_r($data); exit;
        $this->load->view('authority/dashboard', $data);
    }

}
