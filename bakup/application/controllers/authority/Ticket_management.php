<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ticket_management extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
        date_default_timezone_set('Asia/Kolkata');
    }
    function view_admin_ticket() {
        $where['user_ticket.type'] = 'admin';
        $join[0]['table_name'] = 'ticket_location';
        $join[0]['column_name'] = 'ticket_location.id = user_ticket.ticket_location_id';
        $join[0]['type'] = 'left';
        $join[1]['table_name'] = 'user_register';
        $join[1]['column_name'] = 'user_register.id = user_ticket.user_id';
        $join[1]['type'] = 'left';
        $join[2]['table_name'] = 'ticket_status';
        $join[2]['column_name'] = 'ticket_status.id = user_ticket.ticket_status_id';
        $join[2]['type'] = 'left';
        $data['ticket_details'] = $this->Production_model->jointable_descending(array('user_ticket.*', 'ticket_location.location_name', 'user_register.first_name as name', 'ticket_status.status_name'), 'user_ticket', '', $join, 'id', 'desc', $where);
        $data['type'] = 'admin';
        $this->load->view('authority/ticket-management/view', $data);
    }
    function add() {
        $data['ticket_problem_details'] = $this->Production_model->get_all_with_where('ticket_problem', 'id', 'desc', array('status' => '1'));
        $data['ticket_location_details'] = $this->Production_model->get_all_with_where('ticket_location', 'id', 'desc', array('status' => '1'));
        $data['ticket_status_details'] = $this->Production_model->get_all_with_where('ticket_status', 'id', 'desc', array('status' => '1'));
        $data['priority_details'] = $this->Production_model->get_all_with_where('priority', 'id', 'desc', array('status' => '1'));
        $data['ticket_details'] = array();
        $data['form_title'] = 'Add ticket';
        $this->load->view('authority/ticket-management/add-edit', $data);
    }
    function add_ticket() {
        $admin_id = $this->session->user_info;
        $data = $this->input->post();
        $data['ticket_problem_id'] = implode(',', $data['ticket_problem_id']);
        $data['user_id'] = $admin_id['id'];
        $role_type = $admin_id['role_type'];
        $data['create_date'] = date('Y-m-d', strtotime($data['create_date'])) . ' ' . date('H:i:s');
        $data['preferred_date'] = date('Y-m-d', strtotime($data['preferred_date']));
        $data['type'] = 'admin';
        $record = $this->Production_model->insert_record('user_ticket', $data);
        if ($record != '') {
            //=========================================================================//
            //======================= multiple image upload start =====================//
            //=========================================================================//
            if (!is_dir(TICKET_DOCUMENT)) {
                mkdir(TICKET_DOCUMENT);
                @chmod(TICKET_DOCUMENT, 0777);
            }
            $config['upload_path'] = TICKET_DOCUMENT;
            $config['allowed_types'] = '*';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);
            $files = $_FILES;
            $cpt = count($_FILES['ticket_document']['name']);
            if ($files['ticket_document']['name'][0] != "") {
                for ($i = 0; $i < $cpt; $i++) {
                    $_FILES['ticket_document']['name'] = $files['ticket_document']['name'][$i];
                    $_FILES['ticket_document']['type'] = $files['ticket_document']['type'][$i];
                    $_FILES['ticket_document']['tmp_name'] = $files['ticket_document']['tmp_name'][$i];
                    $_FILES['ticket_document']['error'] = $files['ticket_document']['error'][$i];
                    $_FILES['ticket_document']['size'] = $files['ticket_document']['size'][$i];
                    // $this->load->library('upload',$config);
                    $this->upload->do_upload('ticket_document');
                    $image_name = $this->upload->data();
                    $image_name = $image_name['file_name'];
                    // $this->Production_model->generate_thumbnail(TICKET_DOCUMENT,$image_name);
                    $imagedata = array(
                        'ticket_id' => $record,
                        'ticket_document' => $image_name,
                        'create_date' => date('Y-m-d H:i:s')
                    );
                    // echo "<pre>";print_r($imagedata); 
                    $image_id = $this->Production_model->insert_record('ticket_document_image', $imagedata);
                }
            }
            // exit;
            //=========================================================================//
            //======================= multiple image upload end =======================//
            //=========================================================================//
            /* Mail send all user */
            $get_user = $this->Production_model->get_where_user(array('full_name as name', 'email_address as user_email', 'role_type'), 'user', 'id', 'desc', array('role_type' => $role_type));
            $get_email = $this->Production_model->get_all_with_where('mail_settings', 'id', 'desc', array('request_type' => 'ticket', 'mail_send_by' => '1'));
            $data['get_ticket_image'] = $this->Production_model->get_all_with_where('ticket_document_image', 'image_id', 'desc', array('ticket_id' => $record));
            if (isset($get_email) && $get_email != null) {
                $user_email = array();
                foreach ($get_email as $key => $value) {
                    array_push($user_email, $value['user_email']);
                }
                $request_data = array_merge($data, $get_user[0]);
                $request_data['request_type'] = 'Ticket';
                // $this->load->view('mail_form/user_send_mail/request',$request_data);
                $send_mail = $this->Production_model->mail_send('Ticket-request', $user_email, '', 'mail_form/user_send_mail/request', $request_data, '');
            }
            /* End */
            $this->session->set_flashdata('success', 'Ticket added successfully...!');
            redirect(base_url('authority/ticket-management/view-admin-ticket'));
        } else {
            $this->session->set_flashdata('error', 'Ticket not added...!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    /* Admin ticket end */
    /* Ticket Problem Start */
    public function problem() {
        $data['problem_details'] = $this->Production_model->get_all_with_where('ticket_problem ', 'id', 'desc', array());
        $this->load->view('authority/ticket-management/problem/view', $data);
    }
    function add_problem() {
        $data['problem_details'] = array();
        $data['form_title'] = 'Add problem';
        $this->load->view('authority/ticket-management/problem/add-edit', $data);
    }
    function insert_problem() {
        $data = $this->input->post();
        // echo "<pre>"; print_r($data); exit;
        $this->form_validation->set_rules('problem_name', 'problem name', 'required|is_unique[ticket_problem.problem_name]', array('required' => 'Please enter name', 'is_unique' => 'This name is already available'));
        if ($this->form_validation->run() === FALSE) {
            $data['form_title'] = 'Add problem';
            $data['problem_details'] = array();
            $data = array_merge($data, $_POST);
            $this->load->view('authority/ticket-management/problem/add-edit', $data);
            // echo"<pre>"; print_r($data); exit;
        } else {
            $record = $this->Production_model->insert_record('ticket_problem', $data);
            if ($record != '') {
                $this->session->set_flashdata('success', 'Problem added successfully...!');
                redirect(base_url('authority/ticket-management/problem'));
            } else {
                $this->session->set_flashdata('error', 'Problem not added...!');
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }
    function edit_problem($id) {
        $data['problem_details'] = $this->Production_model->get_all_with_where('ticket_problem ', '', '', array('id' => $id));
        $data['form_title'] = 'Edit Problem';
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
        $this->load->view('authority/ticket-management/problem/add-edit', $data);
    }
    function update_problem() {
        $id = $this->input->post('id');
        $data = $this->input->post();
        // echo"<pre>"; print_r($data); exit;
        $this->form_validation->set_rules('problem_name', 'problem name', 'required|is_unique_with_except_record[ticket_problem.problem_name.id.' . $data['id'] . ']', array('required' => 'Please enter name', "is_unique_with_except_record" => "This name is already available"));
        if ($this->form_validation->run() === FALSE) {
            $data['problem_details'] = $this->Production_model->get_all_with_where('ticket_problem ', '', '', array('id' => $id));
            $data['form_title'] = 'Edit Problem';
            $this->load->view('authority/ticket-management/problem/add-edit', $data);
        } else {
            $record = $this->Production_model->update_record('ticket_problem', $data, array('id' => $id));
            // echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;
            if ($record == 1) {
                $this->session->set_flashdata('success', 'Problem updated successfully...!');
                redirect(base_url('authority/ticket-management/problem'));
            } else {
                $this->session->set_flashdata('error', 'Problem not updated...!');
                redirect(base_url('authority/ticket-management/problem'));
            }
        }
    }
    function delete_problem($id) {
        $record = $this->Production_model->delete_record('ticket_problem', array('id' => $id));
        if ($record == 1) {
            $this->session->set_flashdata('success', 'Problem deleted successfully...!');
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->session->set_flashdata('error', 'Problem not deleted...!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    function multiple_delete_problem() {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        $record = $this->Production_model->get_delete_where_in('ticket_problem', 'id', $chkbox_id);
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Problem deleted successfully...!');
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->session->set_flashdata('error', 'Problem not deleted...!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    /* End */
    /* User ticket */
    function view_ticket() {
        $where['user_ticket.type'] = 'user';
        $join[0]['table_name'] = 'ticket_location';
        $join[0]['column_name'] = 'ticket_location.id = user_ticket.ticket_location_id';
        $join[0]['type'] = 'left';
        $join[1]['table_name'] = 'user_register';
        $join[1]['column_name'] = 'user_register.id = user_ticket.user_id';
        $join[1]['type'] = 'left';
        $join[2]['table_name'] = 'ticket_status';
        $join[2]['column_name'] = 'ticket_status.id = user_ticket.ticket_status_id';
        $join[2]['type'] = 'left';
        $data['ticket_details'] = $this->Production_model->jointable_descending(array('user_ticket.*', 'ticket_location.location_name', 'user_register.first_name as name', 'ticket_status.status_name'), 'user_ticket', '', $join, 'id', 'desc', $where);
        // echo"<pre>"; print_r($data); exit;
        $this->load->view('authority/ticket-management/view', $data);
    }
    function ticket_details($id) {
        $where['user_ticket.id'] = $id;
        $join[0]['table_name'] = 'ticket_location';
        $join[0]['column_name'] = 'ticket_location.id = user_ticket.ticket_location_id';
        $join[0]['type'] = 'left';
        $join[1]['table_name'] = 'user_register';
        $join[1]['column_name'] = 'user_register.id = user_ticket.id';
        $join[1]['type'] = 'left';
        $join[2]['table_name'] = 'ticket_status';
        $join[2]['column_name'] = 'ticket_status.id = user_ticket.ticket_status_id';
        $join[2]['type'] = 'left';
        $join[3]['table_name'] = 'priority';
        $join[3]['column_name'] = 'priority.id = user_ticket.priority_id';
        $join[3]['type'] = 'left';
        $data['ticket_details'] = $this->Production_model->jointable_descending(array('user_ticket.*', 'ticket_location.location_name', 'user_register.first_name as name', 'ticket_status.status_name', 'priority.priority_name'), 'user_ticket', '', $join, 'id', 'desc', $where);
        $data['ticket_documents'] = $this->Production_model->get_all_with_where('ticket_document_image', 'image_id', 'desc', array('ticket_id' => $id));
        // echo"<pre>"; print_r($data); exit;
        $this->load->view('authority/ticket-management/ticket-details', $data);
    }
    function allocate($id) {
        $id = intval($id);
        // $data['user_details'] = $this->Production_model->get_all_with_where('user_register ', '', '', array('role_type_id' => '11', 'status' => '1')); // role-type = enginear user.
        $data['user_details'] = $this->Production_model->get_all_with_where('user_register ', '', '', array('status' => '1')); // role-type = enginear user.
        $data['ticket_problem_details'] = $this->Production_model->get_all_with_where('ticket_problem', 'id', 'desc', array('status' => '1'));
        $data['ticket_location_details'] = $this->Production_model->get_all_with_where('ticket_location', 'id', 'desc', array('status' => '1'));
        $data['ticket_documents'] = $this->Production_model->get_all_with_where('ticket_document_image', 'image_id', 'desc', array('ticket_id' => $id));
        $data['ticket_status_details'] = $this->Production_model->get_all_with_where('ticket_status', 'id', 'desc', array('status' => '1'));
        $data['priority_details'] = $this->Production_model->get_all_with_where('priority', 'id', 'desc', array('status' => '1'));
        $data['form_title'] = 'Allocate ticket';
        $data['ticket_details'] = $this->Production_model->get_all_with_where('user_ticket', '', '', array('id' => $id));
        $allocated_to_id = '';
        $sql = 'SELECT allocated_to_id FROM ticket_activity WHERE ticket_id = "' . $id . '" ORDER BY id DESC LIMIT 1 ';
        $allocated_to_id = $this->common_model->get_data_with_sql($sql);
        if ($allocated_to_id['row_count'] > 0) {
            $allocated_to_id = $allocated_to_id['data'][0]['allocated_to_id'] ? $allocated_to_id['data'][0]['allocated_to_id'] : '';
        } else {
            $allocated_to_id = '';
        }
        $data['allocated_to_id'] = $allocated_to_id;
        if ($this->input->method(true) == 'POST') {
            $form_data = $this->input->post();
            $this->form_validation->set_rules('ticket_id', '', 'trim|required', array('required' => 'Please enter value'));
            $this->form_validation->set_rules('reply_date', '', 'trim|required', array('required' => 'Please select date'));
            $this->form_validation->set_rules('reply_time', '', 'trim|required', array('required' => 'Please select time'));
            $this->form_validation->set_rules('ticket_status_id', '', 'trim|required', array('required' => 'Please select time'));
            if ($this->form_validation->run() === FALSE) {
                $data = array_merge($data, $_POST);
            } else {
                $file_field_name = 'ticket_document';
                $file_name = '';
                if (isset($_FILES[$file_field_name]) && $_FILES[$file_field_name]['name'] != "") {
                    $config['upload_path'] = './uploads/ticket-document/';
                    if (!is_dir($config['upload_path'])) {
                        mkdir($config['upload_path'], true);
                        @chmod($config['upload_path'], 0777);
                    }
                    $config['allowed_types'] = '*';
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload($file_field_name)) {
                    } else {
                        $upload_data = $this->upload->data();
                        $file_name = $upload_data['file_name'];
                    }
                }
                $post_allocated_to_id = $this->input->post('allocated_to_id');
                $allocated_to_type = $this->input->post('allocated_to_type') == '' ? 'user' : $this->input->post('allocated_to_type');
                if ($this->input->post('allocated_to_id') == '') {
                    $post_allocated_to_id = $this->session->userdata['user_info']['id'];
                    $allocated_to_type = 'admin';
                }
            // if ($allocated_to_id != $post_allocated_to_id) {
            //     $records = array('allocated_end_time' => date('Y-m-d H:i:s'));
            //     $conditions = array('ticket_id' => $id, 'ticket_status_id' => '7', 'allocated_to_id' => $allocated_to_id);
            //     $this->common_model->update_data('ticket_activity', $records, $conditions);
            // }
                $used_time = '000:00:00:00';
                $used_time = $this->input->post('days') . ':' . $this->input->post('hours') . ':' . $this->input->post('minutes') . ':' . $this->input->post('seconds');
                // $current_time = $this->get_last_allocated_time($id);
                // $sub_string = '-'.$this->input->post('days').' days -'.$this->input->post('hours').'hours -'.$this->input->post('minutes').' minutes -'.$this->input->post('seconds').' seconds';
                // $allocated_time = date('Y-m-d H:i:s',strtotime($sub_string,strtotime($current_time)));
                $allocated_time = date('Y-m-d H:i:s');
                $records = array(
                    'ticket_id' => $this->input->post('ticket_id'),
                    'reply_date' => date('Y-m-d', strtotime($this->input->post('reply_date'))),
                    'reply_time' => date('H:i:s', strtotime($this->input->post('reply_time'))),
                    'allocated_by_type' => 'admin',
                    'allocated_by_id' => $this->session->user_info['id'],
                    'allocated_to_id' => $post_allocated_to_id,
                    'allocated_to_type' => $allocated_to_type,
                    'ticket_status_id' => $this->input->post('ticket_status_id'),
                    'ticket_reply' => $this->input->post('ticket_reply'),
                    'ticket_document' => $file_name,
                    'allocated_time' => $allocated_time,
                    'used_time' => $used_time,
                    'create_date' => date('Y-m-d H:i:s'),
                    'modified_date' => date('Y-m-d H:i:s'),
                );
                $this->common_model->insert_data('ticket_activity', $records);
                if ($records['ticket_status_id'] == '9') {
                    send_ticket_closed_email($this->input->post('ticket_id'));
                }
                /* Update ticket table */
                $records = array('ticket_status_id' => $records['ticket_status_id']);
                $conditions = array('where' => array('id' => $this->input->post('ticket_id')));
                $this->common_model->update_data('user_ticket', $records, $conditions);
                $this->session->ticket_messages = 'Ticket allocated successfully';
                redirect(base_url() . 'authority/ticket-management/allocate/' . $id);
            }
        }
        $data['ticket_id'] = $id;
        // print_r($data);exit;
        $this->load->view('authority/ticket-management/allocate', $data);
    }
    function add_ticket_allocate() {
        $data = $this->input->post();
        $data['user_id'] = $this->session->userdata('login_id');
        $data['user_type'] = 'admin';
        $this->form_validation->set_rules('software_name', 'software name', 'required', array('required' => 'Please enter software name'));
        $this->form_validation->set_rules('location_id', 'location name', 'required', array('required' => 'Please select site-location'));
        if ($this->form_validation->run() === FALSE) {
            $data = array_merge($data, $_POST);
            $this->load->view('authority/ticket-management/allocate', $data);
        } else {
            unset($data['software_req_details'], $data['location_details'], $data['form_title']);
            $data['notification_status'] = '0';
            if ($_FILES['document']['name'] == "") {
                $img_name = '';
            } else {
                $imagePath = SOFTWARE_DOCUMENT;
                if (!is_dir($imagePath)) {
                    mkdir($imagePath);
                    @chmod($imagePath, 0777);
                }
                $img_name = substr(md5(uniqid(rand(), true)), 0, 16) . '.' . pathinfo($_FILES['document']['name'], PATHINFO_EXTENSION);
                $destFile = $imagePath . $img_name;
                $filename = $_FILES["document"]["tmp_name"];
                move_uploaded_file($filename, $destFile);
                $this->Production_model->generate_thumbnail(SOFTWARE_DOCUMENT, $img_name);
                $data['document'] = $img_name;
            }
            $record = $this->Production_model->insert_record('software_request', $data);
            if ($record != '') {
                /* Mail send all user */
                $where['user_register.status'] = '1';
                $where['user_register.id'] = $data['user_id'];
                $join[0]['table_name'] = 'role_type';
                $join[0]['column_name'] = 'role_type.id = user_register.role_type_id';
                $join[0]['type'] = 'left';
                $get_user = $this->Production_model->jointable_descending(array('user_register.id', 'user_register.first_name as name', 'user_register.user_email', 'user_register.department_id', 'user_register.department_email', 'role_type.role_type'), 'user_register', '', $join, 'user_register.id', 'desc', $where);
                $get_email = $this->Production_model->get_all_with_where('mail_settings', 'id', 'desc', array('request_type' => 'software'));
                if (isset($get_email) && $get_email != null) {
                    foreach ($get_email as $key => $value) {
                        $add_data = array('user_id' => $value['user_id'], 'request_id' => $record, 'request_type' => 'software');
                        $add_record = $this->Production_model->insert_record('mail_activity_user', $add_data);
                    }
                }
                $get_department_email = $this->Production_model->get_all_with_where('user_register', '', '', array('id' => $this->session->userdata('login_id')));
                $department_email = array('user_email' => $get_department_email[0]['department_email']);
                array_push($get_email, $department_email);
                if (isset($get_email) && $get_email != null) {
                    $user_email = array();
                    foreach ($get_email as $key => $value) {
                        array_push($user_email, $value['user_email']);
                    }
                    $request_data = array_merge($data, $get_user[0]);
                    $request_data['request_type'] = 'Software';
                    $request_data['create_date'] = date('Y-m-d h:i:s');
                    $request_data['insert_id'] = $record;
                    $send_mail = $this->Production_model->mail_send('Software-request', $user_email, '', 'mail_form/user_send_mail/request', $request_data, '');
                }
                /* End */
            }
            $this->session->set_flashdata('success', 'Replay added successfully...!');
            redirect(base_url('authority/ticket-management/allocate'));
        }
    }
    function delete_ticket($id) {
        $get_image = $this->Production_model->get_all_with_where('ticket_document_image', '', '', array('ticket_id' => $id));
        if (isset($get_image) && $get_image != null) {
            foreach ($get_image as $key => $value) {
                @unlink(TICKET_DOCUMENT . $value['ticket_document']);
                @unlink(TICKET_DOCUMENT . 'thumbnail/' . $value['ticket_document']);
                $record = $this->Production_model->delete_record('ticket_document_image', array('ticket_id' => $value['ticket_id']));
            }
        }
        $record = $this->Production_model->delete_record('user_ticket', array('id' => $id));
        if ($record == 1) {
            $this->session->set_flashdata('success', 'Ticket deleted successfully...!');
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->session->set_flashdata('error', 'Ticket not deleted...!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    function multiple_delete_ticket() {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $get_image = $this->Production_model->get_all_with_where('ticket_document_image', '', '', array('ticket_id' => $value));
            foreach ($get_image as $key => $img_row) {
                if ($get_image != null && !empty($img_row['ticket_document'])) {
                    @unlink(TICKET_DOCUMENT . $img_row['ticket_document']);
                    @unlink(TICKET_DOCUMENT . 'thumbnail/' . $img_row['ticket_document']);
                    $record = $this->Production_model->delete_record('ticket_document_image', array('ticket_id' => $img_row['ticket_id']));
                }
            }
            $record = $this->Production_model->delete_record('user_ticket', array('id' => $value));
            $record = $this->Production_model->delete_record('ticket_activity', array('ticket_id' => $value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Ticket deleted successfully...!');
            //redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->session->set_flashdata('error', 'Ticket not deleted...!');
            //redirect($_SERVER['HTTP_REFERER']);
        }
        echo json_encode(array('success' => true));
        exit;
    }
    public function get_last_allocated_time($ticket_id) {
        $sql = 'SELECT allocated_time FROM ticket_activity WHERE ticket_id = "' . $ticket_id . '" ORDER BY id DESC LIMIT 1';
        $info = $this->common_model->get_data_with_sql($sql);
        if ($info['row_count'] > 0) {
            return $info['data'][0]['allocated_time'];
        } else {
            return date('Y-m-d H:i:s');
        }
    }
}
