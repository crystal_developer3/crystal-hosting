<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home_slider extends CI_Controller {

	private $_search_array = array('full_name'=>'Full name','user_name'=>'User name');

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function view() {

        // ==================== pagination start ======================== //

        $data = $this->Production_model->pagination_create(base_url('authority/home_slider/view/'),'home_slider','','id','desc',array());
        // echo"<pre>"; print_r($data); exit;
        // ==================== pagination end ======================== //
        
        $this->load->view('authority/home_slider/view',$data);
    }

    function add()
    {
        $data['slider_details'] = array();
        $this->load->view('authority/home_slider/add-edit',$data);
    }

    function add_slider()
    {
        $data = $this->input->post();
        $data['create_date'] = date('Y-m-d h:i:s');
        // echo"<pre>"; print_r($data); exit;
        
        // echo MAX_FILE_SIZE_VIDEO; echo"<pre>"; print_r($_FILES); exit;
        
        if ($_FILES["slider_image"]["size"] >= MAX_FILE_SIZE_IMAGE) {
            $this->session->set_flashdata('error', $this->lang->line("maxfile_upload"). (MAX_FILE_SIZE_IMAGE / 1000000) . 'MB');
            redirect($_SERVER['HTTP_REFERER']);
        }
        if($_FILES['slider_image']['name']==""){ $img_name='';}
        else{
            $imagePath = HOME_SLIDER;            
            if (!is_dir(HOME_SLIDER)) {
                mkdir(HOME_SLIDER);
                @chmod(HOME_SLIDER,0777);
            }

            $img_name = 'SLIDER-'.rand(111,999).create_slug($_FILES['slider_image']['name']);

            $destFile = $imagePath . $img_name; 
            $filename = $_FILES["slider_image"]["tmp_name"];       
            move_uploaded_file($filename,  $destFile);

            /*// create Thumbnail -- IMAGE_SIZES start //
            $image_sizes = array(
                // 'mobile'=>array(75,75),
                'thumb' => array(50, 50), // Slider IMAGE DISPLAY.
                // 'medium' => array(1920, 1280), // Slider IMAGE DISPLAY.
                // 'large' => array(900, 500)
            );

            $this->load->library('image_lib');
            foreach ($image_sizes as $key=>$resize) {
                $config = array(
                    'source_image' => HOME_SLIDER.$img_name,
                    'new_image' => ROOT_UPLOAD_PATH .'/'.$key,
                    'maintain_ratio' => FALSE,
                    'width' => $resize[0],
                    'height' => $resize[1],
                    'quality' =>70,
                );
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();
                // echo"<pre>"; print_r($config); 
            }            
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

            // create Thumbnail -- IMAGE_SIZES end //*/
            $data['slider_image'] = $img_name;
        }
        // echo "<pre>"; print_r($data); exit;

        $record = $this->Production_model->insert_record('home_slider',$data);
        if ($record !='') {
            $this->session->set_flashdata('success', 'Slider Add Successfully....!');
            redirect(base_url('authority/home_slider/view')); 
        }
        else
        {
            $this->session->set_flashdata('error', 'Slider Not Added....!');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function edit($id)
    {
        $data['slider_details'] = $this->Production_model->get_all_with_where('home_slider','','',array('id'=>$id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
        $this->load->view('authority/home_slider/add-edit',$data);
    }

    function update_slider()
    {
        $slider_id = $this->input->post('id');
        $data = $this->input->post();
        $data['modified_date'] = date('Y-m-d H:i:s');
        // echo"<pre>"; print_r($data); exit;
        
        if($_FILES['slider_image']['name'] !='')
        {
            $get_image = $this->Production_model->get_all_with_where('home_slider','','',array('id'=>$slider_id));
            // echo"<pre>"; print_r($get_image); exit;
            if ($get_image !=null && $get_image[0]['slider_image'] !=null && !empty($get_image[0]['slider_image']))
            {
                @unlink(HOME_SLIDER.$get_image[0]['slider_image']);
                // @unlink(HOME_SLIDER.'thumbnail/medium/'.$get_image[0]['slider_image']);
                @unlink(HOME_SLIDER.'thumbnail/thumb/'.$get_image[0]['slider_image']);
            }
            if (!is_dir(HOME_SLIDER)) {
                mkdir(HOME_SLIDER);
                @chmod(HOME_SLIDER,0777);
            }
            $imagePath = HOME_SLIDER;
            $img_name = 'SLIDER-'.rand(111,999).create_slug($_FILES['slider_image']['name']);

            $destFile = $imagePath . $img_name; 
            $filename = $_FILES["slider_image"]["tmp_name"];       
            move_uploaded_file($filename,  $destFile);

            // create Thumbnail -- IMAGE_SIZES start //
            /*$image_sizes = array(
                // 'mobile'=>array(75,75),
                'thumb' => array(50, 50), // Slider IMAGE DISPLAY.
                // 'medium' => array(1920, 1280), // Slider IMAGE DISPLAY.
                // 'large' => array(900, 500)
            );

            $this->load->library('image_lib');
            foreach ($image_sizes as $key=>$resize) {
                $config = array(
                    'source_image' => HOME_SLIDER.$img_name,
                    'new_image' => ROOT_UPLOAD_PATH .'/'.$key,
                    'maintain_ratio' => FALSE,
                    'width' => $resize[0],
                    'height' => $resize[1],
                    'quality' =>70,
                );
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();
                // echo"<pre>"; print_r($config); 
            }            
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

            // create Thumbnail -- IMAGE_SIZES end //*/
            $data['slider_image'] = $img_name;
        }
        // echo "<pre>"; print_r($data); exit;

        $record = $this->Production_model->update_record('home_slider',$data,array('id'=>$slider_id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;
        if ($record == 1) {
            $this->session->set_flashdata('success', 'Slider Update Successfully....!');
            redirect(base_url('authority/home_slider/view'));
        }
        else
        {
            $this->session->set_flashdata('error', 'Slider Not Updated....!');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function delete($id)
    {
        $get_image = $this->Production_model->get_all_with_where('home_slider','','',array('id'=>$id));
        
        if ($get_image !=null && $get_image[0]['slider_image'] !=null && !empty($get_image[0]['slider_image']))
        {
            @unlink(HOME_SLIDER.$get_image[0]['slider_image']);
            // @unlink(HOME_SLIDER.'thumbnail/medium/'.$get_image[0]['slider_image']);
            @unlink(HOME_SLIDER.'thumbnail/thumb/'.$get_image[0]['slider_image']);
        }
        $record = $this->Production_model->delete_record('home_slider',array('id'=>$id));

        if ($record == 1) {
            $this->session->set_flashdata('success', 'Slider Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Slider Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $get_image = $this->Production_model->get_all_with_where('home_slider','','',array('id'=>$value));
  
            if ($get_image !=null && !empty($get_image[0]['slider_image']))
            {
                @unlink(HOME_SLIDER.$get_image[0]['slider_image']);
                @unlink(HOME_SLIDER.'thumbnail/thumb/'.$get_image[0]['slider_image']);
            }
            $record = $this->Production_model->delete_record('home_slider',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Slider Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Slider Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>