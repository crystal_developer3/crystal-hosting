<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Website_settings extends CI_Controller {

	private $_search_array = array('full_name'=>'Full name','user_name'=>'User name');

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function view() {
        // $settings = array(
        //     "url" => site_url() . "authority/userlist/view/",
        //     "per_page" => RECORDS_PER_PAGE,
        // );
  //       $current_filter_users = array('search_key'=> '', 'search_value' => '');
		// if(isset($_REQUEST['clear_filter'])){
		// 	$this->session->current_filter_users = $current_filter_users;
		// 	redirect(base_url().'authority/userlist/view');
		// }
		
		// if(!empty($this->session->current_filter_users)){
		// 	$current_filter_users = $this->session->current_filter_users;
		// }
		// if ($this->input->post("submit")) {
		// 	if($this->input->post('search_key') != '') {
		// 		$current_filter_users['search_key'] = $this->input->post('search_key');
		// 	}
		// 	if($this->input->post('search_value') != '') {
		// 		$current_filter_users['search_value']= $this->input->post('search_value');
		// 	}
		// }
		
		// $like = array();
		// if($current_filter_users['search_key'] != '' && $current_filter_users['search_value']){
		// 	$like = array('LIKE'=>array($current_filter_users['search_key']=>$current_filter_users['search_value']));
		// }
		// if(!empty($like)){
		// 	$conditions = array("select" => "user_id,full_name,user_name,email,mobile_no,date_of_birth,address,profile_picture,gender,status");
		// 	$conditions = array_merge($conditions,$like);
		// } else {
		// 	$conditions = array("select" => "user_id,full_name,user_name,email,mobile_no,date_of_birth,address,profile_picture,gender,status");
		// }
  //       $data = $this->common_model->get_pagination("user_register", $conditions, $settings);
  //       // echo"<pre>"; print_r($data); exit;

  //       if (isset($this->session->users_msg) && $this->session->users_msg != '') {
  //           $data = array_merge($data, array("success" => $this->session->users_msg));
  //           $this->session->users_msg = '';
  //       }
  //       unset($settings, $conditions);

  //       $this->session->current_filter_users = $current_filter_users;
		// $data['current_filter_users'] = $current_filter_users;
		// $data['search_options'] = $this->_search_array;
  //       $conditions = array("select" => "*");

        // ==================== pagination start ======================== //

        $data = $this->Production_model->pagination_create(base_url('authority/website-settings/view/'),'web_settings','','id','asc',array());

        // ==================== pagination end ======================== //
        
        $this->load->view('authority/website-settings/view',$data);
    }

    function add()
    {
        $data['web_setting_data'] = array();
        $this->load->view('authority/website-settings/add-edit',$data);
    }

    function add_web_setting()
    {
        $data = $this->input->post();
        $data['create_date'] = date('Y-m-d h:i:s');
        // echo"<pre>"; print_r($data); exit;
        
        if($_FILES['page_image']['name']==""){ $img_name='';}
        else{
            $imagePath = PAGE_IMAGE;
            if(!is_dir($imagePath)){
                mkdir($imagePath);
                @chmod($imagePath,777);
            }
            $img_name = 'PAGE-'.rand(111,999)."-".str_replace(' ', '_',$_FILES['page_image']['name']);

            $destFile = $imagePath . $img_name; 
            $filename = $_FILES["page_image"]["tmp_name"];       
            move_uploaded_file($filename,  $destFile);
            
            $data['page_image'] = $img_name;
        }
        // echo "<pre>"; print_r($data); exit;

        $record = $this->Production_model->insert_record('web_settings',$data);
        if ($record !='') {
            $this->session->set_flashdata('success', 'Add Successfully....!');
            redirect(base_url('authority/website-settings/view')); 
        }
        else
        {
            $this->session->set_flashdata('error', 'Not Added....!');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function edit($id)
    {
        $data['web_setting_data'] = $this->Production_model->get_all_with_where('web_settings','','',array('id'=>$id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
        $this->load->view('authority/website-settings/add-edit',$data);
    }

    function update_web_setting()
    {
        $web_setting_id = $this->input->post('id');
        $data = $this->input->post();
        $data['modified_date'] = date('Y-m-d H:i:s');
        // echo"<pre>"; print_r($data); exit;

        if($_FILES['page_image']['name'] !='')
        {
            $get_image = $this->Production_model->get_all_with_where('web_settings','','',array('id'=>$web_setting_id));
            // echo"<pre>"; print_r($get_image); exit;
            if ($get_image !=null && $get_image[0]['page_image'] !=null && !empty($get_image[0]['page_image']))
            {
                @unlink(PAGE_IMAGE.$get_image[0]['page_image']);
            }

            $imagePath = PAGE_IMAGE;
            if(!is_dir($imagePath)){
                mkdir($imagePath);
                @chmod($imagePath,777);
            }
            $img_name = 'PAGE-'.rand(111,999)."-".str_replace(' ', '_',$_FILES['page_image']['name']);

            $destFile = $imagePath . $img_name; 
            $filename = $_FILES["page_image"]["tmp_name"];       
            move_uploaded_file($filename,  $destFile);

            $data['page_image'] = $img_name;
        }
        // echo "<pre>"; print_r($data); exit;

        $record = $this->Production_model->update_record('web_settings',$data,array('id'=>$web_setting_id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;
        if ($record == 1) {
            $this->session->set_flashdata('success', 'Update Successfully....!');
            redirect(base_url('authority/website-settings/view'));
        }
        else
        {
            $this->session->set_flashdata('error', 'Not Updated....!');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function delete($id)
    {
        $get_image = $this->Production_model->get_all_with_where('web_settings','','',array('id'=>$id));
        
        if ($get_image !=null && $get_image[0]['page_image'] !=null && !empty($get_image[0]['page_image']))
        {
            @unlink(PAGE_IMAGE.$get_image[0]['page_image']);
        }
        $record = $this->Production_model->delete_record('web_settings',array('id'=>$id));

        if ($record == 1) {
            $this->session->set_flashdata('success', 'Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $get_image = $this->Production_model->get_all_with_where('web_settings','','',array('id'=>$value));
  
            if ($get_image !=null && !empty($get_image[0]['page_image']))
            {
                @unlink(PAGE_IMAGE.$get_image[0]['page_image']);
            }
            $record = $this->Production_model->delete_record('web_settings',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>