<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tutorials extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index($page_number='') {
        $settings = array(
            "url" => site_url() . "authority/tutorials/index/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data = $this->common_model->get_pagination("tutorial", $conditions, $settings);
        // echo"<pre>"; print_r($data); exit;
        if (isset($this->session->tutorial_msg) && $this->session->tutorial_msg != '') {
            $data = array_merge($data, array("success" => $this->session->tutorial_msg));
            $this->session->tutorial_msg = '';
        }
        unset($settings, $conditions);        
        $this->load->view('authority/tutorials/view', $data);
    }
    function add()
    {
        $data['tutorial_details'] = array();       
        $this->load->view('authority/tutorials/add-edit',$data);
    }    
    function insert_tutorial()
    {
        $data = $this->input->post();
        $tutorial_name = $data['tutorial_name']; 
        $tutorial_link = $data['tutorial_link'];  
        $count_tutorial = count($data['tutorial_name']); 
        $create_date = date('Y-m-d H:i:s');
        $resultSet = Array();                 
        if(isset($count_tutorial)) {     
            for($i = 0; $i < $count_tutorial; $i++){
                $get_sub_tutorial_name = $this->Production_model->get_all_with_where('tutorial','','',array('tutorial_name'=> $tutorial_name[$i]));

                if(!empty($get_sub_tutorial_name)) 
                {
                    $resultSet[] = $tutorial_name[$i];
                }
            }
            if(!empty($resultSet)) {
                $error = implode(', ', $resultSet);
                $this->session->set_flashdata('error',"$error Tutorial name is allredy exist...!");
                redirect(base_url('authority/tutorials/add'));
            }else{                           
                for($i = 0; $i < $count_tutorial; $i++) {
                    // if(!is_dir(CAT_IMAGE)){
                    //     mkdir(CAT_IMAGE);                
                    //     @chmod(CAT_IMAGE,0777);
                    // }
                    // tutorial icon
                    /*if($_FILES['cat_image']['name'][$i]==""){ $img_name='';}
                    else{
                        $iconPath = CAT_IMAGE;  
                        $img_name = substr(md5(uniqid(rand(), true)), 0, 16) . '.' . pathinfo($_FILES['cat_image']['name'][$i], PATHINFO_EXTENSION);
                        $destFile = $iconPath . $img_name; 
                        $filename = $_FILES["cat_image"]["tmp_name"][$i];       
                        move_uploaded_file($filename,  $destFile);   
                        $data['cat_image'] = $img_name;
                    }*/
                    $data = array(
                        'tutorial_name' => $tutorial_name[$i],
                        'tutorial_link' => $tutorial_link[$i],
                        'seo_slug' => create_slug($data['tutorial_name'][$i]),
                        'create_date' => $create_date
                    );
                    if($tutorial_name[$i] !=null) {
                        $record = $this->Production_model->insert_record('tutorial',$data);
                    }
                }
                if($record !='') {
                    $this->session->set_flashdata('success', 'Tutorial Add Successfully....!');
                    redirect(base_url('authority/tutorial'));
                }else{
                    $this->session->set_flashdata('error', 'Tutorial Not Added....!');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
           // exit;
        }
    }

    function edit($id)
    {
        $data['tutorial_details'] = $this->Production_model->get_all_with_where('tutorial','','',array('id'=>$id));
        $this->load->view('authority/tutorials/add-edit',$data);
    }

    function update_tutorial()
    {
        $data = $this->input->post();
        $tutorial_name = $data['tutorial_name']; 
        $tutorial_link = $data['tutorial_link']; 
        $modified_date = date('Y-m-d H:i:s');
        $id = $this->input->post('id');
        
        $get_image = $this->Production_model->get_all_with_where('tutorial','','',array('id'=>$id));
        // echo"<pre>"; print_r($get_image); exit;
        $data = array(
            'tutorial_name' => $tutorial_name[0],
            'tutorial_link' => $tutorial_link[0],
            'seo_slug' => create_slug($data['tutorial_name'][0]),
            'modified_date' => $modified_date
        );
        // echo "<pre>";print_r($data);exit;
        /*tutorial icon */
        /*if($_FILES['cat_image']['name'][0] !='')
        {
            $imagePath = CAT_IMAGE;              
            if(!is_dir($imagePath)){
                mkdir($imagePath);                
                @chmod($imagePath,0777);
            }
            // Old image delete
            if ($get_image !=null && $get_image[0]['cat_image'] !=null && !empty($get_image[0]['cat_image']))
            {
                @unlink(CAT_IMAGE.$get_image[0]['cat_image']);
            }
            $img_name = substr(md5(uniqid(rand(), true)), 0, 16) . '.' . pathinfo($_FILES['cat_image']['name'][0], PATHINFO_EXTENSION);
            $destFile = $imagePath . $img_name; 
            $filename = $_FILES["cat_image"]["tmp_name"][0];       
            move_uploaded_file($filename,  $destFile);
            $data['cat_image'] = $img_name;
        }*/
        $record = $this->Production_model->update_record('tutorial',$data,array('id'=>$id));
        if ($record == 1) {
            $this->session->set_flashdata('success', 'Tutorial Update Successfully....');
            redirect(base_url('authority/tutorials'));
        }else{
            $this->session->set_flashdata('error', 'Tutorial Not Updated....');
            redirect($_SERVER['HTTP_REFERER']);
        }        
    }

    function delete_tutorial($id)
    {
        /*Old image delete*/
        $get_image = $this->Production_model->get_all_with_where('tutorial','','',array('id'=>$id));
        /*if ($get_image !=null && $get_image[0]['cat_image'] !=null && !empty($get_image[0]['cat_image']))
        {
           @unlink(CAT_IMAGE.$get_image[0]['cat_image']);
        }*/
        $record = $this->Production_model->delete_record('tutorial',array('id'=>$id));
      
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Tutorial Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Tutorial Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            /*$get_image = $this->Production_model->get_all_with_where('tutorial','','',array('id'=>$value));
            
            if ($get_image !=null && $get_image[0]['cat_image'] !=null && !empty($get_image[0]['cat_image']))
            {
               @unlink(CAT_IMAGE.$get_image[0]['cat_image']);
            }*/
            $record = $this->Production_model->delete_record('tutorial',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Tutorial Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Tutorial Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>