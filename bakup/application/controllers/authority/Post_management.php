<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Post_management extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    private $search_options = array(
        'post_title' => 'title',
        'name' => 'user name',
    );

    private $search_status = array(
        '1' => 'approve',
        '0' => 'reject',
        '2' => 'active',
        '3' => 'deactive',
    );

    public function index($page_number='') {
        /* For clearing a search */
        if ($this->input->get('clear_filter') == 1) {
            $this->session->post_filter = array();
            redirect(base_url() . 'authority/post-management');
        }
        if ($this->input->method(TRUE) == 'POST') {
            $post_filter = $this->input->post();
            // echo"<pre>"; print_r($post_filter); 
        } else if (isset($this->session->post_filter) && !empty($this->session->post_filter)) {
            $post_filter = $this->session->post_filter;
            // echo"<pre>"; print_r($post_filter); exit;
        } else {
            $post_filter = array();
        }

        $search_query = '';
        if (isset($post_filter['search_options']) && $post_filter['search_options'] != '' &&
                isset($post_filter['search_value']) && $post_filter['search_value'] != '') {

            if ($post_filter['search_options'] == 'name') {
                $search_query .= ' AND ur.name LIKE "%' . $post_filter['search_value'] . '%" ';
            } 
            if ($post_filter['search_options'] == 'post_title') {
                $search_query .= ' AND pm.post_title LIKE "%' . $post_filter['search_value'] . '%" ';
            }
            // echo"<pre>"; print_r($post_filter); exit;
        }

        /* For post approve,reject,active,deactive status */
        if (isset($post_filter['search_status']) && $post_filter['search_status'] != '') {
            if ($post_filter['search_status'] == '1') { // approve
                $search_query .= ' AND pm.post_approve_reject = "1" ';
            }
            if ($post_filter['search_status'] == '0') { // reject
                $search_query .= ' AND pm.post_approve_reject = "0" ';
            }
            if ($post_filter['search_status'] == '2') { // active
                $search_query .= ' AND pm.status = "1" ';
            }
            if ($post_filter['search_status'] == '3') { // deactive
                $search_query .= ' AND pm.status = "0" ';
            }
        }

        /* For post start date filter */
        if (isset($post_filter['start_date']) && $post_filter['start_date'] != '' && isset($post_filter['end_date']) && $post_filter['end_date'] != '') {
            $start_date = date('Y-m-d', strtotime($post_filter['start_date']));
            $end_date = date('Y-m-d', strtotime($post_filter['end_date']));
            $search_query .= ' AND DATE(pm.create_date) BETWEEN "' . $start_date . '" AND "' . $end_date . '" ';
        }
        
        $sql = 'SELECT pm.*,ur.name,ur.user_id,pkg.name as package_name,pkg.day,pkg.price as package_price,pkg.currency,pkg.discription FROM post_management pm, user_register ur , package_management pkg  WHERE ur.user_id = pm.user_id AND pkg.id = pm.package_id ' . $search_query . ' ORDER BY pm.post_id desc';
        $post_filter['filter_sql'] = $sql;
        
        $data = $this->common_model->get_data_with_sql($sql);
        // echo $this->db->last_query(); echo"<pre>"; print_r($data); exit;

        if ($data['row_count'] > 0) {
            $settings = array(
                "total_record" => $data['row_count'],
                'url' => site_url() . 'authority/post-management/index/',
                "per_page" => RECORDS_PER_PAGE,
            );
            /* GET PAGINATION */
            $info = $this->common_model->only_pagination($settings);
            $sql .= ' LIMIT ' . $info['start'] . ', ' . $info['limit'] . ' ';
            $data = $this->common_model->get_data_with_sql($sql);
            $data['pagination'] = $info['pagination'];
        }

        $data = array_merge($data, array('search_options' => $this->search_options));
        $data = array_merge($data, array('search_status' => $this->search_status));
        $this->session->post_filter = $post_filter;

        isset($info['start']) ? $data['no'] = $info['start']+1 : '';
        unset($settings, $conditions);
        /*End*/
        // echo"<pre>"; print_r($data); exit;
                
        $this->load->view('authority/post_management/view', $data);
    }

    function add()
    {
        $data['post_details'] = array();       
        $this->load->view('authority/post_management/add-edit',$data);
    }

    // ==================================================================== //
    // ================= excel formate download && insert ================= //
    // ==================================================================== //

    // public function download_excel_category(){

    //     // At the beggining...
    //     ob_start();    
    //     $content="";
    //     $normalout=true;

    //     // ... do some stuff ...

    //     // i guess if some condition is true...
    //     $file_name = 'category.xls';
    //     $content=ob_get_clean();
    //     $normalout=false;
    //     header( "Content-Type: application/vnd.ms-excel" );
    //     header( "Content-disposition: attachment; filename=".$file_name);
    //     echo 'Category Name English'. "\t" .'Category Name Arabic' . "\n";
    //     // echo 'First Name' . "\t" . 'Last Name' . "\t" . 'Phone' . "\n";
    //     // echo 'John' . "\t" . 'Doe' . "\t" . '555-5555' . "\n";


    //     // Here you could die() or continue...
    //     ob_start();
    //     // ... rest of execution ...

    //     $content.=ob_get_clean();
    //     if($normalout)
    //     {
    //         echo($content);
    //     } else {
    //         // Excel provided no output.
    //     }
    // }

    // public function save_excel_category()
    // {  
    //     $this->load->library('excel');
    //     if ($_FILES['xls_file']['error'] == 0) {

    //         $ext = pathinfo($_FILES['xls_file']['name'], PATHINFO_EXTENSION);
           
    //         $imagePath = CATEGORY_EXCEL;
    //         $img_name = 'category_excel-'.rand(111,999)."-".str_replace(' ', '_',$_FILES['xls_file']['name']);

    //         $destFile = $imagePath . $img_name; 
    //         $filename = $_FILES["xls_file"]["tmp_name"];       
    //         move_uploaded_file($filename, $destFile);
            
    //         if(is_array($img_name)){
    //             $this->session->set_flashdata('phperror','File Not Uploaded.!!!'.$img_name['error']);
    //             redirect($_SERVER['HTTP_REFERER']);
    //         }

    //         $path = CATEGORY_EXCEL;
    //         $data['FileName'] = $img_name;
    //         $data['FieldsList'] =array("post_title_english","post_title_arabic");

    //         $excel_data = $this->excel->import_excel($path,$data);
    //         // echo '<pre>'; print_r($excel_data); exit;

    //         $resultSet = Array(); 

    //         //======== check duplicate value get in excel sheet data start ========//

    //         $tmp_resultSet = Array(); 
    //         $duplicates = Array(); 
    //         foreach ($excel_data as $key => $dt) {

    //             if (in_array($dt, $tmp_resultSet)) {
    //                 $duplicates[] = $dt;
    //             }
    //             $tmp_resultSet[] = $dt;
    //         }
    //         $tmp = $duplicates;
    //         $duplicate_cat_name = array();
    //         if ($tmp !=null) {
    //             foreach ($tmp as $key => $duplicate_row){
    //                 $tmp = $duplicate_row['post_title_english'];
    //                 array_push($duplicate_cat_name,$tmp);
    //             }
    //             if (!empty($duplicate_cat_name)){
    //                 $error_cat_name = implode(', ', array_unique($duplicate_cat_name));

    //                 // echo"<pre>"; print_r(array_unique($duplicate_cat_name)); exit;
    //                 $this->session->set_flashdata('error',"$error_cat_name Duplicate category name...!");
    //                 redirect(base_url('authority/post_management/add'));
    //             }
    //         }
               
    //         //========= check duplicate value get in excel sheet data end =========//

    //         foreach ($excel_data as $key => $dt) {
    //             echo"<pre>"; print_r($dt); 

    //             $get_cat_name = $this->Production_model->get_all_with_where('post_management','','',array('post_title_english'=>$dt['post_title_english']));

    //             if(!empty($get_cat_name)) 
    //             {
    //                 $resultSet[] = $dt['post_title_english'];
    //                 // echo"<pre>"; print_r($resultSet);
    //             }
    //         }

    //         if (!empty($resultSet)) {
    //             // echo"<pre>"; print_r($resultSet); exit;

    //             $error = implode(', ', $resultSet);

    //             $this->session->set_flashdata('error',"$error Category name is allredy exist...!");
    //             redirect(base_url('authority/post_management/add'));
    //         }
    //         // echo"<pre>"; print_r($excel_data); exit;
    //         else
    //         {
    //             foreach ($excel_data as $key => $dt) {
    //                 $ins = array(
    //                     'post_title_english'=>$dt['post_title_english'],
    //                     'create_date' => date('Y-m-d H:i:s')
    //                 ); //insert Array

    //                 $this->Production_model->insert_record('post_management',$ins);//Insert Table Code
    //             }
    //         }

    //         $this->session->set_flashdata('success','Excel Imported.!!!');
    //         redirect(base_url('authority/post_management/view'));

    //         // echo '<pre>'; print_r($excel_data); exit;
    //     } else {
    //         $this->session->set_flashdata('error','File Not Uploaded.!!!');
    //         redirect($_SERVER['HTTP_REFERER']);
    //     }
    // }

    // ==================================================================== //
    // =========================== End of this ============================ //
    // ==================================================================== //

    // function add_post()
    // {
    //     $data = $this->input->post();
    //     $data['create_date'] = date('Y-m-d H:i:s');

    //     /*post image icon*/
    //     if($_FILES['post_image']['name']==""){ $img_name='';}
    //     else{
    //         $iconPath = POST_IMAGE;
    //         if(!is_dir($iconPath)){
    //             mkdir($iconPath); 
    //             mkdir($iconPath.'thumbnail/');               
    //             @chmod($iconPath,0777);
    //         }
    //         $image_name = 'POST_IMAGE'.rand(111,999)."-".str_replace(' ', '_',$_FILES['post_image']['name']);
    //         $destFile = $iconPath . $image_name; 
    //         $filename = $_FILES["post_image"]["tmp_name"];       
    //         move_uploaded_file($filename,  $destFile);
    //         $this->Production_model->generate_thumbnail(POST_IMAGE,$image_name);

    //         $data['post_image'] = $image_name;
    //     }
    //     // echo"<pre>"; print_r($data); exit;
    //     // $duplicate_records = $this->Production_model->get_all_with_where('post_management','','',array('post_title'=>$data['post_title']));
    //     // if (count($duplicate_records) > 0) {
    //     //     $this->session->set_flashdata('error', 'Post allredy exist....!');
    //     //     redirect($_SERVER['HTTP_REFERER']);
    //     // }
    //     // else{
    //         $inserted_id = $this->Production_model->insert_record('post_management',$data);

    //         if ($inserted_id !='') {
    //             $this->session->set_flashdata('success', 'Post Add Successfully....!');
    //             redirect(base_url('authority/post-management'));
    //         }
    //         else
    //         {
    //             $this->session->set_flashdata('error', 'Post Not Added....!');
    //             redirect($_SERVER['HTTP_REFERER']);
    //         }
    //     // }
    // }

    function edit($id)
    {
        $data['category_details'] = $this->Production_model->get_all_with_where('category','','',array('status'=>'1'));
        $data['post_details'] = $this->Production_model->get_all_with_where('post_management','','',array('post_id'=>$id));
        $data['similar_img_details'] = $this->Production_model->get_all_with_where('post_similer_image','','',array('post_id'=>$id));
        // echo"<pre>"; print_r($data['similar_img_details']); exit;
        $this->load->view('authority/post_management/add-edit',$data);
    }

    function update_post()
    {
        $data = $this->input->post();
        $data['modified_date'] = date('Y-m-d H:i:s');
        $post_id = $this->input->post('post_id');
        // echo"<pre>"; print_r($data); exit;

        $this->form_validation->set_rules('category_id', 'category name', 'required');
        $this->form_validation->set_rules('i_want', 'I want', 'required');
        $this->form_validation->set_rules('price', 'price', 'required');
        $this->form_validation->set_rules('post_description', 'post description', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER['HTTP_REFERER']); 
        }
        else
        {
            $get_image = $this->Production_model->get_all_with_where('post_management','','',array('post_id'=>$post_id));
            // echo"<pre>"; print_r($get_image); exit;

            /*post image icon*/
            // if($_FILES['post_image']['name'] !='')
            // {
            //     $iconPath = POST_IMAGE;
            //     if(!is_dir($iconPath)){
            //         mkdir($iconPath);                
            //         mkdir($iconPath.'thumbnail/');                
            //         @chmod($iconPath,0777);
            //     }
            //     /*Old image delete*/
            //     if ($get_image !=null && $get_image[0]['post_image'] !=null && !empty($get_image[0]['post_image']))
            //     {
            //         @unlink(POST_NEED_IMG.$get_image[0]['post_image']);
            //         @unlink(POST_NEED_IMG.'thumbnail/'.$get_image[0]['post_image']);
            //     }
            //     $image_name = 'POST_IMAGE'.rand(111,999)."-".str_replace(' ', '_',$_FILES['post_image']['name']);
            //     $destFile = $iconPath . $image_name; 
            //     $filename = $_FILES["post_image"]["tmp_name"];       
            //     move_uploaded_file($filename,  $destFile);
            //     $this->Production_model->generate_thumbnail(POST_IMAGE,$image_name);

            //     $data['post_image'] = $image_name;
            // }
            // echo"<pre>"; print_r($data); exit;

            //=========================================================================//
            //======================= multiple image upload start =====================//
            //=========================================================================//

            // if (!is_dir(POST_NEED_IMG)) {
            //     mkdir(POST_NEED_IMG);
            //     mkdir(POST_NEED_IMG.'thumbnail/');
            //     @chmod(POST_NEED_IMG,0777);
            // }

            // $new_name = 'POST_NEED_IMG'.rand();
            // $config['upload_path']   = POST_NEED_IMG;
            // $config['allowed_types'] = '*';
            // $config['file_name'] = $new_name;           

            // $this->load->library('upload', $config);      
            
            // $files = $_FILES; 
            // $cpt = count($_FILES['similar_image']['name']); 

            // if($files['similar_image']['name'] != "")
            // {               
            //     for($i=0; $i<$cpt; $i++)
            //     {   
            //         $_FILES['similar_image']['name']= $files['similar_image']['name'][$i];
            //         $_FILES['similar_image']['type']= $files['similar_image']['type'][$i];
            //         $_FILES['similar_image']['tmp_name']= $files['similar_image']['tmp_name'][$i];
            //         $_FILES['similar_image']['error']= $files['similar_image']['error'][$i];
            //         $_FILES['similar_image']['size']= $files['similar_image']['size'][$i]; 

            //         // $this->load->library('upload',$config);
            //         $this->upload->do_upload('similar_image');
            //         $image_name = $this->upload->data();
            //         $image_name = $image_name['file_name'];  

            //         $this->Production_model->generate_thumbnail(POST_NEED_IMG,$image_name);
                    
            //         $imagedata = array(
            //             'post_id' => $post_id,
            //             'similar_image' => $image_name,
            //             'create_date' => date('Y-m-d H:i:s')
            //         ); 
            //         // echo "<pre>";print_r($imagedata); 
            //         $image_id = $this->Production_model->insert_record('post_similer_image', $imagedata);
            //     }       
            // }
            // exit;
            //=========================================================================//
            //======================= multiple image upload end =======================//
            //=========================================================================//

            $record = $this->Production_model->update_record('post_management',$data,array('post_id'=>$post_id));
            if ($record == 1) {
                $this->session->set_flashdata('success', 'Post Update Successfully....');
                redirect(base_url('authority/post-management'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Post Not Updated....');
                redirect($_SERVER['HTTP_REFERER']);
            }   
        }
    }

    /*Post approve/disapprove resion*/
    function post_approve_reject()
    {
        $data = $this->input->post();
        $post_data = array(
            'post_approve_reject' => $data['post_approve_reject_status'],
            'post_approve_reject_resion' => isset($data['post_approve_reject_resion']) ? $data['post_approve_reject_resion'] : '',
        );
        if ($data['post_approve_reject_status'] == 1) {
            $post_data['status'] = '1';
        }
        if ($data['post_approve_reject_status'] == 0) {
            $post_data['order_status'] = '1';
        }

        // echo"<pre>"; print_r($post_data); exit;
        $response_array = array();
        $record = $this->Production_model->update_record('post_management',$post_data,array('post_id'=>$data['post_id']));
        if ($record == 1) {
            /*Mail send from user*/
            if ($data['post_approve_reject_status'] == 1) {
                $post_status = 'Approve'; 
                $response_array['message'] = 'Post '.$post_status.' Successfully...!';
            }
            if ($data['post_approve_reject_status'] == 0) {
                $post_status = 'Reject'; 
                $response_array['message'] = 'Post '.$post_status.' Successfully...!';

                $send_mail = $this->Production_model->mail_send('Crystal Hositing-post '.$post_status,$data['user_email'],'','mail_form/post_approve_reject/post_status',$data,''); // user send mail
            }

            /*Notification start*/
            $post_record = $this->Production_model->get_all_with_where('post_management','','',array("post_id"=>$data['post_id']));

            if (isset($post_record) && $post_record !=null) {
                $user_record = $this->Production_model->get_all_with_where('user_register','','',array("user_id"=>$post_record[0]['user_id']));
                $post_resion = isset($data['post_approve_reject_resion']) && $data['post_approve_reject_resion'] !=null ? $data['post_approve_reject_resion'] : 'Post Approve';
                
                if (isset($user_record) && $user_record !=null) {
                    foreach ($user_record as $key => $user_row) {                            
                        $get_user_tocken = $this->Production_model->get_all_with_where('notification_token','','',array("user_id"=>$user_row['user_id']));
                        // echo"<pre>"; print_r($get_user_tocken);
                        if (isset($get_user_tocken) && $get_user_tocken !=null) {
                            
                            foreach ($get_user_tocken as $key => $token_row) {
                                $this->Production_model->sendFcmNotification(1,json_encode(array(
                                    "type"=>1, //post active
                                    "title"=>'Post '.$post_status.' By Admin',
                                    "message"=> 'Post '.$post_status.' reason : '.$post_resion,
                                    "image"=>'',
                                    "product_id"=>$data['post_id'],
                                )),$user_row['user_id'],array($token_row['token']),1,'0');
                            }
                        }
                    }
                }
            }
            /*Notification end*/
            $response_array['post_status'] = '<span class="post_status label label-success">Approve</span>';
            $response_array['success'] = true;
        } 
        echo json_encode($response_array);exit; 
    }

    function delete_category($id)
    {
        /*Old image delete*/
        $get_image = $this->Production_model->get_all_with_where('post_similer_image','','',array('post_id'=>$id));
        // echo"<pre>"; print_r($get_image); exit;
        
        if ($get_image !=null && $get_image[0]['similer_image'] !=null && !empty($get_image[0]['similer_image']))
        {
           @unlink(POST_NEED_IMG.$get_image[0]['similer_image']);
           @unlink(POST_NEED_IMG.'thumbnail/'.$get_image[0]['similer_image']);
           // @unlink(CAT_MEDIUM_IMAGE.$get_image[0]['similer_image']);
        }
        $record = $this->Production_model->delete_record('post_management',array('post_id'=>$id));
        $record = $this->Production_model->delete_record('post_similer_image',array('post_id'=>$id));
       
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Post Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Post Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function delete_image($id)
    {
        /*Old image delete*/
        $get_image = $this->Production_model->get_all_with_where('post_similer_image','','',array('image_id'=>$id));
        
        if ($get_image !=null && $get_image[0]['similar_image'] !=null && !empty($get_image[0]['similar_image']))
        {
           @unlink(POST_NEED_IMG.$get_image[0]['similar_image']);
           @unlink(POST_NEED_IMG.'thumbnail/'.$get_image[0]['similar_image']);
        }
        $record = $this->Production_model->delete_record('post_similer_image',array('image_id'=>$id));
       
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Image Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Image Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');

        foreach ($chkbox_id as $key => $value) {
           /*Old image delete*/
           $get_image = $this->Production_model->get_all_with_where('post_similer_image','','',array('post_id'=>$value));
            
            if ($get_image !=null && $get_image[0]['similer_image'] !=null && !empty($get_image[0]['similer_image']))
            {
               @unlink(POST_NEED_IMG.$get_image[0]['similer_image']);
               @unlink(POST_NEED_IMG.'thumbnail/'.$get_image[0]['similer_image']);
            }
            $record = $this->Production_model->delete_record('post_management',array('post_id'=>$value));
            $record = $this->Production_model->delete_record('post_similer_image',array('post_id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Post Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Post Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  

    public function change_post_status(){
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }
        $error = false;
        $response_array = array('success'=>false);      
        $required_fields = array(
            'id' => 'missing id in the request',
            'table' => 'missing table in the request',
            'current_status' => 'missing current status in the request',
        );
        foreach($required_fields as $key => $value){
            if(strlen(trim($this->input->post($key))) <= 0){
                $response_array['msg'] = $value;
                $error = true;
                break;
            }
        }
        if(!$error){
            if ($this->input->post('current_status') == '0') {
                $status = '1';
            }
            if ($this->input->post('current_status') == '1') {
                $status = '0';
            }
            $records = array(
                'status' => $status,
            );
            $conditions = array(
                "where" => array("post_id" => $this->input->post('id')),
            );
            // echo"<pre>"; print_r($records); exit;
            $this->common_model->update_data($this->input->post('table'), $records, $conditions); 
            // $this->Production_model->update_record('sub_of_sub_category', $records, array("category_id" => $this->input->post('id'))); 

            $response_array['success'] = true;
        }
        echo json_encode($response_array);
        exit;
    }
}
?>