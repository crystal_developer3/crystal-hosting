<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Domains extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index($page_number='') {
        $settings = array(
            "url" => site_url() . "authority/domains/index/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data = $this->common_model->get_pagination("domain", $conditions, $settings);
        // echo"<pre>"; print_r($data); exit;
        if (isset($this->session->domain_msg) && $this->session->domain_msg != '') {
            $data = array_merge($data, array("success" => $this->session->domain_msg));
            $this->session->domain_msg = '';
        }
        unset($settings, $conditions);        
        $this->load->view('authority/domains/view', $data);
    }
    function add()
    {
        $data['domain_details'] = array();       
        $this->load->view('authority/domains/add-edit',$data);
    }    
    function insert_domain()
    {
        $data = $this->input->post();
        $title = $data['title']; 
        $price = $data['price'];  
        $count_domain = count($data['title']); 
        $create_date = date('Y-m-d H:i:s');
        $resultSet = Array();                 
        if(isset($count_domain)) {     
            for($i = 0; $i < $count_domain; $i++){
                $get_sub_title = $this->Production_model->get_all_with_where('domain','','',array('title'=> $title[$i]));
                if(!empty($get_sub_title)) 
                {
                    $resultSet[] = $title[$i];
                }
            }
            if(!empty($resultSet)) {
                $error = implode(', ', $resultSet);
                $this->session->set_flashdata('error',"$error Domain name is allredy exist...!");
                redirect(base_url('authority/domains/add'));
            }else{                           
                for($i = 0; $i < $count_domain; $i++) {
                    $data = array(
                        'title' => $title[$i],
                        'price' => $price[$i],
                        'seo_slug' => create_slug($data['title'][$i]),
                        'create_date' => $create_date
                    );
                    if($title[$i] !=null) {
                        $record = $this->Production_model->insert_record('domain',$data);
                    }
                }
                if($record !='') {
                    $this->session->set_flashdata('success', 'Domain Add Successfully....!');
                    redirect(base_url('authority/domains'));
                }else{
                    $this->session->set_flashdata('error', 'Domain Not Added....!');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
        }
    }
    function edit($id)
    {
        $data['domain_details'] = $this->Production_model->get_all_with_where('domain','','',array('id'=>$id));
        $this->load->view('authority/domains/add-edit',$data);
    }
    function update_domain()
    {
        $data = $this->input->post();
        $title = $data['title']; 
        $price = $data['price']; 
        $modified_date = date('Y-m-d H:i:s');
        $id = $this->input->post('id');
        
        $data = array(
            'title' => $title[0],
            'price' => $price[0],
            'seo_slug' => create_slug($data['title'][0]),
            'modified_date' => $modified_date
        );
        // echo "<pre>";print_r($data);exit;
        $record = $this->Production_model->update_record('domain',$data,array('id'=>$id));
        if ($record == 1) {
            $this->session->set_flashdata('success', 'Domain Update Successfully....');
            redirect(base_url('authority/domains'));
        }else{
            $this->session->set_flashdata('error', 'Domain Not Updated....');
            redirect($_SERVER['HTTP_REFERER']);
        }        
    }
    function delete_domain($id)
    {
        $record = $this->Production_model->delete_record('domain',array('id'=>$id));
      
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Domain Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Domain Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $record = $this->Production_model->delete_record('domain',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Domain Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Domain Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>