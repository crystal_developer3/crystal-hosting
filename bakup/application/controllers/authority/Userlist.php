<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Userlist extends CI_Controller {
    private $_search_array = array('full_name'=>'Full name','user_name'=>'User name');
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function view() {
        $data = array();       
        // ==================== pagination start ======================== //
        $total_records = $this->Production_model->get_total('user_register');
        if ($total_records !=null) 
        {    
            // ==================== pagination start ======================== //
            $tmp_data = $this->Production_model->get_all_with_where('user_register','','',array());
            $tmp_array['total_record'] = count($tmp_data);
            $tmp_array['url'] = base_url('authority/userlist/view/');
            $tmp_array['per_page'] = RECORDS_PER_PAGE;
            $record = $this->Production_model->only_pagination($tmp_array);
            $data['user_details'] = $this->Production_model->get_all_with_where_limit('user_register','id','desc',array(),$record['limit'],$record['start']); 
            $data['pagination'] = $record['pagination']; 
            $data['no'] = $record['no']; 
            // echo"<pre>"; print_r($data); exit;
            // ==================== pagination end ======================== //

            //====================== user filter start ======================//
            $start_date=$this->input->post('start_date');
            $end_date=$this->input->post('end_date');
            if($start_date==""){$data['start_date']='';}else{$data['start_date']=$start_date;}
            if($end_date==""){$data['end_date']='';}else{$data['end_date']=$end_date;}
            if($start_date == '' || $end_date == ''){
                $data['user_details'] = $this->Production_model->get_all_with_where_limit('user_register','id','desc',array(),$record['limit'],$record['start']); 
            }else{
                $data['user_details'] = $this->Production_model->get_all_with_where_limit_between('user_register','id','desc',date('Y-m-d', strtotime($start_date)),date('Y-m-d',strtotime($end_date)),$record['limit'],$record['start']); 
                // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
            }
            //====================== user filter end ======================//
        }
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
        $this->load->view('authority/users-register/view', $data);
    }

    public function add() {
        $data = array("form_title" => "Add User");

        /* Form Validation */
        $this->form_validation->set_rules('user_type', 'user type', 'required', array('required' => 'Please select user type'));
        $this->form_validation->set_rules('name', 'name', 'required', array('required' => 'Please enter user name'));

        $this->form_validation->set_rules('mobile_no', 'mobile number', 'required|numeric|min_length[10]|max_length[10]', array('required' => 'Please enter mobile number'));
        $this->form_validation->set_rules('user_email', 'email', 'required|valid_email|is_unique[user_register.user_email]', array('required' => 'Please enter email addressss', 'is_unique' => 'This email is already available'));
        
        if ($this->form_validation->run() === FALSE) {
            $data = array_merge($data, $_POST);
            // echo"<pre>"; print_r($data); exit;
        } else {
            $error = false;
            $records = $_POST;
            $records['password'] =  $this->encryption->encrypt(rand(0000000,1111111));
            $password['genrate_password'] = $this->encryption->decrypt($records['password']);
            // echo"<pre>"; print_r($records); exit;

            // if (isset($_FILES['profile_photo']) && $_FILES['profile_photo']['name'] != "") {
            //     $config['upload_path'] = './uploads/profile_photo/';
            //     $config['allowed_types'] = 'gif|jpg|png|jpeg|"';                
            //     $this->load->library('upload', $config);
            //     if (!$this->upload->do_upload('profile_photo')) {
            //         $error_msg = array('error' => $this->upload->display_errors());
            //         $error = true;
            //     } else {
            //         /* delete old photo */
            //         if (!is_null($data['profile_photo'])) {
            //             $path = $config['upload_path'] . $data['profile_photo'];
            //             if (is_file($path)) {
            //                 @unlink($path);
            //             }
            //         }
            //         $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            //         $file_name = $upload_data['file_name'];
            //         chmod($config['upload_path'] . $file_name, 0777);
            //         $records["profile_photo"] = $file_name;
            //     }
            // } else {
            //     $records["profile_photo"] = $data['profile_photo'];
            // }           
            if (!$error) {
                $this->common_model->insert_data("user_register",$records);
                
                $data = array_merge($data, $_POST);
                // $data = array_merge($data, array("success" => "User created successfully"));
                $_POST = array();
                $data = array_merge($data, $_POST);

                /*Mail send from user*/
                $send_mail = $this->Production_model->mail_send('Crystal Hositing Account',$records['user_email'],'','mail_form/thankyou_page/registration',$password,''); // user send mail

                $this->session->set_flashdata('success', 'User created Successfully...!');
                redirect(base_url('authority/userlist/view'));
            }
        }
        $this->load->view('authority/users-register/add-edit', $data);
    }

    public function edit($id = "") {
        $this->load->helper("form");
        $data = array("form_title" => "Edit User");
        if ($id != "") {
            $conditions = array("where" => array("id" => $id));
            $user_info = $this->common_model->select_data("user_register", $conditions);
            if ($user_info['row_count'] > 0) {
                $data = array_merge($data, $user_info['data'][0]);
            } else {
                redirect("authority/userlist/view");
            }
        } else {
            redirect("authority/userlist/view");
        }
        /* Form Validation */
        $this->form_validation->set_rules('name', 'Name', 'required', array('required' => 'Please enter user name'));        
        $this->form_validation->set_rules('user_email', 'email address', 'required|valid_email|is_unique_with_except_record[user_register.user_email.id.' . $data['id'] . ']', array('required' => 'Please enter email address', "valid_email" => "Please enter valid email address", "is_unique_with_except_record" => "This email is already available"));       
        $this->form_validation->set_rules('mobile_no', 'mobile number', 'required|numeric|min_length[10]|max_length[10]', array('required' => 'Please enter mobile number'));
        // $this->form_validation->set_rules('mobile_number_2', 'mobile number', 'numeric|min_length[10]|max_length[10]');
        if ($this->form_validation->run() === FALSE) {
            $data = array_merge($data, $_POST);
        } else {
            $error = false;
            $records = $_POST;
            // if (isset($_FILES['profile_photo']) && $_FILES['profile_photo']['name'] != "") {
            //     $config['upload_path'] = './uploads/profile_photo/';
            //     $config['allowed_types'] = 'gif|jpg|png|jpeg|"';                
            //     $this->load->library('upload', $config);
            //     if (!$this->upload->do_upload('profile_photo')) {
            //         $error_msg = array('error' => $this->upload->display_errors());
            //         $error = true;
            //     } else {
            //         /* delete old photo */
            //         if (!is_null($data['profile_photo'])) {
            //             $path = $config['upload_path'] . $data['profile_photo'];
            //             if (is_file($path)) {
            //                 @unlink($path);
            //             }
            //         }
            //         $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            //         $file_name = $upload_data['file_name'];
            //         chmod($config['upload_path'] . $file_name, 0777);
            //         $records["profile_photo"] = $file_name;
            //     }
            // } else {
            //     $records["profile_photo"] = $data['profile_photo'];
            // }
            if (!$error) {
                $conditions = array(
                    "where" => array("id" => $data['id']),
                );
                // echo"<pre>"; print_r($data); exit;
                $this->common_model->update_data("user_register", $records, $conditions);
                $data = array_merge($data, $_POST);
                // $data = array_merge($data, array("success" => "User updated successfully"));
                $_POST = array();
                $data = array_merge($data, $_POST);
                $this->session->set_flashdata('success', 'User updated Successfully...!');
                redirect(base_url('authority/userlist/view'));
                // echo"<pre>"; print_r($data); exit;
            }
        }
        // echo"<pre>"; print_r($data); exit;
        $this->load->view('authority/users-register/add-edit', $data);
    }
    public function delete($id = "") {
        if ($id != "") {
            $conditions = array("select" => "id", "where" => array("id" => intval($id)));
            $user = $this->common_model->select_data("user_register", $conditions);
            // echo"<pre>"; echo $this->db->last_query(); print_r($user); exit;
            // if ($user['row_count'] > 0) {
            //     $photo = $user['data'][0]['profile_picture'];
            //     if (!is_null($photo)) {
            //         $path = "./assets/uploads/profile_picture/" . $photo;
            //         if (is_file($path)) {
            //             @unlink($path);
            //         }
            //     }
            // }
            $conditions = array(
                "where" => array("id" => $id),
            );
            $this->common_model->delete_data("user_register", $conditions);
            $this->session->set_flashdata('success', 'Record Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect("authority/userlist/view");
        }
    }
    public function details($id = "") {
        $data = array();
        if ($id != "") {
            $conditions = array("where" => array("id" => $id));
            $user_info = $this->common_model->select_data("user_register", $conditions);
            if ($user_info['row_count'] > 0) {
                $data = array_merge($data, $user_info['data'][0]);
                // echo"<pre>"; print_r($data); exit;
            } else {
                redirect("authority/userlist/view");
            }
        } else {
            redirect("authority/userlist/view");
        }
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
        $this->load->view('authority/users-register/details', $data);
    }

    // function excel_genrate_report()
    // {       
    //     $tmp = array();
    //     // ==================== pagination start ======================== //
    //     $total_records = $this->Production_model->get_total('user_register');
    //     if ($total_records !=null) 
    //     {    
    //         $config = array();
    //         $config["base_url"] = base_url() . 'authority/userlist/view/';
    //         $config["total_rows"] = $total_records;
    //         $config["per_page"] = RECORDS_PER_PAGE;
    //         $config['use_page_numbers'] = TRUE;
    //         $config["uri_segment"] = 4;
    //         $config['num_links'] = $total_records;
    //         $config['next_link'] = 'Next';
    //         $config['prev_link'] = 'Previous'; 
    //         $config['first_tag_open'] = '<li>';
    //         $config['first_tag_close'] = '</li>';
    //         $config['prev_tag_open'] = '<li class="prev">';
    //         $config['prev_tag_close'] = '</li>';
    //         $config['next_tag_open'] = '<li class="end">';
    //         $config['next_tag_close'] = '</li>';
    //         $config['cur_tag_open'] = '<li class="active"><a href="#">';
    //         $config['cur_tag_close'] = '</a></li>';
    //         $config['num_tag_open'] = '<li>';
    //         $config['num_tag_close'] = '</li>';
    //         $this->pagination->initialize($config);
    //         $page_number = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
    //         if ($page_number > ceil(($config['total_rows'] / $config['per_page']))) {
    //             /* Redirect when page limit exceeded */
    //             redirect($config['base_url']);
    //         }
    //         /* GETTING A OFFSET */
    //         $page = ($page_number == 0) ? 0 : ($page_number * $config['per_page']) - $config['per_page'];
    //         //echo"<pre>"; print_r($page); exit;
    //         //====================== user filter start ======================//
    //         $start_date=$this->input->post('start_date');
    //         $end_date=$this->input->post('end_date');
    //         if($start_date==""){$data['start_date']='';}else{$data['start_date']=$start_date;}
    //         if($end_date==""){$data['end_date']='';}else{$data['end_date']=$end_date;}
    //         if($start_date == '' || $end_date == ''){
    //             $data['user_details'] = $this->Production_model->get_all_with_where_limit('user_register','id','asc',array(),$config["per_page"], $page); 
    //         }else{
    //             $data['user_details'] = $this->Production_model->get_all_with_where_limit_between('user_register','id','asc',date('Y-m-d', strtotime($start_date)),date('Y-m-d',strtotime($end_date)),$config["per_page"], $page); 
    //             // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
    //         }
    //         //====================== user filter end ======================//
    //         // $data["user_details"] = $this->Production_model->get_all_with_where_limit('user_register','id','asc',array(),$config["per_page"], $page); // list for all users ....
    //         $data['links'] = $this->pagination->create_links();  
    //         $data['no'] = $page+1;
    //     }
    //     // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
    //     // ==================== pagination end ======================== //
    //     $tmp = array_merge($tmp,$data['user_details']);
    //     // excel export data strart //
    //     $excel_data = array();
    //     foreach ($tmp as $key => $value) {
    //         // if ($value['user_type'] == '1') {
    //         //     $user_type = "Buyer";
    //         // }
    //         // if ($value['user_type'] == '2') {
    //         //     $user_type = "Selle";
    //         // }
    //         // if ($value['user_type'] == '3') {
    //         //     $user_type = "Both";
    //         // }
    //         $tmp = array(
    //             'Name' => $value['name'],
    //             // 'User Type' => $user_type,
    //             'User Email' => $value['user_email'],
    //             'Mobile No' => $value['mobile_no'],
    //             'Create Date' => date('d-m-Y',strtotime($value['create_date'])),
    //         );          
    //         array_push($excel_data,$tmp);
    //     }
    //     // echo"<pre>"; print_r($excel_data); exit;
    //     function filterData(&$str)
    //     {
    //         $str = preg_replace("/\t/", "\\t", $str);
    //         $str = preg_replace("/\r?\n/", "\\n", $str);
    //         if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
    //     }
    //     // file name for download
    //     $fileName = "User-report" . date('d-m-Y') . ".xls";
    //     // headers for download
    //     header("Content-Disposition: attachment; filename=\"$fileName\"");
    //     header("Content-Type: application/vnd.ms-excel");
    //     $flag = false;
    //     foreach($excel_data as $row) {
    //         if(!$flag) {
    //             // display column names as first row
    //             echo implode("\t", array_keys($row)) . "\n";
    //             $flag = true;
    //         }
    //         // filter data
    //         array_walk($row, 'filterData');
    //         echo implode("\t", array_values($row)) . "\n";
    //     }
    //     exit;
    //     // excel export data end //
    // }

    function excel_genrate_report()
    {       
        $tmp = array();
        // ==================== pagination start ======================== //

        $total_records = $this->Production_model->get_total('user_register');

        if ($total_records !=null) 
        {    

            $start_date=format_date_ymd($this->input->post('start_date'));
            $end_date=format_date_ymd($this->input->post('end_date'));

            if($start_date==""){$data['start_date']='';}else{$data['start_date']=$start_date;}
            if($end_date==""){$data['end_date']='';}else{$data['end_date']=$end_date;}

            if($start_date == '' || $end_date == ''){
                $data['user_details'] = $this->Production_model->get_all_with_where('user_register','id','asc',array()); 
            }else{
                $data['user_details'] = $this->Production_model->get_all_with_where_between('user_register','id','asc',date('Y-m-d', strtotime($start_date)),date('Y-m-d',strtotime($end_date))); 

                // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
            }
        }

        $tmp = array_merge($tmp,$data['user_details']);
        
        // excel export data strart //
        $excel_data = array();

        foreach ($tmp as $key => $value) {

            
            switch ($value['status']) {
                case 1:
                    $status ="Active";
                    break;
                case 0:
                    $status ="Deactive";
                    break;
                default:
                    break;
            }
            $tmp = array(
                'Name' => $value['first_name']." ".$value['last_name'],
                'Email' => $value['email'],
                'Date Of Birth' => date('d-M-Y',strtotime($value['dob'])),
                'Mobile No' => $value['phone_number'],
                'Company Name' => $value['company_name'],
                'Country' => ($value['id_country']!=0)?get_country_by_id($value['id_country']):'-',
                'State' => ($value['id_state']!=0)?get_state_by_id($value['id_state']):'-',
                'City' => ($value['id_city']!=0)?get_city_by_id($value['id_city']):'-',
                'Zip Code' => $value['zip_code'],
                'Address' => $value['address'],
                'Status' => $status,
                'Register Date' => date('d-M-Y',strtotime($value['create_date'])),
            );          
            array_push($excel_data,$tmp);
        }
        // echo"<pre>"; print_r($excel_data); exit;
            
        // file name for download
        $fileName = "User-report-".date('d-m-Y-His').".xls";
        
        // headers for download
        header("Content-Disposition: attachment; filename=\"$fileName\"");
        header("Content-Type: application/vnd.ms-excel");
        
        $flag = false;
        foreach($excel_data as $row) {
            if(!$flag) {
                // display column names as first row
                echo implode("\t", array_keys($row)) . "\n";
                $flag = true;
            }
            // filter data
            array_walk($row, 'filterData');
            echo implode("\t", array_values($row)) . "\n";
        }
        exit;
        // excel export data end //
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $get_record = $this->Production_model->get_all_with_where('user_register','','',array('id'=>$value));
            // if ($get_record !=null && $value['category_image'] !=null && !empty($value['category_image']))   
            // {
            //     unlink(CATEGORY_IMAGE.$value['category_image']);
            // }
            $record = $this->Production_model->delete_record('user_register',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Record deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Record Not deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  

    /*Mail sending*/
    function get_details(){

        $message = $this->input->post('txt_message');
        $user_email = $this->input->post('user_email');           

        // $data['news_details'] = $this->Production_model->get_all_with_where('user_register','','',array('id'=> $id));
        // echo"<pre>"; print_r($id); exit;
        $send_mail = array();

        if ($user_email !=null) {
            // foreach ($user_email as $key => $value) {

            //     $id = array('id'=>$value);
            //     $data1 = $this->Production_model->get_where_user('user_email','user_register','','',array('id'=> $id['id']));
            //     array_push($send_mail,$data1[0]['user_email']);
            // }

            if (!empty($_FILES['image'])) {
                $imagePath = MAIL_SEND_IMAGE;
                if (!is_dir($imagePath)) {
                    mkdir($imagePath);
                    @chmod($imagePath, 0777);
                }
                $img_name = 'mail_img-'.rand(1000,100000).str_replace(' ','_', $_FILES['image']['name']);

                $destFile = $imagePath . $img_name; 
                $filename = $_FILES["image"]["tmp_name"];       
                move_uploaded_file($filename,  $destFile);
            }
            // echo"<pre>"; print_r($send_mail); exit;

            $atachfile_file =  base_url(MAIL_SEND_IMAGE.$img_name);
            // echo"<pre>"; print_r($atachfile_name); exit;

            //Add Mail Code Here....
            $send_mail = $this->Production_model->mail_send('Crystal Hositing',$admin_email[0]['email_address'],'Book your repair','mail_form/admin_send_mail/booking',$data,$atachfile_file !='' ? $atachfile_file : ''); 

            // $send_mail = $this->Production_model->send_email('News latter',$user_email,$message,'','',$atachfile_file !='' ? $atachfile_file :'');

            // echo"<pre>"; print_r($send_mail); exit;

            if ($send_mail == 1) {
                $this->session->set_flashdata('success', 'Mail Send Successfully...!');
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $this->session->set_flashdata('error', 'Mail Not Send...!');
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
        else{
            $this->session->set_flashdata('error', 'Select any one user email after send message...!');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }
    function reset_password($id){
        if($id){
            $new_password = $this->input->post('password');
            $conditions = array(
                "where" => array("id" => $id),
            );
            $records = array(
                'password' => $this->encryption->encrypt($new_password),
            );
            $user_details = $this->Production_model->get_all_with_where('user_register','','',array('id'=>$id));
            if ($user_details !=null) {
                if($user_details[0]['status'] == '1'){   //0 deactive , 1 active
                    $reset = $this->common_model->update_data("user_register", $records, $conditions);
                    if($reset != ''){
                        $this->session->set_flashdata('success', 'Reset Successfully...!');
                    }else{
                        $this->session->set_flashdata('error', 'Reset not applied...!');
                    }
                }else{
                    $this->session->set_flashdata('error', 'User is not active...!');
                }
            }
            redirect(base_url('authority/userlist/view'));
            // echo"<pre>"; print_r($data); exit;
        }
    }
}
?>