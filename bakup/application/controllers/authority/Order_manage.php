<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order_manage extends CI_Controller {

	private $_search_array = array('full_name'=>'Full name','user_name'=>'User name');

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function index($page_number='') {
        // ==================== pagination start ======================== //

        $join[0]['table_name'] = 'user_register';
        $join[0]['column_name'] = 'user_register.user_id = post_management.user_id';
        $join[0]['type'] = 'left';

        $join[1]['table_name'] = 'package_management';
        $join[1]['column_name'] = 'package_management.id = post_management.package_id';
        $join[1]['type'] = 'left';
        
        $tmp_data = $this->Production_model->jointable_descending(array('post_management.*','user_register.name','user_register.user_email','user_register.mobile_no','package_management.name as package_name','package_management.price as package_price','package_management.currency','package_management.day'),'post_management','',$join,'post_id','desc');

        // echo"<pre>"; print_r($tmp_data); exit;
        //================= create pagination start ===================//

        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/order_manage/index/');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;

        $record = $this->Production_model->only_pagination($tmp_array);
        // echo"<pre>"; print_r($record); exit;

        $data['post_details'] = $this->Production_model->jointable_descending(array('post_management.*','user_register.name','user_register.user_email','user_register.mobile_no','package_management.name as package_name','package_management.price as package_price','package_management.currency','package_management.day'),'post_management','',$join,'post_id','desc','','','',$record['limit'],$record['start']);            

        $data['pagination'] = $record['pagination'];
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;

        //================= create pagination end ===================//
        
        $this->load->view('authority/order_manage/view',$data);
    }

    function view($id)
    {
        $where['post_management.post_id'] = $id;

        $join[0]['table_name'] = 'user_register';
        $join[0]['column_name'] = 'user_register.user_id = post_management.user_id';
        $join[0]['type'] = 'left';

        $join[1]['table_name'] = 'country';
        $join[1]['column_name'] = 'country.id = user_register.id_country';
        $join[1]['type'] = 'left';

        $join[2]['table_name'] = 'state';
        $join[2]['column_name'] = 'state.id = user_register.id_state';
        $join[2]['type'] = 'left';

        $join[3]['table_name'] = 'city';
        $join[3]['column_name'] = 'city.id = user_register.id_city';
        $join[3]['type'] = 'left';

        $join[4]['table_name'] = 'package_management';
        $join[4]['column_name'] = 'package_management.id = post_management.package_id';
        $join[4]['type'] = 'left';

        $join[5]['table_name'] = 'category';
        $join[5]['column_name'] = 'category.category_id = post_management.category_id';
        $join[5]['type'] = 'left';

        $join[6]['table_name'] = 'sub_category';
        $join[6]['column_name'] = 'sub_category.sub_category_id = post_management.sub_category_id';
        $join[6]['type'] = 'left';
        
        $data['order_details'] = $this->Production_model->jointable_descending(array('post_management.*','user_register.name','user_register.user_email','user_register.mobile_no','country.country_name','state.state_name','city.city_name','package_management.name','package_management.price as package_price','package_management.currency','category.category_name','sub_category.sub_category_name'),'post_management','',$join,'','',$where,'','','');
        
        $data['payment_details'] = json_decode($data['order_details'][0]['payment_details']);

        $data['post_images'] = $this->Production_model->get_all_with_where('post_similer_image','','',array('post_id'=>$id));

        // echo $data['payment_details']->referenceId;
        // product details start //

        // $join1[0]['table_name'] = 'color';
        // $join1[0]['column_name'] = 'color.color_id = tbl_order_mapping.color_id';
        // $join1[0]['type'] = 'left';

        // $join1[1]['table_name'] = 'product_manage';
        // $join1[1]['column_name'] = 'product_manage.product_id = tbl_order_mapping.product_id';
        // $join1[1]['type'] = 'left';

        // $join1[2]['table_name'] = 'tbl_order';
        // $join1[2]['column_name'] = 'tbl_order.order_id = tbl_order_mapping.order_id';
        // $join1[2]['type'] = 'left';
        
        // $data['product_details'] = $this->Production_model->jointable_descending(array('tbl_order_mapping.*','tbl_order.order_number','color.color_name','product_manage.product_name_english','product_manage.product_name_arabic'),'tbl_order_mapping','',$join1,'mapping_id','desc',$where,'','','');

        // product details end //

        // echo"<pre>"; print_r($data); exit;

        $this->load->view('authority/order_manage/view_details',$data);
    }

    function order_status_change()
    {
        $post_id = $this->input->post('order_number');
        $data = array(
            'order_status' => $this->input->post('order_status_id'),
            'modified_date' => date('Y-m-d H:i:s')
        );

        $record = $this->Production_model->update_record('post_management',$data,array('post_id'=>$post_id));
        
        $get_status = $this->Production_model->get_where_user('order_status','post_management','','',array('post_id'=>$post_id));

        // echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;
        echo $get_status[0]['order_status']; exit;
    }

    function get_order_status()
    {
        $post_id = $this->input->post('order_number');

        $get_status = $this->Production_model->get_where_user('order_status','post_management','','',array('post_id'=>$post_id));

        echo $get_status[0]['order_status']; exit;
        // echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;
    }
    
    function delete($id)
    {
        $record = $this->Production_model->delete_record('post_management',array('post_id'=>$id));
        // $record = $this->Production_model->delete_record('tbl_order_mapping',array('order_id'=>$id));

        if ($record !=0) {
            $this->session->set_flashdata('success', 'Order deleted successfully...!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Order not delete...!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');

        foreach ($chkbox_id as $key => $value) {
            $tmp_record = $this->Production_model->delete_record('post_management',array('post_id'=>$value));
            // $tmp_record = $this->Production_model->delete_record('tbl_order_mapping',array('order_id'=>$value));
        }
        if ($tmp_record != 0) {
            $this->session->set_flashdata('success', 'Order deleted successfully...!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Order not delete...!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>