<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Notification_days extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index($page_number='') {
        $settings = array(
            "url" => site_url() . "authority/notification_days/index/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data = $this->common_model->get_pagination("notify", $conditions, $settings);
        if (isset($this->session->notify_msg) && $this->session->notify_msg != '') {
            $data = array_merge($data, array("success" => $this->session->notify_msg));
            $this->session->plan_msg = '';
        }
        unset($settings, $conditions);        
        $this->load->view('authority/notification_days/view', $data);
    }
    function add()
    {
        $data['notify_details'] = array();       
        $this->load->view('authority/notification_days/add-edit',$data);
    }    
    function insert_notify()
    {
        $data = $this->input->post();
        $days = $data['days']; 
        $count_notify = count($data['days']); 
        $create_date = date('Y-m-d H:i:s');
        $resultSet = Array();                 
        if(isset($count_notify)) {     
            for($i = 0; $i < $count_notify; $i++){
                $get_sub_days = $this->Production_model->get_all_with_where('notify','','',array('days'=> $days[$i]));

                if(!empty($get_sub_days)) 
                {
                    $resultSet[] = $days[$i];
                }
            }
            if(!empty($resultSet)) {
                $error = implode(', ', $resultSet);
                $this->session->set_flashdata('error',"Notify no of days <b>$error</b> is allredy exist...!");
                redirect(base_url('authority/notification_days/add'));
            }else{                           
                for($i = 0; $i < $count_notify; $i++) {
                    $data = array(
                        'days' => $days[$i],
                        'create_date' => $create_date
                    );
                    if($days[$i] !=null) {
                        $record = $this->Production_model->insert_record('notify',$data);
                    }
                }
                if($record !='') {
                    $this->session->set_flashdata('success', 'Notify day Add Successfully....!');
                    redirect(base_url('authority/notification_days'));
                }else{
                    $this->session->set_flashdata('error', 'Notify day Not Added....!');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
        }
    }

    function edit($id)
    {
        $data['notify_details'] = $this->Production_model->get_all_with_where('notify','','',array('id'=>$id));
        $this->load->view('authority/notification_days/add-edit',$data);
    }
    function update_notify()
    {
        $data = $this->input->post();
        $days = $data['days']; 
        $modified_date = date('Y-m-d H:i:s');
        $id = $this->input->post('id');        
        $get_image = $this->Production_model->get_all_with_where('notify','','',array('id'=>$id));
        $data = array(
            'days' => $days[0],
            'modified_date' => $modified_date
        );
        $record = $this->Production_model->update_record('notify',$data,array('id'=>$id));
        if ($record == 1) {
            $this->session->set_flashdata('success', 'Notify Update Successfully....');
            redirect(base_url('authority/notification_days'));
        }else{
            $this->session->set_flashdata('error', 'Notify Not Updated....');
            redirect($_SERVER['HTTP_REFERER']);
        }        
    }
    function delete_notify($id)
    {        
        $record = $this->Production_model->delete_record('notify',array('id'=>$id));
        if($record != 0) {
            $this->session->set_flashdata('success', 'Notify Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Notify Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $record = $this->Production_model->delete_record('notify',array('id'=>$value));
        }
        if($record != 0) {
            $this->session->set_flashdata('success', 'Notify Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Notify Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>