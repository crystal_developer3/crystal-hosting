<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product_manage extends CI_Controller {

	private $_search_array = array('full_name'=>'Full name','user_name'=>'User name');

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function index($page_number='') {
        // $settings = array(
        //     "url" => site_url() . "authority/userlist/view/",
        //     "per_page" => RECORDS_PER_PAGE,
        // );
        //   $current_filter_users = array('search_key'=> '', 'search_value' => '');
		// if(isset($_REQUEST['clear_filter'])){
		// 	$this->session->current_filter_users = $current_filter_users;
		// 	redirect(base_url().'authority/userlist/view');
		// }

		// if(!empty($this->session->current_filter_users)){
		// 	$current_filter_users = $this->session->current_filter_users;
		// }
		// if ($this->input->post("submit")) {
		// 	if($this->input->post('search_key') != '') {
		// 		$current_filter_users['search_key'] = $this->input->post('search_key');
		// 	}
		// 	if($this->input->post('search_value') != '') {
		// 		$current_filter_users['search_value']= $this->input->post('search_value');
		// 	}
		// }

		// $like = array();
		// if($current_filter_users['search_key'] != '' && $current_filter_users['search_value']){
		// 	$like = array('LIKE'=>array($current_filter_users['search_key']=>$current_filter_users['search_value']));
		// }
		// if(!empty($like)){
		// 	$conditions = array("select" => "user_id,full_name,user_name,email,mobile_no,date_of_birth,address,profile_picture,gender,status");
		// 	$conditions = array_merge($conditions,$like);
		// } else {
		// 	$conditions = array("select" => "user_id,full_name,user_name,email,mobile_no,date_of_birth,address,profile_picture,gender,status");
		// }
  //       $data = $this->common_model->get_pagination("user_register", $conditions, $settings);
  //       // echo"<pre>"; print_r($data); exit;

  //       if (isset($this->session->users_msg) && $this->session->users_msg != '') {
  //           $data = array_merge($data, array("success" => $this->session->users_msg));
  //           $this->session->users_msg = '';
  //       }
  //       unset($settings, $conditions);

  //       $this->session->current_filter_users = $current_filter_users;
		// $data['current_filter_users'] = $current_filter_users;
		// $data['search_options'] = $this->_search_array;
  //       $conditions = array("select" => "*");

        // ==================== pagination start ======================== //

        $data = $this->Production_model->pagination_create(base_url('authority/product_manage/index/'),'product_manage','','product_id','desc',array());
        // ==================== pagination end ======================== //
        // echo"<pre>"; print_r($data); exit;
        
        $this->load->view('authority/product/view',$data);
    }

    function add()
    {
        $data['category_details'] = $this->Production_model->get_all_with_where('category','','',array('status'=>'1'));
        $data['product_details'] = array();
        $data['similar_img_details'] = array();
        $this->load->view('authority/product/add-edit',$data);
    }

    function add_product()
    {
        // $data = $this->input->post();
        // echo"<pre>"; print_r($data); exit;

        $create_date = date('Y-m-d h:i:s');
        
        if($_FILES['product_image']['name']==""){ $img_name='';}
        else{
            $imagePath = PRODUCT_IMAGE;
            $img_name = 'PRODUCT-'.rand(111,999)."-".str_replace(' ', '_',$_FILES['product_image']['name']);

            $destFile = $imagePath . $img_name; 
            $filename = $_FILES["product_image"]["tmp_name"];       
            move_uploaded_file($filename,  $destFile);

            // // create Thumbnail -- IMAGE_SIZES start //
            // $image_sizes = array(
            //     // 'mobile'=>array(75,75),
            //     'thumb' => array(50, 50), // PRODUCT IMAGE DISPLAY.
            //     'medium' => array(800, 800), // PRODUCT IMAGE DISPLAY.
            //     // 'large' => array(900, 500)
            // );

            // $this->load->library('image_lib');
            // foreach ($image_sizes as $key=>$resize) {
            //     $config = array(
            //         'source_image' => PRODUCT_IMAGE.$img_name,
            //         'new_image' => PRO_ROOT_UPLOAD_PATH .'/'.$key,
            //         'maintain_ratio' => FALSE,
            //         'width' => $resize[0],
            //         'height' => $resize[1],
            //         'quality' =>70,
            //     );
            //     $this->image_lib->initialize($config);
            //     $this->image_lib->resize();
            //     $this->image_lib->clear();
            //     // echo"<pre>"; print_r($config); 
            // }            
            // $this->load->library('image_lib', $config);
            // $this->image_lib->resize();

            // create Thumbnail -- IMAGE_SIZES end //
            $data['product_image'] = $img_name;
            // echo"<pre>"; print_r($data); 
        }

        $new_arrivals_product = $this->input->post('new_arrivals_product');
        $latest_product = $this->input->post('letest_product');
        $related_product = $this->input->post('related_product');
        $best_seller = $this->input->post('best_seller');
        $populer_product = $this->input->post('populer_product');

        $data = array(
            'category_id' => $this->input->post('category_id'),
            'sub_category_id' => $this->input->post('sub_category_id'),
            'product_name' => $this->input->post('product_name'),
            'product_desc' => $this->input->post('product_desc'), 
            
            'meta_keywords' => str_replace(' ', '-', $this->input->post('meta_keywords')),   
            'meta_description' => str_replace(' ', '-', $this->input->post('meta_description')),   
            'meta_title' => str_replace(' ', '-', $this->input->post('meta_title')),   
            'slug_name' => str_replace(' ', '-', $this->input->post('slug_name')),  

            'new_arrivals_product' =>  $new_arrivals_product !='' ? $new_arrivals_product : '1',
            'latest_product' =>  $latest_product !='' ? $latest_product : '1',
            'related_product' =>  $related_product !='' ? $related_product : '1',
            'best_seller' =>  $best_seller !='' ? $best_seller : '1',
            'populer_product' =>  $populer_product !='' ? $populer_product : '1',
            'product_image' => $img_name,
            'create_date' => $create_date
        ); 
        // echo"<pre>"; print_r($data); exit;

        $insert_record = $this->Production_model->insert_record('product_manage',$data);
        // $insert_record = 1;

        //===================================================================================//
        //=================== multiple insert for product attribute start ===================//
        //===================================================================================//
        
        if ($insert_record !='') {  
            //=========================================================================//
            //======================= multiple image upload start =====================//
            //=========================================================================//

            $new_name = 'PRDCT_SIMILAR_IMG'.rand();

            $config['upload_path']   = PRDCT_SIMILAR_IMG;
            $config['allowed_types'] = '*';
            $config['file_name'] = $new_name;           

            $this->load->library('upload', $config);

            $files = $_FILES;       
            $cpt = count($_FILES['similar_image']['name']); 

            if($files['similar_image']['name'][0] != "")
            {
                for($i=0; $i<$cpt; $i++)
                {   
                    $_FILES['similar_image']['name']= $files['similar_image']['name'][$i];
                    $_FILES['similar_image']['type']= $files['similar_image']['type'][$i];
                    $_FILES['similar_image']['tmp_name']= $files['similar_image']['tmp_name'][$i];
                    $_FILES['similar_image']['error']= $files['similar_image']['error'][$i];
                    $_FILES['similar_image']['size']= $files['similar_image']['size'][$i]; 

                    // $this->load->library('upload',$config);
                    $this->upload->do_upload('similar_image');
                    $image_name = $this->upload->data();
                    $image_name = $image_name['file_name'];  

                    $this->Production_model->generate_thumbnail(PRDCT_SIMILAR_IMG,$image_name);
                    
                    $imagedata = array(
                        'product_id' => $insert_record,
                        'product_similar_image' => $image_name,
                        'create_date' => date('Y-m-d H:i:s')
                    ); 

                    // echo "<pre>";print_r($imagedata); exit;
                    $image_id = $this->Production_model->insert_record('product_similar_image', $imagedata);
                }       
            }
            //=========================================================================//
            //======================= multiple image upload end =======================//
            //=========================================================================//
        }
        //===================================================================================//
        //=================== multiple insert for product attribute end =====================//
        //===================================================================================//

        if ($insert_record !='') {
            $this->session->set_flashdata('success', 'Product Add Successfully....!');
            redirect(base_url('authority/product_manage')); 
        }
        else
        {
            $this->session->set_flashdata('error', 'Product Not Added....!');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function edit($id)
    {
        $data['category_details'] = $this->Production_model->get_all_with_where('category','','',array('status'=>'1'));

        $data['product_details'] = $this->Production_model->get_all_with_where('product_manage','','',array('product_id'=>$id));

        $data['similar_img_details'] = $this->Production_model->get_all_with_where('product_similar_image','','',array('product_id'=>$id));        
        // echo"<pre>"; echo $this->db->last_query(); print_r($data['get_attr_details']); exit;
        $this->load->view('authority/product/add-edit',$data);
    }

    function update_product()
    {
        $product_id = $this->input->post('product_id');
        $modified_date = date('Y-m-d H:i:s');

        $product_data = array(
            'category_id' => $this->input->post('category_id'),
            'sub_category_id' => $this->input->post('sub_category_id'),
            'product_name' => $this->input->post('product_name'),
            'product_desc' => $this->input->post('product_desc'), 
            'meta_keywords' => str_replace(' ', '-', $this->input->post('meta_keywords')),   
            'meta_description' => str_replace(' ', '-', $this->input->post('meta_description')),   
            'meta_title' => str_replace(' ', '-', $this->input->post('meta_title')),   
            'slug_name' => str_replace(' ', '-', $this->input->post('slug_name')),   
            'modified_date' => $modified_date
        ); 
        // echo"<pre>"; print_r($product_data); exit;

        if($_FILES['product_image']['name'] !='')
        {
            $get_image = $this->Production_model->get_all_with_where('product_manage','','',array('product_id'=>$product_id));
            // echo"<pre>"; print_r($get_image); exit;
            if ($get_image !=null && $get_image[0]['product_image'] !=null && !empty($get_image[0]['product_image']) && $get_image[0]['product_image'] !='')
            {
                @unlink(PRODUCT_IMAGE.$get_image[0]['product_image']);
                @unlink(PRODUCT_IMAGE.'/thumbnail/medium/'.$get_image[0]['product_image']);
                @unlink(PRODUCT_IMAGE.'/thumbnail/thumb/'.$get_image[0]['product_image']);
            }

            $imagePath = PRODUCT_IMAGE;
            $img_name = 'PRODUCT-'.rand(111,999)."-".str_replace(' ', '_',$_FILES['product_image']['name']);

            $destFile = $imagePath . $img_name; 
            $filename = $_FILES["product_image"]["tmp_name"];       
            move_uploaded_file($filename,  $destFile);

            // create Thumbnail -- IMAGE_SIZES start //
            // $image_sizes = array(
            //     // 'mobile'=>array(75,75),
            //     'thumb' => array(50, 50), // PRODUCT IMAGE DISPLAY.
            //     'medium' => array(800, 800), // PRODUCT IMAGE DISPLAY.
            //     // 'large' => array(900, 500)
            // );

            // $this->load->library('image_lib');
            // foreach ($image_sizes as $key=>$resize) {
            //     $config = array(
            //         'source_image' => PRODUCT_IMAGE.$img_name,
            //         'new_image' => PRO_ROOT_UPLOAD_PATH .'/'.$key,
            //         'maintain_ratio' => FALSE,
            //         'width' => $resize[0],
            //         'height' => $resize[1],
            //         'quality' =>70,
            //     );
            //     $this->image_lib->initialize($config);
            //     $this->image_lib->resize();
            //     $this->image_lib->clear();
            //     // echo"<pre>"; print_r($config); 
            // }            
            // $this->load->library('image_lib', $config);
            // $this->image_lib->resize();

            // create Thumbnail -- IMAGE_SIZES end //
            $product_data['product_image'] = $img_name;
        }

        //=========================================================================//
        //======================= multiple image upload start =====================//
        //=========================================================================//

        $new_name = 'PRDCT_SIMILAR_IMG'.rand();
        $config['upload_path']   = PRDCT_SIMILAR_IMG;
        $config['allowed_types'] = '*';
        $config['file_name'] = $new_name;           

        $this->load->library('upload', $config);

        $files = $_FILES;       
        $cpt = count($_FILES['similar_image']['name']); 

        if($files['similar_image']['name'] != "")
        {
            for($i=0; $i<$cpt; $i++)
            {   
                $_FILES['similar_image']['name']= $files['similar_image']['name'][$i];
                $_FILES['similar_image']['type']= $files['similar_image']['type'][$i];
                $_FILES['similar_image']['tmp_name']= $files['similar_image']['tmp_name'][$i];
                $_FILES['similar_image']['error']= $files['similar_image']['error'][$i];
                $_FILES['similar_image']['size']= $files['similar_image']['size'][$i]; 

                // $this->load->library('upload',$config);
                $this->upload->do_upload('similar_image');
                $image_name = $this->upload->data();
                $image_name = $image_name['file_name'];  

                $this->Production_model->generate_thumbnail(PRDCT_SIMILAR_IMG,$image_name);
                
                $imagedata = array(
                    'product_id' => $product_id,
                    'product_similar_image' => $image_name,
                    'create_date' => date('Y-m-d H:i:s')
                ); 
                // echo "<pre>";print_r($imagedata); exit;
                $image_id = $this->Production_model->insert_record('product_similar_image', $imagedata);
            }       
        }

        //=========================================================================//
        //======================= multiple image upload end =======================//
        //=========================================================================//

        $record = $this->Production_model->update_record('product_manage',$product_data,array('product_id'=>$product_id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;

        if ($record == 1) {
            $this->session->set_flashdata('success', 'Product Update Successfully....!');
            redirect(base_url('authority/product_manage'));
        }
        else
        {
            $this->session->set_flashdata('error', 'Product Not Updated....!');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    // delete product similar image start //

    function delete_image($id)
    {
        $get_image = $this->Production_model->get_all_with_where('product_similar_image','','',array('image_id'=>$id));
        // echo"<pre>"; print_r($get_image); exit;

        if ($get_image !=null && $get_image[0]['product_similar_image'] !=null && !empty($get_image[0]['product_similar_image']) && $get_image[0]['product_similar_image'] !='') {

            @unlink(PRDCT_SIMILAR_IMG.$get_image[0]['product_similar_image']);
            @unlink(PRDCT_SIMILAR_IMG.'thumbnail/'.$get_image[0]['product_similar_image']);
        }

        $record = $this->Production_model->delete_record('product_similar_image',array('image_id'=>$id));
        // echo "<pre>"; print_r($record); exit; 

        if ($record == 1) {
            $this->session->set_flashdata('success', 'Product Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Product Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    // delete product similar image end //

    function delete($id)
    {
        $get_image = $this->Production_model->get_all_with_where('product_manage','','',array('product_id'=>$id));

        if ($get_image !=null && $get_image[0]['product_image'] !=null && !empty($get_image[0]['product_image']) && $get_image[0]['product_image'] !='')
        {
            @unlink(PRODUCT_IMAGE.$get_image[0]['product_image']);
            @unlink(PRODUCT_IMAGE.'thumbnail/medium/'.$get_image[0]['product_image']);
            @unlink(PRODUCT_IMAGE.'thumbnail/thumb/'.$get_image[0]['product_image']);
        }

        $get_smlr_image = $this->Production_model->get_all_with_where('product_similar_image','','',array('product_id'=>$id));
        if ($get_smlr_image !=null) {
            foreach ($get_smlr_image as $key => $img_row) {
                @unlink(PRDCT_SIMILAR_IMG.$img_row['product_similar_image']);
                @unlink(PRDCT_SIMILAR_IMG.'thumbnail/'.$img_row['product_similar_image']);
                $tmp_record = $this->Production_model->delete_record('product_similar_image',array('product_id'=>$img_row['product_id']));
            }
        }
        $record = $this->Production_model->delete_record('product_manage',array('product_id'=>$id));

        if ($record == 1) {
            $this->session->set_flashdata('success', 'Product Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Product Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $get_image = $this->Production_model->get_all_with_where('product_manage','','',array('product_id'=>$value));
            
            if ($get_image !=null && !empty($get_image[0]['product_image']) && $get_image[0]['product_image'] !='')
            {
                @unlink(PRODUCT_IMAGE.$get_image[0]['product_image']);
                @unlink(PRO_MEDIUM_IMAGE.$get_image[0]['product_image']);
                @unlink(PRO_THUMB_IMAGE.$get_image[0]['product_image']);
            }

            $get_smlr_image = $this->Production_model->get_all_with_where('product_similar_image','','',array('product_id'=>$value));
            if ($get_smlr_image !=null)
            {
                foreach ($get_smlr_image as $key => $img_row) {
                    @unlink(PRDCT_SIMILAR_IMG.$img_row['product_similar_image']);
                    @unlink(PRDCT_SIMILAR_IMG.'thumbnail/'.$img_row['product_similar_image']);
                    $tmp_record = $this->Production_model->delete_record('product_similar_image',array('product_id'=>$value));
                }
            }
            $record = $this->Production_model->delete_record('product_manage',array('product_id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Product Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Product Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  

    // ajax call category wise all subcategory list start // 
    
    function get_sub_cat_name(){
        $cat_id = $this->input->post('category_id');
        $sub_cat_id = $this->input->post('sub_cat_id');

        $get_sub_cat = $this->Production_model->get_all_with_where('sub_category','','',array('category_id'=>$cat_id));
        // echo"<pre>"; print_r($get_sub_cat); exit;
        echo"<option value=''>Select</option>";
        if ($get_sub_cat !=null) {
            foreach ($get_sub_cat as $key => $sub_cat_row) {
                ?>
                <option value="<?= $sub_cat_row['sub_category_id']?>" <?= ($sub_cat_row['sub_category_id'] == $sub_cat_id) ? 'selected' : '';?>>
                    <?php   
                    echo $sub_cat_row['sub_category_name'];
                    ?>
                </option>
            <?php }
        }
    }   
}
?>