<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        // header('Content-Type: application/json');
    }
	
    function login() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!decodeJson(array("email","password"),$data)){
				$email = $data['email'];
				$password = $data['password'];
				if(!checkEmpty($is_api_call)){ // for api
					header('Content-Type: application/json');
					if(checkHeader()){
						login($email,$password,$is_api_call);            
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
	
				}
			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function getCountries() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					getCountry($is_api_call);            
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function getStates() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			$country_id = isset($data->country_id) ? $data->country_id :'';

			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					if(checkEmpty($country_id)){
						echo json_encode(returnResponse("Fail","Country id is empty",null));exit;
					}else{
						getStates($country_id,$is_api_call);            
					}
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function getCities() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			$state_id = isset($data->state_id) ? $data->state_id :'';
			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					if(checkEmpty($state_id)){
						echo json_encode(returnResponse("Fail","State id is empty",null));exit;
					}else{
						getCities($state_id,$is_api_call);            
					}
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function registration() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!decodeJson(array('firstName','lastName','dob','email','password','address','city','states','country','contactNumber','zipCode'),$data)){
				$first_name=$data['firstName'];
				$last_name=$data['lastName'];
				$dob=$data['dob'];
				$email=$data['email'];
				$password=$data['password'];
				$address=$data['address'];
				$city=$data['city'];
				$state=$data['states'];
				$country=$data['country'];
				$phone_number=$data['contactNumber'];
				$zip_code=$data['zipCode'];

				if(!checkEmpty($is_api_call)){ // for api
					if(checkHeader()){
					registration($first_name ,$last_name ,$dob ,$email ,$password ,	$address ,	$city ,	$state ,$country ,$phone_number ,	$zip_code ,$is_api_call); 		
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
	
				}
			}
		}else {
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}
	
	function forgotPassword() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!decodeJson(array("email"),$data)){
				$email = $data['email'];
				if(!checkEmpty($is_api_call)){ // for api
					header('Content-Type: application/json');
					if(checkHeader()){
						forgotPassword($email,$is_api_call);            
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
	
				}
			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function changePassword() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!decodeJson(array('id','old_password','new_password'),$data)){
				$id=$data['id'];
				$oldPassword=$data['old_password'];
				$newPassword=$data['new_password'];
								
				if(!checkEmpty($is_api_call)){ // for api
					if(checkHeader()){
						changePassword($id,$oldPassword,$newPassword,$is_api_call); 		
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
					
				}
			}
		}else {
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}
	
	function getProfile() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!decodeJson(array("id"),$data)){
				$user_id = $data['id'];
				if(!checkEmpty($is_api_call)){ // for api
					header('Content-Type: application/json');
					if(checkHeader()){
						getProfile($user_id,$is_api_call);            
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
	
				}
			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function updateProfile() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data'],true);
			$is_api_call = isset($data['is_api_call']) ? $data['is_api_call'] :'';
			if(!decodeJson(array('id','firstName','lastName','dob','email','address','city','states','country','contactNumber','zipCode'),$data)){
				$id=$data['id'];
				$first_name=$data['firstName'];
				$last_name=$data['lastName'];
				$dob=$data['dob'];
				$email=$data['email'];
				$address=$data['address'];
				$city=$data['city'];
				$state=$data['states'];
				$country=$data['country'];
				$phone_number=$data['contactNumber'];
				$zip_code=$data['zipCode'];
				
				if(!checkEmpty($is_api_call)){ // for api
					if(checkHeader()){
						updateProfile($id,$first_name,$last_name,$dob,$email,$address,$city,$state,$country,$phone_number,$zip_code,$is_api_call); 		
					}else {
						echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
					}
				}else{ //for web
					
				}
			}
		}else {
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}
}
?>
