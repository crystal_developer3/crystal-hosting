<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
      <link rel="stylesheet" href="<?=base_url('assets/css/jquery.dataTables.min.css')?>">
    </head>
    <body>
        <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          <!-- about start -->
            <div class="allpage_banner_email allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'email.jpg')?>);">
              <h1 class="title_h1">TUTORIALS</h1>
              <p><a href="<?=base_url()?>">Home </a> / TUTORIALS</p>
            </div>
            <!-- Tutorial Strat Here -->
            <?php 
              if(isset($tutorial_details) && $tutorial_details !=null){ 
                $tutorials = $tutorial_details[0];
                ?>
                <div class="tutorial">
                  <div class="container padding_all">
                    <div class="row">
                      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="tutorial-link">
                          <div class="title" style="margin-bottom: 20px;">
                            <h2><b><u><?=$tutorials['tutorial_name']?></u></b></h2>
                          </div>
                          <a href="<?=base_url('tutorials/video-tutorials')?>" class="btn btn-primary">Back</a>
                          <div class="video" style="margin-top: 20px;">
                            <div class="img-container text-center">
                              <iframe width="620" height="400" src="https://www.youtube.com/embed/<?=getYoutubeKey($tutorials['tutorial_link']);?>">
                              </iframe>
                              <!-- <video width="620" controls
                                poster="<?=getYoutubeThumbnail($tutorials['tutorial_link']);?>" >
                                <source
                                  src="https://www.youtube.com/embed/<?=getYoutubeKey($tutorials['tutorial_link']);?>"
                                  type="video/mp4">
                                <source
                                  src="https://www.youtube.com/embed/<?=getYoutubeKey($tutorials['tutorial_link']);?>"
                                  type="video/ogg">
                                <source
                                  src="https://www.youtube.com/embed/<?=getYoutubeKey($tutorials['tutorial_link']);?>"
                                  type="video/avi">
                              </video> -->
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php
              }
            ?>
            <!-- Tutorial End Here -->            
      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?> 
   </body>
</html> 