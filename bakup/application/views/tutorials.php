<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
      <link rel="stylesheet" href="<?=base_url('assets/css/jquery.dataTables.min.css')?>">
    </head>
    <body>
      <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          <!-- about start -->
            <div class="allpage_banner_email allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'email.jpg')?>);">
              <h1 class="title_h1">Tutorials</h1>
              <p><a href="<?=base_url()?>">Home </a> / Tutorials</p>
            </div>
            <!-- Tutorial Strat Here -->
            <div class="tutorial">
              <div class="container padding_all">
                <div class="row panel_row">
                  <h2>Frequently Asked Questions</h2>
                  <div class="col-md-12 col-xs-12">
                    <div class="panel-group" id="accordion">
                      <?php 
                        if(isset($tutorial_faq_details) && !empty($tutorial_faq_details)){ 
                          $i=1;
                          foreach ($tutorial_faq_details as $key => $value) {
                            ?>
                            <div class="panel panel-default">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$i?>"><div class="panel-heading">
                                <h4 class="panel-title">
                                  <?=$value['title']?> <span class="fa fa-arrow-circle-down"></span><span class="fa fa-arrow-circle-up"></span>
                                </h4>
                              </div></a>
                              <div id="collapse<?=$i?>" class="panel-collapse collapse <?=($i==1)?'in':''?>">
                                <div class="panel-body"><?=$value['description']?></div>
                              </div>
                            </div>
                            <?php 
                            $i++;
                          }
                        } 
                      ?>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Tutorial End Here -->
            <!-- footer us start -->
            
      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?>       
   </body>
</html> 