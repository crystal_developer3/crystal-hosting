<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
      <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/view_ticket.css">
    </head>
    <body>
      <div class="main aos-all" id="transcroller-body">
        <?php $this->load->view('include/header');?>
        <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
        <!-- about start -->
        <div class="allpage_banner_about allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'about.jpg')?>);">
            <h1 class="title_h1">Ticket Replies</h1>
            <p><a href="<?=base_url()?>">Home </a> / Ticket Replies</p>
        </div>
        <!-- domain_style start -->
        <div class="padding_all text-center domain_style">
          <div class="container">
            <h5></h5>
            <div class="row">
              <div class="col-md-3 pull-md-left sidebar">
                <div menuitemname="Client Details" class="panel panel-default">
                  <div class="panel-heading">
                      <h3 class="panel-title"> <i class="fa fa-user"></i>&nbsp; Your Info
                      </h3>
                  </div>
                  <div class="panel-body">
                    <p><strong><span id="first_name"></span> <span id="last_name"></span></strong></p><p></p>
                    <p>
                      <span id="address"></span>
                      <span id="country"></span>,<span id="state"></span>,<span id="city"></span>
                    </p>
                  </div>
                  <div class="panel-footer clearfix">
                    <a href="<?=base_url('profile')?>" class="btn btn-success btn-sm btn-block">
                      <i class="fa fa-pencil"></i> Update
                    </a>
                  </div>
                </div>
              </div>
              <div class="col-md-9 " data-aos="flip-left">
                <h3 class="well">Ticket Replies</h3>
              </div>
              <div class="col-md-9 ">             
                <!-- <div class="well well-sm col-md-12" style="padding:4em"  data-aos="flip-left" >
                  <div id="accordion" role="tablist">
                    <div class="card">
                      <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <div class="card-header" role="tab" id="headingTwo">
                          <h5 class="mb-0">
                            <i class="fa fa-pencil"></i>  Replay
                          </h5>
                        </div>
                      </a>
                      <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                          <div class="card-body">
                            <form class="row" id="ticket-reply">
                              
                              <div class="form-group col-md-12 col-lg-12 col-sm-12">
                                <label for="messege">Messege</label>
                                <textarea class="form-control" id="messege" rows="3"></textarea>
                              </div>
                              <div class="form-group col-md-12">
                                <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                            </form>
                          </div>
                      </div>
                    </div>
                  </div>
                  <div class="ticket-reply">
                    <div class="user_date">
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <div class="user text-left">
                            <i class="fa fa-user"></i>
                            Crystal Hosting
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <div class="user text-right">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            12/10/2019
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="ticket-reply-messege text-left">
                      <p class="mb-10">Dear Sir/Madam,</p>
                      <p class="mb-10">Greetings from Crystal Hosting !!</p>
                      <p class="mb-10">Let us know if need further assistance .</p>
                      <p>Thanks & Regards <br> Umesh Chauhan <br> Server Administrator<br>crystalhosting.com</p>
                    </div>
                    <div class="rating">
                      <a href="#" class="active"><span class="fa fa-star"></span></a>
                      <a href="#" class="active"><span class="fa fa-star"></span></a>
                      <a href="#" class="active"><span class="fa fa-star"></span></a>
                      <a href="#"><span class="fa fa-star-o"></span></a>
                      <a href="#"><span class="fa fa-star-o"></span></a>
                    </div>
                  </div>
                  <div class="ticket-reply">
                    <div class="user_date">
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <div class="user text-left">
                            <i class="fa fa-user"></i>
                            Crystal Hosting
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <div class="user text-right">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            12/10/2019
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="ticket-reply-messege text-left">
                      <p class="mb-10">Dear Sir/Madam,</p>
                      <p class="mb-10">Greetings from Crystal Hosting !!</p>
                      <p class="mb-10">Let us know if need further assistance .</p>
                      <p>Thanks & Regards <br> Umesh Chauhan <br> Server Administrator<br>crystalhosting.com</p>
                    </div>
                    <div class="ip-address text-left">
                      <p>IP Address: 123.201.65.102</p>
                    </div>
                  </div>
                </div> -->

                <div >
                    <div id="accordion" role="tablist">
                        <div class="card">
                          <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <div class="card-header " role="tab" id="headingTwo">
                              <h3 style="margin-left: 1em;">
                                Reply
                              </h3>
                            </div>
                          </a>
                          <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                              <div class="card-body">
                                <form class="row" id="ticket_replies" method="post">
                                    <input type="hidden" name="reply_type" value="0">
                                    <input type="hidden" name="reply_from" id="reply_from" >
                                    <input type="hidden" name="reply_to" value="1">
                                    <input type="hidden" name="ticket_id" value="<?=$id?>">
                                  <div class="form-group col-md-12 col-lg-12 col-sm-12">
                                    <label for="ticket_reply">Messege :</label>
                                    <textarea class="form-control" id="ticket_reply" name="ticket_reply" rows="3"></textarea>
                                  </div>
                                  <div class="form-group col-md-12 col-lg-12 col-sm-12">
                                      <label for="ticket_document">Ticket Document :</label>
                                      <input name="ticket_document" id="ticket_document" type="file" class="input_all">
                                      <p class="err_p" id="ticket_document_err"></p>
                                  </div>
                                  <div class="form-group col-md-12">
                                    <input type="button" class="btn btn-primary check" value="Submit">
                                  </div>
                                </form>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h3>Ticket Replies</h3>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped ticket_repies">
                                <thead>
                                    <tr>
                                        <th>Reply Date & Time</th>
                                        <th>Replied By</th>
                                        <th>Reply</th>
                                        <th>Document</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($tickets_details)) {
                                        foreach ($tickets_details as $key => $value) {
                                            if($value['reply_type'] == '1'){
                                                $reply_type = 'Admin';
                                            }else{
                                                $reply_type = $first_name;
                                            }
                                            if($value['reply_from'] == '1'){
                                                $reply_from = 'Admin';
                                            }else{
                                                $reply_from = $first_name;
                                            }
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo isset($value['reply_date']) ? date('d-m-Y', strtotime($value['reply_date'])) . ' ' : ''; ?>
                                                    <?php echo isset($value['reply_time']) ? date('H:i A', strtotime($value['reply_time'])) : ''; ?>
                                                </td>
                                                <td>
                                                    <?=$reply_type?>
                                                </td>
                                                <td><a href="javascript:void(0);" class="view-reply">View</a></td>
                                                <td>
                                                    <?php
                                                    if ($value['ticket_document'] == '') {
                                                        echo 'Not available';
                                                    } else {
                                                        $path = base_url(TICKET_REPLY_FILE).$value['ticket_document'];
                                                        echo '<a href="'.$path.'" target="_blank" download>Download & View</a>';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr style="display:none;" class="pop_up_message">
                                                <td colspan="6">
                                                    <p><b>Reply</b></p>
                                                    <div class="problem">                                        
                                                        <?php echo isset($value['ticket_reply']) ? closetags($value['ticket_reply']) : ''; ?>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        echo '<tr><td colspan="5" align="center">Records not available</td></tr>';
                                    }
                                    ?>                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- domain_style over -->
        <!-- help_line start -->
        <div class="padding_all help_line" style="background-image: url(<?=base_url(IMAGES.'mail.jpg')?>);">
          <div class="container text-center">
            <h1 class="h1_title">Need Help?</h1>
              <h4>Let us help you make the right decision!</h4>
            <div class="row margin_top">
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border">
                  <a href="javascript:;"><i class="fa fa-phone"></i></a>
                  <h3>Call Us</h3>
                  <p>Give us a call & ask all of your questions</p>
                </div>
              </div>
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border ">
                  <a href="javascript:;"><i class="fa fa-pencil"></i></a>
                  <h3>Email Us</h3>
                  <p>Send us an email with all of your questions</p>
                </div>
              </div>
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border margin_row">
                  <a href="javascript:;"><i class="fa fa-video-camera"></i></a>
                  <h3>Live Chat</h3>
                  <p>Chat with a member of our support team now</p>
                </div>
              </div>
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border margin_row">
                  <a href="javascript:;"><i class="fa fa-certificate"></i></a>
                  <h3>Real Reviews</h3>
                  <p>Read what real customers have to say</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php $this->load->view('include/footer');?>  
    </div>
    <?php $this->load->view('include/footer_js');?>
    <script type="text/javascript">
      $(document).ready(function () {
        get_user_profile('<?=$this->session->login_id?>');
      });
      function get_user_profile(user_id){
          var uurl = BASE_URL+"api/user/getProfile";
          // var post_data = {id:user_id};
           $.ajax({
               url: uurl,
               method: 'POST',
               dataType:'json',
               data: {id:user_id},
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){
                
                if (response.result=="Success") {
                  var userdata = response.data;
                  console.log(userdata);
                  $('#first_name').text(userdata.first_name);
                  $('#reply_from').val(userdata.id);

                  $('#last_name').text(userdata.last_name);
                  $('#email').text(userdata.email);
                  $('#address').text(userdata.address);
                  $('#country').text(userdata.country_id_info);
                  $('#state').text(userdata.state_id_info);
                  $('#city').text(userdata.city_id_info);
                }
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
           });
          
        }
    </script> 
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
    <script type="text/javascript">

        $('#ticket_replies').validate({
            rules: {
                ticket_reply: {
                    required: true,
                },
            },
            messages: {
                ticket_reply: {
                  required: "Please enter your message",
                },
            },
        });

        var form = $("#ticket_replies");
        form.validate();
        $(document).on('click','.check',function(){
          var formData = new FormData($('#ticket_replies')[0]);
          if(form.valid()){
            add_ticket_reply();
          }
        });
        function add_ticket_reply(){          
          var formData = new FormData($('#ticket_replies')[0]);
          var uurl = BASE_URL+"api/plans/add_ticket_reply";
          var ticketURL = BASE_URL+"ticket/ticket-reply/<?=$id?>";

           $.ajax({
               url: uurl,
               type: 'POST',
               data: formData,
               dataType:'json',
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){
                 if (response.result=="Success") {
                    $.alert({
                      title: 'Message',
                      type: 'green',
                      content: response.message,
                    });
                    setTimeout(function() { window.location.href = ticketURL; }, 2000);
                 }else{
                   $.alert({
                        title: 'Message',
                        type: 'red',
                        content: 'Reply not added. !!',
                    });
                 }
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
               cache: false,
               contentType: false,
               processData: false
           });
        }
        $('.view-reply').click(function () {
            $(this).closest('tr').next('tr').fadeToggle();
        });
    </script> 
   </body>
</html> 