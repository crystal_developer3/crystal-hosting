<?php
$ticket_info = get_ticket($ticket_id);
$ticket_history_info = get_ticket_history($ticket_id);
?>
<div class="row">
    <div class="col-md-12">
        <h3>Ticket Information</h3>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Ticket Date & Time</th>
                        <th>Ticket Number</th>
                        <th>Current Status</th>
                        <th>Priority</th>
                        <th>Preferred Date & Time</th>
                        <th>Problem</th> 
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php echo isset($ticket_info['create_date']) ? date('d-m-Y', strtotime($ticket_info['create_date'])) . ' ' : ''; ?>
                            <?php echo isset($ticket_info['time']) ? $ticket_info['time'] : ''; ?>
                        </td>
                        <td><?php echo isset($ticket_info['ticket_no']) ? $ticket_info['ticket_no'] : ''; ?></td>
                        <td><?php echo isset($ticket_info['status_name']) ? $ticket_info['status_name'] : ''; ?></td>
                        <td><?php echo isset($ticket_info['priority_name']) ? $ticket_info['priority_name'] : ''; ?></td>
                        <td>
                            <?php echo isset($ticket_info['preferred_date']) ? date('d-m-Y', strtotime($ticket_info['preferred_date'])) . ' ' : ''; ?>
                            <?php echo isset($ticket_info['preferred_time']) ? $ticket_info['preferred_time'] : ''; ?>
                        </td>
                        <td><a href="javascript:void(0);" class="view-problem">View</a></td>
                    </tr>
                    <tr style="display:none;">
                        <td colspan="6">
                            <p><b>Problem</b></p>
                            <div class="problem">
                                <?php echo isset($ticket_info['problem']) ? closetags($ticket_info['problem']) : ''; ?>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3>Ticket History</h3>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Reply Date & Time</th>
                        <th>Allocated To</th>
                        <th>Comment By</th>
                        <th>Status</th>
                        <th>Reply</th>
                        <th>Document</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($ticket_history_info)) {
                        foreach ($ticket_history_info as $key => $value) {
                            ?>
                            <tr>
                                <td>
                                    <?php echo isset($value['reply_date']) ? date('d-m-Y', strtotime($value['reply_date'])) . ' ' : ''; ?>
                                    <?php echo isset($value['reply_time']) ? date('H:i A', strtotime($value['reply_time'])) : ''; ?>
                                </td>
                                <td><?php echo isset($value['full_name']) ? ucwords($value['full_name']) : ''; ?></td>
                                <td><?php echo isset($value['allocated_by_full_name']) ? ucwords($value['allocated_by_full_name']) : ''; ?></td>
                                <td><?php echo isset($value['status_name']) ? $value['status_name'] : ''; ?></td>
                                <td><a href="javascript:void(0);" class="view-problem">View</a></td>
                                <td>
                                    <?php
                                    if ($value['ticket_document'] == '') {
                                        echo 'Not available';
                                    } else {
                                        $path = base_url().'uploads/ticket-document/'.$value['ticket_document'];
                                        echo '<a href="'.$path.'" target="_blank" download>Download & View</a>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr style="display:none;">
                                <td colspan="6">
                                    <p><b>Reply</b></p>
                                    <div class="problem">                                        
                                        <?php echo isset($value['ticket_reply']) ? closetags($value['ticket_reply']) : ''; ?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        echo '<tr><td colspan="5" align="center">Records not available</td></tr>';
                    }
                    ?>                    
                </tbody>
            </table>
        </div>
    </div>
</div>