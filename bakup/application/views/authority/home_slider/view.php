<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/css/footable.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/all.css">

<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Slider images</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Slider images</li>
		</ol>
	</section>

	<!-- <section class="content-header">
        <div class="form-group">
            <div class="input-group">
                 <span class="input-group-addon">Search</span>
                 <input type="text" name="search_text" id="search_text" placeholder="Type to Search" class="form-control"  />
            </div>
        </div>
    </section> -->
	
    <section class="content">
    	<?php $this->load->view('authority/common/messages');?>
        <!-- /.row -->
		<div class="clearfix"></div>
        <div class="row">
        	<form method="post">
	            <div class="col-xs-12">
	            	<?php
						if ($data !=null) {
							?>
								<input type="submit" class="btn btn-md btn-danger chk_submit" value="Delete" formaction="<?= base_url('authority/home_slider/multiple_delete')?>">
							<?php
						}
					?>
	                <a href="<?= base_url('authority/home_slider/add')?>" class="btn btn-md btn-primary">Add</a> 
	                <div class="box">	                    
	                    <div class="box-body table-responsive no-padding">
	                        <table id="myTable" class="table table-bordred table-striped">
	                            <thead>
	                                <tr>
	                                	<th>
											<div class="checkbox" style="margin: -20px 0 0 30px;"><input type="checkbox" name="check_all" id="select_all"></div>
										</th>
	                                    <th>No</th>
	                                    <!-- <th>Title</th>
										<th>Description</th> -->
	                                    
	                                    <!-- <th>Slider link</th> -->
	                                    <th>Image</th>
	                                    <th>Slider position</th>
										<th>Add modified date</th>
										<th data-hide="phone,medium">Status<br/><small>(Click to change status)</small></th>
	                                    <th data-hide="phone,medium" align="center">Action</th>
									</tr>
								</thead>
	                            <tbody>
	                                <?php
										if ($data !=null):
		                                    foreach ($data  as $key => $value) {
		                                    	$id = $value['id'];
											?>
												<tr data-expanded="true">
													<td>
														<div class="checkbox" style="margin-left: 30px"><input type="checkbox" name="chk_multi_checkbox[]" class="chk_all" value="<?= $id?>"><div class="checkbox">
													</td>
													<td><?= $no+$key;?></td>
													<!-- <td><?= $value['title'];?></td>
													<td><?= substr($value['discription'],0,20);?></td> -->

													

													<!-- <td><a href="<?= $value['slider_link']?>"><?= $value['slider_link']?></a></td> -->

													<td>
														<img src="<?= base_url(HOME_SLIDER).$value['slider_image']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="110px">
													</td>
													<td align="center"><?= $value['slider_position']?></td>
													<td>
														<b>Added Date :</b> <?= date('d-m-Y h:i:s A',strtotime($value['create_date']));?><br>
														<b>Modified Date :</b> <?= date('d-m-Y h:i:s A',strtotime($value['modified_date']));?>
													</td>
													<td align="center">
														<?php 
															if($value['status'] == '1'){
																echo '<span class="label label-success change-status" data-table="home_slider" data-id="'.$id.'" data-current-status="1">Active</span>';
																} else {
																echo '<span class="label label-danger change-status" data-table="home_slider" data-id="'.$id.'" data-current-status="0">Deactive</span>';
															} 
														?>
													</td>

													<td class="action" align="center">
														<p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo site_url(); ?>authority/home_slider/edit/<?php echo $id; ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a></p>
														
														<p data-placement="top" data-toggle="tooltip" title="Delete"><button type="button" class="btn btn-danger btn-xs delete-btn" data-href="<?php echo site_url(); ?>authority/home_slider/delete/<?php echo $id; ?>" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p>
													</td>
												</tr>
											<?php
										}
									?>
									<?php else: ?>
									<tr data-expanded="true">
										<td colspan="10" align="center">Records not found</td>
									</tr>
									<?php endif; ?>
								</tbody>
							</table>
						</div>
						<div class="row">
	                        <div class="col-md-12" style="padding: 0px 30px 0px 0;">
	                            <ul class="pagination pull-right">
	                                <?php
	                                    if(isset($links)){
					                        echo $links;    
					                    }
	                                ?>
	                            </ul>
	                        </div>
	                    </div>

						<?php /* if (isset($links) && $links != "") { ?>
							<div class="box-footer clearfix">
								<?php echo $links; ?>
							</div>
							<?php
							} */
						?>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
			</form>
		</div>
	</section>
</div>

<!-- DELETE POPUP -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
				<h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" value="" name="delete_link" id="delete_link"/>
				<div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
				
			</div>
			<div class="modal-footer ">
				<button type="button" class="btn btn-success btn-confirm-yes" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
				<button type="button" class="btn btn-default btn-confirm-no" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
			</div>
		</div>
		<!-- /.modal-content --> 
	</div>
	<!-- /.modal-dialog --> 
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/js/footable.min.js"></script>

<script type="text/javascript">
	$(document).ready(function () {	
		
		$(document).on('click','.change-status',function(){
			var current_element = jQuery(this);
			var id = jQuery(this).data('id');
			var table = jQuery(this).data('table');
			var current_status = jQuery(this).attr('data-current-status');
			// alert(current_status);

			var post_data = {
				'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
				'action': 'change_status',
				'id': id,
				'table': table,
				'current_status': current_status,
			}
			$.ajax({
				type: "POST",
				url: BASE_URL + 'authority/ajax/change_slider_status',
				data: post_data,
				async: false,
				success: function (response) {
					var response = JSON.parse(response);
					if (response.success) {
						current_element.toggleClass('label-danger label-success');
						if(current_element.hasClass('label-success')){
							current_element.text('Active');
							current_element.attr('data-current-status','1');
							} else {
							current_element.text('Deactive');
							current_element.attr('data-current-status','0');
						}
						} else {
						window.location = window.location.href;
					}
				}
			});
		});

		$(document).on("click", ".delete-btn", function () {
			$("#delete_link").val($(this).data("href"));
		});
		
		$(".btn-confirm-yes").on("click", function () {
			window.location = $("#delete_link").val();
		});
	});

    // multiple delete //
    $('.chk_submit').on('click', function() {
		var boxes = $('.chk_all:checkbox');
        if(boxes.length > 0) {
            if($('.chk_all:checkbox:checked').length < 1) {
                $.alert({
			        title: 'Confirm Delete',
			        content: 'Please select at least one checkbox',
			    });
                return false;
            }
            else{
        		confirm('Are you sure you want to delete this item?');
	        	return true;
            }
        }
	});
</script>
<?php $this->view('authority/common/footer'); ?>									