<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/css/footable.bootstrap.min.css">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/all.css">
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Post Report</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Post Report</li>
        </ol>
    </section>
    <section class="content-header">
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon">Search</span>
                <input type="text" name="search_text" id="search_text" placeholder="Type to Search" class="form-control"  />
            </div>
        </div>
    </section>

    <section class="content-header">
        <div class="box box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">SEARCH</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="filter-section">
                    <?php
                    // echo '<pre>'; print_r($this->session->post_filter);exit;
                    $form_data = array(
                        'name' => 'form',
                        'id' => 'form',
                        'method' => 'POST',
                    );
                    echo form_open('', $form_data);
                    ?>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Category</label>
                                <?php
                                    $options = array(
                                        '' => 'Select',
                                    );
                                    $category_details = $this->Production_model->get_all_with_where('category', 'category_id', 'asc', array('status' => '1'));
                                    if (isset($category_details) && !empty($category_details)) {
                                        foreach ($category_details as $key => $value) {
                                            $options[$value['category_id']] = ucwords($value['category_name']);
                                        }
                                    }
                                    $selected = isset($this->session->post_filter['category_id']) ? $this->session->post_filter['category_id'] : '';
                                    echo form_dropdown('category_id', $options, $selected, ' class="form-control" id="category_id"');
                                ?>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Sub Category</label>
                                <select id="sub_category_id" name="sub_category_id" class='form-control'>
                                    <option value="">Select</option>
                                </select>
                                <?php
                                    // $options = array(
                                    //     '' => 'Select',
                                    // );
                                    // if (isset($search_status) && !empty($search_status)) {
                                    //     foreach ($search_status as $key => $value) {
                                    //         $options[$key] = ucwords($value);
                                    //     }
                                    // }
                                    // $selected = isset($this->session->post_filter['sub_category_id']) ? $this->session->post_filter['sub_category_id'] : '';
                                    // echo form_dropdown('sub_category_id', $options, $selected, ' class="form-control" id="sub_category_id"');
                                ?>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Location</label>
                                <select name="city_id" class="form-control city_id">
                                    <option value="">Select Location</option>
                                    <?php
                                        $city = $this->Production_model->get_all_with_where('city','','',array('id_country'=>101));
                                            if (isset($city) && $city != null) {
                                                foreach ($city as $key => $value) {
                                                ?>
                                                    <option value="<?= $value['id']?>" <?= isset($this->session->post_filter['city_id']) && $this->session->post_filter['city_id'] == $value['id'] ? 'selected' : '';?>><?= $value['city_name']?></option>
                                                <?php }
                                            }
                                        ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="filter_by_day">Start date</label>
                                <input class="form-control date" name="start_date" placeholder="Start date" type="text" value="<?php echo isset($this->session->post_filter['start_date']) ? $this->session->post_filter['start_date'] : ''; ?>" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="filter_by_day">End date</label>
                                <input class="form-control date" name="end_date" placeholder="End date" type="text" value="<?php echo isset($this->session->post_filter['end_date']) ? $this->session->post_filter['end_date'] : ''; ?>" readonly>
                            </div>
                        </div>                             
                    </div>
                    <div class="row">
                        <div class="col-md-1">
                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <button type="submit" class="btn btn-primary btn-sm search" name="submit" value="submit">SEARCH</button>
                        </div>
                        <div class="col-md-2">
                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><br>
                            <button type="button" onclick="window.location = window.location.href + '?clear_filter=1';" class="btn btn-primary btn-sm">CLEAR SEARCH</button>
                        </div>
                        <div class="col-md-2">
                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <button type="submit" class="btn btn-success form-control btn-sm" formaction="<?= base_url('authority/report/add-posted/excel_genrate_report')?>"><?= strtoupper('Export Excel')?></button>
                        </div>
                    </div>

                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <?php $this->load->view('authority/common/messages');?>
        <!-- /.row -->
        <div class="row">
            <form method="post">
                <div class="col-xs-12">
                    <?php
                        if (isset($data) && $data !=null) {
                            ?>
                                <input type="submit" class="btn btn-md btn-danger chk_submit" value="Delete" formaction="<?= base_url('authority/post-management/multiple_delete')?>">
                            <?php
                        }
                    ?>
                    <!-- <a href="<?= base_url('authority/post-management/add')?>" class="btn btn-md btn-primary">Add</a>  -->

                    <div class="box">
                        <div class="box-body table-responsive no-padding">
                            <table id="myTable" class="table table-bordred table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            <div class="checkbox" style="margin: -20px 0 0 30px;"><input type="checkbox" name="check_all" id="select_all"></div>
                                        </th>
                                        <th>No</th>
                                        <th>Post Details</th>
                                        <th>Add Modified Date</th>
                                        <th>Post Visiter</th>
                                        <th>Image</th>
                                        <th>Approve/dis-approve</th>
                                        <th data-hide="phone,medium">Status<br/><small>(Click to change status)</small></th>
                                        <th data-hide="phone,medium" align="center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($row_count > 0):
                                        foreach ($data as $key=> $value) {
                                            $id = $value['post_id'];
                                            ?>
                                            <tr data-expanded="true">
                                                <td>
                                                    <div class="checkbox" style="margin-left: 30px"><input type="checkbox" name="chk_multi_checkbox[]" class="chk_all" value="<?= $id?>"><div class="checkbox">
                                                </td>
                                                <td><?= $no+$key;?></td>
                                                <td>
                                                    <?php 
                                                        $tmp = $this->Production_model->get_where_user('name,user_email','user_register','','',array('user_id'=>$value['user_id']));
                                                        
                                                        $tmp1 = $this->Production_model->get_all_with_where('category','','',array('category_id'=>$value['category_id']));

                                                        $tmp2 = $this->Production_model->get_all_with_where('sub_category','','',array('sub_category_id'=>$value['sub_category_id']));

                                                    ?>
                                                    <b>Category name :</b> <?= $tmp1 !=null ? $tmp1[0]['category_name'] : '';?><br>
                                                    <?php
                                                        if ($tmp2 !=null) {
                                                        ?>
                                                            <b>Sub Category name :</b> <?= $tmp2 !=null ? $tmp2[0]['sub_category_name'] : '';?><br>
                                                        <?php }
                                                    ?>

                                                    <b>Add type :</b> <?= $value['add_type'];?><br>
                                                    <b>I Want :</b> <?= $value['i_want'];?><br>
                                                    <b>Price :</b> <?= '$'.$value['price'];?><br>
                                                    <b>User name :</b> <?= $tmp !=null ? $tmp[0]['name'] : '';?><br>
                                                    <b>User email :</b> <?= $tmp !=null ? $tmp[0]['user_email'] : '';?><br>
                                                    <b>Post description :</b> <?= substr($value['post_description'], 0,100);?><br>

                                                    <b>Package name :</b> <?= $value['package_name'];?><br>
                                                    <b>Day :</b> <?= $value['day'];?><br>
                                                    <b>Package price :</b> 
                                                    <?php
                                                        if ($value['currency'] == 'INR') {
                                                            echo 'Rs '.$value['package_price'];
                                                        }
                                                        elseif ($value['currency'] == 'USD') {
                                                            echo '$ '.$value['package_price'];
                                                        }
                                                        elseif ($value['currency'] == 'EUR') {
                                                            echo '€ '.$value['package_price'];
                                                        }
                                                        elseif ($value['currency'] == 'GBP') {
                                                            echo '£ '.$value['package_price'];
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <b>Added Date :</b> <?= date('d-m-Y h:i:s A',strtotime($value['create_date']));?><br>
                                                    <b>Modified Date :</b> <?= ($value['modified_date'] != '0000-00-00 00:00:00') ? date('d-m-Y h:i:s A',strtotime($value['modified_date'])) : '';?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        $visitor_count = $this->Production_model->get_where_user('post_id','post_visitor','','',array('post_id'=>$id));
                                                        ?>
                                                            <span class="label label-warning"><?= count($visitor_count);?></span>
                                                        <?php
                                                    ?>
                                                </td>

                                                <!-- <td style="background-color: #1E88E5">
                                                    <img src="<?= base_url(CAT_ICON.'thumbnail/').$value['cat_icon']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">
                                                </td> -->

                                                <td>
                                                    <img src="<?= base_url(POST_IMAGE.'thumbnail/').$value['post_image']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">
                                                </td>
                                                <td>
                                                    <?php 
                                                        if($value['post_approve_reject'] == '1'){
                                                            echo '<span class="label label-primary change-post-status" data-table="post_management" data-id="'.$id.'" data-post-status="0" data-toggle="modal" data-current="#myModal" data-approve-reject="Reject" data-user-email="'.$tmp[0]['user_email'].'">'.'Approve'.'</span>';
                                                            } else {
                                                            echo '<span class="label label-warning change-post-status" data-table="post_management" data-id="'.$id.'" data-post-status="1" data-toggle="modal" data-current="#myModal" data-approve-reject="Approve" data-user-email="'.$tmp[0]['user_email'].'">'.'Reject'.'</span>';
                                                        } 
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        if($value['status'] == '1'){
                                                            echo '<span class="label label-success change-status" data-table="post_management" data-id="'.$id.'" data-current-status="1">'.'Active'.'</span>';
                                                            } else {
                                                            echo '<span class="label label-danger change-status" data-table="post_management" data-id="'.$id.'" data-current-status="0">'.'Deactive'.'</span>';
                                                        } 
                                                    ?>
                                                </td>

                                                <td align="center" class="action">
                                                    <p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo site_url(); ?>authority/post-management/edit/<?php echo $id; ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a></p>
                                                   
                                                    <p data-placement="top" data-toggle="tooltip" title="Delete"><button type="button" class="btn btn-danger btn-xs delete-btn" data-href="<?php echo site_url(); ?>authority/post-management/delete_category/<?php echo $id; ?>" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    <?php else: ?>
                                        <tr data-expanded="true">
                                            <td colspan="5" align="center">Records not found</td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <?php 
                            if (isset($pagination) && $pagination != "") { ?>
                                <div class="box-footer clearfix">
                                    <?php echo $pagination; ?>
                                </div>
                                <?php
                            }
                        ?>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </form>
        </div>
    </section>
</div>

<!-- DELETE POPUP -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" name="delete_link" id="delete_link"/>
                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>

            </div>
            <div class="modal-footer ">
                <button type="button" class="btn btn-success btn-confirm-yes" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                <button type="button" class="btn btn-default btn-confirm-no" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>

<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/js/footable.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", ".delete-btn", function () {
            $("#delete_link").val($(this).data("href"));
        });

        $(".btn-confirm-yes").on("click", function () {
            window.location = $("#delete_link").val();
        });
        get_sub_category();
    });
    
    $("#category_id").change(function () {
        get_sub_category();        
    });

    function get_sub_category(){
        var cat_id = $('#category_id').val();
        // alert(cat_id);
        $.ajax({
            url: "<?= base_url('authority/report/add_posted/get_sub_cat_name') ?>",
            data: {category_id: cat_id},
            type: "POST",
            dataType: "html",
            success: function (data) {
                // alert(data);
                $("#sub_category_id").html(data);
            }
        });
    }

    // multiple delete //
    $('.chk_submit').on('click', function() {
        var boxes = $('.chk_all:checkbox');
        if(boxes.length > 0) {
            if($('.chk_all:checkbox:checked').length < 1) {
                $.alert({
                    title: 'Confirm Delete',
                    content: 'Please select at least one checkbox',
                });
                return false;
            }
            else{
                confirm('Are you sure you want to delete this item?');
                return true;
            }
        }
    });
</script>
<?php $this->view('authority/common/footer'); ?>