<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/css/jquery-ui-timepicker-addon.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/css/select2.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home </a></li>
            <li><a href="<?php echo site_url() . "authority/social-links/view"; ?>">Social-links </a></li>
            <!-- <li class="active">Add</li> -->
        </ol>
    </section>

    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <?php $action = ($social_links == null) ? base_url('authority/social-links/add_links') : base_url('authority/social-links/update_links')?>

                    <form id="form" action="<?= $action ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id" id="social_links_id" value="<?= ($social_links != null) ? $social_links[0]['id'] : '';?>">

                        <div class="box-header with-border">
                            <h3 class="box-title"><?= $social_links == null ? 'Add' :'Edit'?> Social-links</h3>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <?php $this->load->view('authority/common/messages')?>
                            <div class="form-group">
                                <label for="facebook_link">Facebook-link:</label>
                                <input type="text" name="facebook_link" class="form-control facebook_link" placeholder="Facebook-link" value="<?= ($social_links != null) ? $social_links[0]['facebook_link'] : '';?>">
                            </div>
                            
                            <div class="form-group">
                                <label for="instagram_link">Instagram-link:</label>
                                <input type="text" name="instagram_link" class="form-control instagram_link" placeholder="Instagram-link" value="<?= ($social_links != null) ? $social_links[0]['instagram_link'] : '';?>">
                                <span class="error_title" style="color: #fc3a3a;"></span>
                            </div>

                            <div class="form-group">
                                <label for="twiter_link">Twiter-link:</label>
                                <input type="text" name="twiter_link" class="form-control twiter_link" placeholder="Twiter-link" value="<?= ($social_links != null) ? $social_links[0]['twiter_link'] : '';?>">
                                <span class="error_title" style="color: #fc3a3a;"></span>
                            </div>

                            <div class="form-group">
                                <label for="link_in_link">Skype:</label>
                                <input type="text" name="link_in_link" class="form-control link_in_link" placeholder="Skype" value="<?= ($social_links != null) ? $social_links[0]['skype_link'] : '';?>">
                                <span class="error_title" style="color: #fc3a3a;"></span>
                            </div>

                            <div class="form-group">
                                <label for="pinterest_link">Pinterest-link:</label>
                                <input type="text" name="pinterest_link" class="form-control pinterest_link" placeholder="Pinterest-link" value="<?= ($social_links != null) ? $social_links[0]['pinterest_link'] : '';?>">
                                <span class="error_title" style="color: #fc3a3a;"></span>
                            </div>

                            <div class="form-group">
                                <label for="youtube_link">Youtube-link:</label>
                                <input type="text" name="youtube_link" class="form-control youtube_link" placeholder="Youtube-link" value="<?= ($social_links != null) ? $social_links[0]['youtube_link'] : '';?>">
                                <span class="error_title" style="color: #fc3a3a;"></span>
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-success text-uppercase check" value="Submit">
                                <a href="<?= base_url('authority/social-links/view')?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery.ui.touch-punch.min.js"></script>

<script>
    $(document).ready(function(){
        /*FORM VALIDATION*/
        // $("#form").validate({
        //  rules: {
        //      user_name: "required",            
        //      last_name: "required",
        //      email_phone: 'required',
        //  },
        //  messages: {
        //      user_name: "Please enter first name",
        //      last_name: "Please enter last name",
        //      email_phone: 'Please enter email address or phone number',
        //  }
        // });
        
        $('.check').click(function(){
            var social_links_id = $("#social_links_id").val();

            var facebook_link = $(".facebook_link").val();
            var google_plus_link = $(".google_plus_link").val();
            var twiter_link = $(".twiter_link").val();
            var link_in_link = $(".link_in_link").val();
            var insta_link = $(".insta_link").val();
            
            
            if (facebook_link =='' && google_plus_link ==''){
                $('.error_title').text('Please enter english OR arabic title.');
                $('.google_plus_link').focus();
                return false;
            }
            // if ((dis_english =='<br>' || dis_english =='') || dis_arabic =='<br>' || dis_arabic ==''){
            //     $('.error_dis').text('Please enter english OR arabic discription.');
            //     $('.discription_arabic').focus();
            //     return false;
            // }
            // if (social_links_id == ''){
            //     if(about_image === 0){
            //         $('.error_file').text("Please select image.");
            //         return false;
            //     }
            // }
        });
    })

</script>
<?php $this->view('authority/common/footer'); ?>