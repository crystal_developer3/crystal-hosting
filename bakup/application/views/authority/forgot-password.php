<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo SITE_TITLE; ?></title>
        <link rel="icon" href="<?php echo base_url(); ?>assets/admin/images/favicon.png" />
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/admin/images/favicon.ico" />
        <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/admin/images/favicon.png" />
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/dist/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/square/blue.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-black-light login-page">
        <div class="login-box">
            <div class="login-box-body">
                <div class="login-logo">
                    <img src="<?php echo site_url(); ?>assets/admin/images/logo.png" alt="logo"/>
                </div>
                <h3 align="center" class='text-info'>
                    Forgot Password
                </h3>
                <h5 align="center">
                    To reset your password, enter your e-mail address and press reset password. You will receive an e-mail shortly with a new password.
                </h5>
                <br/>
                <?php
                if (isset($success) && $success != "") {
                    echo "<h5 class='text-success'>" . $success . "</h5>";
                }
                ?>
                <form action="" method="POST" id="form">
                    <div class="form-group has-feedback">
                        <input type="email_address" name="email_address" class="form-control" placeholder="Email Address" value="<?php echo (isset($email_address) && $email_address != "") ? $email_address : ""; ?>">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        <?php echo form_error('email_address', '<div class="error">', '</div>'); ?>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block">RESET PASSWORD</button>
                        </div>
                        <div class="col-xs-12">
                            <div class="checkbox icheck pull-right">
                                <a href="<?php echo base_url(); ?>authority" title="login" class="">LOGIN</a>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <!-- /.social-auth-links -->

                <!--        <a href="#">I forgot my password</a><br>-->
                <!-- <a href="register.html" class="text-center">Register a new membership</a> -->

            </div>
            <!-- /.login-box-body -->
        </div>

        <!-- jQuery 3 -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/iCheck/icheck.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });

            /*FORM VALIDATION*/
            $("#form").validate({
                rules: {
                    email_address: {required: true},
                    password: {required: true},
                },
                messages: {
                    email_address: "Please enter email address",
                    password: {required: "Please enter password"},
                }
            });
        </script>
    </body>
</html>
