<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Category Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Category Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url() . "authority/sub_of_subcat/view"; ?>"> Sub sub category </a></li>
        </ol>
    </section>
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $sub_of_subcat_details !=null ? 'Edit' : 'Add'?> Sub sub category</h3>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php $this->load->view('authority/common/messages');?>
                        <?php $action = $sub_of_subcat_details !=null ? base_url('authority/sub_of_subcat/update_sub_of_subcat') : base_url('authority/sub_of_subcat/insert_sub_of_subcat'); ?>

                        <form id="form" method="post" action="<?= $action?>">
                            <input type="hidden" name="sub_of_subcat_id" value="<?= ($sub_of_subcat_details !=null) ? $sub_of_subcat_details[0]['sub_of_subcat_id'] : ""; ?>">

                            <!-- ajax call to get subcat. list -->
                            <input type="hidden" id="sub_cat_id" value="<?= ($sub_of_subcat_details !=null) ? $sub_of_subcat_details[0]['sub_category_id'] : '';?>">
                            <!-- end -->
                            
                            <div class="form-group">
                                <div class="col-md-10">
                                    <label for="category_name">Category Name:<span class="required">*</span></label>
                                    <select class="form-control category_id" name="category_id" id="category_id">
                                        <option value="">Select category</option>
                                        <?php
                                            if ($category_details !=null) {
                                                foreach ($category_details as $key => $value) {
                                                    ?>
                                                        <option value="<?= $value['category_id']?>" 
                                                        <?php 
                                                            if ($sub_of_subcat_details !=null) {
                                                                if ($value['category_id'] == $sub_of_subcat_details[0]['category_id']) {
                                                                    echo"selected";
                                                                }
                                                            }
                                                       ?>>
                                                       <?= $value['category_name'];?>
                                                       </option>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                    <span class="error_cat_name" style="color: #fc3a3a;"></span>
                                    <?php echo form_error("category_name", "<div class='error'>", "</div>"); ?>
                                </div>
                            </div><br><br><br><br>

                            <div class="form-group">
                                <div class="col-md-10">
                                    <label for="category_name">Sub Category Nname:<span class="required">*</span></label>
                                    <select class="form-control sub_category_id" name="sub_category_id" id="sub_category_id">
                                        <option value="">Select subcategory</option>
                                    </select>
                                    <span class="error_sub_cat_name" style="color: #fc3a3a;"></span>

                                    <?php echo form_error("sub_category_name", "<div class='error'>", "</div>"); ?>
                                </div>
                            </div><br><br><br>

                            <div class="form-group">
                                <div class="clone-section-main">   
                                    <div class="clone-section-sub">                                 
                                        <div class="col-md-10">
                                            <label for="category_name">Sub sub category:<span class="required">*</span></label>

                                            <input class="form-control sub_of_subcat_name" name="sub_of_subcat_name[]" placeholder="Enter Sub Of Sub Category Name" type="text" value="<?= ($sub_of_subcat_details !=null) ? $sub_of_subcat_details[0]['sub_of_subcat_name'] : ""; ?>">
                                            <?php echo form_error("sub_of_subcat_name", "<div class='error'>", "</div>"); ?><br>                                            
                                            <span class="error_subsub_cat_name" style="color: #fc3a3a;"></span>
                                        </div>

                                        <div class="col-md-2">
                                            <?php
                                                if ($sub_of_subcat_details == null) {
                                                    ?>                                                        
                                                        <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                        <button type="button" class="btn newitem add-more pull-left" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                        
                                                        <button type="button" class="btn remove-more btn-danger pull-right" title="Delete"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                    <?php 
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" style="margin-top: 2%;">
                                <div class="col-md-12">
                                    <input class="btn btn-success text-uppercase check" value="Submit" type="submit">
                                    <a href="<?php echo site_url() . 'authority/sub_of_subcat'; ?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
<script>
    /*FORM VALIDATION*/
    $("#form").validate({
        rules: {
            category_id : {required: true},
            sub_category_id : {required: true},
            'sub_of_subcat_name[]' : {required: true},
        },
        messages: {
            category_id:  {required: 'Please select category'},
            sub_category_id:  {required: 'Please select sub-category'},
            'sub_of_subcat_name[]' : {required: 'Please enter subcategory name'},
        }
    });

    // $('.check').click(function(){
    //     var cat_name = $(".category_id").val();
    //     var sub_cat_name = $(".sub_category_id").val();
    //     var cat_name = $(".sub_of_subcat_name").val();

    //     if (cat_name ==''){
    //         $('.error_cat_name').text('Please select category-name.');
    //         $('.category_id').focus();
    //         return false;
    //     }

    //     if (sub_cat_name ==''){
    //         $('.error_sub_cat_name').text('Please select sub-category-name.');
    //         $('.sub_category_id').focus();
    //         return false;
    //     }

    //     if (cat_name ==''){
    //         $('.error_subsub_cat_name').text('Please enter sub-sub-category-name.');
    //         $('.sub_of_subcat_name').focus();
    //         return false;
    //     } 
    // });

    $(document).on('click','.newitem',function(){
        $clone = $('.clone-section-sub:last').clone();
        $('.clone-section-main').append($clone);
        $clone.find('input[type="text"]').val('');
    });

    $(document).on('click', '.remove-more', function () {
        if (check_clone_lang_section()) {
            $(this).closest('.clone-section-sub').remove();
        } else {
            alert('You can not remove the current section');
        }
    });

    function check_clone_lang_section() {
        if ($('.clone-section-main').find('.clone-section-sub').length > 1) {
            return true;
        } else {
            return false;
        }
    }

    $(document).ready(function() {
        get_sub_category();
    });

    function get_sub_category(){
        var cat_id = $('#category_id').val();
        var sub_cat_id = $('#sub_cat_id').val();
        $.ajax({
            url: "<?= base_url('authority/sub_of_subcat/get_sub_cat_name')?>",
            data: {category_id:cat_id,sub_cat_id:sub_cat_id},
            type: "POST",
            dataType: "html",
            success: function (data) {
                // alert(data);
                $("#sub_category_id").html(data); 
            }
        });
    }
    $("#category_id").change(function(){
        var cat_id = $('#category_id').val();
        $.ajax({
            url: "<?= base_url('authority/sub_of_subcat/get_sub_cat_name')?>",
            data: {category_id:cat_id},
            type: "POST",
            dataType: "html",
            success: function (data) {
                // alert(data);
                $("#sub_category_id").html(data); 
            }
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>