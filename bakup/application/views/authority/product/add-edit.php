<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/css/jquery-ui-timepicker-addon.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/css/select2.min.css">
<style>
.form-control.error{border:1px solid red;}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home </a></li>
            <li><a href="<?php echo site_url() . "authority/product_manage"; ?>">Product </a></li>
            <!-- <li class="active">Add</li> -->
        </ol>
    </section>
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <?php $action = ($product_details == null) ? base_url('authority/product_manage/add_product') : base_url('authority/product_manage/update_product')?>
                    <form id="form" action="<?= $action ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="product_id" id="product_id" value="<?= ($product_details != null) ? $product_details[0]['product_id'] : '';?>">
                        <input type="hidden" name="sub_cat_id" class="sub_cat_id" value="<?= ($product_details != null) ? $product_details[0]['sub_category_id'] : '';?>">
                       
                        <div class="box-header with-border">
                            <h3 class="box-title"><?= $product_details == null ? 'add' : 'edit'?> Product</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <?php $this->load->view('authority/common/messages')?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="full_name">Category name:<span class="required">*</span></label>
                                    <select class="form-control category_id" name="category_id">
                                        <option value="">Select</option>
                                        <?php
                                            if ($category_details !=null) {
                                                foreach ($category_details as $key => $value) {
                                                    ?>
                                                        <option value="<?= $value['category_id']?>" 
                                                        <?php 
                                                            if ($product_details !=null) {
                                                                if ($value['category_id'] == $product_details[0]['category_id']) {
                                                                    echo"selected";
                                                                }
                                                            }
                                                       ?>>
                                                       <?= $value['category_name'];?>
                                                       </option>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                    <span class="error_cat_name" style="color: #fc3a3a; position: absolute;"></span>
                                </div>
                            </div>   
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="full_name">Sub category name:</label>
                                    <select class="form-control sub_category_id" name="sub_category_id">
                                        <option>Select</option>
                                    </select>
                                    <span class="error_subcat_name" style="color: #fc3a3a;position: absolute;"></span>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="product_name_english">Product name:<span class="required">*</span></label>
                                    <input type="text" name="product_name" class="form-control product_name" value="<?= $product_details !=null ? $product_details[0]['product_name'] : '';?>" placeholder="Product name">
                                    <span class="error_product_name" style="color: #fc3a3a;position: absolute;"></span>
                                </div>   
                            </div>
                           
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="product_image">Product image:<span class="required">*</span>(upload by 600&#215;800)</label>
                                    <input type="file" name="product_image" class="form-control product_image" accept="image/*">  
                                    <span class="error_file" style="color: #fc3a3a;position: absolute;"></span>
                                </div>   
                            </div>  
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="similar_
                                    image">Similar image:<span class="required">*</span>(upload by 600&#215;800)</label>
                                    <input type="file" name="similar_image[]" class="form-control similar_image" accept="image/*" multiple>  
                                    <span class="error_similar_file" style="color: #fc3a3a;position: absolute;"></span>
                                </div>   
                            </div> 
                            <div class="col-md-4">
                                <?php
                                    if ($product_details != null) {
                                    ?>
                                        <div class="form-group">
                                            <label for="last_name">Current image:</label><br>
                                            <img src="<?= base_url(PRODUCT_IMAGE.'thumbnail/thumb/').$product_details[0]['product_image']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">
                                        </div>
                                    <?php }
                                ?>      
                            </div>
                            <div class="col-md-8">
                                <?php
                                    if ($similar_img_details != null) {
                                    ?>
                                        <div class="form-group">
                                            <label for="last_name">Similar image:</label><br>
                                            <?php 
                                                foreach ($similar_img_details as $key => $value){
                                                    $img_id = $value['image_id'];
                                                ?>
                                                    <img src="<?= base_url(PRDCT_SIMILAR_IMG.'thumbnail/').$value['product_similar_image']?>" height="50px" width="50px">
                                                    <a href="<?= base_url('authority/product_manage/delete_image/'.$img_id)?>" title="Delete image" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash-o" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <?php }
                                            ?>                                                
                                        </div>
                                    <?php }
                                ?> 
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="last_name">Description:<span class="required">*</span></label>
                                    <textarea name="product_desc" id="editor1" class="form-control product_desc" rows="3" cols="80" placeholder="Description"><?= ($product_details != null) ? $product_details[0]['product_desc'] : '';?></textarea>
                                    <span class="error_dis" style="color: #fc3a3a;position: absolute;"></span>
                                </div>  
                            </div>
                            
                            <!--SEO slug-->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="meta_keywords">Meta keywords</label>
                                    <input type="text" name="meta_keywords" class="form-control meta_keywords" value="<?= $product_details !=null ? $product_details[0]['meta_keywords'] : '';?>" placeholder="Meta keywords">
                                </div> 
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="meta_description">Meta description</label>
                                    <input type="text" name="meta_description" class="form-control description" value="<?= $product_details !=null ? $product_details[0]['meta_description'] : '';?>" placeholder="Meta description">
                                </div> 
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="meta_title">Meta title</label>
                                    <input type="text" name="meta_title" class="form-control meta_title" value="<?= $product_details !=null ? $product_details[0]['meta_title'] : '';?>" placeholder="Meta title">
                                </div> 
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="slug_name">Slug name</label>
                                    <input type="text" name="slug_name" class="form-control slug_name" value="<?= $product_details !=null ? $product_details[0]['slug_name'] : '';?>" placeholder="Slug name">
                                </div> 
                            </div>

                            <?php
                                if ($product_details == null) {
                                ?>
                                    <div class="col-md-12 row">
                                        <!-- <div class="form-group col-md-3">
                                            <label class="checkbox-inline">
                                                <input name="new_arrivals_product" id="new_arrivals_product" value="0" type="checkbox"><b>New arrivals..?</b>
                                            </label>
                                        </div> -->
                                        <div class="form-group col-md-3">
                                            <label class="checkbox-inline">
                                                <input name="letest_product" id="letest_product" value="0" type="checkbox"><b>Latest product..?</b>
                                            </label>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="checkbox-inline">
                                                <input name="related_product" id="related_product" value="0" type="checkbox"><b>Related product..?</b>
                                            </label>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="checkbox-inline">
                                                <input name="best_seller" id="best_seller" value="0" type="checkbox"><b>Best seller..?</b>
                                            </label>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="checkbox-inline">
                                                <input name="populer_product" id="populer_product" value="0" type="checkbox"><b>Populer Product..?</b>
                                            </label>
                                        </div>
                                    </div>
                                <?php }
                            ?>
                            <div class="form-group col-md-12" style="margin-top: 2%">
                                <input type="submit" class="btn btn-success text-uppercase check" value="Submit">
                                <a href="<?= base_url('authority/product_manage')?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery.ui.touch-punch.min.js"></script>
<script>
    $(document).ready(function(){
        get_sub_category();

        $(".category_id").change(function(){
            var cat_id = $('.category_id').val();
            $.ajax({
                url: "<?= base_url('authority/product_manage/get_sub_cat_name')?>",
                data: {category_id:cat_id},
                type: "POST",
                dataType: "html",
                success: function (data) {
                    // alert(data);
                    $(".sub_category_id").html(data); 
                }
            });
        });
        function get_sub_category(){
            var cat_id = $('.category_id').val();
            var sub_cat_id = $('.sub_cat_id').val();
            $.ajax({
                url: "<?= base_url('authority/product_manage/get_sub_cat_name')?>",
                data: {category_id:cat_id,sub_cat_id:sub_cat_id},
                type: "POST",
                dataType: "html",
                success: function (data) {
                    // alert(data);
                    $(".sub_category_id").html(data); 
                }
            });
        }
        $(".sub_category_id").change(function(){
            var cat_id = $('.sub_category_id').val();
            $.ajax({
                url: "<?= base_url('authority/product_manage/get_subsub_cat_name')?>",
                data: {sub_cat_id:cat_id},
                type: "POST",
                dataType: "html",
                success: function (data) {
                    // alert(data);
                    $(".sub_of_subcat_id").html(data); 
                }
            });
        });

        $('.check').click(function(){
            var product_id = $("#product_id").val();
            var category_id = $(".category_id").val();
            var product_name = $(".product_name").val();
            var product_image = $(".product_image")[0].files.length;
            var similar_image = $(".similar_image")[0].files.length;
            var nicInstance = nicEditors.findEditor('editor1');       
            var messageContent = nicInstance.getContent();
            ;
            if (category_id ==''){
                $('.error_cat_name').text('Please select category.');
                $('.category_id').focus();
                return false;
            }
            if (product_name ==''){
                $('.error_product_name').text('Please enter product name.');
                $('.product_name').focus();
                return false;
            }
           
            if (product_id == ''){
                if(product_image === 0){
                    $('.error_file').text("Please select image.");
                    return false;
                }
                if(similar_image === 0){
                    $('.error_similar_file').text("Please select similar image.");
                    return false;
                }
            }
            if(messageContent=="<br>") { 
                $('.error_dis').text('Please enter product description.');
                return false;
            }
            // if($('.color_id:checkbox:checked').length == 0)
            // {
            //     $('.err_color_msg').text('Select one color.');
            //     return false;
            // }
        });
    });
</script>
<?php $this->view('authority/common/footer'); ?>