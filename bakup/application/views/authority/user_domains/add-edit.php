<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>

<?php $this->view('authority/common/sidebar'); ?>
<!-- User Domains Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- User Domains Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php echo site_url() . "authority/user_domains/view"; ?>">User Domains</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $user_domains_details !=null ? 'Edit' : 'Add'?> User Domains</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php $this->load->view('authority/common/messages');?>
                        <?php $action = $user_domains_details !=null ? base_url('authority/user_domains/update_invoice') : base_url('authority/user_domains/insert_invoice'); ?>

                        <form id="form" method="post" action="<?= $action?>" enctype="multipart/form-data">
                            <div class="clone-section-main">   
                                <div class="clone-section-sub">   
                                    <div class="form-group">
                                        <input type="hidden" name="id" id="user_domain_id" value="<?= ($user_domains_details !=null) ? $user_domains_details[0]['id'] : ""; ?>">
                                        <div class="col-md-3">
                                            <label for="domain_name">Domain Name :<span class="required">*</span></label> 
                                            <?php
                                                $input_fields = array(
                                                    'name' => 'domain_name',
                                                    'placeholder' => 'Domain Name',
                                                    'class' => 'form-control domain_name txtonly',
                                                    'id' => 'domain_name',
                                                    'value' => (isset($user_domains_details) && $user_domains_details !=null ? $user_domains_details[0]['domain_name'] : ""),
                                                );
                                                echo form_input($input_fields);
                                                echo form_error("domain_name", "<div class='error'>", "</div>");
                                            ?>
                                            <span class="error_domain_name" style="color: #fc3a3a;"></span>
                                            <div class="clearfix" ></div>
                                            <hr>
                                        </div>
                                        <div class="col-md-3">
                                            
                                                <label for="domain_type_id">Domain Type :<span class="required">*</span></label>
                                                <select class="form-control" id="domain_type_id" name="domain_type_id">
                                                    <option value="">Select</option>
                                                    <?php 
                                                        foreach ($domain_type_data['data'] as $key => $value1) { ?>
                                                            <option value="<?=$value1['id']?>" <?=(isset($user_domains_details[0]['domain_type_id']) && $value1['id']==$user_domains_details[0]['domain_type_id'])?'selected':''?>><?=$value1['title']?>
                                                                
                                                            </option>
                                                        <?php }
                                                    ?>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="book_date">Book Date :<span class="required">*</span></label> 
                                                <?php
                                                    $input_fields = array(
                                                        'name' => 'book_date',
                                                        'placeholder' => 'Book Date',
                                                        'class' => 'form-control date',
                                                        'id' => 'book_date',
                                                        'value' => (isset($user_domains_details) && $user_domains_details !=null ? $user_domains_details[0]['book_date'] : ""),
                                                    );
                                                    echo form_input($input_fields);
                                                    echo form_error("book_date", "<div class='error'>", "</div>");
                                                ?>
                                                <span class="error_title" style="color: #fc3a3a;"></span>
                                                <div class="clearfix" ></div>
                                                <hr>  
                                            </div>
                                            <!-- <div class="col-md-3">
                                                <label for="expiry_date">Due Date :<span class="required">*</span></label> 
                                                <?php
                                                    $input_fields = array(
                                                        'name' => 'expiry_date',
                                                        'placeholder' => 'Expiry Date',
                                                        'class' => 'form-control date',
                                                        'id' => 'expiry_date',
                                                        'value' => (isset($user_domains_details) && $user_domains_details !=null ? $user_domains_details[0]['expiry_date'] : ""),
                                                    );
                                                    echo form_input($input_fields);
                                                    echo form_error("expiry_date", "<div class='error'>", "</div>");
                                                ?>
                                                <span class="expiry_date" style="color: #fc3a3a;"></span>
                                                <div class="clearfix" ></div>
                                                <hr>  
                                            </div> -->
                                            <div class="clearfix" ></div>
                                            <div class="col-md-4">
                                                <label for="user_id">Select User :<span class="required">*</span></label>
                                                <?php 
                                                    if ($user_details !=null) {
                                                        $option[null] = 'Select'; 
                                                        $data = array(
                                                            'class' => 'form-control user_id',
                                                            'id' => 'user_id',
                                                        );
                                                        foreach ($user_details['data'] as $key => $value) {
                                                            $option[$value['id']] = $value['first_name']." ".$value['last_name'];
                                                        }
                                                    }
                                                    echo form_dropdown('user_id',$option,isset($user_domains_details[0]['user_id']) ? $user_domains_details[0]['user_id'] : '',$data);
                                                ?>
                                                <span class="error_user_id" style="color: #fc3a3a;"></span>
                                                <?php echo form_error("user_id", "<div class='error'>", "</div>"); ?>
                                                <hr>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="currency_id">Currency :<span class="required">*</span></label>
                                                <select class="form-control" id="currency_id" name="currency_id">
                                                    <option value="">Select</option>
                                                    <?php 
                                                        foreach ($currency_data['data'] as $key => $value1) { ?>
                                                            <option value="<?=$value1['id']?>" <?=(isset($user_domains_details[0]['currency_id']) && $value1['id']==$user_domains_details[0]['currency_id'])?'selected':''?>><?=$value1['currency_code']?> | <?=$value1['currency_name']?>
                                                                
                                                            </option>
                                                        <?php }
                                                    ?>
                                                </select>
                                                
                                            </div>
                                            <div class="col-md-2">
                                                <label for="price">Price :<span class="required">*</span></label>
                                                <input type="number" id="price" name="price" placeholder='Enter Unit Price' class="form-control price" value="<?=(isset($user_domains_details) && $user_domains_details !=null ? $user_domains_details[0]['price'] : "")?>">
                                            </div>

                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input class="btn btn-success text-uppercase check" value="Submit" type="submit">
                                    <a href="<?php echo site_url() . 'authority/feature'; ?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </section>
    </div>
    <?php $this->view('authority/common/copyright'); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
    <script>
        /*FORM VALIDATION*/
        
        var user_domain_id = $("#user_domain_id").val();
        $("#form").validate({
            rules: {
                'domain_name': {required: true},
                'book_date': {required: true},
                'domain_type_id':{required: true},
                // 'expiry_date': {required: true},
                'user_id':{required: true},
                'currency_id':{required: true},
                'price': {required: true},
            },
            messages: {
                'domain_name': "Please enter domain name",
                'book_date': "Please enter book date",
                'domain_type_id': "Please select domain type",
                'user_id': "Please select user",
                'currency_id': "Please select currency",
                // 'expiry_date': "Please enter expiry date",
                'price': "Please enter price",
            }

        });

        $('.check_excel').click(function(){
            if(isemptyfocus('xls_file'))
            {
                return false;
            }
        });

    </script>
</script>
    
<?php $this->view('authority/common/footer'); ?>