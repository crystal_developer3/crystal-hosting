<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Security Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Security Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php echo site_url() . "authority/security/view"; ?>">Security</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $security_details !=null ? 'Edit' : 'Add'?> Security</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php $this->load->view('authority/common/messages');?>
                        <?php $action = $security_details !=null ? base_url('authority/security/update_security') : base_url('authority/security/insert_security'); ?>

                        <form id="form" method="post" action="<?= $action?>" enctype="multipart/form-data">
                            <div class="clone-section-main">   
                                <div class="clone-section-sub">   
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <input type="hidden" name="id" id="plan_id" value="<?= ($security_details !=null) ? $security_details[0]['id'] : ""; ?>">
                                            <label for="title">Security Title :<span class="required">*</span></label> 
                                            <?php
                                                $input_fields = array(
                                                    'name' => 'title',
                                                    'placeholder' => 'Security Title',
                                                    'class' => 'form-control title txtonly',
                                                    'id' => 'title',
                                                    'value' => (isset($security_details) && $security_details !=null ? $security_details[0]['title'] : ""),
                                                );
                                                echo form_input($input_fields);
                                                echo form_error("title", "<div class='error'>", "</div>");
                                            ?>
                                            <span class="error_title" style="color: #fc3a3a;"></span>
                                            <div class="clearfix" ></div>
                                            <hr>
                                        </div>
                                        
                                        <div class="col-md-10">
                                            <div class="col-md-12"><label>Plan Features :</label></div>
                                            <?php
                                            if ($features_details !=null) {
                                                $id = (isset($security_details[0]['id']) && $security_details[0]['id'] !='')?$security_details[0]['id']:0;
                                                // echo "<pre>";print_r($features_details['data']);exit;
                                                foreach ($features_details['data'] as $key => $value) {
                                                    
                                                    $get_feature = $this->Production_model->get_all_with_where('security_related_feature','','',array('security_id'=>$id,'feature_id'=>$value['id']));
                                                    if(isset($get_feature) && $get_feature!=null){
                                                        // echo "<pre>";print_r($get_feature);
                                                        ?>
                                                        <div class="col-md-6">
                                                            <div class="checkbox">
                                                                <?php 
                                                                    if($value['id'] == $get_feature[0]['feature_id']){ 
                                                                        ?>
                                                                    
                                                                            <label class="change-status deactive" data-table="security_related_feature" data-id="<?=$get_feature[0]['id']?>" data-current-status="<?=$get_feature[0]['status']?>">
                                                                                <input name="feature_id[]" type="checkbox" value="<?= $value['id']?>" <?= ($value['id'] == $get_feature[0]['feature_id']  && $get_feature[0]['status'] == '1')  ?'checked' : '';?> > 
                                                                                <?= $value['title']?>
                                                                            </label>
                                                                    <?php 
                                                                    }else{ ?>
                                                                        <label>
                                                                            <input name="feature_id[]" type="checkbox" value="<?= $value['id']?>" <?= ($value['id'] == $get_feature[0]['feature_id'])  ?'checked' : '';?> > 
                                                                            <?= $value['title']?>
                                                                        </label>
                                                                    <?php 

                                                                    }

                                                                ?>
                                                                <!-- <label><input name="feature_id[]" type="checkbox" value="<?= $value['id']?>" <?= ($value['id'] == $get_feature[0]['feature_id'])  ?'checked' : '';?>
                                                                > 
                                                                    <?= $value['title']?>
                                                                </label> -->
                                                            </div>    
                                                        </div>
                                                        <?php      
                                                    }else{
                                                        ?>
                                                        <div class="col-md-6">
                                                            <div class="checkbox">
                                                                <label><input name="feature_id[]" type="checkbox" value="<?= $value['id']?>" > 
                                                                    <?= $value['title']?>
                                                                </label>
                                                            </div>    
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input class="btn btn-success text-uppercase check" value="Submit" type="submit">
                                    <a href="<?php echo site_url() . 'authority/feature'; ?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </section>
    </div>
    <?php $this->view('authority/common/copyright'); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
    <script>
        /*FORM VALIDATION*/
        $("#price").keyup(function(){
            var price = this.value = this.value.replace(/[^0-9]/g, '');
          $("#price").val(price);

        });
        var plan_id = $("#plan_id").val();
        $("#form").validate({
            rules: {
                'title': {required: true},
                'price': {required: true},
            },
            messages: {
                'title': "Please enter title",
                'price': "Please enter price",
            }
        });

        $('.check_excel').click(function(){
            if(isemptyfocus('xls_file'))
            {
                return false;
            }
        });

        $(document).on('click', '.add-more', function() {
            $clone = $('.clone-section-sub:last').clone();
            $('.clone-section-main').append($clone);
            $('.clone-section-sub:last').find('input[type="text"]').val('');
        });

        $(document).on('click', '.remove-more', function () {
            if (check_clone_lang_section()) {
                $(this).closest('.clone-section-sub').remove();
            } else {
                alert('You can not remove the current section');
            }
        });

        function check_clone_lang_section() {
            if ($('.clone-section-main').find('.clone-section-sub').length > 1) {
                return true;
            } else {
                return false;
            }
        } 
    </script>
    <script type="text/javascript">
        $(document).on('click','.change-status',function(){
            var current_element = jQuery(this);
            var id = jQuery(this).data('id');
            var table = jQuery(this).data('table');
            var current_status = jQuery(this).attr('data-current-status');
            // alert(current_status);

            var post_data = {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                'action': 'change_status',
                'id': id,
                'table': table,
                'current_status': current_status,
            }
            $.ajax({
                type: "POST",
                url: BASE_URL + 'authority/ajax/change_status',
                data: post_data,
                async: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.success) {
                        current_element.toggleClass('deactive active');
                        if(current_element.hasClass('active')){
                            current_element.attr('data-current-status','1');
                            } else {
                            current_element.attr('data-current-status','0');
                        }
                    } else {
                        window.location = window.location.href;
                    }
                }
            });
        });
    </script>
<?php $this->view('authority/common/footer'); ?>