<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/css/jquery-ui-timepicker-addon.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/css/select2.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home </a></li>
            <li><a href="<?php echo site_url() . "authority/about/view"; ?>">About </a></li>
            <!-- <li class="active">Add</li> -->
        </ol>
    </section>

    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <?php $action = ($about_data == null) ? base_url('authority/about/add_about') : base_url('authority/about/update_about')?>

                    <form id="form" action="<?= $action ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id" id="about_id" value="<?= ($about_data != null) ? $about_data[0]['id'] : '';?>">

                        <div class="box-header with-border">
                            <h3 class="box-title"><?= $about_data == null ? 'Add' : 'Edit'?> About</h3>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <?php $this->load->view('authority/common/messages')?>
                            <div class="form-group">
                                <label for="full_name">Title :<span class="required">*</span></label>
                                <input type="text" name="title" class="form-control title" placeholder="Title" value="<?= ($about_data != null) ? $about_data[0]['title'] : '';?>">
                                <span class="error_title" style="color: #fc3a3a;"></span>
                            </div>

                            <div class="form-group">
                                <label for="last_name">Description :<span class="required">*</span></label>
                                <textarea name="description" id="editor1" class="form-control description" placeholder="Description"><?= ($about_data != null) ? $about_data[0]['description'] : '';?></textarea>
                                <span class="error_desc" style="color: #fc3a3a;"></span>
                            </div>

    						<div class="form-group">
                                <label for="last_name">Image : (Upload by 260&#xd7;514)</label>
    							<input type="file" name="about_image" class="form-control about_image" accept="image/*">	
                                <span class="error_file" style="color: #fc3a3a;"></span>
                            </div>   

                            <?php
                                if ($about_data != null) {
                                ?>
                                    <div class="form-group">
                                        <label for="last_name">Current image:<span class="required">*</span></label><br>
                                        <img src="<?= base_url(ABOUT_IMAGE).$about_data[0]['about_image']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'"  height="50px" width="50px">
                                    </div>
                                <?php }
                            ?>

                            <div class="form-group">
                                <input type="submit" class="btn btn-success text-uppercase check" value="Submit">
                                <a href="<?= base_url('authority/about/view')?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery.ui.touch-punch.min.js"></script>

<script>
	$(document).ready(function(){

		/*FORM VALIDATION*/
		// $("#form").validate({
		// 	rules: {
		// 		user_name: "required",            
		// 		last_name: "required",
		// 		email_phone: 'required',
		// 	},
		// 	messages: {
		// 		user_name: "Please enter first name",
		// 		last_name: "Please enter last name",
		// 		email_phone: 'Please enter email address or phone number',
		// 	}
		// });
        
        $('.check').click(function(){
            var about_id = $("#about_id").val();

            var title = $(".title").val();
            var nicInstance = nicEditors.findEditor('editor1');
            var dis = nicInstance.getContent();
            // var about_image = $(".about_image")[0].files.length;

            if (title ==''){
                $('.error_title').text('Please enter title.');
                $('.title').focus();
                return false;
            }
            if (dis =="<br>"){
                $('.error_desc').text('Please enter description.');
                return false;
            }

            // if (about_id == ''){
            //     if(about_image === 0){
            //         $('.error_file').text("Please select image.");
            //         return false;
            //     }
            // }
        });
	})

</script>
<?php $this->view('authority/common/footer'); ?>