<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/css/jquery-ui-timepicker-addon.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/css/select2.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i><?= $this->lang->line('home');?> </a></li>
            <li><a href="<?php echo site_url() . "authority/seo_page/view"; ?>"><?= $this->lang->line('seo_page');?> </a></li>
            <!-- <li class="active">Add</li> -->
        </ol>
    </section>

    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <?php $action = ($meta_details == null) ? base_url('authority/seo_page/add_meta') : base_url('authority/seo_page/update_meta')?>

                    <form id="form" action="<?= $action ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id" id="blog_id" value="<?= ($meta_details != null) ? $meta_details[0]['id'] : '';?>">

                        <div class="box-header with-border">
                            <h3 class="box-title"><?= $meta_details == null ? $this->lang->line('add') : $this->lang->line('edit')?> <?= $this->lang->line('meta');?></h3>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <?php $this->load->view('authority/common/messages')?>

                            <div class="form-group col-md-6">
                                <label for="full_name"><?= $this->lang->line('page')?> :<span class="required">*</span></label>
                                <select class="form-control menu_id" name="menu_id">
                                    <option value="">Select</option>
                                    <?php
                                        if ($menu_details !=null) {
                                            foreach ($menu_details as $key => $value) {
                                            ?>
                                                <option value="<?= $value['menu_id']?>" <?= $meta_details !=null && $meta_details[0]['menu_id'] == $value['menu_id'] ? 'selected' : '';?>><?= $value['menu_name']?></option>
                                            <?php }
                                        }
                                    ?>
                                </select>
                                <span class="error_page" style="color: #fc3a3a;position: absolute;"></span>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="full_name"><?= $this->lang->line('meta_keywords')?>: <span class="required">*</span></label>
                                <input type="text" name="meta_keywords" class="form-control meta_keywords" placeholder="<?= $this->lang->line('meta_keywords')?>" value="<?= ($meta_details != null) ? $meta_details[0]['meta_keywords'] : '';?>">
                                <span class="error_meta_keywords" style="color: #fc3a3a;position: absolute;"></span>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="full_name"><?= $this->lang->line('meta_description')?>: <span class="required">*</span></label>
                                <input type="text" name="meta_description" class="form-control meta_description" placeholder="<?= $this->lang->line('meta_description')?>" value="<?= ($meta_details != null) ? $meta_details[0]['meta_description'] : '';?>">
                                <span class="error_meta_description" style="color: #fc3a3a;position: absolute;"></span>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="full_name"><?= $this->lang->line('meta_title')?>: <span class="required">*</span></label>
                                <input type="text" name="meta_title" class="form-control meta_title" placeholder="<?= $this->lang->line('meta_title')?>" value="<?= ($meta_details != null) ? $meta_details[0]['meta_title'] : '';?>">
                                <span class="error_meta_title" style="color: #fc3a3a;position: absolute;"></span>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="full_name"><?= $this->lang->line('slug_name')?>: <span class="required">*</span></label>
                                <input type="text" name="slug_name" class="form-control slug_name" placeholder="<?= $this->lang->line('slug_name')?>" value="<?= ($meta_details != null) ? $meta_details[0]['slug_name'] : '';?>">
                                <span class="error_slug_name" style="color: #fc3a3a;position: absolute;"></span>
                            </div> 

                            <div class="form-group col-md-6">
                                <label for="full_name">&nbsp;</label><br>
                                <input type="submit" class="btn btn-success text-uppercase check" value="<?= $this->lang->line('submit');?>">
                                <a href="<?= base_url('authority/seo_page/view')?>" class="btn btn-danger text-uppercase <?= LANGUAGE_TYPE == 2 ? 'pull-left' : 'pull-right'?>"><?= $this->lang->line('back');?></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery.ui.touch-punch.min.js"></script>

<script>
	$(document).ready(function(){
		/*FORM VALIDATION*/
		// $("#form").validate({
		// 	rules: {
		// 		user_name: "required",            
		// 		last_name: "required",
		// 		email_phone: 'required',
		// 	},
		// 	messages: {
		// 		user_name: "Please enter first name",
		// 		last_name: "Please enter last name",
		// 		email_phone: 'Please enter email address or phone number',
		// 	}
		// });
        
        $('.check').click(function(){

            var menu_id = $(".menu_id").val();
            var meta_keywords = $(".meta_keywords").val();
            var meta_description = $(".meta_description").val();
            var meta_title = $(".meta_title").val();
            var slug_name = $(".slug_name").val();

            if (menu_id ==''){
                $('.error_page').text('Please select page.');
                return false;
            }
            if(meta_keywords == '') { 
                $('.error_meta_keywords').text('Please enter meta keywords');
                $('.meta_keywords').focus();
                return false;
            }
            if(meta_description == '') { 
                $('.error_meta_description').text('Please enter meta description');
                $('.meta_description').focus();
                return false;
            }
            if(meta_title == '') { 
                $('.error_meta_title').text('Please enter meta title');
                $('.meta_title').focus();
                return false;
            }
            if(slug_name == '') { 
                $('.error_slug_name').text('Please enter slug name');
                $('.slug_name').focus();
                return false;
            }
        });
	})
</script>
<?php $this->view('authority/common/footer'); ?>