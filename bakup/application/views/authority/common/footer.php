	<script src="<?php echo base_url(); ?>assets/admin/dist/js/adminlte.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/validation/common.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>

 	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script> -->
 	

 	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/nicEdit-latest.js"></script>
	<script src="<?php echo base_url(); ?>assets/admin/js/jquery.counterup.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/admin/js/moment.min.js"></script>

	<script src="<?php echo base_url(); ?>assets/admin/js/fullcalendar.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>
	<script src="<?php echo base_url(); ?>assets/validation/admin_common.js"></script>
	<script src="<?php echo base_url(); ?>assets/admin/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jscolor.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/address.js"></script>
	<script>
		// area1 = new nicEditor({fullPanel : true}).panelInstance('editor1',{hasPanel : true});
		// area1 = new nicEditor({fullPanel : true}).panelInstance('editor2',{hasPanel : true});
	
		 bkLib.onDomLoaded(function () {
        if ($('#editor1').length > 0) {
            new nicEditor({fullPanel: true}).panelInstance('editor1');
        }
        if ($('#editor2').length > 0) {
            new nicEditor({fullPanel: true}).panelInstance('editor2');
        }
        if ($('#editor3').length > 0) {
            new nicEditor({fullPanel: true}).panelInstance('editor3');
        }
    });
	</script> 

	<script type="text/javascript">
		$(document).ready(function() {
			$('.loading').addClass('hidden');
	        $('.select2').select2();
	    });

		/*Loader display in click button*/
	    $('.search').on('click', function() {
	        $('.loading').removeClass('hidden');
	    });

	    $(".date").datepicker({
		    dateFormat: 'dd-mm-yy',
		    changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
        	todayHighlight: true,
			startDate:'+0d',
        	autoclose: true,        	
		});

		$(".filter_date").datepicker({
		    dateFormat: 'dd-mm-yy',
        	todayHighlight: true,
        	autoclose: true,        	
		});

		// select all checkbox
		$('#select_all').on('click', function(e) {
			if($(this).is(':checked',true)) {
				$(".chk_all").prop('checked', true);
			}
			else {
				$(".chk_all").prop('checked',false);
			}
		});
	</script>
	<script>
		$(document).ready(function(){
		  $("#search_text").on("keyup", function() {
		    var value = $(this).val().toLowerCase();
		    // alert(value);
		    $("#myTable tbody tr").filter(function() {
		      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		    });
		  });
		});
	</script>   
	</body>
</html>