<?php
    $login_success = $this->session->flashdata('success');
    $login_error = $this->session->flashdata('error');
    $validation = $this->session->flashdata('validation');                      
 
    if (isset($login_success))
    {
      ?>
          <div class="col-md-12 text-center offset4 alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?= $this->lang->line("success")?></strong> <?=$login_success?>
          </div>  
    <?php
    }                            

    elseif(isset($login_error))
    {
      ?>
          <div class="col-md-12 text-center alert alert-warning">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?= $this->lang->line("error")?></strong> <?=$login_error?>
          </div>  
      <?php          
    }

    elseif(isset($validation))
    {
      ?>
          <div class="col-md-12 text-center alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>validation Error!</strong> <?=$validation?>
          </div>  
      <?php          
    }  
?> 

