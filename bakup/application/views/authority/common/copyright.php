<!-- /.content-wrapper -->
<footer class="main-footer">
    <strong>Copyright &copy; <?php echo date("Y"); ?> <a href="<?php echo site_url(); ?>admin/dashboard"><?php echo SITE_TITLE; ?></a>.</strong> All rights
    reserved.
</footer>
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/dist/js/adminlte.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/dist/js/demo.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/functions.js"></script>
<script src="<?php echo base_url(); ?>assets/validation/jquery.validate.js"></script>
