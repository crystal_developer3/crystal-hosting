<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Notification Days Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Notification Days Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php echo site_url() . "authority/notification_days"; ?>">Notification Days</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $notify_details !=null ? 'Edit' : 'Add'?> Notification Days</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php $this->load->view('authority/common/messages');?>
                        <?php $action = $notify_details !=null ? base_url('authority/notification_days/update_notify') : base_url('authority/notification_days/insert_notify'); ?>

                        <form id="form" method="post" action="<?= $action?>" enctype="multipart/form-data">
                            <div class="clone-section-main">   
                                <div class="clone-section-sub">   
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <input type="hidden" name="id" id="notify_day_id" value="<?= ($notify_details !=null) ? $notify_details[0]['id'] : ""; ?>">
                                            <label for="title">Notify Days :<span class="required">*</span></label> 
                                            <?php
                                                $input_fields = array(
                                                    'name' => 'days[]',
                                                    'placeholder' => 'Notify Days',
                                                    'class' => 'form-control days',
                                                    'value' => (isset($notify_details) && $notify_details !=null ? $notify_details[0]['days'] : ""),
                                                );
                                                echo form_input($input_fields);
                                                echo form_error("days", "<div class='error'>", "</div>");
                                            ?>
                                            <span class="error_title" style="color: #fc3a3a;"></span>
                                            <hr>
                                        </div>
                                        <div class="col-md-2">
                                            <?php
                                                if ($notify_details == null) {
                                                    ?>   
                                                        <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                        <button type="button" class="btn add-more pull-left" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                        
                                                        <button type="button" class="btn remove-more btn-danger pull-right" title="Delete"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                    <?php 
                                                }
                                            ?>
                                            <br><br>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input class="btn btn-success text-uppercase check" value="Submit" type="submit">
                                    <a href="<?php echo site_url() . 'authority/notification_days'; ?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </section>
    </div>
    <?php $this->view('authority/common/copyright'); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
    <script>
        $(document).on('keyup', '.days', function() {
            var days = this.value = this.value.replace(/[^0-9]/g, '');
            $(this).val(days);
        });
        /*FORM VALIDATION*/
        var notify_day_id = $("#notify_dayid").val();
        $("#form").validate({
            rules: {
                'days[]': {required: true},
            },
            messages: {
                'days[]': "Please enter days",
            }
        });

        $(document).on('click', '.add-more', function() {
            $clone = $('.clone-section-sub:last').clone();
            $('.clone-section-main').append($clone);
            $('.clone-section-sub:last').find('input[type="text"]').val('');
        });

        $(document).on('click', '.remove-more', function () {
            if(check_clone_lang_section()) {
                $(this).closest('.clone-section-sub').remove();
            }else{
                alert('You can not remove the current section');
            }
        });

        function check_clone_lang_section() {
            if ($('.clone-section-main').find('.clone-section-sub').length > 1) {
                return true;
            }else{
                return false;
            }
        } 
    </script>
<?php $this->view('authority/common/footer'); ?>