<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    
    .countup {text-align: right;margin-bottom: 20px;}
    .countup .timeel {display: inline-block;padding: 10px;background: #151515;margin: 0;color: white;min-width: 2.6rem;margin-left: 13px;border-radius: 10px 0 0 10px;}
    .countup span[class*="timeRef"] {border-radius: 0 10px 10px 0;margin-left: 0;background: #e8c152;color: black;}
</style>
<?php $this->view('authority/common/sidebar'); ?>

<div class="content-wrapper">

    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <section class="content-header">
            <div class="breadcrumbs-area">
                <h3><?= $form_title ?></h3>
                <ul>
                    <li><a href="<?= base_url('authority/dashboard') ?>">Home</a></li>
                    <li><?= $form_title ?></li>
                </ul>
            </div>
        </section>
        <!-- Breadcubs Area End Here -->

        <!-- Admit Form Area Start Here -->
        <section class="content">
            <div class="card height-auto">        
                <div class="card-body">
                    <?php
                    $ticket_info = isset($ticket_details) && $ticket_details != null ? $ticket_details[0] : array();
                    if ($ticket_info['ticket_status_id'] == '9') {
                        echo '<div class="row"><div class="col-md-8">';
                        echo '<h3 class="text-left text-success">Ticket has been closed</h3>';
                        echo '</div><div class="col-md-4">';
                        echo '<a class="btn-fill-xs font-normal no-radius text-light btn-gradient-yellow pull-right pl-2 pr-2" href="' . base_url() . 'authority/ticket-management/view-ticket' . '">BACK</a>';
                        echo '</div></div>';
                    } else {
                        ?>
                        <form class="new-added-form" action="" id="form" method="POST" enctype="multipart/form-data">
                            <?php
                            $ticket_id = isset($ticket_details) && $ticket_details != null ? $ticket_details[0]['id'] : '';
                            $ticket_status_id = isset($ticket_details) && $ticket_details != null ? $ticket_details[0]['ticket_status_id'] : '';
                            ?>
                            <input type="hidden" name="ticket_id" value="<?= isset($ticket_details) && $ticket_details != null ? $ticket_details[0]['id'] : '' ?>">            
                            <div class="row">
                                <div class="col-md-5">
                                    <?php
                                    $priority_id = get_priority($ticket_info['priority_id']);
                                    if (!empty($priority_id)) {
                                        $priority_id = $priority_id[0];
                                        ?>
                                        <div class="alert alert-warning">
                                            <strong>Priority:</strong> <?php echo $priority_id['priority_name']; ?>, <strong>Completion Time:</strong> <?php echo $priority_id['hour']; ?> Hour
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-md-7">
                                    <?php
                                    $selected = isset($ticket_details[0]['ticket_status_id']) ? $ticket_details[0]['ticket_status_id'] : '';
                                    $used_time = '000:00:00:00';
                                    if ($selected == '4' || $selected == '9') {
                                        $sql = 'SELECT used_time FROM ticket_activity WHERE ticket_id = "' . $ticket_id . '" AND used_time != "" ORDER BY id DESC LIMIT 1 ';
                                        $info = $this->common_model->get_data_with_sql($sql);
                                        if ($info['row_count'] > 0) {
                                            $used_time = $info['data'][0]['used_time'];
                                        }
                                    }
                                    $used_time = explode(':', $used_time);
                                    ?>
                                    <div class="countup" id="countup1">
                                        <p class="pull-left" style="max-width:100px;float:left;">Current Time</p>
                                        <span class="timeel days hidden"><?php echo isset($used_time[0]) ? $used_time[0] : ''; ?></span>
                                        <span class="timeel timeRefDays hidden">days</span>
                                        <span class="timeel hours"><?php echo isset($used_time[1]) ? $used_time[1] : ''; ?></span>
                                        <span class="timeel timeRefHours">hours</span>
                                        <span class="timeel minutes"><?php echo isset($used_time[2]) ? $used_time[2] : ''; ?></span>
                                        <span class="timeel timeRefMinutes">minutes</span>
                                        <span class="timeel seconds"><?php echo isset($used_time[3]) ? $used_time[3] : ''; ?></span>
                                        <span class="timeel timeRefSeconds">seconds</span>
                                        <input type="hidden" class="days" name="days" value="<?php echo isset($used_time[0]) ? $used_time[0] : ''; ?>">
                                        <input type="hidden" class="hours" name="hours" value="<?php echo isset($used_time[1]) ? $used_time[1] : ''; ?>">
                                        <input type="hidden" class="minutes" name="minutes" value="<?php echo isset($used_time[2]) ? $used_time[2] : ''; ?>">
                                        <input type="hidden" class="seconds" name="seconds" value="<?php echo isset($used_time[3]) ? $used_time[3] : ''; ?>">
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-3 col-12 form-group">
                                    <label>Ticket No *</label>
                                    <input type="text" name="ticket_no" placeholder="Ticket No" class="form-control only_digits" value="<?= isset($ticket_details) && $ticket_details != null ? $ticket_details[0]['ticket_no'] : ''; ?>" readonly="readonly">
                                    <?php echo form_error("ticket_no", "<div class='error'>", "</div>"); ?>
                                </div>
                                <div class="col-xl-3 col-md-3 col-12 form-group">
                                    <label>Reply Date*</label>
                                    <input type="text" placeholder="Date" name="reply_date" class="form-control start_date" data-position='bottom right' value="<?= date('d-m-Y') ?>" readonly="readonly">
                                    <i class="far fa-calendar-alt" style="top: 50px;"></i>
                                    <div id="replay_date_validate"></div>
                                </div>
                                <div class="col-xl-3 col-md-3 col-12 form-group">
                                    <label>Time *</label>
                                    <input type="text" name="reply_time" id="timepicker1" placeholder="Time" class="form-control replay_time" value="<?= date("h:i:s A", strtotime("now")) ?>">
                                    <div id="replay_time_validate"></div>
                                </div>
                                <div class="col-xl-3 col-md-3 col-12 form-group">
                                    <label>Ticket Status *</label>
                                    <select class="select2" name="ticket_status_id">
                                        <option value="">Please Select Status *</option>
                                        <?php
                                        $ticket_status_details = ticket_status_admin();
                                        if (!ticket_inprogress_available($ticket_id)) {
                                            if (isset($ticket_status_details[9])) {
                                                unset($ticket_status_details[9]);
                                            }
                                        }
                                        if (isset($ticket_status_details) && $ticket_status_details != null) {
                                            $selected = isset($ticket_details[0]['ticket_status_id']) ? $ticket_details[0]['ticket_status_id'] : '';
                                            foreach ($ticket_status_details as $key => $value) {
                                                $selected_text = '';
                                                if ($selected == $key) {
                                                    $selected_text = 'selected="selected"';
                                                }
                                                ?>
                                                <option value="<?= $key; ?>" <?php echo $selected_text; ?>><?= $value; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <div id="ticket_status_id_validate"></div>
                                </div>
                                <div class="col-xl-3 col-md-3 col-12 form-group allocate-new">
                                    <label>Allocated to *</label>
                                    <?php
                                    echo form_hidden('allocated_to_type', '');
                                    ?>
                                    <select class="select2" name="allocated_to_id">
                                        <option value="">Please Select User *</option>
                                        <?php
                                        $selected = isset($allocated_to_id) ? $allocated_to_id : '';
                                        if (isset($user_details) && $user_details != null) {
                                            foreach ($user_details as $key => $value) {
                                                $selected_text = '';
                                                if ($selected == $value['id']) {
                                                    $selected_text = 'selected="selected"';
                                                }
                                                ?>
                                                <option value="<?= $value['id'] ?>" data-type="user" <?php echo $selected_text; ?>><?= $value['first_name'] ?></option>
                                                <?php
                                            }
                                        }
                                        $admin_user_info = get_admin_info();
                                        if (!empty($admin_user_info)) {
                                            foreach ($admin_user_info as $key => $value) {
                                                $selected_text = '';
                                                if ($selected == $value['id']) {
                                                    $selected_text = 'selected="selected"';
                                                }
                                                ?>
                                                <option value="<?= $value['id'] ?>" data-type="admin" <?php echo $selected_text; ?>><?= $value['full_name'] ?> (Admin)</option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <div id="user_id_validate"></div>
                                </div>

                                <div class="col-xl-6 col-lg-6 col-12 form-group">
                                    <label>Upload Document</label>
                                    <input type="file" name="ticket_document" class="" accept=".xlsx,.xls,image/*,.doc, .docx,.txt,.pdf">
                                </div>
                                <div class="col-lg-12 col-12 form-group">
                                    <label>Personal Remark</label>
                                    <textarea class="textarea form-control" name="ticket_reply" id="editor1" cols="10" rows="9"></textarea>
                                </div>
                                <div class="col-lg-12 col-12 form-group mg-t-8">
                                    <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                                    <!--                        <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>-->
                                </div>
                            </div>                    
                        </form>
                        <?php
                    }
                    ?>
                    <?php
                    $new_data = array('ticket_id' => isset($ticket_id) ? $ticket_id : '');
                    $this->load->view('common/ticket-history', $new_data);
                    ?>
                </div>
            </div>
        </section>
    </div>
</div>
    <?php $this->view('authority/common/copyright'); ?>

    <script>
        /*FORM VALIDATION*/
        $("#form").validate({
            rules: {
                reply_date: {required: true},
                reply_time: {required: true},
                ticket_status_id: {required: true},
            },
            errorPlacement: function (error, element) {
                var name = $(element).attr("name");
                error.appendTo($("#" + name + "_validate"));
            },
            messages: {
                reply_date: {required: "Please select date"},
                reply_time: {required: "Please select time"},
                ticket_status_id: {required: "Please select ticket-status"},
            }
        });
        $(document).ready(function () {

            $(document).on('change', '[name="allocated_to_id"]', function () {
                $('[name="allocated_to_type"]').val($(this).find('option:selected').attr('data-type'));
            });

            $('[name="allocated_to_type"]').val($('[name="allocated_to_id"]').find('option:selected').attr('data-type'));

            $(document).on('change', '[name="ticket_status_id"]', function () {
                if ($(this).val() == '7') {
                    $('.allocate-new').show();
                } else {
                    $('.allocate-new').hide();
                }
            });
            if ($('[name="ticket_status_id"]').val() == '7') {
                $('.allocate-new').show();
            } else {
                $('.allocate-new').hide();
            }
        });

        <?php
        $start_time = get_timer_start_time($ticket_id);
        $selected = isset($ticket_details[0]['ticket_status_id']) ? $ticket_details[0]['ticket_status_id'] : '';
        $statuses = array('7', '5');
        if ($start_time != '' && in_array($ticket_status_id, $statuses)) {
            ?>
                    countUpFromTime("<?php echo $start_time; ?>", 'countup1'); // ****** Change this line!
            <?php
        }
        ?>

        function countUpFromTime(countFrom, id) {
            countFrom = new Date(countFrom).getTime();
            var now = new Date(),
                    countFrom = new Date(countFrom),
                    timeDifference = (now - countFrom);
            var secondsInADay = 60 * 60 * 1000 * 24,
                    secondsInAHour = 60 * 60 * 1000;

            days = Math.floor(timeDifference / (secondsInADay) * 1);
            hours = Math.floor((timeDifference % (secondsInADay)) / (secondsInAHour) * 1);
            mins = Math.floor(((timeDifference % (secondsInADay)) % (secondsInAHour)) / (60 * 1000) * 1);
            secs = Math.floor((((timeDifference % (secondsInADay)) % (secondsInAHour)) % (60 * 1000)) / 1000 * 1);

            var idEl = document.getElementById(id);
            idEl.getElementsByClassName('days')[0].innerHTML = days;
            idEl.getElementsByClassName('hours')[0].innerHTML = hours;
            idEl.getElementsByClassName('minutes')[0].innerHTML = mins;
            idEl.getElementsByClassName('seconds')[0].innerHTML = secs;
            $('[name="days"]').val(days);
            $('[name="hours"]').val(hours);
            $('[name="minutes"]').val(mins);
            $('[name="seconds"]').val(secs);
            clearTimeout(countUpFromTime.interval);
            countUpFromTime.interval = setTimeout(function () {
                countUpFromTime(countFrom, id);
            }, 1000);
        }
    </script>
    <?php $this->view('authority/common/footer'); ?>