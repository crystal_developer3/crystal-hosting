<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->view('authority/common/header');
$this->view('authority/common/sidebar');
?>

<!-- Sidebar Area End Here -->
<div class="dashboard-content-one">
    <!-- Breadcubs Area Start Here -->
    <div class="breadcrumbs-area">
        <ul>
            <li>
                <a href="<?= base_url('authority/dashboard') ?>">Home</a>
            </li>
            <li>Ticket List</li> 
        </ul>
    </div>
    <!-- Breadcubs Area End Here -->
    <!-- Student Table Area Start Here -->
    <div class="card height-auto">
        <?php $this->view('authority/common/messages'); ?>
        <div class="card-body">
            <div class="heading-layout1">
                <div class="item-title">
                    <h3>Ticket List</h3>
                </div>
            </div>            
            <div class="row"> 
                <?php
                if (isset($type)) {
                    ?>
                    <div class="col-xl-1 col-lg-6 form-group">
                        <a href="<?= base_url('authority/ticket-management/add') ?>" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Add</a>
                    </div>
                <?php }
                ?>
                <?php
                if (isset($ticket_details) && $ticket_details != null) {
                    ?>
                    <div class="col-xl-2 col-lg-6 form-group">
                        <button type="button" class="btn-fill-lg bg-blue-dark btn-hover-yellow chk_submit" data-form-action="<?= base_url('authority/ticket-management/multiple-delete-ticket') ?>">Delete All</button>
                    </div>

                    <div class="<?= isset($type) ? 'col-xl-7' : 'col-xl-8'; ?> col-lg-6 col-12 form-group"></div>
                    <div class="col-xl-2 col-lg-6 col-12 form-group">
                        <input type="text"  name="search_text" id="search_text" placeholder="Search by Name ..." class="form-control">
                    </div>
                <?php }
                ?>
            </div>

            <div class="table-responsive">
                <table class="table display data-table text-nowrap" id="myTable">
                    <thead>
                        <tr>
                            <th>
                                <div class="form-check">
                                    <input type="checkbox" name="check_all" class="form-check-input checkAll">
                                    <label class="form-check-label">&nbsp;</label>
                                </div>
                            </th>
                            <th>User name</th>
                            <th>Ticket details</th>
                            <th>Ticket status</th>
                            <th>Location</th>
                            <th>View Problem</th>
                            <th>Allocate</th>
                            <th>Create date</th>
                            <!--<th>Action</th>-->
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($ticket_details) && $ticket_details != null) {
                            // echo"<pre>"; print_r($ticket_details);
                            foreach ($ticket_details as $key => $value) {
                                $name = array();
                                $id = $value['id'];
                                $allocate_user_dtls = $this->Production_model->get_all_with_where('ticket_allocate', 'id', 'desc', array('ticket_id' => $value['id']));
                                $user_id = isset($allocate_user_dtls) && $allocate_user_dtls != null ? $allocate_user_dtls[0]['user_id'] : '';
                                // echo"<pre>"; print_r($ticket_allocate_dtls);
                                $get_name = $this->Production_model->get_all_with_where_in('ticket_problem', 'id', 'desc', 'id', explode(',', $value['ticket_problem_id']), array('status' => '1'));
                                if (isset($get_name) && $get_name != null) {
                                    foreach ($get_name as $key1 => $name_row) {
                                        array_push($name, $name_row['problem_name']);
                                    }
                                }
                                $ticket_problem_name = implode(' , ', $name);
                                ?>
                                <tr>
                                    <td>
                                        <div class="form-check">
                                            <input type="checkbox" name="chk_multi_checkbox[]" class="form-check-input chk_all" value="<?= $id ?>">
                                            <label class="form-check-label">&nbsp;</label>
                                        </div>
                                    </td>       
                                    <td>
                                        <?php
                                        if ($value['type'] == 'user') {
                                            echo $value['name'];
                                        }
                                        if ($value['type'] == 'admin') {
                                            $get_admin_name = $this->Production_model->get_all_with_where('user', '', '', array());

                                            echo isset($get_admin_name) && $get_admin_name != null ? $get_admin_name[0]['username'] : '';
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <b>Ticket no : </b><?= $value['ticket_no']; ?><br>
                                        <?php
                                        $ticket_allocate_dtls = $this->Production_model->get_all_with_where('ticket_allocate', 'id', 'desc', array('ticket_id' => $value['id']));
                                        if (isset($ticket_allocate_dtls) && $ticket_allocate_dtls != null) {
                                            $user_dtls = $this->Production_model->get_where_user('name,user_email', 'user_register', '', '', array('id' => $ticket_allocate_dtls[0]['user_id']));
                                            if (isset($user_dtls) && $user_dtls != null) {
                                                ?>
                                                <b>Allocated by : </b><?= isset($user_dtls) && $user_dtls != null ? $user_dtls[0]['name'] : ''; ?>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td><?= $value['status_name']; ?></td>
                                    <td><?= $value['location_name']; ?></td>
                                    <td>
                                        <button type="button" class="btn-fill-xs font-normal no-radius text-light btn-gradient-yellow" onclick="view_details('<?= $id ?>', '<?= $ticket_problem_name ?>', '<?= base64_encode($value['problem']) ?>');">View</button>
                                    </td>
                                    <td>
                                        <!-- <button type="button" class="btn-fill-xs font-normal no-radius text-light gradient-pastel-green" onclick="ticket_allocate('<?= $id ?>','<?= $user_id ?>');">Allocate</button> -->
                                        <a href="<?= base_url('authority/ticket-management/allocate/' . $id) ?>" style="color: #fff;"><button class="btn-fill-xs font-normal no-radius text-light gradient-pastel-green">Allocate/View Details</button></a>
                                    </td>
                                    <td>
                                        <b>Create Date :</b> <?= date('d-m-Y h:i A', strtotime($value['create_date'])); ?>
                                    </td>
                                        <!--<td>     
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <span class="flaticon-more-button-of-three-dots"></span>
                                            </a> 
                                            <div class="dropdown-menu dropdown-menu-right">
                                                 <a class="dropdown-item" href="<?= base_url('authority/ticket-management/edit/' . $id); ?>"><i class="fas fa-cogs text-dark-pastel-green"></i>Edit</a> 

                                                <a class="dropdown-item" href="<?= base_url('authority/ticket-management/ticket-details/' . $id); ?>"><i class="fas fa-eye text-dark-pastel-green"></i>View</a>

                                                <a class="dropdown-item delete-btn" href="<?= base_url('authority/ticket-management/delete-ticket/' . $id) ?>" data-title="Delete" data-toggle="modal" data-target="#delete"><i class="fas fa-times text-orange-red"></i>Delete</a>
                                            </div>
                                        </div>
                                    </td>-->
                                </tr> 
                                <?php
                            }
                        }
                        ?>                      
                    </tbody>
                </table> 
            </div>

        </div>
    </div>

    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" value="" name="delete_link" id="delete_link"/>
                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-success btn-confirm-yes" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                    <button type="button" class="btn btn-default btn-confirm-no" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                </div>
            </div>
        </div>
    </div>
    <!-- DELETE POPUP -->

    <!-- View ticket details -->
    <div class="modal fade" id="view_details" tabindex="-1">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ticket Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <form action="#" method="post" enctype="multipart/form-data">
                                <input class="form-control" id="ticket_id" type="hidden" />

                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label for=""><b>Problem</b></label>
                                    <!--<input type="text" id="problem" class="form-control" value="" disabled/>-->
                                        <p id="problem"></p>
                                    </div>

                                    <div class="form-group">
                                        <label for=""><b>Problem Description</b></label>
                                    <!--<textarea id="problem_desc" class="form-control" value="" disabled placeholder="Problem" style="height: 200px;"></textarea>-->
                                        <p id="problem_desc"></p>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- View ticket details -->

    <!-- Ticket allocate details -->
    <!-- <div class="modal fade" id="ticket_allocate" tabindex="-1">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Allocate Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <form action="<?= base_url('authority/ticket_management/ticket_allocate') ?>" method="post" enctype="multipart/form-data">
                                <input class="form-control" id="ticket_allocate_id" name="ticket_id" type="hidden"/>

                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label for="">User name</label>
                                        <select class="form form-control" name="user_id" id="user_id">
                                            <option value="">Select</option>
    <?php
    if (isset($user_details) && $user_details != null) {
        foreach ($user_details as $key => $value) {
            ?>
                                                                        <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
            <?php
        }
    }
    ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" id="save" class="btn btn-success pull-left check">Allocate</button>

                                        <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div> -->
    <!-- Ticket allocate details -->

    <?php $this->view('authority/common/copyright'); ?>

    <script type="text/javascript">
        $('.check').click(function () {
            if (isemptyfocus('user_id')) {
                return false;
            }
        });

        $(document).ready(function () {
            $(document).on("click", ".delete-btn", function () {
                $("#delete_link").val($(this).attr("href"));
            });
            $(".btn-confirm-yes").on("click", function () {
                window.location = $("#delete_link").val();
            });
        });

        // multiple delete //
        $('.chk_submit').on('click', function () {
            var current = $(this);
            var boxes = $('.chk_all:checkbox');
            if (boxes.length > 0) {
                if ($('.chk_all:checkbox:checked').length < 1) {
                    $.alert({
                        title: 'Confirm Delete',
                        content: 'Please select at least one checkbox',
                    });
                    return false;
                } else {
                    $.confirm({
                        title: 'Confirm!',
                        content: 'Are you sure you want to delete?',
                        buttons: {
                            confirm: function () {
                                var record = [];
                                $('.chk_all:checkbox:checked').each(function () {
                                    record.push($(this).val());
                                });
                                $.ajax({
                                    url: '<?php echo base_url(); ?>authority/ticket-management/multiple_delete_ticket',
                                    method: 'POST',
                                    data: {'chk_multi_checkbox': record},
                                    dataType: 'json',
                                    success: function (response) {
                                        if (response.success) {
                                            window.location = window.location.href;
                                        }
                                    }
                                });
                            },
                            cancel: function () {
                            },
                        }
                    });
                    return false;
                }
            }
        });

        function view_details(id, problem, problem_desc)
        {
            // alert(type);
            var decodedetails = Base64.decode(problem_desc);
            $('#ticket_id').val(id);
            $('#problem').html(problem);
            $('#problem_desc').html(decodedetails);
            $("#view_details").modal('show');
        }
    </script>
    <?php $this->view('authority/common/footer'); ?>									