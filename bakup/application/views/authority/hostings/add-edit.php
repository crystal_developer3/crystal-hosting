<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Hostings Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Hostings Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php echo site_url() . "authority/hostings/view"; ?>">Hostings</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $hosting_details !=null ? 'Edit' : 'Add'?> Hostings</h3>

                        <!-- <form method="post" action="<?//= base_url('authority/hostings/save_excel_hosting')?>" enctype="multipart/form-data"><br>                            
                            <div class="col-md-4"> 
                                <input type="file" name="xls_file" id="xls_file" class="form-control" accept=".xlsx, .xls">
                            </div>
                            <div class="col-md-2"> 
                                <input type="submit" class="btn btn-sm btn-success check_excel" value="submit">
                            </div>
                            <div class="col-md-4">                            
                                <a href="<?//= base_url('authority/hostings/download_excel_hosting');?>" class="btn btn-sm btn-warning">Excel Download</a>
                            </div>
                        </form>
                        <div class="col-md-12" style="color: red; font-size: 20px;"><center>(OR)</center></div> -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php $this->load->view('authority/common/messages');?>
                        <?php $action = $hosting_details !=null ? base_url('authority/hostings/update_hosting') : base_url('authority/hostings/insert_hosting'); ?>

                        <form id="form" method="post" action="<?= $action?>" enctype="multipart/form-data">
                            <div class="clone-section-main">   
                                <div class="clone-section-sub">   
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <input type="hidden" name="id" id="hosting_id" value="<?= ($hosting_details !=null) ? $hosting_details[0]['id'] : ""; ?>">
                                            <label for="name">Hostings name :<span class="required">*</span></label> 
                                            <?php
                                                $input_fields = array(
                                                    'name' => 'name[]',
                                                    'placeholder' => 'Hostings name',
                                                    'class' => 'form-control name txtonly',
                                                    'id' => 'name',
                                                    'value' => (isset($hosting_details) && $hosting_details !=null ? $hosting_details[0]['name'] : ""),
                                                );
                                                echo form_input($input_fields);
                                                echo form_error("name", "<div class='error'>", "</div>");
                                            ?>
                                            <span class="error_name" style="color: #fc3a3a;"></span>
                                            <br>
                                        </div>
                                        <div class="col-md-2">
                                            <?php
                                                if ($hosting_details == null) {
                                                    ?>   
                                                        <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                        <button type="button" class="btn add-more pull-left" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                        
                                                        <button type="button" class="btn remove-more btn-danger pull-right" title="Delete"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                    <?php 
                                                }
                                            ?>
                                        </div>
                                        <br><br>
                                        <div class="col-md-10">
                                            <label for="details">Details :<span class="required">*</span></label> 
                                            <?php
                                                $input_fields = array(
                                                    'name' => 'details[]',
                                                    'placeholder' => 'Hostings Link',
                                                    'class' => 'form-control details txtonly',
                                                    'id' => 'editor1',
                                                    'value' => (isset($hosting_details) && $hosting_details !=null ? $hosting_details[0]['details'] : ""),
                                                );
                                                // echo form_input($input_fields);
                                                echo form_textarea($input_fields);
                                                echo form_error("details", "<div class='error'>", "</div>");
                                            ?>
                                            <span class="error_details" style="color: #fc3a3a;"></span>
                                            <br>
                                        </div>
                                    </div>

                                    <!-- <div class="col-md-4">
                                        <label for="cat_icon">Hostings Icon :
                                            <span class="required">*</span>(Upload by 60&#215;60)
                                        </label>

                                        <input type="file" name="cat_icon" class="form-control cat_icon" class="form-control cat_icon" accept="image/*">
                                        <span class="error_icon" style="color: #fc3a3a;"></span>
                                    </div> -->
                                    
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <label for="host_image">Hostings Image :(Upload by 30&#215;30)
                                            </label>

                                            <input type="file" name="host_image[]" class="form-control host_image" class="form-control host_image" accept="image/*">
                                            <span class="error_file" style="color: #fc3a3a;"></span>
                                            <?php
                                                if ($hosting_details != null) {
                                                    ?>
                                                    <div class="form-group col-md-10">
                                                        <label for="last_name">Current image</label><br>
                                                        <img src="<?= base_url(HOSTING_IMAGE).$hosting_details[0]['host_image']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">
                                                    </div>
                                                <?php }
                                            ?>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input class="btn btn-success text-uppercase check" value="Submit" type="submit">
                                    <a href="<?php echo site_url() . 'authority/hostings'; ?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </section>
    </div>
    <?php $this->view('authority/common/copyright'); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
    <script>
        /*FORM VALIDATION*/
        var hosting_id = $("#hosting_id").val();
        $("#form").validate({
            rules: {
                'name[]': {required: true},      
                'host_image[]': { 
                    required: function(element) {
                        if (hosting_id == '') {  
                            return true;
                        }
                        else {
                            return false;
                        }
                    }, 
                },                  
            },
            messages: {
                'name[]': "Please enter hosting name",
                'host_image[]': "Please select image",       
            }
        });

        // $('.check').click(function(){
        //     var hosting_id = $("#hosting_id").val();

        //     var name = $(".name_english").val();
        //     var host_image = $(".host_image")[0].files.length;

        //     if (name_english =='' && name_arabic ==''){
        //         $('.error_name').text('Please enter english OR arabic hosting-name.');
        //         $('.name_arabic').focus();
        //         return false;
        //     }  

        //     if (hosting_id == ''){
        //         if(host_image === 0){
        //             $('.error_file').text("Please select image.");
        //             return false;
        //         }
        //     } 
        // });

        $('.check_excel').click(function(){
            if(isemptyfocus('xls_file'))
            {
                return false;
            }
        });

        $(document).on('click', '.add-more', function() {
            $clone = $('.clone-section-sub:last').clone();
            $('.clone-section-main').append($clone);
            $('.clone-section-sub:last').find('input[type="text"]').val('');
            $('.clone-section-sub:last').find('input[type="file"]').val('');
        });

        $(document).on('click', '.remove-more', function () {
            if (check_clone_lang_section()) {
                $(this).closest('.clone-section-sub').remove();
            } else {
                alert('You can not remove the current section');
            }
        });

        function check_clone_lang_section() {
            if ($('.clone-section-main').find('.clone-section-sub').length > 1) {
                return true;
            } else {
                return false;
            }
        } 
    </script>
<?php $this->view('authority/common/footer'); ?>