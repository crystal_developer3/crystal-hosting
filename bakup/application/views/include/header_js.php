
    <!-- Meta Tags -->
    <title><?= SITE_TITLE?></title>
    
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url()?>assets/image/logo.png">
    <!-- Bootstrap CSS -->

    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/aos.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/demo1.css">
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?= base_url()?>assets/css/jquery.animateSlider.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/style.css">
    <style type="text/css">
    @import url('https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i');
    body{font-family: 'Roboto', sans-serif;}
    .main-container{max-width: 90%;margin: auto;}
    .main-border{border: 1px solid #000000;}
    .b-bottom {border-bottom: 1px solid #000000;}
    .b-right {border-right: 1px solid #000000;}
    .lh-32{line-height: 32px;}
    .t-color{color: #f0ffff00;}
    .tb{border-spacing: 0px;}
    .tb-bordered td, .tb-bordered th {
        text-align: center;
        border-right: 1px solid #000000;
        border-bottom: 1px solid #000000;
    }
    .b-right-0{border-right:0 !important;}
    .bg-1{background-color: antiquewhite;}
    * {box-sizing: border-box;}
    .line {display: flex;flex-wrap: wrap;}
    .header-c {max-width: 33.333333%;text-align: center;margin-left: 33.333333%;position: relative;width: 100%;padding-right: 15px;padding-left: 15px;}
    .header-r{max-width: 33.333333%;position: relative;width: 100%;min-height: 1px;padding: 29px;text-align:right}
    .heading-l{max-width: 50%;position: relative;width: 100%;padding: 15px;}
    .heading-l p{margin:5px 0;}
    .heading-r{max-width: 50%;position: relative;width: 100%;padding-right: 15px;padding-left: 15px;}
    .heading-r .first-p{display: inline;margin: 0;}
    .invoice{padding: 5px 20px;}
    .footer h4 {width:100%;}
    .footer{padding: 0 16px;text-align: right;width: 100%; background-color: #cacaca; }
    .item-name p{border-bottom: 1px solid;padding: 10px;margin: 0;}
    .bd-0{border-bottom:0 !important;}
</style>


    <script>
        var BASE_URL = '<?php echo base_url(); ?>';
    </script>
   



