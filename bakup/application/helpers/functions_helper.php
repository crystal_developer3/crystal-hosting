<?php

defined('BASEPATH') OR exit('No direct script access allowed');
if (!function_exists('format_date_dmy')) {

    function format_date_dmy($date) {
        if (isset($date) && $date != "" && $date != '0000-00-00') {
            $date = date("d M Y", strtotime($date));
        } else {
            $date = "";
        }
        return $date;
    }

}
if (!function_exists('format_date_Mdy')) {

    function format_date_Mdy($date) {
        if (isset($date) && $date != "" && $date != '0000-00-00') {
            $date = date("F d Y", strtotime($date));
        } else {
            $date = "";
        }
        return $date;
    }

}

if (!function_exists('format_date_Mdy_time')) {

    function format_date_Mdy_time($date) {
        if (isset($date) && $date != "" && $date != '0000-00-00') {
            $date = date("F d Y h:i A", strtotime($date));
        } else {
            $date = "";
        }
        return $date;
    }

}
if (!function_exists('format_date_ymd')) {

    function format_date_ymd($date) {
        if (isset($date) && $date != "" && $date != '0000-00-00') {
            $date = date("Y-m-d", strtotime($date));
        } else {
            $date = "";
        }
        return $date;
    }

}
if (!function_exists('add_year_date_ymd')) {

    function add_year_date_ymd($date,$year) {
        if (isset($date) && $date != "" && $date != '0000-00-00') {
            $y=2;
            $date = strtotime($date);
            $new_date = strtotime('+ '.$year.' year', $date);
            
            $date = date("Y-m-d", $new_date);
        } else {
            $date = "";
        }
        return $date;
    }

}

if (!function_exists('blank_errorcheck')) {

    function blank_errorcheck($fieldname) {
        $errMessage = array();
        foreach ($fieldname as $key => $message) {
            if (strlen(trim(@$_POST[$key])) <= 0) {
                $errMessage[$key] = $message;
                $error = true;
            }
        }
        return $errMessage;
    }

}

if (!function_exists('is_url_exist')) {

    function is_url_exist($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($code == 200) {
            $status = true;
        } else {
            $status = false;
        }
        curl_close($ch);
        return $status;
    }

}

if (!function_exists('check_value_by_key')) {

    function check_value_by_key($array, $key, $val) {
        foreach ($array as $item)
            if (isset($item[$key]) && $item[$key] == $val) {
                return $item["file"];
            }
        return false;
    }

}

if (!function_exists("get_date_diff")) {


    /**
     * 
     * @param type $past_date
     * @param type $cur_date
     * @return string
     */
    function get_date_diff($past_date, $cur_date) {
        $past_date = date('d-m-Y', strtotime($past_date));
        $cur_date = date('d-m-Y', strtotime($cur_date));

        $diff12 = date_diff(date_create($cur_date), date_create($past_date));
        $days = $diff12->d;
        $months = $diff12->m;
        $years = $diff12->y;

        $return_data = "";
        if ($years > 0) {
            $return_data .= $years . " YEAR" . (($years > 1) ? "S, " : "");
        }
        if ($months > 0) {
            $return_data .= $months . " MONTH" . (($months > 1) ? "S, " : "");
        }
        if ($days > 0) {
            $return_data .= $days . " DAY" . (($days > 1) ? "S " : "");
        }
        return $return_data;
    }

}

if (!function_exists("days_in_month")) {

    function days_in_month($month, $year = "") {
        if ($year == "") {
            $year = date("Y");
        }
        // calculate number of days in a month
        return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
    }

}

if (!function_exists("encrypt_cookie")) {

    function encrypt_cookie($value) {
        if (!$value) {
            return false;
        }
        $key = 'htd7ZxUONSQ4NMAd';
        // Remove the base64 encoding from our key
        $encryption_key = base64_decode($key);
        // Generate an initialization vector
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
        $encrypted = openssl_encrypt($value, 'aes-256-cbc', $encryption_key, 0, $iv);
        // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
        return base64_encode($encrypted . '::' . $iv);
    }

}
if (!function_exists("decrypt_cookie")) {

    /**
     * 
     * @param type $value
     * @return boolean
     */
    function decrypt_cookie($value) {
        if (!$value) {
            return false;
        }
        $key = 'htd7ZxUONSQ4NMAd';
        $encryption_key = base64_decode($key);
        // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
        list($encrypted_data, $iv) = explode('::', base64_decode($value), 2);
        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
    }

}

if (!function_exists('get_pagination_data')) {

    function get_pagination_data($total_records, $current_page = 1, $records_per_page) {
        if ($total_records > 0) {
            if (isset($current_page)) {
                $pn = intval(abs($current_page));
            } else {
                $pn = 1;
            }
            $last_page = ceil($total_records / $records_per_page);
            if ($pn < 1) {
                $pn = 1;
            } else if ($pn > $last_page) {
                $pn = $last_page;
            }
            $center_pages = array();
            $sub1 = $pn - 1;
            //$sub2 = $pn - 2;
            $add1 = $pn + 1;
            //$add2 = $pn + 2;
            if ($pn == 1) {
                //$center_pages['sub2'] = 0;
                $center_pages['sub1'] = 0;
                $center_pages['current_page'] = $pn;
                $center_pages['add1'] = $add1;
                //$center_pages['add2'] = $add2;
            } else if ($pn == $last_page) {
                //$center_pages['sub2'] = $sub2;
                $center_pages['sub1'] = $sub1;
                $center_pages['current_page'] = $pn;
                $center_pages['add1'] = 0;
                //$center_pages['add2'] = 0;
            } else if ($pn > 2 && $pn < ($last_page - 1)) {
                //$center_pages['sub2'] = $sub2;
                $center_pages['sub1'] = $sub1;
                $center_pages['current_page'] = $pn;
                $center_pages['add1'] = $add1;
                //$center_pages['add2'] = $add2;
            } else if ($pn > 1 && $pn < $last_page) {
                //$center_pages['sub2'] = 0;
                $center_pages['sub1'] = $sub1;
                $center_pages['current_page'] = $pn;
                $center_pages['add1'] = $add1;
                //$center_pages['add2'] = 0;
            }
            $pagination_records = array();
            $limit_start = ($pn - 1) * $records_per_page;
            $limit_end = $records_per_page;
            if ($last_page != "1") {
                if ($pn != 1) {
                    $previous = $pn - 1;
                    //$first = 1;
                    //$pagination_records['first'] = $first;
                    $pagination_records['previous'] = $previous;
                }
                $pagination_records['center_pages'] = $center_pages;
                if ($pn != $last_page) {
                    $nextPage = $pn + 1;
                    //$last = $last_page;
                    $pagination_records['next'] = $nextPage;
                    //$pagination_records['last'] = $last;
                }
            }
//            if (!isset($pagination_records['first'])) {
//                $pagination_records['first'] = 0;
//            }
            if (!isset($pagination_records['previous'])) {
                $pagination_records['previous'] = 0;
            }
            if (!isset($pagination_records['center_pages'])) {
                //$center_pages['sub2'] = 0;
                $center_pages['sub1'] = 0;
                $center_pages['current_page'] = $pn;
                $center_pages['add1'] = 0;
                //$center_pages['add2'] = 0;
                $pagination_records['center_pages'] = $center_pages;
            }
            if (!isset($pagination_records['next'])) {
                $pagination_records['next'] = 0;
            }
//            if (!isset($pagination_records['last'])) {
//                $pagination_records['last'] = 0;
//            }
        } else {
            $pagination_records = false;
            $last_page = $limit_start = $limit_end = false;
        }
        $return_array = array();
        $return_array['pagination'] = (empty($pagination_records) ? 0 : $pagination_records);
        $return_array['total_pages'] = $last_page;
        $return_array['limit_start'] = $limit_start;
        $return_array['limit_end'] = $limit_end;
        return $return_array;
    }

}

if (!function_exists('force_download_file')) {

    function force_download_file($source_file, $download_name, $mime_type = '') {
        /*
          $source_file = path to a file to output
          $download_name = filename that the browser will see
          $mime_type = MIME type of the file (Optional)
         */
        if (!is_readable($source_file)) {
            die('File not found or inaccessible!');
        }

        $size = filesize($source_file);
        $download_name = rawurldecode($download_name);

        /* Figure out the MIME type (if not specified) */
        $known_mime_types = array(
            "pdf" => "application/pdf",
            "csv" => "application/csv",
            "txt" => "text/plain",
            "html" => "text/html",
            "htm" => "text/html",
            "exe" => "application/octet-stream",
            "zip" => "application/zip",
            "doc" => "application/msword",
            "xls" => "application/vnd.ms-excel",
            "ppt" => "application/vnd.ms-powerpoint",
            "gif" => "image/gif",
            "png" => "image/png",
            "jpeg" => "image/jpg",
            "jpg" => "image/jpg",
            "php" => "text/plain"
        );

        if ($mime_type == '') {
            $file_extension = strtolower(substr(strrchr($source_file, "."), 1));
            if (array_key_exists($file_extension, $known_mime_types)) {
                $mime_type = $known_mime_types[$file_extension];
            } else {
                $mime_type = "application/force-download";
            };
        };

        @ob_end_clean(); //off output buffering to decrease Server usage
// if IE, otherwise Content-Disposition ignored
        if (ini_get('zlib.output_compression')) {
            ini_set('zlib.output_compression', 'Off');
        }

        header('Content-Type: ' . $mime_type);
        header('Content-Disposition: attachment; filename="' . $download_name . '.' . $file_extension . '"');
        header("Content-Transfer-Encoding: binary");
        header('Accept-Ranges: bytes');

        header("Cache-control: private");
        header('Pragma: private');
        header("Expires: Thu, 26 Jul 2012 05:00:00 GMT");

// multipart-download and download resuming support
        if (isset($_SERVER['HTTP_RANGE'])) {
            list($a, $range) = explode("=", $_SERVER['HTTP_RANGE'], 2);
            list($range) = explode(",", $range, 2);
            list($range, $range_end) = explode("-", $range);
            $range = intval($range);
            if (!$range_end) {
                $range_end = $size - 1;
            } else {
                $range_end = intval($range_end);
            }

            $new_length = $range_end - $range + 1;
            header("HTTP/1.1 206 Partial Content");
            header("Content-Length: $new_length");
            header("Content-Range: bytes $range-$range_end/$size");
        } else {
            $new_length = $size;
            header("Content-Length: " . $size);
        }

        /* output the file itself */
        $chunksize = 1 * (1024 * 1024); //you may want to change this
        $bytes_send = 0;
        if ($source_file = fopen($source_file, 'r')) {
            if (isset($_SERVER['HTTP_RANGE'])) {
                fseek($source_file, $range);
            }

            while (!feof($source_file) && (!connection_aborted()) && ($bytes_send < $new_length)) {
                $buffer = fread($source_file, $chunksize);
                print($buffer); //echo($buffer); // is also possible
                flush();
                $bytes_send += strlen($buffer);
            }
            fclose($source_file);
        } else {
            die('Error - can not open file.');
        }

        die();
    }

}

/* Editor content get */

function closetags($html) {
    preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
    $openedtags = $result[1];
    preg_match_all('#</([a-z]+)>#iU', $html, $result);
    $closedtags = $result[1];
    $len_opened = count($openedtags);
    if (count($closedtags) == $len_opened) {
        return $html;
    }
    $openedtags = array_reverse($openedtags);
    for ($i = 0; $i < $len_opened; $i++) {
        if (!in_array($openedtags[$i], $closedtags)) {
            $html .= '</' . $openedtags[$i] . '>';
        } else {
            unset($closedtags[array_search($openedtags[$i], $closedtags)]);
        }
    }
    return $html;
}

function get_user_info($user_id) {
    $CI = &get_instance();
    $conditions = array('where' => array('id' => $user_id));
    $info = $CI->common_model->select_data('user_register', $conditions);
    if ($info['row_count'] > 0) {
        return $info['data'][0];
    } else {
        return array();
    }
}

function get_admin_info($user_id = '') {
    $CI = &get_instance();
    if ($user_id != '') {
        $conditions = array('where' => array('id' => $user_id));
    } else {
        $conditions = array();
    }
    $info = $CI->common_model->select_data('user', $conditions);
    if ($info['row_count'] > 0) {
        if ($user_id != '') {
            return $info['data'][0];
        } else {
            return $info['data'];
        }
    } else {
        return array();
    }
}

function create_directory($path) {
    $path = explode('/', $path);
    $string = '';
    foreach ($path as $key => $value) {
        $string .= $value;
        if (!is_dir($string)) {
            mkdir($string);
            @chmod($string, 0777);
        }
        $string .= '/';
    }
}

function get_department($department_id = '') {
    $CI = &get_instance();
    $return = array();
    $conditions = array();
    if ($department_id != '') {
        $conditions['IN'] = array('id' => explode(',', $department_id));
    }
    $info = $CI->common_model->select_data('department', $conditions);
    if ($info['row_count'] > 0) {
        $return = $info['data'];
    }
    return $return;
}

function get_location($id = '') {
    $CI = &get_instance();
    $return = array();
    $conditions = array('select' => 'location_name,location_logo,id');
    if ($id != '') {
        $conditions['IN'] = array('id' => explode(',', $id));
    }
    $info = $CI->common_model->select_data('ticket_location', $conditions);
    if ($info['row_count'] > 0) {
        $return = $info['data'];
    }
    return $return;
}

function get_requirement($id = '') {
    $CI = &get_instance();
    $return = array();
    $conditions = array('select' => 'auditorium,id');
    if ($id != '') {
        $conditions['IN'] = array('id' => explode(',', $id));
    }
    $info = $CI->common_model->select_data('auditorium', $conditions);
    if ($info['row_count'] > 0) {
        $return = $info['data'];
    }
    return $return;
}

function send_email($settings = array()) {
    $mail = new PHPMailer;

    $mail->IsSMTP();         // Set mailer to use SMTP
    $mail->Host = SMTP_HOST; // Specify main and backup server
    $mail->Port = SMTP_PORT; // Set the SMTP port
    $mail->SMTPAuth = true;  // Enable SMTP authentication
    $mail->Username = SMTP_USERNAME; // SMTP username
    $mail->Password = SMTP_PASSWORD; // SMTP password
    //$mail->SMTPSecure = 'tls'; // Enable encryption, 'ssl' also accepted
    $mail->From = FROM_EMAIL; // SMTP username
    $mail->FromName = FROM_EMAIL_TITLE;

    foreach ($settings['to'] as $email) {
        $mail->AddAddress($email);
    }
    // $mail->AddAddress($to);
    $mail->IsHTML(true);
    $mail->Subject = $settings['subject'];
    //$mail->addAttachment($attech_file);
    $mail->Body = $settings['content'];
    //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
    if (!$mail->Send()) {
//        echo 'Message could not be sent.';
//        echo 'Mailer Error: ' . $mail->ErrorInfo;
        return false;
    } else {
        return true;
    }
}

function get_engineer() {
    $CI = &get_instance();
    return $CI->Production_model->get_all_with_where('user_register ', '', '', array('role_type_id' => '11', 'status' => '1'));
}

function upload_file($file_name, $path) {
    $CI = &get_instance();
    create_directory($path);
    $return = array('status' => 'failure', 'file_name' => '', 'error' => '');
    if (isset($_FILES[$file_name]) && $_FILES[$file_name]['name'] != "") {
        $config['upload_path'] = $path;
        $config['allowed_types'] = '*';
        $config['encrypt_name'] = true;
        $CI->load->library('upload', $config);
        if (!$CI->upload->do_upload($file_name)) {
            $return['error'] = $CI->upload->display_errors();
        } else {
            $upload_data = $CI->upload->data();
            $file_name = $upload_data['file_name'];
            @chmod($config['upload_path'] . $file_name, 0777);
            $return['status'] = 'success';
            $return['file_name'] = $file_name;
        }
    } else {
        $return['error'] = '';
    }
    return $return;
}

function ticket_color() {
    return array(
        //'3' => array('name' => 'Resolved', 'color' => 'blue'),
        '4' => array('name' => 'Hold', 'color' => 'orange'),
        '5' => array('name' => 'In Progress', 'color' => 'green'),
        '6' => array('name' => 'New', 'color' => 'blue'),
        '7' => array('name' => 'Queue', 'color' => 'red'),
        '8' => array('name' => 'Reopen', 'color' => 'yellow'),
        '9' => array('name' => 'Closed', 'color' => 'magenta'),
    );
}

/**
 * This returns table names that are used in requests
 * @return type
 */
function request_tables() {
    return array('user_ticket', 'assets_request', 'auditorium_book', 'desk_move_request', 'pendrive_request', 'software_request');
}

function filterData(&$str) {
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", "\\n", $str);
    if (strstr($str, '"'))
        $str = '"' . str_replace('"', '""', $str) . '"';
}
if(!function_exists('create_slug')){
    function create_slug($string) {
       $string = strtolower(str_replace(' ', '-', $string)); // Replaces all spaces with hyphens.
       $string = preg_replace('/[^A-Za-z0-9.\-]/', '', $string); // Removes special chars.

       return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}
if (!function_exists('generate_invoice_no')) {

    function generate_invoice_no($size) {
        $alpha_key = '';
        $keys = range('A', 'Z');

        for ($i = 0; $i < 1; $i++) {
            $alpha_key .= $keys[array_rand($keys)];
        }

        $length = $size - 1;

        $key = '';
        $keys = range(0, 9);
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $alpha_key . $key;
    }

}

function get_user_domain_count($user_id) {
    $CI = & get_instance();
    $conditions = array("select" => "id", "where" => array("status" => "1","user_id"=>$user_id));
    $info = $CI->common_model->select_data("user_domain", $conditions);
    if ($info['row_count'] > 0) {
        return $info['row_count'];
    } else {
        return 0;
    }
}
function get_user_hosting_count($user_id) {
    $CI = & get_instance();
    $conditions = array("select" => "id", "where" => array("user_id"=>$user_id));
    $info = $CI->common_model->select_data("user_hosting", $conditions);
    if ($info['row_count'] > 0) {
        return $info['row_count'];
    } else {
        return 0;
    }
}

function get_user_ticket_count($user_id) {
    $CI = & get_instance();
    $conditions = array("select" => "id", "where" => array("user_id"=>$user_id));
    $info = $CI->common_model->select_data("tickets", $conditions);
    if ($info['row_count'] > 0) {
        return $info['row_count'];
    } else {
        return 0;
    }
}
function get_user_invoice_count($user_id) {
    $CI = & get_instance();
    $conditions = array("select" => "id", "where" => array("user_id"=>$user_id));
    $info = $CI->common_model->select_data("invoice", $conditions);
    if ($info['row_count'] > 0) {
        return $info['row_count'];
    } else {
        return 0;
    }
}

function get_notify_days(){
    $CI = & get_instance();
    $conditions = array("select" => "days","where" => array("status"=>'1'));
    $info = $CI->common_model->select_data("notify", $conditions);
    // echo "<pre>";print_r($info['data']);exit;

    if ($info['row_count'] > 0) {
        return $info['data'];
    } else {
        return array();
    }
}