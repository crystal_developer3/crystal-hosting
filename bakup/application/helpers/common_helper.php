<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('createResponse')) {
	function createResponse($is_api_call,$result,$message,$data=null){
		$return =array();
		$return['result'] = $result;
		$return['message'] = $message;
		$return['data'] = $data;
		
		if($is_api_call !=null && !empty($is_api_call) && isset($is_api_call)){
			echo json_encode($return);exit();
		}else{
			return $return;
		}
	}
}

if (!function_exists('checkHeader')) {
	function checkHeader(){
		$CI =& get_instance();
		$headers = $CI->input->get_request_header('Authenticate');
		if (isset($headers) && !empty($headers) && $headers ==$CI->config->item('apikey')) {
			return true;
		}else{
			return false;
		}
	}
}
if (!function_exists('checkEmpty')) {
	function checkEmpty($value){
		if(isset($value) && !empty($value) && $value !=null){
			return false;
		}else{
			return true;
		}
	}
}
if (!function_exists('returnResponse')) {
	function returnResponse($result,$message,$data=null){
		$return =array();
		$return['result'] = $result;
		$return['message'] = $message;
		$return['data'] = $data;
		return $return;
	}
}
if (!function_exists('decodeJson')) {
	function decodeJson($Json,$fields){
		$error=false;
		foreach($Json as $key => $val) {
			if (array_key_exists($val, $fields)) {
				if(checkEmpty($val)){
					$error= true;
					echo json_encode(returnResponse("Fail",$val." is empty",null));exit;
					break;
				}
			}else{
				$error= true;
				echo json_encode(returnResponse("Fail",$val." is empty",null));exit;
				break;
			}
		}
		return $error;
	}

	if (!function_exists('generate_password')) {

		function generate_password($len = 8,$numbersonly=false) {

			// Array of potential characters, shuffled.
			if($numbersonly){
				$chars = array(
					'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				);
			}else{
				$chars = array(
					'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
					'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
					'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
					'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
					'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				);
			}
			
			shuffle($chars);

			$num_chars = count($chars) - 1;
			$token = '';

			// Create random token at the specified length.
			for ($i = 0; $i < $len; $i++) {
				$token .= $chars[mt_rand(0, $num_chars)];
			}

			return $token;
		}

	}
}

/*Common for the all things needed..*/

if (!function_exists('getCountry')) {
    function getCountry($is_api_call=''){
		$CI =& get_instance();
		$rec = array();		
		$country_details = $CI->Production_model->get_all_record('','country','');
		if ($country_details !=null) {
			foreach ($country_details as $key => $value) {
				$rec[$key]['id'] = $value['id'];
				$rec[$key]['name'] = $value['country_name'];					
			}
			createResponse($is_api_call,"Success","",$rec);
		}else{
			createResponse($is_api_call,"Fail","No country found!",null);
		}
    }
}

if (!function_exists('getStates')) {
    function getStates($country_id ='',$is_api_call=''){
		$CI =& get_instance();
		$rec = array();		
		$state_detail = $CI->Production_model->get_all_with_where('state','id','asc',array('id_country'=>$country_id));
		if ($state_detail !=null) {
			foreach ($state_detail as $key => $value) {
				$rec[$key]['id'] = $value['id'];
				$rec[$key]['name'] = $value['state_name'];					
			}
			createResponse($is_api_call,"Success","",$rec);
		}else{
			createResponse($is_api_call,"Fail","No states found!",null);
		}
	}
}

if (!function_exists('getCities')) {
    function getCities($state_id ='',$is_api_call=''){
		$CI =& get_instance();
		$rec = array();		
		$city_detail = $CI->Production_model->get_all_with_where('city','id','asc',array('id_state'=>$state_id));
		if ($city_detail !=null) {
			foreach ($city_detail as $key => $value) {
				$rec[$key]['id'] = $value['id'];			
				$rec[$key]['name'] = $value['city_name'];					
			}
			createResponse($is_api_call,"Success","",$rec);
		}else{
			createResponse($is_api_call,"Fail","No city found!",null);
		}
	}
}

if (!function_exists('getCustomerSupport')) {
    function getCustomerSupport($is_api_call=''){
		$CI =& get_instance();
		$rec = array();		
		$rec ='<!DOCTYPE html>
		<html>
		<head>
			<title></title>
		</head>
		<body>
			<h1>Email Support</h1>
		</body>
		</html>'; 
		$country_details = $CI->Production_model->get_all_record('','country','');
		if ($country_details !=null) {
			/*foreach ($country_details as $key => $value) {
				$rec[$key]['id'] = $value['id'];
				$rec[$key]['name'] = $value['country_name'];					
			}*/


			createResponse($is_api_call,"Success","",$rec);
		}else{
			createResponse($is_api_call,"Fail","No country found!",null);
		}
    }
}

if (!function_exists('getDepartments')) {
    function getDepartments($is_api_call=''){
		$CI =& get_instance();
		$rec = array();		
		$plan_metadata= array();
		$plan_detail = $CI->Production_model->get_all_with_where('departments','id','desc',array('status'=>"1"));
		// print_r($plan_detail);
		if ($plan_detail !=null) {
			foreach ($plan_detail as $key => $value) {
				$rec[$key]['id'] = $value['id'];			
				$rec[$key]['name'] = $value['name'];
				$rec[$key]['description'] = $value['description'];
			}

			createResponse($is_api_call,"Success","",$rec);
		}else{
			createResponse($is_api_call,"Fail","No Plan found!",null);
		}
	}
}

if (!function_exists('getHostingDetails')) {
    function getHostingDetails($is_api_call=''){
		$CI =& get_instance();
		$rec = array();		
		$plan_metadata= array();
		$plan_detail = $CI->Production_model->get_all_with_where('hosting_plan','id','desc',array('status'=>"1"));
		// print_r($plan_detail);
		if ($plan_detail !=null) {
			foreach ($plan_detail as $key => $value) {
				$rec[$key]['id'] = $value['id'];			
				$rec[$key]['name'] = $value['name'];
				$rec[$key]['image'] = $value['image'];
			}

			createResponse($is_api_call,"Success","",$rec);
		}else{
			createResponse($is_api_call,"Fail","No Plan found!",null);
		}
	}
}
if(!function_exists('getYoutubeThumbnail')) {
	function getYoutubeThumbnail($url){
        //preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $matches);
        $url = str_replace('https://www.youtube.com/watch?v=', '', $url);
        $url = str_replace('https://youtu.be/', '', $url);
        //return 'http://img.youtube.com/vi/'.$matches[0].'/mqdefault.jpg';
        return 'http://img.youtube.com/vi/'.$url.'/mqdefault.jpg';
    }
}
if(!function_exists('getYoutubeKey')) {
    function getYoutubeKey($url){
        $url = str_replace('https://www.youtube.com/watch?v=', '', $url);
        $url = str_replace('https://youtu.be/', '', $url);
        // preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $matches);
        return $url;
    }
}
?>
