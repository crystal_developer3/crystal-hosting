<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('send_chat_invitation')) {
    function send_chat_invitation($product_id ='',$buyer_id ='',$seller_id=''){
       	$CI =& get_instance();
		
        if(empty($product_id) || empty($buyer_id) || empty($seller_id)){
            $return['result'] = "fail";
            $return['message'] = "Invalid details you entered...!";
            echo json_encode($return);exit;
        }else{
            $result = $CI->Production_model->get_all_with_where('chat_invitation', '', '', array('buyer_id' => $buyer_id, 'seller_id' => $seller_id,'product_id'=>$product_id));
            // echo $CI->db->last_query(); echo"<pre>"; print_r($result); exit;

            if(count($result) > 0 ){
                $seller_status = $result[0]['seller_status'];
                $return['result'] = 'false';
                $return['seller_status'] = $seller_status;
                $return['message'] = "You already sent invitation";
                echo json_encode($return);exit;
            }else{

                $CI->Production_model->insert_record('chat_invitation',array('buyer_id' => $buyer_id, 'seller_id' => $seller_id, 'product_id' => $product_id));
            
                /*Notification start*/
                $sender_record = $CI->Production_model->get_all_with_where('user_register','','',array("user_id"=>$buyer_id));

                $notification_token = $CI->Production_model->get_all_with_where('notification_token','','',array("user_id"=>$seller_id));

                $get_product_name = $CI->Production_model->get_all_with_where('post_management','','',array("post_id"=>$product_id));

                // echo $this->db->last_query(); echo"<pre>"; print_r($notification_token); exit;

                if (isset($notification_token) && $notification_token !=null && isset($sender_record) && $sender_record !=null) {
                    foreach ($notification_token as $key => $token_row) {
                        $CI->Production_model->sendFcmNotification(2,json_encode(array(
                            "type"=>2, //Chat invitation
                            "title"=> 'Chat invitation received',
                            "message"=>  $sender_record[0]['name'].' has sent invitation for chat (Postname : '.$get_product_name[0]['i_want'].')',
                            "image"=>'',
                            "product_id"=>$product_id,
                            "buyer_id"=>$buyer_id,
                        )),$seller_id,array($token_row['token']),1,'1');
                    }
                }
                /*Notification end*/
                $return['seller_status'] = '0';
                $return['result'] = 'true';
                $return['message'] = "Your invitation sent successfully";
                echo json_encode($return);exit;
            }
    	}
    }
}

if (!function_exists('accept_reject_chat_invitation')) {
	function accept_reject_chat_invitation($product_id ='',$buyer_id ='',$seller_id='',$status='')
    {
    	$CI =& get_instance();
    	if(empty($buyer_id) || empty($seller_id)){
            $return['result'] = "fail";
            $return['message'] = "Blank data...!";
            echo json_encode($return);exit;
        }else{
            $result = $CI->Production_model->get_all_with_where('chat_invitation', '', '', array('buyer_id' => $buyer_id, 'seller_id' => $seller_id, 'product_id'=>$product_id));
            if(count($result) > 0 ){
                $msgStatus = $status ==1 ? "Accept" : "Reject";

                if($result[0]['seller_status']==2){
                    $return['result'] = "fail";
                    $return['message'] = "You already reject chat invitation";
                    $return['post_status'] = 'Reject';
                    echo json_encode($return);exit;    
                }else if($result[0]['seller_status']==1){
                    $return['result'] = "fail";
                    $return['message'] = "You already accepted chat invitation";
                    $return['post_status'] = 'Accept';
                    echo json_encode($return);exit;    
                }else{

                    /*Notification start*/
                    $sender_record = $CI->Production_model->get_all_with_where('user_register','','',array("user_id"=>$seller_id));
                    
                    $notification_token = $CI->Production_model->get_all_with_where('notification_token','','',array("user_id"=>$buyer_id));

                    $get_product_name = $CI->Production_model->get_all_with_where('post_management','','',array("post_id"=>$product_id));

                    if (isset($notification_token) && $notification_token !=null && isset($sender_record) && $sender_record !=null) {
                        foreach ($notification_token as $key => $token_row) {
                            $CI->Production_model->sendFcmNotification(2,json_encode(array(
                                "type"=>2, //Chat invitation
                                "title"=> 'Chat invitation '.$msgStatus,
                                "message"=>  'Your chat with '.$sender_record[0]['name'].' is '.$msgStatus . ' (Postname : '.$get_product_name[0]['i_want'].')',
                                "image"=>'',
                                "product_id"=>$product_id,
                                "buyer_id"=>$seller_id,
                            )),$buyer_id,array($token_row['token']),1,'1');
                        }
                    }
                    /*Notification end*/

                    $CI->Production_model->update_record('chat_invitation', array('seller_status' => $status), array('buyer_id' => $buyer_id, 'seller_id' => $seller_id,'product_id'=>$product_id));
                    $return['result'] = "success";
                    $return['message'] = "You " .$msgStatus. " chat invitation";
                    $return['post_status'] = $msgStatus;
                    echo json_encode($return);exit;
                }
            }else{
                $return['result'] = "fail";
                $return['message'] = "Chat receiver user not found!!!";
                echo json_encode($return);exit;
            }
        }
    }
}

if (!function_exists('add_chat')) {	
	function add_chat($product_id = '',$buyer_id = '',$seller_id = '',$message_send_by = '',$message = '',$last_chat_id = ''){
    	$CI =& get_instance();
        if(empty($product_id) || empty($buyer_id) || empty($seller_id) || empty($message_send_by) || empty($message)){
            $return['result'] = "fail";
            $return['message'] = "Blank data...!";
            echo json_encode($return);exit;
        }else{

        	$data = array(
	    		'product_id' => $product_id,
	    		'buyer_id' => $buyer_id,
	    		'seller_id' => $seller_id,
	    		'message_send_by' => $message_send_by,
	    		'message' => $message
	    	);
	    	$record = $CI->Production_model->insert_record('chat',$data); 
	    	if ($record !=0) {
	    		/*Get last chat record (only use for apk)*/
	    		$rec = array();
	    		if (isset($last_chat_id) && $last_chat_id !=null) {
	    			
		    		$sql = 'SELECT * FROM chat WHERE product_id = '.$product_id.' AND buyer_id = '.$buyer_id.' AND seller_id = '.$seller_id.' AND is_viewed=0 AND id BETWEEN '.$last_chat_id.' AND '.$record.'';
		    		$query = $CI->db->query($sql);
	        		$get_chat_details = $query->result_array();
			        // echo"<pre>"; echo $CI->db->last_query(); print_r($get_chat_details); exit;

			        if (isset($get_chat_details) && $get_chat_details != null) {
			        	foreach ($get_chat_details as $key => $value) {
			        		$get_name = $CI->Production_model->get_where_user('user_id,name','user_register','','',array('user_id' => $value['message_send_by']));
			        		$datetime = date('d-m-Y h:i:s A',strtotime($value['created_at']));
			        		
			        		if ($value['message_send_by'] != $message_send_by) { //seller-messages. 
				            	$count = 0; 
					        	foreach ($get_chat_details as $key => $value) {
					        		$get_name = $CI->Production_model->get_where_user('user_id,name','user_register','','',array('user_id' => $value['message_send_by']));

					        		$datetime = date('d-m-Y h:i:s A',strtotime($value['created_at']));
					        		$rec[$count]['chat_id'] = $value['id'];
					        		$rec[$count]['buyer_id'] = $value['buyer_id'];
				                    $rec[$count]['seller_id'] = $value['seller_id'];
				                    $rec[$count]['name'] = $get_name[0]['name'];
				                    $rec[$count]['product_id'] = $value['product_id'];
				                    $rec[$count]['message'] = $value['message'];
				                    $rec[$count]['message_send_by'] = $value['message_send_by'];
				                    $rec[$count]['is_viewed'] = $value['is_viewed'];
				                    $rec[$count]['created_date'] = $datetime;
				                    $count++;           
					        	}
			        		}
			        	}		
			        }
			    }
	    		/*End*/
		    	
		    	$return['result'] = 'success';
		    	$return['chat_id'] = $record;
		    	$return['data'] = $rec;
		        $return['message'] = "";
        		echo json_encode($return);exit;
	    	}   
        }
    }
}

if (!function_exists('user_chat_history')) {	
	function user_chat_history($product_id = '',$buyer_id = '',$seller_id = '',$message_send_by = ''){ //User chat history.   	      
    	$CI =& get_instance();
        if(empty($product_id) || empty($buyer_id) || empty($seller_id) || empty($message_send_by)){
            $return['result'] = "fail";
            $return['message'] = "Blank data...!";
            echo json_encode($return);exit;
        }else{

	        $update_record = array('is_viewed'=>'1');
	        $update_chat_details = $CI->Production_model->update_record('chat',$update_record,array('product_id' => $product_id,'buyer_id' => $buyer_id,'seller_id' => $seller_id,'message_send_by !='=>$message_send_by));

	        $get_chat_details = $CI->Production_model->get_all_with_where('chat','id','asc', array('product_id' => $product_id,'buyer_id' => $buyer_id,'seller_id' => $seller_id));

	        // echo"<pre>"; echo $CI->db->last_query(); print_r($get_chat_details); exit;

	        if (isset($get_chat_details) && $get_chat_details != null) {
	        	$rec = array(); 
            	$count = 0; 
	        	foreach ($get_chat_details as $key => $value) {
	        		$get_name = $CI->Production_model->get_where_user('user_id,name','user_register','','',array('user_id' => $value['message_send_by']));

	        		$datetime = date('d-m-Y h:i:s A',strtotime($value['created_at']));
	        		$rec[$count]['chat_id'] = $value['id'];
	        		$rec[$count]['buyer_id'] = $value['buyer_id'];
                    $rec[$count]['seller_id'] = $value['seller_id'];
                    $rec[$count]['name'] = $get_name[0]['name'];
                    $rec[$count]['product_id'] = $value['product_id'];
                    $rec[$count]['message'] = $value['message'];
                    $rec[$count]['message_send_by'] = $value['message_send_by'];
                    $rec[$count]['is_viewed'] = $value['is_viewed'];
                    $rec[$count]['created_date'] = $datetime;
                    $count++;           
	        	}
	        	$return['result'] = "success";
	            $return['data'] = $rec;
	            echo json_encode($return); 
	        }
	        else{
	        	$return['result'] = "fail";
                $return['message'] = "Chat not available for this post...!";
                echo json_encode($return);exit;
	        }
	    }
    }
}

if (!function_exists('get_chat_message')) {	
	// 1.message_send_by = user-login-id. 
	function get_chat_message($product_id = '',$buyer_id = '',$seller_id = '',$message_send_by = '',$chat_id = ''){ 
    	$CI =& get_instance();
        if(empty($product_id) || empty($buyer_id) || empty($seller_id) || empty($message_send_by) || empty($chat_id)){
            $return['result'] = "fail";
            $return['message'] = "Blank data...!";
            echo json_encode($return);exit;
        }else{
        	// $get_chat_details = $CI->Production_model->get_all_with_where('chat','id','asc', array('product_id' => $product_id,'buyer_id' => $buyer_id,'seller_id' => $seller_id,'is_viewed'=>'0'));
        	$get_chat_details = $CI->Production_model->get_all_with_where('chat','id','asc', array('product_id' => $product_id,'buyer_id' => $buyer_id,'seller_id' => $seller_id,'is_viewed'=>'0','id >'=>$chat_id));
	        // echo"<pre>"; echo $CI->db->last_query(); print_r($get_chat_details); exit;

	        $update_record = array('is_viewed'=>'1');
	        $update_chat_details = $CI->Production_model->update_record('chat',$update_record,array('product_id' => $product_id,'buyer_id' => $buyer_id,'seller_id' => $seller_id,'message_send_by !='=>$message_send_by));

	        $rec = array();
	        if (isset($get_chat_details) && $get_chat_details != null) {
	        	foreach ($get_chat_details as $key => $value) {
	        		$get_name = $CI->Production_model->get_where_user('user_id,name','user_register','','',array('user_id' => $value['message_send_by']));
	        		$datetime = date('d-m-Y h:i:s A',strtotime($value['created_at']));
	        		
	        		// if ($value['message_send_by'] != $message_send_by) { //seller-messages. 
		            	$count = 0; 
			        	foreach ($get_chat_details as $key => $value) {
			        		$get_name = $CI->Production_model->get_where_user('user_id,name','user_register','','',array('user_id' => $value['message_send_by']));

			        		$datetime = date('d-m-Y h:i:s A',strtotime($value['created_at']));
			        		$rec[$count]['chat_id'] = $value['id'];
			        		$rec[$count]['buyer_id'] = $value['buyer_id'];
		                    $rec[$count]['seller_id'] = $value['seller_id'];
		                    $rec[$count]['name'] = $get_name[0]['name'];
		                    $rec[$count]['product_id'] = $value['product_id'];
		                    $rec[$count]['message'] = $value['message'];
		                    $rec[$count]['message_send_by'] = $value['message_send_by'];
		                    $rec[$count]['is_viewed'] = $value['is_viewed'];
		                    $rec[$count]['created_date'] = $datetime;
		                    $count++;           
			        	}
	        		// }
	        	}
	        	$return['result'] = "success";
	            $return['data'] = $rec;
	            echo json_encode($return); exit; 		
	        }
	        else{
	        	$return['result'] = "fail";
	            echo json_encode($return); exit;	
	        }
	    }
    }
}

if (!function_exists('get_chat_list')) {
    function get_chat_list($user_id){
       $CI =& get_instance();

       $sql = "SELECT notification.*,notification_status.read_status,notification_status.remove_status,post_management.add_type,post_management.i_want,post_management.post_image,post_management.post_description,post_management.price,post_management.order_id,post_management.package_id,post_management.payment_details,chat_invitation.seller_status,
       IF(notification.product_id=chat_invitation.product_id AND chat_invitation.seller_id=notification.user_id , 'Show', 'Hide') as buttons

        FROM notification
        LEFT JOIN notification_status ON notification_status.notification_id = notification.notification_id
        LEFT JOIN post_management ON post_management.post_id = notification.product_id
        LEFT JOIN chat_invitation ON chat_invitation.product_id = notification.product_id
        WHERE notification.user_id = '".$user_id."' AND notification.type=2 AND chat_invitation.seller_status=1 ORDER BY notification.notification_id DESC";

        $query = $CI->db->query($sql);
        $get_user = $query->result_array();

        // echo"<pre>"; echo $CI->db->last_query(); print_r($get_user); exit; 

        if ($get_user !=null) { 
            $rec = array(); 
            $chat_data = array();
            $count = 0; 
            foreach ($get_user as $key => $value) {  
                $owner_name = $CI->Production_model->get_all_with_where('user_register','','',array("user_id"=>$value['buyer_id']));             
                
                if ($value['seller_status'] == 0) {
                    $status = 'Pending';
                }
                if ($value['seller_status'] == 1) {
                    $status = 'Accept';
                }
                if ($value['seller_status'] == 2) {
                    $status = 'Reject';
                }
                $rec[$count]['notification_id'] = $value['notification_id'];
                $rec[$count]['title'] = $value['title'];
                $rec[$count]['message'] = $value['description'];
                $rec[$count]['image'] = $value['image'];
                $rec[$count]['type'] = $value['type'];

                $rec[$count]['read_status'] = $value['read_status'];
                $rec[$count]['remove_status'] = $value['remove_status'];
                $rec[$count]['product_id'] = $value['product_id'];
                $rec[$count]['user_id'] = $value['user_id'];
                $rec[$count]['buyer_id'] = $value['buyer_id'];
                $rec[$count]['add_type'] = $value['add_type'];
                $rec[$count]['i_want'] = $value['i_want'];
                $rec[$count]['post_description'] = $value['post_description'];
                $rec[$count]['post_status'] = $status;
                $rec[$count]['buttons'] = $value['buttons'];
                $rec[$count]['created_date'] = date('d-m-Y h:i:s',strtotime($value['create_date']));

                $chat_data = array(
                    'post_owner_name' => $owner_name[0]['name'],
                    'profile_picture' => $owner_name[0]['profile_picture'],
                    'post_title' => $value['i_want'],
                    'post_description' => $value['post_description'],
                    'status' => $status,
                );
                $rec[$count]['chat_data'] = $chat_data;
                $count++; 
            }   
            
            $return['result'] = "success";
            $return['message'] = "Chat Found Successfully...!";
            $return['data'] = $rec;
            return json_encode($return); 
        }
        else{
            $return['result'] = "fail";
            $return['message'] = "Chat not available...!";
            return json_encode($return);
        }
    } 
}

if (!function_exists('delete_chat')) { 
    function delete_chat($chat_id = ''){ 
        $CI =& get_instance();
        if(empty($chat_id)){
            $return['result'] = "fail";
            $return['message'] = "Blank data...!";
            echo json_encode($return);exit;
        }else{
            $id = explode(',',$chat_id);
            $record = $CI->Production_model->get_delete_where_in('chat','id',$id);

            if ($record != 0) {
                $return['result'] = "success";
                $return['message'] = 'Chat deleted successfully...!';
                echo json_encode($return); exit;        
            }
            else{
                $return['result'] = "fail";
                echo json_encode($return); exit;    
            }
        }
    }
}
?>