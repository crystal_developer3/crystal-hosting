$(document).ready(function () {
    
    remove_link_on_sundays();
    function remove_link_on_sundays() {
        $(".calendar tr").each(function () {
            var month_day = $(this).find("td.day:first a").data("date");
            $(this).find("td.day:first a").closest(".day").html(month_day).addClass("text-danger");
        });
    }
    
    // Return today's date and time
    var currentTime = new Date();
    // returns the month (from 0 to 11)
    var month = currentTime.getMonth() + 1;
    // returns the day of the month (from 1 to 31)
    var day = currentTime.getDate();
    // returns the year (four digits)
    var year = currentTime.getFullYear();

    hide_dates("future");

    function hide_dates(type) {
        $("#year").change(function () {
            if ($(this).val() < year) {
                $("#month option").show();
            } else if (year = $(this).val()) {
                $("#month option").each(function () {
                    if ($(this).val() <= month) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
                /*For Hiding next link*/
                if ($("#month").val() >= month) {
                    $(".next-link").hide();
                } else {
                    $(".next-link").show();
                }
                /*For Hiding future days*/
                if ($("#month").val() == month) {
                    $(".calendar tr a").each(function () {
                        var month_day = $(this).data("date");
                        if ($(this).data("date") > day) {
                            $(this).closest(".day").html(month_day);
                        }
                    });
                }
            } else {
                $("#month option").hide();
            }
        }).trigger("change");
    }
});