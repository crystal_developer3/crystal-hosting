/* loader */
function loader(status) {
    if (status == "show") {
        $('.loader-overlay,.loader-img').show();
    } else {
        $('.loader-overlay,.loader-img').hide();
    }
}

/* For clearing form validation errors */
function clear_errors() {
    $("label.error,div.error").remove();
}
