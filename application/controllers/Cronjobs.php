<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Cronjobs extends CI_Controller {
		
		public function __construct()
		{
			parent::__construct();
		}

		function domain_notify()
		{
			$where = array();
			$where['user_domain.notification_off'] = '0';

			$notify_data = get_notify_days();
			$days = array();
			foreach ($notify_data as $key => $value) {
				$days[]= $value['days'];
			}
			// print_r($days);exit;
			$having['days <='] = max($days);

			$join[0]['table_name'] = 'user_register';
			$join[0]['column_name'] = 'user_register.id = user_domain.user_id';
			$join[0]['type'] = 'left';

	        $join[1]['table_name'] = 'domain';
			$join[1]['column_name'] = 'domain.id = user_domain.domain_type_id';
			$join[1]['type'] = 'left';

			$user_domains_details = $this->Production_model->jointable_descending_having(array('user_domain.*','concat(user_register.first_name," ",user_register.last_name) as name','user_register.email', 'user_domain.id','domain.title','ABS(DATEDIFF(CURDATE(),STR_TO_DATE(expiry_date, "%Y-%m-%d"))) AS days','concat(user_domain.domain_name,domain.title) as domain_name'), 'user_domain', '', $join, 'user_domain.id', 'desc', $where,'','','','',$having);

			// echo"<pre>";echo $this->db->last_query();print_r($user_domains_details);exit;

			foreach ($user_domains_details as $key => $value) {
				$remain_days = $value['days'];
				if(in_array($remain_days, $days)){
					// echo "sent";
					$send_mail = $this->Production_model->mail_send('Notification',$value['email'],'','mail_form/thankyou_page/domain_notification',$value,''); // Notification mail 
				}else{
					// echo "not sent";
				}
			}
		}

		function hosting_notify()
		{
			$where = array();
			$where['user_hosting.notification_off'] = '0';

			$notify_data = get_notify_days();
			$days = array();
			foreach ($notify_data as $key => $value) {
				$days[]= $value['days'];
			}
			// print_r($days);exit;
			$having['days <='] = max($days);

			$join[0]['table_name'] = 'user_register';
			$join[0]['column_name'] = 'user_register.id = user_hosting.user_id';
			$join[0]['type'] = 'left';

			$user_hostings_details = $this->Production_model->jointable_descending_having(array('user_hosting.*','concat(user_register.first_name," ",user_register.last_name) as name','user_register.email', 'user_hosting.id','ABS(DATEDIFF(CURDATE(),STR_TO_DATE(expiry_date, "%Y-%m-%d"))) AS days',), 'user_hosting', '', $join, 'user_hosting.id', 'desc', $where,'','','','',$having);

			// echo"<pre>";echo $this->db->last_query();print_r($user_hostings_details);exit;
			foreach ($user_hostings_details as $key => $value) {
				$remain_days = $value['days'];
				if(in_array($remain_days, $days)){
					// echo "sent";
					$send_mail = $this->Production_model->mail_send('Notification',$value['email'],'','mail_form/thankyou_page/domain_notification',$value,''); // Notification mail 
				}else{
					// echo "not sent";
				}
			}
		}
	}
?>