<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class My_invoices extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();		
			$this->column_search = array('invoice_no','invoice_date','due_date','total_amount','status');	
		}
		function index()
		{
			// $data['invoice_data'] = $this->Production_model->get_all_with_where('invoice','id','desc',array('user_id'=>$this->session->login_id));
			// echo "<pre>".$this->db->last_query();print_r($data);exit;
			$this->load->view('my_invoices');
		}
		function view_invoice($id){
			$where['invoice.id'] = $id;
			$join1[0]['table_name'] = 'invoice_item';
            $join1[0]['column_name'] = 'invoice_item.invoice_id = invoice.id';
            $join1[0]['type'] = 'left';
            
            $data['invoice_details'] = $this->Production_model->jointable_descending(array('invoice.*','invoice_item.description','invoice_item.price','invoice_item.quantity','invoice_item.total'), 'invoice', '', $join1, 'invoice.id', 'desc', $where);
			$data['invoice_details'] = $this->Production_model->get_all_with_where('user','','',array());
			$this->load->view('invoice',$data);	
		}
		function getLists(){
	        $data = $row = array();

	        $where['invoice.user_id'] = $this->session->login_id;
			$join1[0]['table_name'] = 'invoice_item';
            $join1[0]['column_name'] = 'invoice_item.invoice_id = invoice.id';
            $join1[0]['type'] = 'left';
            
            // $invoice_details = $this->Production_model->jointable_descending(array('invoice.*','invoice_item.description','invoice_item.price','invoice_item.quantity','invoice_item.total'), 'invoice', '', $join1, 'invoice.id', 'desc', $where);
            $invoice_details = $this->Production_model->get_all_with_where('invoice','id','desc',array('user_id'=>$this->session->login_id));
			// $data['invoice_details'] = $this->Production_model->get_all_with_where('user','','',array());
	        // echo"<pre>";print_r($invoice_details); exit;

	        // Fetch member's records
	        
	        $i = $_POST['start'];
	        foreach($invoice_details as $key => $value){

	        	switch ($value['payment_status']) {
	                case '0':
	                  $payment_status_class = 'dactive-btn';
	                  $payment_status_label = 'Unpaid';
	                  break;
	                case '1':
	                  $payment_status_class = 'paid-btn';
	                  $payment_status_label = 'Paid';
	                  break;
	                case '2':
	                  $payment_status_class = 'dactive-btn';
	                  $payment_status_label = 'Cancelled';
	                  break;
	                default:
	                  break;
	            }
	            $i++;
	            $created = format_date_d_m_y($value['create_date']);
	            $status = '<a class="active-btn" href="<?=site_url(); ?>generated_invoices/'.$value['file_name'].'" target="_blank">View</a>
                                    <a class="<?=$payment_status_class?>" href=""><?=$payment_status_label?></a>
                                    <a class="pay-btn" href="#">Pay</a>';

	            $data[] = array($i, $value['invoice_no'], format_date_d_m_y($value['invoice_date']), format_date_d_m_y($value['due_date']), $value['total_amount'], $status);
	        }
	        
	        $output = array(
	            "draw" => $_POST['draw'],
	            "recordsTotal" => count($invoice_details),
	            "recordsFiltered" => $this->countFiltered($_POST),
	            "data" => $data,
	        );
	        // Output to JSON format
	        echo json_encode($output);
	    }
	    public function getRows($postData){
	        $this->_get_datatables_query($postData);
	        if($postData['length'] != -1){
	            $this->db->limit($postData['length'], $postData['start']);
	        }
	        $query = $this->db->get();
	        return $query->result();
	    }
	    private function _get_datatables_query($postData){
         
	        $this->db->where('user_id',$this->session->login_id);
	        $this->db->from('invoice');
	 
	        $i = 0;
	        // loop searchable columns 
	        foreach($this->column_search  as $item){
	            // if datatable send POST for search
	            if($postData['search']['value']){
	                // first loop
	                if($i===0){
	                    // open bracket
	                    $this->db->group_start();
	                    $this->db->like($item, $postData['search']['value']);
	                }else{
	                    $this->db->or_like($item, $postData['search']['value']);
	                }
	                
	                // last loop
	                if(count($this->column_search) - 1 == $i){
	                    // close bracket
	                    $this->db->group_end();
	                }
	            }
	            $i++;
	        }
	        
	        if(isset($postData['order'])){
	            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
	        }else if(isset($this->order)){
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	    public function countFiltered($postData){
	        $this->_get_datatables_query($postData);
	        $query = $this->db->get();
	        return $query->num_rows();
	    }
	}
?>