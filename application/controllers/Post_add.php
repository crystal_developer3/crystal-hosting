<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_add extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    function index() {
        $data['category_details'] = $this->Production_model->get_all_with_where('category', 'category_id', 'asc', array('status' => '1'));

        $data['user_details'] = $this->Production_model->get_all_with_where('user_register', '', '', array('user_id' => $this->session->userdata('login_id')));

        $data['package_details'] = $this->Production_model->get_all_with_where('package_management', 'position', 'asc', array('status' => '1'));

        // echo"<pre>";echo $this->db->last_query(); print_r($data); exit;
        $this->load->view('post_add', $data);
    }

    function add_post() {
        // echo"<pre>"; print_r($_POST); exit;		
        // $data['create_date'] = date('Y-m-d H:i:s');
        //     	if ($this->session->userdata('login_id') == null) {
        //     		$this->session->set_flashdata('error', 'Please login after add this post...!');
        // redirect($_SERVER['HTTP_REFERER']);
        //     	}
        //     	else{
        // echo"<pre>"; print_r($_FILES); exit;

        /* Auto registration in post add */
        if ($this->session->userdata('login_id') == null) {

            $add_reg = array(
                'name' => $this->input->post('name'),
                'mobile_no' => $this->input->post('mobile_no'),
                'user_email' => $this->input->post('user_email'),
                'password' => $this->encryption->encrypt($this->input->post('password'))
            );
            // unset($this->input->post('confirm_password'));

            $add_reg['type'] = 'web';
            // echo "<pre>"; print_r($add_reg); exit;

            $get_user_email = $this->Production_model->get_all_with_where('user_register', '', '', array('user_email' => $this->input->post('user_email')));
            if (count($get_user_email) > 0) {
                $this->session->set_flashdata('error', 'Email id allredy exist....!');
                redirect($_SERVER['HTTP_REFERER']);
            }

            $get_user_mobile = $this->Production_model->get_all_with_where('user_register', '', '', array('mobile_no' => $this->input->post('mobile_no')));
            if (count($get_user_mobile) > 0) {
                $this->session->set_flashdata('error', 'Mobile no allredy exist....!');
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                // echo"<pre>"; print_r($add_reg); exit;		
                // $this->load->view('mail_form/admin_send_mail/registration', $add_reg);		
                $record = $this->Production_model->insert_record('user_register', $add_reg);
                if ($record != '') {
                    $send_mail = $this->Production_model->mail_send('Crystal Hositing Account', $this->input->post('user_email'), '', 'mail_form/thankyou_page/registration', '', ''); // user send email thank-you page

                    $admin_email = $this->Production_model->get_all_with_where('user', '', '', array());
                    $send_mail = $this->Production_model->mail_send('Crystal Hositing Account', $admin_email[0]['email_address'], '', 'mail_form/admin_send_mail/registration', $add_reg, ''); // admin send mail
                    // if ($send_mail == 1) {
                    // }
                    // $this->session->set_flashdata('success', 'Thank you for registration');
                    // redirect($_SERVER['HTTP_REFERER']);
                }

                /* Post complete after login user is automatically */
                $session = array(
                    'login_id' => $record,
                    'username' => $this->input->post('name'),
                    'useremail' => $this->input->post('user_email'),
                    'useremobile' => $this->input->post('mobile_no'),
                );
                $this->session->set_userdata($session);
            }
        }
        /* End */

        $data = $this->input->post();
        $order_id = rand(00000000, 99999999);
        $data['order_id'] = $order_id;
        $data['ads_status'] = 0;
        // echo"<pre>"; print_r($data); exit;
        $data['user_id'] = $this->session->userdata('login_id') == null ? $record : $this->session->userdata('login_id');

        unset($data['name']);
        unset($data['mobile_no']);
        unset($data['user_email']);
        unset($data['password']);
        unset($data['confirm_password']);
        unset($data['similar_image']);
        // echo"<pre>"; print_r($data); exit;

        $record = $this->Production_model->insert_record('post_management', $data);
        if ($record != '') {
            //=========================================================================//
            //======================= multiple image upload start =====================//
            //=========================================================================//

            if (!is_dir(POST_NEED_IMG)) {
                mkdir(POST_NEED_IMG);
                mkdir(POST_NEED_IMG . 'thumbnail/');
                @chmod(POST_NEED_IMG, 0777);
            }
            // $new_name = 'POST_NEED_IMG' . rand();

            $config['upload_path'] = POST_NEED_IMG;
            $config['allowed_types'] = '*';
            $config['encrypt_name'] = TRUE;
            // $config['file_name'] = $new_name;

            $this->load->library('upload', $config);

            $files = $_FILES;
            $cpt = count($_FILES['similar_image']['name']);

            if ($files['similar_image']['name'][0] != "") {
                for ($i = 0; $i < $cpt; $i++) {
                    $_FILES['similar_image']['name'] = $files['similar_image']['name'][$i];
                    $_FILES['similar_image']['type'] = $files['similar_image']['type'][$i];
                    $_FILES['similar_image']['tmp_name'] = $files['similar_image']['tmp_name'][$i];
                    $_FILES['similar_image']['error'] = $files['similar_image']['error'][$i];
                    $_FILES['similar_image']['size'] = $files['similar_image']['size'][$i];

                    // $this->load->library('upload',$config);
                    $this->upload->do_upload('similar_image');
                    $image_name = $this->upload->data();
                    $image_name = $image_name['file_name'];

                    $this->Production_model->generate_thumbnail(POST_NEED_IMG, $image_name);

                    $imagedata = array(
                        'post_id' => $record,
                        'user_id' => $this->session->userdata('login_id'),
                        'similar_image' => $image_name,
                        'create_date' => date('Y-m-d H:i:s')
                    );

                    // echo "<pre>";print_r($imagedata);
                    $image_id = $this->Production_model->insert_record('post_similer_image', $imagedata);
                }
            }
            // exit;
            //=========================================================================//
            //======================= multiple image upload end =======================//
            //=========================================================================//
            // Add Mail Code Here....
            //$to_mail = $this->config->item('from_email');
            // $send_mail = $this->Production_model->send_email('Otg contact-us','milan.v@crystalinfoway.com','','mail_form/contact_form',$data,''); 

            $user_info = $this->Production_model->get_all_with_where('user_register', '', '', array('user_id' => $data['user_id']));
            $user_info = $user_info[0];

            // $post_info = $this->Production_model->get_all_with_where('post_management', '', '', array('post_id' => $record));
            $post_info = $this->Production_model->get_where_user('post_id,category_id,sub_category_id,add_type,i_want,post_image,post_description,price,user_id,order_id,package_id,payment_details,invoice,create_date as post_create_date','post_management','','',array('post_id' => $record));
            $post_info = $post_info[0];
            // echo"<pre>"; print_r($post_info); exit;
            // $data = array_merge($data, $user_info);

            $package_info = $this->Production_model->get_where_user('id,name as package_name, currency,day_month,day,discription,price as package_price','package_management','','',array('id' => $data['package_id']));
            $package_info = $package_info[0];

            $data = array();
            unset($user_info['password']);
            $data = array_merge($data, $package_info,$user_info,$post_info);

            $data['get_post_image'] = $this->Production_model->get_all_with_where('post_similer_image', '', '', array('post_id' => $record));
            // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;	
            // $this->load->view('mail_form/admin_send_mail/postneed', $data);	

            //================ genrate pdf in order details start ================//
                    
            require_once 'dompdf/autoload.inc.php';
            $dompdf = new Dompdf\Dompdf(array('enable_remote' => true));                
            $html = ($this->load->view('order_genrate/order_invoice',$data,true));
            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'landscape');
            $dompdf->render();
            // $pdf = $dompdf->output();     
            // $dompdf->stream();
            $file_name = date('d-m-Y').rand(00000000,11111111).'-order-invoice.pdf';
            file_put_contents('order_details_page/'.$file_name, $dompdf->output()); // file save in folder
            // echo base_url('order_details_page/'.$file_name); exit;

            /*Save invoice genrate name*/
            $update_invoice = array('invoice'=>$file_name);
            $record = $this->Production_model->update_record('post_management',$update_invoice,array('post_id'=>$record));
            /*End*/

            //================= genrate pdf in order details end =================//

            $send_mail = $this->Production_model->mail_send('Crystal Hositing-post', $this->session->userdata('useremail'), '', 'mail_form/admin_send_mail/postneed', $data, ''); // user send email thank-you page

            $admin_email = $this->Production_model->get_all_with_where('user', '', '', array());
            $send_mail = $this->Production_model->mail_send('Crystal Hositing-post', $admin_email[0]['email_address'], '', 'mail_form/admin_send_mail/postneed', $data, ''); // admin send mail

            if ($send_mail == 1) {
                $this->session->set_flashdata('success', 'Thank you for your post our executive will contact you soon.');
                redirect(base_url('home'));
            }
            redirect(base_url('home'));
        } else {
            $this->session->set_flashdata('error', 'Please try later...!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        // }
    }

    /**
     * This function is used for an online payment
     */
    function add_post_online() {
        $error = false;
        if ($this->session->userdata('login_id') == null) {

            $add_reg = array(
                'name' => $this->input->post('name'),
                'mobile_no' => $this->input->post('mobile_no'),
                'user_email' => $this->input->post('user_email'),
                'password' => $this->encryption->encrypt($this->input->post('password'))
            );

            $add_reg['type'] = 'web';

            $get_user_email = $this->Production_model->get_all_with_where('user_register', '', '', array('user_email' => $this->input->post('user_email')));
            if (count($get_user_email) > 0) {
                $this->session->set_flashdata('error', 'Email id allredy exist....!');
                redirect($_SERVER['HTTP_REFERER']);
            }

            $get_user_mobile = $this->Production_model->get_all_with_where('user_register', '', '', array('mobile_no' => $this->input->post('mobile_no')));
            if (count($get_user_mobile) > 0) {
                $this->session->set_flashdata('error', 'Mobile no allredy exist....!');
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                // echo"<pre>"; print_r($add_reg); exit;		
                // $this->load->view('mail_form/admin_send_mail/registration', $add_reg);		
                $record = $this->Production_model->insert_record('user_register', $add_reg);
                if ($record != '') {
                    $send_mail = $this->Production_model->mail_send('Crystal Hositing Account', $this->input->post('user_email'), '', 'mail_form/thankyou_page/registration', '', ''); // user send email thank-you page

                    $admin_email = $this->Production_model->get_all_with_where('user', '', '', array());
                    $send_mail = $this->Production_model->mail_send('Crystal Hositing Account', $admin_email[0]['email_address'], '', 'mail_form/admin_send_mail/registration', $add_reg, ''); // admin send mail                    
                }

                /* Post complete after login user is automatically */
                $session = array(
                    'login_id' => $record,
                    'username' => $this->input->post('name'),
                    'useremail' => $this->input->post('user_email'),
                    'useremobile' => $this->input->post('mobile_no'),
                );
                $this->session->set_userdata($session);
            }
        }

        $data = $this->input->post();
        $data['user_id'] = $this->session->userdata('login_id') == null ? $record : $this->session->userdata('login_id');

        unset($data['name']);
        unset($data['mobile_no']);
        unset($data['user_email']);
        unset($data['password']);
        unset($data['confirm_password']);
        unset($data['similar_image']);


        $user_info = $this->Production_model->get_all_with_where('user_register', '', '', array('user_id' => $data['user_id']));
        if (empty($user_info)) {
            $error = true;
            $this->session->set_flashdata('error', 'Please try again');
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $user_info = $user_info[0];
        }

        $package_info = $this->Production_model->get_all_with_where('package_management', '', '', array('id' => $data['package_id']));
        if (empty($user_info)) {
            $error = true;
            $this->session->set_flashdata('error', 'Please select correct plan');
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $package_info = $package_info[0];
        }

        $order_id = rand(00000000, 99999999);
        $data['order_id'] = $order_id;
        $data['ads_status'] = 1;

        $payment_data = array(
            'orderId' => $order_id,
            'orderAmount' => isset($package_info['price']) ? floatval($package_info['price']) : 0,
            'orderCurrency' => isset($package_info['currency']) && $package_info['currency'] != '' ? $package_info['currency'] : 'INR',
            'orderNote' => '',
            'customerName' => isset($user_info['name']) ? $user_info['name'] : '',
            'customerPhone' => isset($user_info['mobile_no']) ? $user_info['mobile_no'] : '',
            'customerEmail' => isset($user_info['user_email']) ? $user_info['user_email'] : '',
            'returnUrl' => base_url() . 'post_add/payment_success',
            'notifyUrl' => base_url() . 'post_add/payment_failure',
        );
        // echo"<pre>"; print_r($data); exit;
        $record = $this->Production_model->insert_record('post_management', $data);
        if ($record != '') {

            if (!is_dir(POST_NEED_IMG)) {
                mkdir(POST_NEED_IMG);
                mkdir(POST_NEED_IMG . 'thumbnail/');
                @chmod(POST_NEED_IMG, 0777);
            }
            $new_name = 'POST_NEED_IMG' . rand();

            $config['upload_path'] = POST_NEED_IMG;
            $config['allowed_types'] = '*';
            $config['file_name'] = $new_name;

            $this->load->library('upload', $config);

            $files = $_FILES;
            $cpt = count($_FILES['similar_image']['name']);

            if ($files['similar_image']['name'][0] != "") {
                for ($i = 0; $i < $cpt; $i++) {
                    $_FILES['similar_image']['name'] = $files['similar_image']['name'][$i];
                    $_FILES['similar_image']['type'] = $files['similar_image']['type'][$i];
                    $_FILES['similar_image']['tmp_name'] = $files['similar_image']['tmp_name'][$i];
                    $_FILES['similar_image']['error'] = $files['similar_image']['error'][$i];
                    $_FILES['similar_image']['size'] = $files['similar_image']['size'][$i];

                    // $this->load->library('upload',$config);
                    $this->upload->do_upload('similar_image');
                    $image_name = $this->upload->data();
                    $image_name = $image_name['file_name'];

                    $this->Production_model->generate_thumbnail(POST_NEED_IMG, $image_name);

                    $imagedata = array(
                        'post_id' => $record,
                        'user_id' => $this->session->userdata('login_id'),
                        'similar_image' => $image_name,
                        'create_date' => date('Y-m-d H:i:s')
                    );

                    $image_id = $this->Production_model->insert_record('post_similer_image', $imagedata);
                }
            }
            $this->load->view('cashfree-signature', $payment_data);
        } else {
            $this->session->set_flashdata('error', 'Please try later...!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function payment_success() {

        $secretkey = CASHFREE_SECRET_KEY;
        $orderId = $_POST["orderId"];
        $orderAmount = $_POST["orderAmount"];
        $referenceId = $_POST["referenceId"];
        $txStatus = $_POST["txStatus"];
        $paymentMode = $_POST["paymentMode"];
        $txMsg = $_POST["txMsg"];
        $txTime = $_POST["txTime"];
        $signature = $_POST["signature"];
        $data = $orderId . $orderAmount . $referenceId . $txStatus . $paymentMode . $txMsg . $txTime;
        $hash_hmac = hash_hmac('sha256', $data, $secretkey, true);
        $computedSignature = base64_encode($hash_hmac);
        if ($signature == $computedSignature) {
            $records = array(
                'payment_details' => json_encode($_POST),
                // 'status' => '0',
            );
            $conditions = array(
                'order_id' => $_POST['orderId']
            );
            $this->Production_model->update_record('post_management', $records, $conditions);

            $data = array();

            $post_info = $this->Production_model->get_where_user('post_id,category_id,sub_category_id,add_type,i_want,post_image,post_description,price,user_id,order_id,package_id,payment_details,invoice,create_date as post_create_date','post_management','','',array('order_id' => $_POST['orderId']));

            if (!empty($post_info)) {

                $post_info = $post_info[0];
                $data = array_merge($data, $post_info);

                $user_info = $this->Production_model->get_all_with_where('user_register', '', '', array('user_id' => $post_info['user_id']));
                $user_info = $user_info[0];
                $data = array_merge($data, $user_info);

                $package_info = $this->Production_model->get_where_user('id,name as package_name, currency,day_month,day,discription,price as package_price','package_management','','',array('id' => $post_info['package_id']));
                $package_info = $package_info[0];
                $data = array_merge($data, $package_info);

                $data['get_post_image'] = $this->Production_model->get_all_with_where('post_similer_image', '', '', array('post_id' => $post_info['post_id']));

                // $this->load->view('mail_form/admin_send_mail/postneed', $data);
                // echo"<pre>"; print_r($data); 

                //================ genrate pdf in order details start ================//
                    
                require_once 'dompdf/autoload.inc.php';
                $dompdf = new Dompdf\Dompdf(array('enable_remote' => true));                
                $html = ($this->load->view('order_genrate/order_invoice',$data,true));
                $dompdf->loadHtml($html);
                $dompdf->setPaper('A4', 'landscape');
                $dompdf->render();
                // $pdf = $dompdf->output();     
                // $dompdf->stream();
                $file_name = date('d-m-Y').'-'.rand(00000000,11111111).'-order-invoice.pdf';
                file_put_contents('order_details_page/'.$file_name, $dompdf->output()); // file save in folder
                // echo base_url('order_details_page/'.$file_name); exit;

                /*Save invoice genrate name*/
                $update_invoice = array('invoice'=>$file_name);
                $record = $this->Production_model->update_record('post_management',$update_invoice,array('post_id'=>$post_info['post_id']));
                /*End*/

                //================= genrate pdf in order details end =================//

                $send_mail = $this->Production_model->mail_send('Crystal Hositing-post', $this->session->userdata('useremail'), '', 'mail_form/admin_send_mail/postneed', $data, ''); // user send email thank-you page

                $admin_email = $this->Production_model->get_all_with_where('user', '', '', array());

                $send_mail = $this->Production_model->mail_send('Crystal Hositing-post', $admin_email[0]['email_address'], '', 'mail_form/admin_send_mail/postneed', $data, ''); // admin send mail

                if ($send_mail == 1) {
                    $this->session->set_flashdata('success', 'Thank you for your post our executive will contact you soon.');
                    redirect(base_url('home'));
                } else {
                    $this->session->set_flashdata('error', 'Payment failed. Please try again');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            } else {
                $this->session->set_flashdata('error', 'Payment failed. Please try again');
                redirect($_SERVER['HTTP_REFERER']);
            }
        } else {
            $this->session->set_flashdata('error', 'Signature Verification failed. Please try again');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function payment_failure() {
        $secretkey = CASHFREE_SECRET_KEY;
        $orderId = $_POST["orderId"];
        $orderAmount = $_POST["orderAmount"];
        $referenceId = $_POST["referenceId"];
        $txStatus = $_POST["txStatus"];
        $paymentMode = $_POST["paymentMode"];
        $txMsg = $_POST["txMsg"];
        $txTime = $_POST["txTime"];
        $signature = $_POST["signature"];
        $data = $orderId . $orderAmount . $referenceId . $txStatus . $paymentMode . $txMsg . $txTime;
        $hash_hmac = hash_hmac('sha256', $data, $secretkey, true);
        $computedSignature = base64_encode($hash_hmac);
        if ($signature == $computedSignature) {
            $records = array(
                'payment_details' => json_encode($_POST),
                'status' => '0',
            );
            $conditions = array(
                'order_id' => $_POST['orderId']
            );
            $this->Production_model->update_record('post_management', $records, $conditions);
            $this->session->set_flashdata('error', 'Payment failed. Please try again');
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->session->set_flashdata('error', 'Payment failed. Please try again');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    // ajax call category wise all subcategory list start // 

    function get_sub_cat_name() {
        $cat_id = $this->input->post('category_id');
        $sub_cat_id = $this->input->post('sub_category_id');

        $get_sub_cat = $this->Production_model->get_all_with_where('sub_category', 'sub_category_id', 'asc', array('category_id' => $cat_id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($get_sub_cat); exit;
        ?><option value=''>Select subcategory</option><?php
        if ($get_sub_cat != null) {
            foreach ($get_sub_cat as $key => $sub_cat_row) {
                ?><option value="<?= $sub_cat_row['sub_category_id'] ?>" <?= ($sub_cat_row['sub_category_id'] == $sub_cat_id) ? 'selected' : ''; ?>>
                    <?php
                    echo $sub_cat_row['sub_category_name'];
                    ?>
                </option><?php
            }
        }
    }

    // ajax call category wise all subcategory list end // 
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
?>