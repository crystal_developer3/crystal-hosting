<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Home extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();	
			// if(base_url() == "http://localhost/online-cart/"){
			// 	$this->output->enable_profiler(TRUE);
			// }		
		}

		function index()
		{	
			$data['slider_details'] = $this->Production_model->get_all_with_where('home_slider','slider_position','asc',array('status'=>'1'));
			
            $where['post_management.status'] = '1';                       
            $where['post_management.post_approve_reject'] = '1';                       
            $where['post_management.order_status'] = '0'; 
                                  
			$join[0]['table_name'] = 'category';
			$join[0]['column_name'] = 'category.category_id = post_management.category_id';
			$join[0]['type'] = 'left';

			$join[1]['table_name'] = 'sub_category';
			$join[1]['column_name'] = 'sub_category.sub_category_id = post_management.sub_category_id';
			$join[1]['type'] = 'left';

			$join[2]['table_name'] = 'user_register';
			$join[2]['column_name'] = 'user_register.user_id = post_management.user_id';
			$join[2]['type'] = 'left';

			$join[3]['table_name'] = 'country';
			$join[3]['column_name'] = 'country.id = user_register.id_country';
			$join[3]['type'] = 'left';
			
			$join[4]['table_name'] = 'state';
			$join[4]['column_name'] = 'state.id = user_register.id_state';
			$join[4]['type'] = 'left';

			$join[5]['table_name'] = 'city';
			$join[5]['column_name'] = 'city.id = user_register.id_city';
			$join[5]['type'] = 'left';

			$join[6]['table_name'] = 'package_management';
	        $join[6]['column_name'] = 'package_management.id = post_management.package_id';
	        $join[6]['type'] = 'left';

			
			$tmp_data = $this->Production_model->jointable_descending(array('post_management.*','category.category_name','category.cat_image','sub_category.sub_category_name','user_register.name','user_register.user_email','user_register.id_country','user_register.id_state','user_register.id_city','user_register.address','user_register.gender','user_register.mobile_no','country.country_name','state.state_name','city.city_name','package_management.name as package_name','package_management.price as package_price','package_management.currency','package_management.day'),'post_management','',$join,'post_id','desc',$where);

			// echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
			//================= create pagination start ===================//

			$tmp_array['total_record'] = count($tmp_data);
			$tmp_array['url'] = base_url('home/index/page/');
			$tmp_array['per_page'] = RECORDS_PER_PAGE_FRONT;

			$record = $this->Production_model->only_pagination($tmp_array);

			$data['post_details'] = $this->Production_model->jointable_descending(array('post_management.*','category.category_name','category.cat_image','sub_category.sub_category_name','user_register.name','user_register.user_email','user_register.id_country','user_register.id_state','user_register.id_city','user_register.address','user_register.gender','user_register.mobile_no','country.country_name','state.state_name','city.city_name','package_management.name as package_name','package_management.price as package_price','package_management.currency','package_management.day'),'post_management','',$join,'post_id','desc',$where,'','',$record['limit'],$record['start'],$where);			

			$data['pagination'] = $record['pagination'];			
			// echo"<pre>";echo $this->db->last_query(); print_r($data['best_seller_product_details']); exit;
			
			$this->load->view('index',$data);	
		}	

		function filter()
		{	
			$data['slider_details'] = $this->Production_model->get_all_with_where('home_slider','slider_position','asc',array('status'=>'1'));

			$data['category_details'] = $this->Production_model->get_all_with_where('category','category_id','desc',array('status'=>'1'));

			$data = $this->input->post();
			if (isset($data['city_id']) && $data['city_id'] !=null) {
				$where['city.id'] = $data['city_id'];
			}
			if (isset($data['sub_cat_id']) && $data['sub_cat_id'] !=null) {
				$where['sub_category.sub_category_id'] = $data['sub_cat_id'];
			}
			// echo"<pre>"; print_r($data); exit;
			$where['post_management.post_approve_reject'] = '1';                       
            $where['post_management.order_status'] = '0';
            
			$join[0]['table_name'] = 'category';
			$join[0]['column_name'] = 'category.category_id = post_management.category_id';
			$join[0]['type'] = 'left';

			$join[1]['table_name'] = 'sub_category';
			$join[1]['column_name'] = 'sub_category.sub_category_id = post_management.sub_category_id';
			$join[1]['type'] = 'left';

			$join[2]['table_name'] = 'user_register';
			$join[2]['column_name'] = 'user_register.user_id = post_management.user_id';
			$join[2]['type'] = 'left';

			$join[3]['table_name'] = 'country';
			$join[3]['column_name'] = 'country.id = user_register.id_country';
			$join[3]['type'] = 'left';
			
			$join[4]['table_name'] = 'state';
			$join[4]['column_name'] = 'state.id = user_register.id_state';
			$join[4]['type'] = 'left';

			$join[5]['table_name'] = 'city';
			$join[5]['column_name'] = 'city.id = user_register.id_city';
			$join[5]['type'] = 'left';

			$join[6]['table_name'] = 'package_management';
	        $join[6]['column_name'] = 'package_management.id = post_management.package_id';
	        $join[6]['type'] = 'left';
			
			$tmp_data = $this->Production_model->jointable_descending(array('post_management.*','category.category_name','category.cat_image','sub_category.sub_category_name','user_register.name','user_register.user_email','user_register.id_country','user_register.id_state','user_register.id_city','user_register.address','user_register.gender','user_register.mobile_no','country.country_name','state.state_name','city.city_name','package_management.name as package_name','package_management.price as package_price','package_management.currency','package_management.day'),'post_management','',$join,'post_id','desc',$where);

			// echo"<pre>"; print_r($tmp_data); exit;

			$tmp_array['total_record'] = count($tmp_data);
			$tmp_array['url'] = base_url('home/index/page/');			
			$tmp_array['per_page'] = RECORDS_PER_PAGE_FRONT;

			if(isset($data[0]['page_no']) && $data[0]['page_no'] != ''){
				$tmp_array['page_no'] = isset($data[0]['page_no']) ? $data[0]['page_no'] : '';
			}

			$record = $this->Production_model->only_pagination($tmp_array);
			// echo"<pre>"; print_r($record); exit;

			$filteredData = $this->Production_model->jointable_descending(array('post_management.*','category.category_name','category.cat_image','sub_category.sub_category_name','user_register.name','user_register.user_email','user_register.id_country','user_register.id_state','user_register.id_city','user_register.address','user_register.gender','user_register.mobile_no','country.country_name','state.state_name','city.city_name','package_management.name as package_name','package_management.price as package_price','package_management.currency','package_management.day'),'post_management','',$join,'post_id','desc',$where,'','',$record['limit'],$record['start']);

			$pagination= $record['pagination'];
			// echo"<pre>"; echo $this->db->last_query(); print_r($filteredData); exit;
			
			ob_start();
			if (isset($filteredData) && !empty($filteredData) ) { 
				// echo"<pre>" ;print_r($filteredData);exit;
	    		foreach ($filteredData as $key => $value) {
		        	$id = $value['post_id'];	
		        	$created_date = date('Y-m-d',strtotime($value['create_date']));
                	$expire_post = date('Y-m-d',strtotime($created_date . + $value['day'] .'days'));
				
					if (date('Y-m-d') <= $expire_post) { 	        	
					?>
						<div class="col-xl-4 col-lg-6 col-md-4 col-sm-6 col-12">
                            <div class="product-box item-mb zoom-gallery">
                                <div class="item-mask-wrapper">
                                    <div class="">
                                        <img src="<?= base_url(CAT_IMAGE.$value['cat_image'])?>" alt="categories" class="img-fluid">
                                    </div>
                                </div>
                                <div class="item-content">
                                    <!-- <div class="title-ctg">Clothing</div> -->
                                    <h3 class="short-title"><a href="<?= base_url('post-management/details/'.$id)?>"><?= $value['i_want']?></a></h3>
                                    <h3 class="long-title"><a href="<?= base_url('post-management/details/'.$id)?>"><?= $value['i_want']?></a></h3>

                                    <ul class="upload-info">
                                        <li class="date"><i class="fa fa-clock-o" aria-hidden="true"></i><?= date('d M, Y',strtotime($value['create_date']))?></li>
                                        
                                        <li class="place"><i class="fa fa-map-marker" aria-hidden="true"></i><?= $value['country_name'].', '. $value['state_name'].', '. $value['city_name']?></li>
                                        
                                        <li class="tag-ctg"><i class="fa fa-tag" aria-hidden="true"></i><?= $value['category_name']?></li><br><br><br>

                                        <?php 
                                            if (isset($value['sub_category_name']) && $value['sub_category_name'] !=null) {
                                            ?>
                                                <li class="tag-ctg"><i class="fa fa-tag" aria-hidden="true"></i><?= $value['sub_category_name']?></li>
                                            <?php }
                                        ?>
                                    </ul>
                                    <ul class="upload-info">
                                        <li class="tag-ctg"><i class="fa fa-user" aria-hidden="true"></i><?= $value['name']?></li>
                                    </ul>
                                    <p><?= substr($value['post_description'],0,100)?></p>
                                        
                                    <a href="<?= base_url('post-management/details/'.$id)?>" class="product-details-btn">Details</a>
                                </div>
                            </div>
                        </div>
		        	<?php }
		        }		        
	    	}else{
                echo"<center><h4>Post Not Available...!</h4></center>";
	    	}
	    	?>
            <?php
	    	echo ob_get_clean(); 
		}

		function category_filter()
		{
			$category_name = $this->input->post('category_name');

			$filteredData = $this->Production_model->get_all_with_like('category',array('category_name'=>$category_name),array('status'=>'1'),'category_id','desc');

			// echo"<pre>"; echo $this->db->last_query(); print_r($filteredData); exit;

			ob_start();
			if (isset($filteredData) && !empty($filteredData) ) { 
				//print_r($filteredData);exit;
	    		foreach ($filteredData as $key => $cat_row) {
		        	$cat_id = $cat_row['category_id'];
                    $sub_category_details = $this->Production_model->get_all_with_where('sub_category','sub_category_id','desc',array('category_id'=>$cat_id,'status'=>'1'));
                    // echo"<pre>"; print_r($sub_category_details); 	        	
					?>
						<li>
                            <a href="<?= base_url()?>" class="main-cat">
                                <img src="<?= base_url()?>assets/image/house.png" alt="category" class="img-fluid">
                                Homes <span>(0)</span>
                            </a>                                       
                        </li>
						<li>
                            <a href="javascript:void(0)" class="main-cat">
                                <img src="<?= base_url(CAT_IMAGE.$cat_row['cat_image'])?>" alt="category" class="img-fluid">
                                <?= $cat_row['category_name']?> <span>(<?= count($sub_category_details)?>)</span>
                                <span data-toggle="collapse" data-target="#cat-<?= $key+1?>" aria-expanded="false" class="right-arrow">
                                    <?php
                                        if (isset($sub_category_details) && $sub_category_details !=null) {
                                        ?>
                                            <i class="fa" aria-hidden="true"></i>
                                        <?php } 
                                    ?>
                                </span>
                            </a>
                            <div id="cat-<?= $key+1?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body sub-card-body">
                                    <ul>
                                        <?php
                                            if ($sub_category_details !=null) {
                                                foreach ($sub_category_details as $key => $sub_cat_row) {
                                                    $sub_cat_id = $sub_cat_row['sub_category_id'];
                                                    if ($sub_cat_row['category_id'] == $cat_row['category_id']) {
                                                    ?>
                                                        <li>
                                                            <a href="#" class="sub-cat">
                                                                <?= $sub_cat_row['sub_category_name']?>
                                                            </a>
                                                        </li>
                                                    <?php } 
                                                }
                                            } 
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </li>
		        	<?php
		        }		        
	    	}else{
                echo"<center><h4>Category Not Available...!</h4></center>";
	    	}
	    	?>
            <?php
	    	echo ob_get_clean(); 
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>