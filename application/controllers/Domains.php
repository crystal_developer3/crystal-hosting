<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Domains extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();			
		}

		function index()
		{
			$data['domains_data'] = $this->Production_model->get_all_with_where('domain','','',array('status'=>'1'));
			$data['domain_info_data'] = $this->Production_model->get_all_with_where('domain_information','','',array('status'=>'1'));
			// echo"<pre>";echo $this->db->last_query(); print_r($data); exit;
			$this->load->view('domains',$data);	
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>