<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Api extends CI_Controller {
	    function __construct()
		{
			parent::__construct();
			header('Content-Type: application/json');
		}

		public function index(){
			echo 'Index Method Of Api Controller';
		}

		function check_user_exist()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')){
				if(isset($_POST['data']) && !empty($_POST['data'])){
					$post_data = json_decode($_POST['data']); // get posted data
					
					if(!empty($post_data->email) && !empty($post_data->mobileno)){
						$record = $this->Production_model->get_where_or('user_register','','',array('mobile_no' =>$post_data->mobileno,'user_email' => $post_data->email),array());
						// echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;

						if(!empty($record)){
							$return['result'] = "fail";
							$return['message'] = $record[0]['user_email'] == $post_data->email ? "Email address already in record!" : "Mobile no already in record!";
							echo json_encode($return); exit;
						}else{
							$return['result'] = "success";
							$return['message'] = "";
							echo json_encode($return); exit;
						}
					}else{
						$return['result'] = "fail";
						$return['message'] = "Invalid details you entered...!";
						echo json_encode($return); exit;
					}			
				}else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...";
					echo json_encode($return); exit;
				}
			}else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}
		}

		function register()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')){
				
				if(isset($_POST['data']) && !empty($_POST['data'])){
					$post_data = json_decode($_POST['data']); // get posted data
					// echo "<pre>";print_r($post_data);exit;

				    $full_name = isset($post_data->full_name) ? $post_data->full_name : '' ; // required
				    $mobile_no = isset($post_data->mobile_no) ? $post_data->mobile_no : ''; // required		   
				    $email = isset($post_data->email) ? $post_data->email :''; // required
				    $password = isset($post_data->password) ? $post_data->password :''; // required             
				    $create_date = date('Y-m-d H:i:s'); // required added from server side time stamp

					if(empty($full_name) || empty($mobile_no) || empty($email) || empty($password))
					{
						$return['result'] = "fail";
						$return['message'] = "All fields required data...!";
						echo json_encode($return); exit;
						return;
					}

				 //    if (!empty($_FILES['profile_pic'])) {
					// 	$imagePath = PROFILE_PIC;
					// 	$img_name = 'PROFILE-'.rand(1000,100000)."-".str_replace(' ', '_',$_FILES['profile_pic']['name']);

					// 	$destFile = $imagePath . $img_name; 
					// 	$filename = $_FILES["profile_pic"]["tmp_name"];       
					// 	move_uploaded_file($filename,  $destFile);
					// }
					// else{
					// 	$img_name = '';
					// }
					
					$data = array(
						'name' => $full_name,
						'mobile_no' => $mobile_no,
						'user_email' => $email,
						'password' => $this->encryption->encrypt($password),
						// 'profile_pic' => $img_name,
						'type' => 'android', // set default value cant't be change.
						'create_date' => $create_date
					);
					// echo"<pre>"; print_r($data); exit;

					$get_user_email = $this->Production_model->get_all_with_where('user_register','','',array('user_email'=>$email));
					if (count($get_user_email) > 0) {
						$return['result'] = "fail";
						$return['message'] = "Email allredy exist...!";
						echo json_encode($return); exit;
					}

					$get_user_phone = $this->Production_model->get_all_with_where('user_register','','',array('mobile_no'=>$mobile_no));
					if (count($get_user_phone) > 0) {
						$return['result'] = "fail";
						$return['message'] = "Mobile number allredy exist...!";
						echo json_encode($return); exit;
					}
					else{
						$record = $this->Production_model->insert_record('user_register',$data);
						if($record !=''){
							//Add Mail Code Here....
							$send_mail = $this->Production_model->mail_send('Crystal Hositing Account',$email,'','mail_form/thankyou_page/registration','',''); // user send email thank-you page

							$admin_email = $this->Production_model->get_all_with_where('user','','',array()); 
							$send_mail = $this->Production_model->mail_send('Crystal Hositing Account',$admin_email[0]['email_address'],'','mail_form/admin_send_mail/registration',$data,''); // admin send mail
							// echo"<pre>"; print_r($send_mail); exit;

							$return['result'] = "success";
							$return['user_id'] = (string) $record;
							$return['message'] = "Registration successfully";
							echo json_encode($return); exit;
						}else{
							$return['result'] = "fail";
							$return['message'] = "Invalid details you entered...!";
							echo json_encode($return); exit;
						}
					}
				}else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}
		}

		function login()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')){
				if(isset($_POST['data']) && !empty($_POST['data'])){
					
					$data = json_decode($_POST['data']); // get posted data
					// echo "<pre>";print_r($data);exit;
					$email_or_mobile = isset($data->email_or_mobile)?$data->email_or_mobile:'';
					$password = isset($data->password)?$data->password:'';					
					
					if(!empty($email_or_mobile) && !empty($password)){

						$user_details = $this->Production_model->email_or_mobile_login('user_register',$email_or_mobile);
						// echo "<pre>";print_r($user_details);exit;
			           	if ($user_details !=null) {
			           		if($user_details[0]['status'] == '1'){	
				           		$decrypt_pwd = $this->encryption->decrypt($user_details[0]['password']);
				           		if ($decrypt_pwd == $password) {			        
				           			if ($user_details[0]['id_country'] == 0 || $user_details[0]['id_state'] == 0 || $user_details[0]['id_city'] == 0) { //0-incomplete , 1-complete
				           				$profile_complete = "0";
				           			}   		
				           			else{
				           				$profile_complete = "1";
				           			}
				            		// echo"<pre>"; echo $this->db->last_query(); print_r($user_details); exit;
				           			
				            		$return['result'] = "success";
									$return['message'] = "Login successfully...!";
									$return['user_id'] = $user_details[0]['user_id'];
									$return['user_email'] = $user_details[0]['user_email'];
									$return['mobile_no'] = $user_details[0]['mobile_no'];
									$return['name'] = $user_details[0]['name'];
									$return['mobile_no_display'] = $user_details[0]['mobile_no_display'];
									$return['profile_picture'] = $user_details[0]['profile_picture'];
									$return['profile_complete'] = $profile_complete;
									echo json_encode($return); exit;
				            	}else{
				            		$return['result'] = "fail";
									$return['message'] = "Your password is incorrect...!";
									echo json_encode($return); exit;	
				            	}
				            }
				            else{
			            		$return['result'] = "fail";
								$return['message'] = "Your account is not active please contact admin...!";
								echo json_encode($return); exit;	
			            	}
			            }else{
			            	$return['result'] = "fail";
							$return['message'] = "Email or phone is not valid...!";
							echo json_encode($return); exit;
			            }
					}else{
						$return['result'] = "fail";
						$return['message'] = "Blank Data Received";
						echo json_encode($return); exit;
					}								
				}else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}	
		}

		function changePassword(){
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')){
				if(isset($_POST['data']) && !empty($_POST['data'])){
					
					$post_data = json_decode($_POST['data']); // get posted data
					$user_id    = $post_data->user_id;
					$old_password   = $post_data->old_password;
					$new_password = $post_data->new_password;
					
					if(!empty($old_password) && !empty($user_id) && !empty($new_password)){
							
						$get_record = $this->Production_model->get_all_with_where('user_register' ,'','',array('user_id' => $user_id));
						// echo"<pre>"; print_r($get_record); exit;
						
						if ($get_record !=null) {
		           			$decrypt_pwd = $this->encryption->decrypt($get_record[0]['password']);
		           			if ($decrypt_pwd == $old_password) {	

								$data = array("password"=>$this->encryption->encrypt($new_password));									
								$record = $this->Production_model->update_record('user_register',$data,array("user_id"=>$user_id));

								if($record == 1){
									$return['result'] = "success";
									$return['message'] = "Password change successfully!";
									echo json_encode($return); exit;	
								}else{
									$return['result'] = "fail";
									$return['message'] = "Please Try After Some time";
									echo json_encode($return); exit;	
								}
							}
							else{
								$return['result'] = "fail";
								$return['message'] = "Old Password Is Wrong";
								echo json_encode($return); exit;
							}
						}
						else{
							$return['result'] = "fail";
							$return['message'] = "Record not available";
							echo json_encode($return); exit;
						}						
					}else{
						$return['result'] = "fail";
						$return['message'] = "Blank Data Received";
						echo json_encode($return); exit;
					}	
				}else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}				
		}

		function forgotPassword(){
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')){
				
				if(isset($_POST['data']) && !empty($_POST['data'])){
					$post_data = json_decode($_POST['data']); // get posted data
					$email = $post_data->email;
					
					if(!empty($email)){
						$record = $this->Production_model->get_all_with_where('user_register' ,'','',array('user_email' => $email));
						// echo"<pre>"; print_r($record); exit;

						if($record !=null){
							// $otp = rand(111111,999999);
							// $data = array('otp'=>$otp);

							$tocken = rand(111111,999999);
							$data = array('tocken'=>$tocken);
							$data['password'] = $this->encryption->decrypt($record[0]['password']); 

							/*call reset password link*/
							$update_tocken = array('forgot_pwd_tocken'=>$tocken);
							$tocken_record = $this->Production_model->update_record('user_register',$update_tocken,array('user_email'=>$email));

							//Add Mail Code Here....

							$send_mail = $this->Production_model->mail_send('Forgotpassword',$email,'','mail_form/forgot_password/forgot_email',$data,'');

							$return['result'] = "success";
							$return['message'] = "Please check your mail and enter OTP !";
							$return['user_id'] = $record[0]['user_id'];
							// $return['otp'] = $otp;
							echo json_encode($return); exit;								
						}else{
							$return['result'] = "fail";
							$return['message'] = "Email Is not exist in our record";
							echo json_encode($return); exit;
						}
					}else{
						$return['result'] = "fail";
						$return['message'] = "Blank Data Received";
						echo json_encode($return); exit;
					}	
				}else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}	
		}

		function resetPassword(){
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')){
				if(isset($_POST['data']) && !empty($_POST['data'])){
					
					$data = json_decode($_POST['data']); // get posted data
					$password = isset($data->password) ? $data->password : '';
					$user_id = isset($data->user_id) ? $data->user_id : '';
				    
				    if(!empty($password) && !empty($user_id)){
				    	$data = array("password"=>$this->encryption->encrypt($password),'forgot_pwd_tocken'=>'');
						$record = $this->Production_model->update_record('user_register',$data,array("user_id"=>$user_id));
						
						if($record == 1){
							$return['result'] = "success";
							$return['message'] = "Password reset done successfully!";
							echo json_encode($return); exit;	
						}else{
							$return['result'] = "fail";
							$return['message'] = "Please Try After Some time";
							echo json_encode($return); exit;	
						}	
				    }
				    else{
				    	$return['result'] = "fail";
						$return['message'] = "Blank Data Received";
						echo json_encode($return); exit;	
				    }
				}else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}				
		}

	    function category_list()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{
				if(isset($_POST['data']) && !empty($_POST['data'])){
					$data  = json_decode($_POST['data']);
					$num = isset($data->num)?$data->num:'';

					$rec = array();	
					if($num=='0'){
						$get_category = $this->Production_model->get_all_with_where_limit_like('category','',array('status'=>'1'),8,$num,'category_id','asc'); 
					}else{
						$get_category = $this->Production_model->get_all_with_where_limit_like('category','',array('status'=>'1'),10,$num,'category_id','asc'); 
						//$get_category = $this->Production_model->get_all_with_where('category','category_id','desc',array('status'=>'1')); 
					}

					// echo"<pre>"; echo $this->db->last_query(); print_r($get_category); exit;

					if ($get_category !=null) {					
				        foreach ($get_category as $key => $value) {
				        	$rec[$key]['category_id'] = $value['category_id'];
							$rec[$key]['category_name'] = $value['category_name'];					
							$rec[$key]['category_image'] = $value['cat_image'];
						}							
						$return['result'] = "success";
						$return['message'] = "Record Found Successfully...!";
						$return['data'] = $rec;
						echo json_encode($return); exit;
					}
					else{
					    if($num>0){
							$return['result'] = "fail";
							$return['message'] = "No More Category Found...!";
							echo json_encode($return); exit;
						}else{
							$return['result'] = "fail";
							$return['message'] = "No Category Found...!";
							echo json_encode($return); exit;
						}
					}					
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}	
		}

		function sub_category_list()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{
				if(isset($_POST['data']) && !empty($_POST['data'])){
					$data  = json_decode($_POST['data']);
					$category_id = isset($data->category_id)?$data->category_id:'';
					$num = isset($data->num)?$data->num:'';

					if(!empty($category_id)){
						$rec = array();

						// $get_sub_cat = $this->Production_model->get_all_with_where('sub_category','sub_category_id','desc',array('category_id'=>$category_id,'status'=>'1')); 

						$get_sub_category = $this->Production_model->get_all_with_where_limit_like('sub_category','',array('category_id'=>$category_id,'status'=>'1'),10,$num,'sub_category_id','desc'); 

						// echo"<pre>"; echo $this->db->last_query(); print_r($get_sub_category); exit;

						if ($get_sub_category !=null) {					
					        foreach ($get_sub_category as $key => $value) {
					        	$rec[$key]['sub_category_id'] = $value['sub_category_id'];
								$rec[$key]['sub_category_name'] = $value['sub_category_name'];	
							}	
							
							$return['result'] = "success";
							$return['message'] = "Record Found Successfully...!";
							$return['data'] = $rec;
							echo json_encode($return); exit;
						}
						else{
							$return['result'] = "fail";
							$return['message'] = "No More Category Found...!";
							echo json_encode($return); exit;
						}
					}
					else{
				    	$return['result'] = "fail";
						$return['message'] = "Blank Data Received";
						echo json_encode($return); exit;
					}
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}	
		}

		function package_list()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{				
				$rec = array();						
				$get_packages = $this->Production_model->get_all_with_where('package_management','id','desc',array('status'=>'1')); 					

				// echo"<pre>"; echo $this->db->last_query(); print_r($get_packages); exit;

				if ($get_packages !=null) {					
			        foreach ($get_packages as $key => $value) {
			        	$rec[$key]['id'] = $value['id'];
						$rec[$key]['name'] = $value['name'];					
						$rec[$key]['price'] = $value['price'];
						$rec[$key]['currency'] = $value['currency'];
						$rec[$key]['day_month'] = $value['day_month'];
						$rec[$key]['day'] = $value['day'];
						$rec[$key]['discription'] = $value['discription'];
						$rec[$key]['create_date'] = date('d-m-Y',strtotime($value['create_date']));
					}							
					$return['result'] = "success";
					$return['message'] = "Record Found Successfully...!";
					$return['data'] = $rec;
					echo json_encode($return); exit;
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "No More Packages...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}	
		}

		function get_profile(){

			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{
				if(isset($_POST['data']) && !empty($_POST['data'])){
					$data  = json_decode($_POST['data']);
					$user_id = isset($data->user_id)?$data->user_id:'';
					
					if(!empty($user_id)){
						$get_user_dtls = $this->Production_model->get_all_with_where('user_register','','',array('user_id'=>$user_id)); 
						// echo"<pre>".$this->db->last_query(); print_r($get_user_dtls);exit;

						if ($get_user_dtls !=null)
						{	
							$get_country = $this->Production_model->get_all_with_where('country','','',array('id'=>$get_user_dtls[0]['id_country'])); 

							$get_state = $this->Production_model->get_all_with_where('state','','',array('id'=>$get_user_dtls[0]['id_state'])); 

							$get_city = $this->Production_model->get_all_with_where('city','','',array('id'=>$get_user_dtls[0]['id_city']));

				        	$rec['user_id'] = isset($get_user_dtls[0]['user_id']) ?  $get_user_dtls[0]['user_id'] : '';
				        	$rec['name'] = isset($get_user_dtls[0]['name']) ?  $get_user_dtls[0]['name'] : '';
							$rec['email'] = isset($get_user_dtls[0]['user_email']) ?  $get_user_dtls[0]['user_email'] : '';
							$rec['mobile_no'] = isset($get_user_dtls[0]['mobile_no']) ?  $get_user_dtls[0]['mobile_no'] : '';
							$rec['gender'] = isset($get_user_dtls[0]['gender']) ?  $get_user_dtls[0]['gender'] : '' ;
							$rec['mobile_no_display'] = isset($get_user_dtls[0]['mobile_no_display']) ?  $get_user_dtls[0]['mobile_no_display'] : '' ;
							$rec['address'] = isset($get_user_dtls[0]['address']) ?  $get_user_dtls[0]['address'] : '';
							$rec['country_id'] = isset($get_user_dtls[0]['id_country']) ?  $get_user_dtls[0]['id_country'] : '';
							$rec['country_name'] = isset($get_country[0]['country_name']) ? $get_country[0]['country_name'] : '';
							$rec['state_id'] = isset($get_user_dtls[0]['id_state']) ?  $get_user_dtls[0]['id_state'] : '';
							$rec['state_name'] = isset($get_state[0]['state_name']) ? $get_state[0]['state_name'] : '';
							$rec['city_id'] = isset($get_user_dtls[0]['id_city']) ?  $get_user_dtls[0]['id_city'] : '';
							$rec['city_name'] = isset($get_city[0]['city_name']) ? $get_city[0]['city_name'] : '';
							$rec['profile_picture'] = isset($get_user_dtls[0]['profile_picture']) ?  $get_user_dtls[0]['profile_picture'] : '';                            
							$rec['create_date'] = date('d-m-Y', strtotime($get_user_dtls[0]['create_date']));

							$return['result'] = "success";
							$return['message'] = "Record Found Successfully...!";
							$return['data'] = $rec;
							echo json_encode($return); exit;
						}
						else{
							$return['result'] = "fail";
							$return['message'] = "Record Not Found...!";
							echo json_encode($return); exit;
						}
					}
					else{
				    	$return['result'] = "fail";
						$return['message'] = "Blank Data Received";
						echo json_encode($return); exit;	
				    }
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}	
		}		

		function update_phone_status(){
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{	
				if(isset($_POST['data']) && !empty($_POST['data'])){
					$post_data  = json_decode($_POST['data']);

					$user_id = isset($post_data->user_id) ? $post_data->user_id : ''; // required
					$status = isset($post_data->status) ? $post_data->status : ''; // required
			        if(!empty($user_id)){
						$data = array(
							'mobile_no_display' => $status
						);
						$record = $this->Production_model->update_record('user_register',$data,array('user_id'=>$user_id));
						if ($record == 1) {	
							$return['result'] = "success";
							$return['message'] = "Status Updated Successfully...!";
							echo json_encode($return); exit;
						}
						else{
							$return['result'] = "fail";
							$return['message'] = "Profile Not Updated...!";
							echo json_encode($return); exit;
						}
					}
					else{
						$return['result'] = "fail";
						$return['message'] = "Blank Data Received";
						echo json_encode($return); exit;	
					}				
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}	
		}


		function profile_update(){

			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{	
				if(isset($_POST['data']) && !empty($_POST['data'])){
					$post_data  = json_decode($_POST['data']);

					$user_id = isset($post_data->user_id) ? $post_data->user_id : ''; // required
					$name = isset($post_data->name) ? $post_data->name : ''; // required
				    $address = isset($post_data->address) ? $post_data->address : ''; // required
				    // $email = isset($post_data->email) ? $post_data->email : ''; // required
				    $mobile_no = isset($post_data->mobile_no) ? $post_data->mobile_no : ''; // required
				    $country_id = isset($post_data->country_id) ? $post_data->country_id : ''; // required
				    $state_id = isset($post_data->state_id) ? $post_data->state_id : ''; // required
				    $city_id = isset($post_data->city_id) ? $post_data->city_id : ''; // required

				    $get_user_phone = $this->Production_model->get_all_with_where('user_register','','',array('mobile_no'=>$mobile_no,'user_id !='=>$user_id));
					if (count($get_user_phone) > 0) {
						$return['result'] = "fail";
						$return['message'] = "Mobile number allredy exist...!";
						echo json_encode($return); exit;
					}
			        else {
						if(!empty($user_id) && !empty($name) && !empty($address) && !empty($country_id) && !empty($state_id) && !empty($city_id)){

							$data = array(
								'name' => $name,
								// 'email' => $email,
								'address' => $address,
								'mobile_no' => $mobile_no,
								'id_country' => $country_id,
								'id_state' => $state_id,
								'id_city' => $city_id,
								'modified_date' => date('Y-m-d H:i:s')
							);

							if(isset($_FILES['profile_picture']['name']) && $_FILES['profile_picture']['name'] !='')
		        			{
								$get_image = $this->Production_model->get_all_with_where('user_register','','',array('user_id'=>$user_id));
								if ($get_image !=null && $get_image[0]['profile_picture'] !=null && !empty($get_image[0]['profile_picture'])) {
									@unlink(PROFILE_PICTURE.$get_image[0]['profile_picture']);
								}
								
								$imagePath = PROFILE_PICTURE;
								$img_name = 'PROFILE-PICTURE-'.rand(111,999)."-".str_replace(' ', '_',$_FILES['profile_picture']['name']);
								$destFile = $imagePath . $img_name; 
								$filename = $_FILES["profile_picture"]["tmp_name"];       
								move_uploaded_file($filename,  $destFile);

								$data['profile_picture'] = $img_name;
							}
							
							// echo"<pre>"; print_r($data); exit;

							$record = $this->Production_model->update_record('user_register',$data,array('user_id'=>$user_id));
							// echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;

							if ($record == 1) {	
								$return['result'] = "success";
								$return['message'] = "Profile Updated Successfully...!";
								echo json_encode($return); exit;
							}
							else{
								$return['result'] = "fail";
								$return['message'] = "Profile Not Updated...!";
								echo json_encode($return); exit;
							}
						}
						else{
					    	$return['result'] = "fail";
							$return['message'] = "Blank Data Received";
							echo json_encode($return); exit;	
					    }
					}					
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}	
		}

		function get_password(){
			$email = $_REQUEST['email'];		    
		    // echo $pass = $this->encryption->encrypt('div@123$'); exit;

		    if(!empty($email)){
				$record = $this->Production_model->get_all_with_where('tbl_user','','',array("email"=>$email));
				if($record !=null){
					$decode_pwd = $this->encryption->decrypt($record[0]['password']);

					$return['result'] = "success";
					$return['password'] = $decode_pwd;
					echo json_encode($return); exit;	
				}else{
					$return['result'] = "fail";
					$return['message'] = "Email-id not available...!";
					echo json_encode($return); exit;	
				}	
		    }
		    else{
		    	$return['result'] = "fail";
				$return['message'] = "Blank Data Received";
				echo json_encode($return); exit;	
		    }			
		}

		function post_add()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{
				if(isset($_POST['data']) && !empty($_POST['data'])){
					$data  = json_decode($_POST['data']);
					$user_id = isset($data->user_id) ? $data->user_id : '';
					$name = isset($data->name) ? $data->name : '';
					$mobile_no = isset($data->mobile_no) ? $data->mobile_no : '';
					$user_email = isset($data->user_email) ? $data->user_email : '';
					$password = isset($data->password) ? $data->password : '';

					$category_id = isset($data->category_id) ? $data->category_id : '';
					$sub_category_id = isset($data->sub_category_id) ? $data->sub_category_id : '';
					$add_type = isset($data->add_type) ? $data->add_type : '';
					$i_want = isset($data->i_want) ? $data->i_want : '';
					$post_description = isset($data->post_description) ? $data->post_description : '';
					$price = isset($data->price) ? $data->price : '';
					$package_id = isset($data->package_id) ? $data->package_id : '';
					$payment_details = isset($data->payment_details) ? $data->payment_details : '';
					$ads_status = !empty($payment_details) ? 1 : 0;

					/*Auto registration in post add*/
		        	if (empty($user_id)){	
		        		if (!empty($name) && !empty($mobile_no) && !empty($user_email) && !empty($password))
		        		{        		
				        	$add_reg = array(
				        		'name' => $name,
				        		'mobile_no' => $mobile_no,
				        		'user_email' => $user_email,
				        		'password' => $this->encryption->encrypt($password)
				        	);
				        	// unset($this->input->post('confirm_password'));
				        	
				        	$add_reg['type'] = 'android';
				          	// echo "<pre>"; print_r($add_reg); exit;

				          	$get_user_email = $this->Production_model->get_all_with_where('user_register','','',array('user_email'=>$user_email));
							if (count($get_user_email) > 0) {

								$return['result'] = "fail";
								$return['message'] = "Email id allredy exist....!";
								echo json_encode($return); exit;
							}

							$get_user_mobile = $this->Production_model->get_all_with_where('user_register','','',array('mobile_no'=>$mobile_no));
							if (count($get_user_mobile) > 0) {

								$return['result'] = "fail";
								$return['message'] = "Mobile no allredy exist....!";
								echo json_encode($return); exit;
							}
							else
							{		
								// echo"<pre>"; print_r($add_reg); exit;		
								// $this->load->view('mail_form/admin_send_mail/registration', $add_reg);		
								$record = $this->Production_model->insert_record('user_register',$add_reg);
								if ($record !='') {
									$send_mail = $this->Production_model->mail_send('Crystal Hositing Account',$user_email,'','mail_form/thankyou_page/registration','',''); // user send email thank-you page

									$admin_email = $this->Production_model->get_all_with_where('user','','',array()); 
									$send_mail = $this->Production_model->mail_send('Crystal Hositing Account',$admin_email[0]['email_address'],'','mail_form/admin_send_mail/registration',$add_reg,''); // admin send mail
								}
							}
						}
						else{
							$return['result'] = "fail";
							$return['message'] = "User details is not valid";
							echo json_encode($return); exit;
						}
					}
					/*End*/

					if (!empty($category_id) && !empty($add_type) && !empty($i_want) && !empty($post_description) && !empty($price) && !empty($package_id))
					{
						$data = array(
							'category_id' => $category_id, 
							'sub_category_id' => isset($sub_category_id) ? $sub_category_id : '', 
							'add_type' => $add_type, 
							'i_want' => $i_want, 
							'post_description' => $post_description, 
							'price' => $price,
							'package_id' => $package_id,
							'payment_details' => $payment_details,
							'ads_status' => $ads_status,
						);

						$data['user_id'] = empty($user_id) ? $record : $user_id;
						$order_id = rand(00000000, 99999999);
        				$data['order_id'] = $order_id;
						//echo"<pre>"; print_r($data); exit;

						$record = $this->Production_model->insert_record('post_management',$data);
						if ($record !='') {

							//=========================================================================//
				            //======================= multiple image upload start =====================//
				            //=========================================================================//

							$files = $_FILES;		
						    $cpt = count($_FILES);
						    if (!is_dir(POST_NEED_IMG)) {
								mkdir(POST_NEED_IMG);
								mkdir(POST_NEED_IMG.'thumbnail/');
								@chmod(POST_NEED_IMG,0777);
							}
						    for($i=0; $i<$cpt; $i++)
							{ 
								$j = $i+1;
								if (isset($files['post_image'.$j])) {

									// print_r($_FILES);
								    $_FILES['post_image']['name']= $files['post_image'.$j]['name'];
									$_FILES['post_image']['type']= $files['post_image'.$j]['type'];
									$_FILES['post_image']['tmp_name']= $files['post_image'.$j]['tmp_name'];
									$_FILES['post_image']['error']= $files['post_image'.$j]['error'];
									$_FILES['post_image']['size']= $files['post_image'.$j]['size']; 

									$config['upload_path']   = POST_NEED_IMG;
							        $config['allowed_types'] = '*';
							        $config['encrypt_name'] = TRUE;    	

									$this->load->library('upload', $config);
									// $this->load->library('upload',$config);
									$this->upload->do_upload('post_image');
									$image_name = $this->upload->data();
									$image_name = $image_name['file_name']; 

									$this->Production_model->generate_thumbnail(POST_NEED_IMG,$image_name);

									$imagedata = array(
				                        'post_id' => $record,
				                        'user_id' => $data['user_id'],
				                        'similar_image' => $image_name,
				                        'create_date' => date('Y-m-d H:i:s')
				                    ); 

				                    // echo "<pre>";print_r($imagedata);
				                    $image_id = $this->Production_model->insert_record('post_similer_image', $imagedata);
								}
							}

				            //=========================================================================//
				            //======================= multiple image upload end =======================//
				            //=========================================================================//

							$return['result'] = "success";
							$return['message'] = "Thank you for your post our executive will contact you soon.";
							$return['post_id'] = $record;
							$return['user_id'] = intval($data['user_id']);
							echo json_encode($return); 

							// Add Mail Code Here....

				            $user_info = $this->Production_model->get_all_with_where('user_register', '', '', array('user_id' => $data['user_id']));
				            $user_info = $user_info[0];

				            $post_info = $this->Production_model->get_where_user('post_id,category_id,sub_category_id,add_type,i_want,post_image,post_description,price,user_id,order_id,package_id,payment_details,invoice,create_date as post_create_date','post_management','','',array('post_id' => $record));
				            $post_info = $post_info[0];
				            // echo"<pre>"; print_r($post_info); exit;

				            $package_info = $this->Production_model->get_where_user('id,name as package_name, currency,day_month,day,discription,price as package_price','package_management','','',array('id' => $package_id));
				            $package_info = $package_info[0];

				            $data = array();
				            unset($user_info['password']);
				            $data = array_merge($data, $package_info,$user_info,$post_info);

				            $data['get_post_image'] = $this->Production_model->get_all_with_where('post_similer_image','','',array('post_id'=>$record));

				            // echo"<pre>"; print_r($data); exit;
							$send_mail = $this->Production_model->mail_send('Crystal Hositing-post',$user_info['user_email'],'','mail_form/admin_send_mail/postneed',$data,''); // user send email thank-you page

							$admin_email = $this->Production_model->get_all_with_where('user','','',array());
							$send_mail = $this->Production_model->mail_send('Crystal Hositing-post',$admin_email[0]['email_address'],'','mail_form/admin_send_mail/postneed',$data,''); // admin send mail

							//================ genrate pdf in order details start ================//
                    
				            require_once 'dompdf/autoload.inc.php';
				            $dompdf = new Dompdf\Dompdf(array('enable_remote' => true));                
				            $html = ($this->load->view('order_genrate/order_invoice',$data,true));
				            $dompdf->loadHtml($html);
				            $dompdf->setPaper('A4', 'landscape');
				            $dompdf->render();
				            // $pdf = $dompdf->output();     
				            // $dompdf->stream();
				            $file_name = date('d-m-Y').rand(00000000,11111111).'-order-invoice.pdf';
				            file_put_contents('order_details_page/'.$file_name, $dompdf->output()); // file save in folder

				            /*Save invoice genrate name*/
				            $update_invoice = array('invoice'=>$file_name);
				            $record = $this->Production_model->update_record('post_management',$update_invoice,array('post_id'=>$record));
				            /*End*/

				            //================= genrate pdf in order details end =================//
						}
					}
					else{ 
						$return['result'] = "fail";
						$return['message'] = "Blank Data Received";
						echo json_encode($return); exit;
					}
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}
		}

		function post_edit()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{
				if(isset($_POST['data']) && !empty($_POST['data'])){
					$data  = json_decode($_POST['data']);
					$post_id = isset($data->post_id) ? $data->post_id : '';
					$category_id = isset($data->category_id) ? $data->category_id : '';
					$sub_category_id = isset($data->sub_category_id) ? $data->sub_category_id : '';
					$add_type = isset($data->add_type) ? $data->add_type : '';
					$i_want = isset($data->i_want) ? $data->i_want : '';
					$price = isset($data->price) ? $data->price : '';
					$post_description = isset($data->post_description) ? $data->post_description : '';
					$remove_image_id = isset($data->remove_image_id) ? $data->remove_image_id : '';
					$user_id = isset($data->user_id) ? $data->user_id : '';

					if (!empty($post_id) && !empty($category_id) && !empty($add_type) && !empty($i_want) && !empty($post_description) && !empty($price))
					{
						/*Removing image*/
						if (isset($remove_image_id) && $remove_image_id !=null) {
							$img_id = explode(',',$remove_image_id);
					        $get_image = $this->Production_model->get_all_with_where_in('post_similer_image','image_id','desc','image_id',$img_id);
					        if (isset($get_image) && $get_image !=null)
					        {
					            foreach ($get_image as $key => $value) {
					                @unlink(POST_NEED_IMG.$value['similar_image']);
					                @unlink(POST_NEED_IMG.'thumbnail/'.$value['similar_image']);
					            }
					        }                 
					        $record = $this->Production_model->get_delete_where_in('post_similer_image','image_id',explode(',',$remove_image_id));
					    }

					    /*Updsate record*/

						$data = array(
							'category_id' => $category_id, 
							'sub_category_id' => isset($sub_category_id) ? $sub_category_id : '', 
							'add_type' => $add_type, 
							'i_want' => $i_want, 
							'post_description' => $post_description, 
							'price' => $price
							// 'package_id' => $package_id,
						);

						$record = $this->Production_model->update_record('post_management',$data,array('post_id'=>$post_id));
						// echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;

						$files = $_FILES;		
						$cpt = count($_FILES);
						if ($cpt > 0) {
							//=========================================================================//
				            //======================= multiple image upload start =====================//
				            //=========================================================================//
							
						    if (!is_dir(POST_NEED_IMG)) {
								mkdir(POST_NEED_IMG);
								mkdir(POST_NEED_IMG.'thumbnail/');
								@chmod(POST_NEED_IMG,0777);
							}
						    for($i=0; $i<$cpt; $i++)
							{ 
								$j = $i+1;
								if (isset($files['post_image'.$j])) {

									// print_r($_FILES);
								    $_FILES['post_image']['name']= $files['post_image'.$j]['name'];
									$_FILES['post_image']['type']= $files['post_image'.$j]['type'];
									$_FILES['post_image']['tmp_name']= $files['post_image'.$j]['tmp_name'];
									$_FILES['post_image']['error']= $files['post_image'.$j]['error'];
									$_FILES['post_image']['size']= $files['post_image'.$j]['size']; 

									$config['upload_path']   = POST_NEED_IMG;
							        $config['allowed_types'] = '*';
							        $config['encrypt_name'] = TRUE;    	

									$this->load->library('upload', $config);
									// $this->load->library('upload',$config);
									$this->upload->do_upload('post_image');
									$image_name = $this->upload->data();
									$image_name = $image_name['file_name']; 

									$this->Production_model->generate_thumbnail(POST_NEED_IMG,$image_name);

									$imagedata = array(
				                        'post_id' => $post_id,
				                        'similar_image' => $image_name,
				                        'user_id' => $user_id,
				                        'create_date' => date('Y-m-d H:i:s')
				                    ); 

				                    // echo "<pre>";print_r($imagedata);
				                    $image_id = $this->Production_model->insert_record('post_similer_image', $imagedata);
								}
							}

				            //=========================================================================//
				            //======================= multiple image upload end =======================//
				            //=========================================================================//
						}
						$return['result'] = "success";
						$return['message'] = "Post updated successfully...!";
						echo json_encode($return); 			
					}
					else{ 
						$return['result'] = "fail";
						$return['message'] = "Blank Data Received";
						echo json_encode($return); exit;
					}
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}
		}

		function post_listing()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{
				if(isset($_POST['data']) && !empty($_POST['data'])){
					$data  = json_decode($_POST['data']);
					$num = isset($data->num)? $data->num : '';
					$city_name = isset($data->city_name)? $data->city_name : '';
					$sub_category_id = isset($data->sub_category_id)? $data->sub_category_id : '';
					$user_id = isset($data->user_id)? $data->user_id : '';

					$rec = array();

					$where['post_management.status'] = '1';
					$where['post_management.post_approve_reject'] = '1';
					$where['post_management.order_status'] = '0';

					if (isset($city_name) && $city_name !=null && !empty($city_name)) {
						$where['city.city_name'] = $city_name;
					}
					if (isset($sub_category_id) && $sub_category_id !=null && !empty($sub_category_id)) {
						$where['post_management.sub_category_id'] = $sub_category_id;
					}
					$join[0]['table_name'] = 'category';
					$join[0]['column_name'] = 'category.category_id = post_management.category_id';
					$join[0]['type'] = 'left';

					$join[1]['table_name'] = 'sub_category';
					$join[1]['column_name'] = 'sub_category.sub_category_id = post_management.sub_category_id';
					$join[1]['type'] = 'left';

					$join[2]['table_name'] = 'user_register';
					$join[2]['column_name'] = 'user_register.user_id = post_management.user_id';
					$join[2]['type'] = 'left';

					$join[3]['table_name'] = 'country';
					$join[3]['column_name'] = 'country.id = user_register.id_country';
					$join[3]['type'] = 'left';
					
					$join[4]['table_name'] = 'state';
					$join[4]['column_name'] = 'state.id = user_register.id_state';
					$join[4]['type'] = 'left';

					$join[5]['table_name'] = 'city';
					$join[5]['column_name'] = 'city.id = user_register.id_city';
					$join[5]['type'] = 'left';

					$join[6]['table_name'] = 'package_management';
					$join[6]['column_name'] = 'package_management.id = post_management.package_id';
					$join[6]['type'] = 'left';

					$post_details = $this->Production_model->jointable_descending(array('post_management.*','category.category_name','category.cat_image','sub_category.sub_category_name','user_register.name','user_register.user_email','user_register.id_country','user_register.id_state','user_register.id_city','user_register.address','user_register.gender','user_register.mobile_no','user_register.profile_picture','user_register.mobile_no_display','country.country_name','state.state_name','city.city_name','package_management.name as package_name','package_management.price as package_price','package_management.discription as package_description','package_management.day'),'post_management',$where,$join,'post_id','desc','','','',10,$num);

					// echo"<pre>"; echo $this->db->last_query(); print_r($post_details); exit;

					if ($post_details !=null) {
						$featured_add = array();
						$free_add = array();
						foreach ($post_details as $key => $value) {

							$created_date = date('Y-m-d',strtotime($value['create_date']));
                            $expire_post = date('Y-m-d',strtotime($created_date . + $value['day'] .'days'));
							
							if (date('Y-m-d') <= $expire_post) { 
								
								$post_record = array();
								$available_wishlist_pro = '0';
								$available_wishlist_pro = $this->Production_model->get_all_with_where('product_like', '', '', array('product_id' => $value['post_id'], 'user_id' => $user_id));
								//echo $value['post_id'];
								$rec[$key]['post_id'] = $value['post_id'];
								$rec[$key]['user_id'] = $value['user_id'];
								$rec[$key]['category_id'] = $value['category_id'];
								$rec[$key]['category_name'] = $value['category_name'];
								$rec[$key]['cat_image'] = $value['cat_image'];
								$rec[$key]['sub_category_id'] = $value['sub_category_id'];
								$rec[$key]['sub_category_name'] = $value['sub_category_name'];
								$rec[$key]['i_want'] = $value['i_want'];
								$rec[$key]['post_description'] = $value['post_description'];
								$rec[$key]['price'] = $value['price'];
								$rec[$key]['post_approve_reject_resion'] = $value['post_approve_reject_resion'];
								$rec[$key]['add_type'] = $value['add_type'];
								$rec[$key]['payment_details'] = $value['payment_details'];
								$rec[$key]['create_date'] = date('d M, Y',strtotime($value['create_date']));

								$rec[$key]['country_id'] = $value['id_country'];
								$rec[$key]['country_name'] = $value['country_name'];
								$rec[$key]['state_id'] = $value['id_state'];
								$rec[$key]['state_name'] = $value['state_name'];
								$rec[$key]['city_id'] = $value['id_city'];
								$rec[$key]['city_name'] = $value['city_name'];

								$rec[$key]['user_name'] = $value['name'];
								$rec[$key]['email'] = $value['user_email'];
								$rec[$key]['mobile_no_display'] = $value['mobile_no_display']; //0=hide , 1=show.
								$rec[$key]['mobile_no'] = $value['mobile_no'];
								$rec[$key]['profile_pic'] = $value['profile_picture']; 

								$rec[$key]['package_name'] = $value['package_name'];
								$rec[$key]['package_price'] = $value['package_price'];
								$rec[$key]['package_day'] = $value['day'];
								$rec[$key]['package_expire'] = $expire_post;
								$rec[$key]['is_expire'] = $expire_post > date('Y-m-d') ? '0' : '1';
								$rec[$key]['package_description'] = $value['package_description'];
								$rec[$key]['ads_status'] = $value['ads_status'];
								$rec[$key]['invoice'] = base_url(POST_INVOICE.$value['invoice']);
								$rec[$key]['wishlist'] = !empty($available_wishlist_pro) && $available_wishlist_pro != null ? 1 : 0;

								$post_images = $this->Production_model->get_all_with_where('post_similer_image','image_id','desc',array('post_id'=>$value['post_id']));

								$images = array();
								
								foreach ($post_images as $key1 => $value) {
									$images[] = array(
										'image_id' => $value['image_id'], 
										'post_images' => $value['similar_image'] 
									);
								}	

						       	$rec[$key]['post_images'] = $images !=null ? $images : null;
								if ($rec[$key]['ads_status'] == 1) {
									$featured_add[] = $rec[$key];
								}
								else {
									$free_add[] = $rec[$key];
								}
						    }	
					    }
					    $post_record['featured_add']= !empty($featured_add) ? $featured_add : null;
					    $post_record['free_add']= !empty($free_add) ? $free_add : null;
					    if (isset($post_record) && $post_record !=null) {
							$return['result'] = "success";
							$return['message'] = "Record Found Successfully...!";
							$return['data'] = $post_record;
							echo json_encode($return);
						}	
						else{				    		
				    		if($num>0){
								$return['result'] = "fail";
								$return['message'] = "No More Record Found...!";
								echo json_encode($return); exit;
							}else{
								$return['result'] = "fail";
								$return['message'] = "No Record Found...!";
								echo json_encode($return); exit;
							}
					    }					
					}
					else{						
						$return['result'] = "fail";
						$return['message'] = "No Record Found...!";
						echo json_encode($return); exit;
					}
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}	
		}

		function post_filter()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{
				if(isset($_POST['data']) && !empty($_POST['data'])){
					$data  = json_decode($_POST['data']);

					$num = isset($data->num)? $data->num : '';
					$city_name = isset($data->city_name) ? $data->city_name : '';
					$sub_category_id = isset($data->sub_category_id) ? $data->sub_category_id : '';
					$user_id = isset($data->user_id) ? $data->user_id : '';
					$ads_status = isset($data->ads_status) ? $data->ads_status : '';
					
					$rec = array();
					$where = array();
					$where['post_management.status'] = '1';
					$where['post_management.ads_status'] = $ads_status;
					$where['post_management.post_approve_reject'] = '1';
					$where['post_management.order_status'] = '0';

					// if (isset($user_id) && $user_id !=null && !empty($user_id)) {
					// 	$where['post_management.user_id'] = $user_id;
					// }
					if (isset($city_name) && $city_name !=null && !empty($city_name)) {
						$where['city.city_name'] = $city_name;
					}
					if (isset($sub_category_id) && $sub_category_id !=null && !empty($sub_category_id)) {
						$where['post_management.sub_category_id'] = $sub_category_id;
					}
					// echo"<pre>"; print_r($data); exit;

					$join[0]['table_name'] = 'category';
					$join[0]['column_name'] = 'category.category_id = post_management.category_id';
					$join[0]['type'] = 'left';

					$join[1]['table_name'] = 'sub_category';
					$join[1]['column_name'] = 'sub_category.sub_category_id = post_management.sub_category_id';
					$join[1]['type'] = 'left';

					$join[2]['table_name'] = 'user_register';
					$join[2]['column_name'] = 'user_register.user_id = post_management.user_id';
					$join[2]['type'] = 'left';

					$join[3]['table_name'] = 'country';
					$join[3]['column_name'] = 'country.id = user_register.id_country';
					$join[3]['type'] = 'left';
					
					$join[4]['table_name'] = 'state';
					$join[4]['column_name'] = 'state.id = user_register.id_state';
					$join[4]['type'] = 'left';

					$join[5]['table_name'] = 'city';
					$join[5]['column_name'] = 'city.id = user_register.id_city';
					$join[5]['type'] = 'left';

					$join[6]['table_name'] = 'package_management';
					$join[6]['column_name'] = 'package_management.id = post_management.package_id';
					$join[6]['type'] = 'left';
					
					$filteredData = $this->Production_model->jointable_descending(array('post_management.*','category.category_name','category.cat_image','sub_category.sub_category_name','user_register.name','user_register.user_email','user_register.id_country','user_register.id_state','user_register.id_city','user_register.address','user_register.gender','user_register.mobile_no','user_register.mobile_no_display','user_register.profile_picture','country.country_name','state.state_name','city.city_name','package_management.name as package_name','package_management.price as package_price','package_management.discription as package_description','package_management.day'),'post_management','',$join,'post_id','desc',$where,'','',10,$num);

					// echo"<pre>"; echo $this->db->last_query(); print_r($filteredData); exit;

					if ($filteredData !=null) {
						foreach ($filteredData as $key => $value) {
							$available_wishlist_pro = '0';
							$available_wishlist_pro = $this->Production_model->get_all_with_where('product_like', '', '', array('product_id' => $value['post_id'], 'user_id' => $user_id));

							$created_date = date('Y-m-d',strtotime($value['create_date']));
                        	$expire_post = date('Y-m-d',strtotime($created_date . + $value['day'] .'days'));
						
							if (date('Y-m-d') <= $expire_post) { 

								$rec[$key]['post_id'] = $value['post_id'];
								$rec[$key]['user_id'] = $value['user_id'];
								$rec[$key]['category_id'] = $value['category_id'];
								$rec[$key]['category_name'] = $value['category_name'];
								$rec[$key]['cat_image'] = $value['cat_image'];
								$rec[$key]['sub_category_id'] = $value['sub_category_id'];
								$rec[$key]['sub_category_name'] = $value['sub_category_name'];
								$rec[$key]['i_want'] = $value['i_want'];
								$rec[$key]['post_description'] = $value['post_description'];
								$rec[$key]['price'] = $value['price'];
								$rec[$key]['post_approve_reject_resion'] = $value['post_approve_reject_resion'];
								$rec[$key]['add_type'] = $value['add_type'];
								$rec[$key]['ads_status'] = $value['ads_status'];
								$rec[$key]['payment_details'] = $value['payment_details'];
								$rec[$key]['create_date'] = date('d M, Y',strtotime($value['create_date']));

								$rec[$key]['country_id'] = $value['id_country'];
								$rec[$key]['country_name'] = $value['country_name'];
								$rec[$key]['state_id'] = $value['id_state'];
								$rec[$key]['state_name'] = $value['state_name'];
								$rec[$key]['city_id'] = $value['id_city'];
								$rec[$key]['city_name'] = $value['city_name'];

								$rec[$key]['user_name'] = $value['name'];
								$rec[$key]['email'] = $value['user_email'];
								$rec[$key]['mobile_no'] = $value['mobile_no'];
								$rec[$key]['mobile_no_display'] = $value['mobile_no_display']; //0=hide , 1=show.
								$rec[$key]['profile_pic'] = $value['profile_picture']; 

								$rec[$key]['package_name'] = $value['package_name'];
								$rec[$key]['package_price'] = $value['package_price'];
								$rec[$key]['package_day'] = $value['day'];
								$rec[$key]['package_expire'] = $expire_post;
								$rec[$key]['is_expire'] = $expire_post > date('Y-m-d') ? '0' : '1';
								$rec[$key]['package_description'] = $value['package_description'];
								$rec[$key]['invoice'] = base_url(POST_INVOICE.$value['invoice']);
								$rec[$key]['wishlist'] = !empty($available_wishlist_pro) && $available_wishlist_pro != null ? 1 : 0;	

								$post_images = $this->Production_model->get_all_with_where('post_similer_image','image_id','desc',array('post_id'=>$value['post_id']));

								$images = array();
								foreach ($post_images as $key1 => $value) {
									$images[] = array(
										'image_id' => $value['image_id'], 
										'post_images' => $value['similar_image'] 
									);
								}				
						       	$rec[$key]['post_images'] = $images !=null ? $images : null;
						    }
					    }
						
						$return['result'] = "success";
						$return['message'] = "Record Found Successfully...!";
						$return['data'] = $rec;
						echo json_encode($return); exit;
					}
					else{
						if($num>0){
							$return['result'] = "fail";
							$return['message'] = "No More Record Found...!";
							echo json_encode($return); exit;
						}else{
							$return['result'] = "fail";
							$return['message'] = "No Record Found...!";
							echo json_encode($return); exit;
						}
					}
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}	
		}

		function post_by_user()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{
				if(isset($_POST['data']) && !empty($_POST['data'])){
					$data  = json_decode($_POST['data']);

					$num = isset($data->num)? $data->num : '';
					$user_id = isset($data->user_id)?$data->user_id:'';

					if(!empty($user_id)){
						$rec = array();

						$where['post_management.user_id'] = $user_id;

						$join[0]['table_name'] = 'category';
						$join[0]['column_name'] = 'category.category_id = post_management.category_id';
						$join[0]['type'] = 'left';

						$join[1]['table_name'] = 'sub_category';
						$join[1]['column_name'] = 'sub_category.sub_category_id = post_management.sub_category_id';
						$join[1]['type'] = 'left';

						$join[2]['table_name'] = 'user_register';
						$join[2]['column_name'] = 'user_register.user_id = post_management.user_id';
						$join[2]['type'] = 'left';

						$join[3]['table_name'] = 'country';
						$join[3]['column_name'] = 'country.id = user_register.id_country';
						$join[3]['type'] = 'left';
						
						$join[4]['table_name'] = 'state';
						$join[4]['column_name'] = 'state.id = user_register.id_state';
						$join[4]['type'] = 'left';

						$join[5]['table_name'] = 'city';
						$join[5]['column_name'] = 'city.id = user_register.id_city';
						$join[5]['type'] = 'left';

						$join[6]['table_name'] = 'package_management';
				        $join[6]['column_name'] = 'package_management.id = post_management.package_id';
				        $join[6]['type'] = 'left';

						$post_details = $this->Production_model->jointable_descending(array('post_management.*','category.category_name','category.cat_image','sub_category.sub_category_name','user_register.name','user_register.user_email','user_register.id_country','user_register.id_state','user_register.id_city','user_register.address','user_register.gender','user_register.mobile_no','user_register.profile_picture','user_register.mobile_no_display','user_register.mobile_no_display','country.country_name','state.state_name','city.city_name','package_management.name as package_name','package_management.price as package_price','package_management.discription as package_description','package_management.day'),'post_management','',$join,'post_id','desc',$where,'','',10,$num);

						// echo"<pre>"; echo $this->db->last_query(); print_r($post_details); exit;

						if ($post_details !=null) {
							foreach ($post_details as $key => $value) {
								$created_date = date('Y-m-d',strtotime($value['create_date']));
                                $expire_post = date('Y-m-d',strtotime($created_date . + $value['day'] .'days'));

								$rec[$key]['post_id'] = $value['post_id'];
								$rec[$key]['user_id'] = $value['user_id'];
								$rec[$key]['category_id'] = $value['category_id'];
								$rec[$key]['category_name'] = $value['category_name'];
								$rec[$key]['cat_image'] = $value['cat_image'];
								$rec[$key]['sub_category_id'] = $value['sub_category_id'];
								$rec[$key]['sub_category_name'] = $value['sub_category_name'];
								$rec[$key]['i_want'] = $value['i_want'];
								$rec[$key]['post_description'] = $value['post_description'];
								$rec[$key]['price'] = $value['price'];
								$rec[$key]['post_approve_reject'] = $value['post_approve_reject'];
								$rec[$key]['post_approve_reject_resion'] = $value['post_approve_reject_resion'];
								$rec[$key]['add_type'] = $value['add_type'];
								$rec[$key]['create_date'] = date('d M, Y',strtotime($value['create_date']));

								$rec[$key]['country_id'] = $value['id_country'];
								$rec[$key]['country_name'] = $value['country_name'];
								$rec[$key]['state_id'] = $value['id_state'];
								$rec[$key]['state_name'] = $value['state_name'];
								$rec[$key]['city_id'] = $value['id_city'];
								$rec[$key]['city_name'] = $value['city_name'];

								$rec[$key]['user_name'] = $value['name'];
								$rec[$key]['email'] = $value['user_email'];
								$rec[$key]['mobile_no_display'] = $value['mobile_no_display']; //0=hide , 1=show.
								$rec[$key]['mobile_no'] = $value['mobile_no'];
								$rec[$key]['profile_pic'] = $value['profile_picture']; 

								$rec[$key]['post_approve_reject_resion'] = $value['post_approve_reject_resion']; 
								$rec[$key]['post_approve_reject'] = $value['post_approve_reject']; 
								$rec[$key]['ads_status'] = $value['ads_status']; 
								$rec[$key]['status'] = $value['status']; 
								$rec[$key]['order_status'] = $value['order_status']; 

								$rec[$key]['package_name'] = $value['package_name'];
								$rec[$key]['package_price'] = $value['package_price'];
								$rec[$key]['package_day'] = $value['day'];
								$rec[$key]['package_expire'] = $expire_post;
								$rec[$key]['is_expire'] = $expire_post > date('Y-m-d') ? '0' : '1';
								$rec[$key]['package_description'] = $value['package_description'];
								$rec[$key]['invoice'] = base_url(POST_INVOICE.$value['invoice']);

								$post_images = $this->Production_model->get_all_with_where('post_similer_image','image_id','desc',array('post_id'=>$value['post_id']));

								$images = array();
								foreach ($post_images as $key1 => $value) {
									$images[] = array(
										'image_id' => $value['image_id'], 
										'post_images' => $value['similar_image'] 
									);
								}				
						       	$rec[$key]['post_images'] = $images !=null ? $images : null;	
						    }
							
							$return['result'] = "success";
							$return['message'] = "Record Found Successfully...!";
							$return['data'] = $rec;
							echo json_encode($return); exit;
						}
						else{
							if($num>0){
								$return['result'] = "fail";
								$return['message'] = "No More Record Found...!";
								echo json_encode($return); exit;
							}else{
								$return['result'] = "fail";
								$return['message'] = "No Record Found...!";
								echo json_encode($return); exit;
							}
							
						}
					}
					else{
				    	$return['result'] = "fail";
						$return['message'] = "Blank Data Received";
						echo json_encode($return); exit;
					}
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}	
		}

		function category_filter()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{
				if(isset($_POST['data']) && !empty($_POST['data'])){
					$data  = json_decode($_POST['data']);

					$num = isset($data->num)? $data->num : '';
					$category_name = isset($data->category_name)?$data->category_name:'';

					if(!empty($category_name)){
						$rec = array();

						$filteredData = $this->Production_model->get_all_with_like('category',array('category_name'=>$category_name),array('status'=>'1'),'category_id','desc');

						// echo"<pre>"; echo $this->db->last_query(); print_r($filteredData); exit;

						if ($filteredData !=null) {
							foreach ($filteredData as $key => $value) {
								$rec[$key]['category_id'] = $value['category_id'];
								$rec[$key]['category_name'] = $value['category_name'];
								$rec[$key]['cat_image'] = $value['cat_image'];	
						    }
							
							$return['result'] = "success";
							$return['message'] = "Record Found Successfully...!";
							$return['data'] = $rec;
							echo json_encode($return); exit;
						}
						else{
							$return['result'] = "fail";
							$return['message'] = "No More Record Found...!";
							echo json_encode($return); exit;
						}
					}
					else{
				    	$return['result'] = "fail";
						$return['message'] = "Blank Data Received";
						echo json_encode($return); exit;
					}
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}	
		}

		function country_list()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{
				$rec = array();			
				$get_country = $this->Production_model->get_all_with_where('country','id','asc',array()); 

				// echo"<pre>"; echo $this->db->last_query(); print_r($get_country); exit;
				if ($get_country !=null) {					
			        foreach ($get_country as $key => $value) {
			        	$rec[$key]['id'] = $value['id'];
						$rec[$key]['country_name'] = $value['country_name'];					
					}							
					$return['result'] = "success";
					$return['message'] = "Record Found Successfully...!";
					$return['data'] = $rec;
					echo json_encode($return); exit;
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "No more country available...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}	
		}
		function state_list()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{
				if(isset($_POST['data']) && !empty($_POST['data'])){
					$data  = json_decode($_POST['data']);
					$country_id = isset($data->country_id) ? $data->country_id : '';
					$rec = array();			

					if (empty($country_id)) {						
						$return['result'] = "fail";
						$return['message'] = "Blank Data Received...!";
						echo json_encode($return); exit;						
					}

					$get_state = $this->Production_model->get_all_with_where('state','id','asc',array('id_country'=>$country_id)); 

					// echo"<pre>"; echo $this->db->last_query(); print_r($get_city); exit;

					if ($get_state !=null) {					
				        foreach ($get_state as $key => $value) {
				        	$rec[$key]['id'] = $value['id'];
							$rec[$key]['state_name'] = $value['state_name'];					
						}							
						$return['result'] = "success";
						$return['message'] = "Record Found Successfully...!";
						$return['data'] = $rec;
						echo json_encode($return); exit;
					}
					else{
						$return['result'] = "fail";
						$return['message'] = "No more state available...!";
						echo json_encode($return); exit;
					}					
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}	
		}

		function city_list()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{
				if(isset($_POST['data']) && !empty($_POST['data'])){
					$data  = json_decode($_POST['data']);
					$state_id = isset($data->state_id) ? $data->state_id : '';
					$country_name = isset($data->country_name) ? $data->country_name : '';
					$country_id = isset($data->country_id) ? $data->country_id : '';
					$rec = array();	

					if(isset($state_id) && $state_id !=''){//get city by state selection
						$get_city = $this->Production_model->get_all_with_where('city','id','asc',array('id_state'=>$state_id)); 
					}else if(isset($country_name) && $country_name !=''){//get city by country name in location filter
						//$filteredData = $this->Production_model->get_all_with_like('country',array('country_name'=>$country_name),array(),'','');
						$filteredData = $this->Production_model->get_all_with_where('country','id','asc',array('country_name'=>$country_name));
						if($filteredData !=null && isset($filteredData)){
							$countryId = $filteredData[0]['id'];
							$get_city = $this->Production_model->get_all_with_where('city','city_name','asc',array('id_country'=>$countryId)); 
						}
					}else if(isset($country_id) && $country_id !=''){
						$get_city = $this->Production_model->get_all_with_where('city','city_name','asc',array('id_country'=>$country_id)); 
					}else{//default return city of india
						$get_city = $this->Production_model->get_all_with_where('city','city_name','asc',array('id_country'=>101)); 
					}
									
					if ($get_city !=null) {					
				        foreach ($get_city as $key => $value) {
				        	$rec[$key]['id'] = $value['id'];
							$rec[$key]['city_name'] = $value['city_name'];					
						}							
						$return['result'] = "success";
						$return['message'] = "Record Found Successfully...!";
						$return['data'] = $rec;
						echo json_encode($return); exit;
					}
					else{
						$return['result'] = "fail";
						$return['message'] = "No city available...!";
						echo json_encode($return); exit;
					}					
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return); exit;
			}	
		}

		// function delete_rate()
		// {
		// 	$headers = $this->input->get_request_header('Authenticate');
		// 	if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')){
				
		// 		if(isset($_POST['data']) && !empty($_POST['data'])){
		// 			$post_data = json_decode($_POST['data']); // get posted data
		// 		    $user_id = $post_data->user_id ; // required
		// 		    $product_id = $post_data->product_id ; // required
				   
		// 		    if (empty($product_id) || empty($user_id)){
				    	
		// 		    	$return['result'] = "fail";
		// 				$return['message'] = "Invalid details you entered...!";
		// 				echo json_encode($return); exit;
		// 		    }
		// 		    else{
		// 		    	// $get_product = $this->Production_model->get_all_with_where('tbl_product_rating','','',array('user_id'=>$user_id,'product_id' => $product_id));
						
		// 				$record = $this->Production_model->delete_record('tbl_product_rating',array('user_id'=>$user_id,'product_id' => $product_id));
		// 				if ($record == 1) {
		// 					$return['result'] = "success";
		// 					$return['message'] = "Rate deleted successfully...!";
		// 					echo json_encode($return); exit;
		// 				}
		// 				else{
		// 					$return['result'] = "fail";
		// 					$return['message'] = "Rate not deleted...!";
		// 					echo json_encode($return); exit;		
		// 				}
		// 		    }
		// 		}else{
		// 			$return['result'] = "fail";
		// 			$return['message'] = "Invalid details you entered...!";
		// 			echo json_encode($return); exit;
		// 		}
		// 	}else{
		// 		$return['result'] = "fail";
		// 		$return['message'] = "You are not authorized user!!!";
		// 		echo json_encode($return); exit;
		// 	}
		// }
		
		function get_about()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{						
				$rec = array();
				$get_record = $this->Production_model->get_all_with_where('about','','',array()); 

				// echo"<pre>"; echo $this->db->last_query(); print_r($get_record); exit;
				
		        if ($get_record  !=null) {		        	
		        	$rec['id'] = $get_record[0]['id'];
		        	$rec['name'] = $get_record[0]['title'];
					$rec['details'] = $get_record[0]['discription'];					

					$return['result'] = "success";
					$return['message'] = "Record Found Successfully...!";
					$return['data'] = $rec;
					echo json_encode($return); exit;
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Record Not Found...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "Invalid details you entered...!";
				echo json_encode($return); exit;
			}	
		}

		function get_leadership()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{						
				$rec = array();
				$get_record = $this->Production_model->get_all_with_where('web_settings','','',array('id'=>
					8)); 

				// echo"<pre>"; echo $this->db->last_query(); print_r($get_record); exit;
				
		        if ($get_record  !=null) {		        	
		        	$rec['id'] = $get_record[0]['id'];
		        	$rec['name'] = $get_record[0]['name'];
					$rec['details'] = $get_record[0]['details'];					

					$return['result'] = "success";
					$return['message'] = "Record Found Successfully...!";
					$return['data'] = $rec;
					echo json_encode($return); exit;
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Record Not Found...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "Invalid details you entered...!";
				echo json_encode($return); exit;
			}	
		}

		function get_copyright_policy()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{						
				$rec = array();
				$get_record = $this->Production_model->get_all_with_where('web_settings','','',array('id'=>4)); 

				// echo"<pre>"; echo $this->db->last_query(); print_r($get_record); exit;
				
		        if ($get_record  !=null) {		        	
		        	$rec['id'] = $get_record[0]['id'];
		        	$rec['name'] = $get_record[0]['name'];
					$rec['details'] = $get_record[0]['details'];					

					$return['result'] = "success";
					$return['message'] = "Record Found Successfully...!";
					$return['data'] = $rec;
					echo json_encode($return); exit;
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Record Not Found...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "Invalid details you entered...!";
				echo json_encode($return); exit;
			}	
		}

		function get_hyperlinking_policy()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{						
				$rec = array();
				$get_record = $this->Production_model->get_all_with_where('web_settings','','',array('id'=>5)); 

				// echo"<pre>"; echo $this->db->last_query(); print_r($get_record); exit;
				
		        if ($get_record  !=null) {		        	
		        	$rec['id'] = $get_record[0]['id'];
		        	$rec['name'] = $get_record[0]['name'];
					$rec['details'] = $get_record[0]['details'];					

					$return['result'] = "success";
					$return['message'] = "Record Found Successfully...!";
					$return['data'] = $rec;
					echo json_encode($return); exit;
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Record Not Found...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "Invalid details you entered...!";
				echo json_encode($return); exit;
			}	
		}

		function get_privacy_policy()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{						
				$rec = array();
				$get_record = $this->Production_model->get_all_with_where('web_settings','','',array('id'=>2)); 

				// echo"<pre>"; echo $this->db->last_query(); print_r($get_record); exit;
				
		        if ($get_record  !=null) {		        	
		        	$rec['id'] = $get_record[0]['id'];
		        	$rec['name'] = $get_record[0]['name'];
					$rec['details'] = $get_record[0]['details'];					

					$return['result'] = "success";
					$return['message'] = "Record Found Successfully...!";
					$return['data'] = $rec;
					echo json_encode($return); exit;
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Record Not Found...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "Invalid details you entered...!";
				echo json_encode($return); exit;
			}	
		}

		function get_cookie_policy()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{						
				$rec = array();
				$get_record = $this->Production_model->get_all_with_where('web_settings','','',array('id'=>3)); 

				// echo"<pre>"; echo $this->db->last_query(); print_r($get_record); exit;
				
		        if ($get_record  !=null) {		        	
		        	$rec['id'] = $get_record[0]['id'];
		        	$rec['name'] = $get_record[0]['name'];
					$rec['details'] = $get_record[0]['details'];					

					$return['result'] = "success";
					$return['message'] = "Record Found Successfully...!";
					$return['data'] = $rec;
					echo json_encode($return); exit;
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Record Not Found...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "Invalid details you entered...!";
				echo json_encode($return); exit;
			}	
		}

		function get_terms_condition()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{						
				$rec = array();
				$get_record = $this->Production_model->get_all_with_where('web_settings','','',array('id'=>1)); 

				// echo"<pre>"; echo $this->db->last_query(); print_r($get_record); exit;
				
		        if ($get_record  !=null) {		        	
		        	$rec['id'] = $get_record[0]['id'];
		        	$rec['name'] = $get_record[0]['name'];
					$rec['details'] = $get_record[0]['details'];					

					$return['result'] = "success";
					$return['message'] = "Record Found Successfully...!";
					$return['data'] = $rec;
					echo json_encode($return); exit;
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Record Not Found...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "Invalid details you entered...!";
				echo json_encode($return); exit;
			}	
		}

		function get_content_review_policy()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{						
				$rec = array();
				$get_record = $this->Production_model->get_all_with_where('web_settings','','',array('id'=>9)); 

				// echo"<pre>"; echo $this->db->last_query(); print_r($get_record); exit;
				
		        if ($get_record  !=null) {		        	
		        	$rec['id'] = $get_record[0]['id'];
		        	$rec['name'] = $get_record[0]['name'];
					$rec['details'] = $get_record[0]['details'];					

					$return['result'] = "success";
					$return['message'] = "Record Found Successfully...!";
					$return['data'] = $rec;
					echo json_encode($return); exit;
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Record Not Found...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "Invalid details you entered...!";
				echo json_encode($return); exit;
			}	
		}

		function get_intellectual_property_rights()
		{
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{						
				$rec = array();
				$get_record = $this->Production_model->get_all_with_where('web_settings','','',array('id'=>10)); 
				// echo"<pre>"; echo $this->db->last_query(); print_r($get_record); exit;
				
		        if ($get_record  !=null) {		        	
		        	$rec['id'] = $get_record[0]['id'];
		        	$rec['name'] = $get_record[0]['name'];
					$rec['details'] = $get_record[0]['details'];					

					$return['result'] = "success";
					$return['message'] = "Record Found Successfully...!";
					$return['data'] = $rec;
					echo json_encode($return); exit;
				}
				else{
					$return['result'] = "fail";
					$return['message'] = "Record Not Found...!";
					echo json_encode($return); exit;
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "Invalid details you entered...!";
				echo json_encode($return); exit;
			}	
		}

		function generateToken(){
			$headers = $this->input->get_request_header('Authenticate');
			if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey'))
			{	
				$data  = json_decode($_POST['data']);		
				$orderAmount = isset($data->orderAmount)?$data->orderAmount:'';
				$orderNote = isset($data->orderNote)?$data->orderNote:'';
				$customerName = isset($data->customerName)?$data->customerName:'';
				$customerPhone = isset($data->customerPhone)?$data->customerPhone:'';
				$customerEmail = isset($data->customerEmail)?$data->customerEmail:'';
				$orderCurrency = isset($data->orderCurrency)?$data->orderCurrency:'';

				$mode = CASHFREE_MODE; //USE "PROD" when you are ready to go live.
				if($mode == 'PROD'){
					$host = "https://api.cashfree.com";
					$appId = CASHFREE_APP_ID; //CODE for fetching your appId from your config files
					$secretKey= CASHFREE_SECRET_KEY; //CODE to fetching your secretKey from your config files
				} else {
					$host = "https://test.cashfree.com";
					$appId = CASHFREE_APP_ID; //CODE for fetching your appId from your config files
					$secretKey= CASHFREE_SECRET_KEY; //CODE to fetching your secretKey from your config files
				}
				$url = $host . "/api/v2/cftoken/order";
				
				$orderId=rand(000000, 999999);
				// Get cURL resource
				$curl = curl_init();
				// Set some options - we are passing in a useragent too here
				curl_setopt($curl, CURLOPT_HTTPHEADER, array(
					'x-client-id: ' . $appId,
					'x-client-secret: ' . $secretKey,
					'Content-Type:application/json'
				));

				$json = json_encode (array(
						'orderId' =>  $orderId,
						'orderAmount' => $orderAmount,
						'orderCurrency' => $orderCurrency,
					));
				curl_setopt_array($curl, array(
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL => $url,
					CURLOPT_POST => 1,
					CURLOPT_POSTFIELDS => $json
				));
			
				// Send the request & save response to $resp
				$resp = curl_exec($curl);
				// Close request to clear up some resources
				curl_close($curl);
				$cf_response = json_decode($resp, true);
				//{"status":"OK","message":"Token generated","cftoken":"5u9JCN4MzUIJiOicGbhJCLiQ1VKJiOiAXe0Jye.3P0nI0UzM3QzM1AzMlV2Y1IiOiQHbhN3XiwSMxgTNwcTM2UTM6ICc4VmIsIiUOlkI6ISej5WZyJXdDJXZkJ3biwiIwATNiojI05Wdv1WQyVGZy9mIsMjN4AjN3ojIklkclRmcvJye.3JYHUWluTVa-y3_eJ0-WbpyPqz9W-NwC7y6gEJCge6h5DD9kzSgIGsMCzicop2_k3_"}
				if(isset($cf_response) && isset($cf_response['status']) && $cf_response['status'] !=null && $cf_response['status']=='OK'){
					$token = $cf_response["cftoken"];
					$response = array("token" => $token, "orderId" => $orderId,"appId" => $appId,"mode"=>$mode);
					$return['result'] = "success";
					$return['message'] = "";
					$return['data'] = $response;
					echo json_encode($return); exit;
				}else{
					$return['result'] = "fail";
					$return['message'] = "";
					echo json_encode($return); exit;	
				}
			}
			else{
				$return['result'] = "fail";
				$return['message'] = "Invalid details you entered...!";
				echo json_encode($return); exit;
			}
		}

		function delete_favourite() {
			$headers = $this->input->get_request_header('Authenticate');
			if (isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')) {
	
				if (isset($_POST['data']) && !empty($_POST['data'])) {
					$post_data = json_decode($_POST['data']); // get posted data
					$wishlist_id = $post_data->wishlist_id; // required
	
					if (empty($wishlist_id)) {
	
						$return['result'] = "fail";
						$return['message'] = "Invalid details you entered...!";
						echo json_encode($return);
						exit;
					} else {
						$record = $this->Production_model->delete_record('product_like', array('like_id' => $wishlist_id));
						if ($record == 1) {
							$return['result'] = "success";
							$return['message'] = "Wishlist deleted successfully...!";
							echo json_encode($return);
							exit;
						} else {
							$return['result'] = "fail";
							$return['message'] = "Wishlist not deleted...!";
							echo json_encode($return);
							exit;
						}
					}
				} else {
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return);
					exit;
				}
			} else {
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return);
				exit;
			}
		}

		function update_post_status() {
			$headers = $this->input->get_request_header('Authenticate');
			if (isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')) {
	
				if (isset($_POST['data']) && !empty($_POST['data'])) {
					$post_data = json_decode($_POST['data']); // get posted data
					$product_id = isset($post_data->product_id) ? $post_data->product_id : ''; // required
					$status = isset($post_data->status) ? $post_data->status : ''; // required
					$call_for = isset($post_data->call_for) ? $post_data->call_for : ''; // required
					/*calfor : approve-reject , delete , close*/

					if (empty($product_id) && empty($status) && empty($call_for)) {
	
						$return['result'] = "fail";
						$return['message'] = "Invalid details you entered...!";
						echo json_encode($return);exit;
					} else {
						if ($call_for == 'enable-disable') { //enable-disable advertisement
							if ($status == '1') { // enable
								$status = '1';
								$order_status = '0';
							}
							elseif ($status == '0') { // disable
								$status = '0';
								$order_status = '1';
							}
							$data = array('status'=>$status,'order_status'=>$order_status);
							$record = $this->Production_model->update_record('post_management',$data,array('post_id' => $product_id));
							$message = $status == '0' ? "Post disable successfully...!" : "Post enable successfully...!";							
						}
						elseif ($call_for == 'close') {//close advertisement
							$data = array('order_status'=>'1');
							$record = $this->Production_model->update_record('post_management',$data,array('post_id' => $product_id));
							$message = "Post close successfully...!";
						}
						elseif ($call_for == 'delete') { //delete advertisement
							$record = $this->Production_model->delete_record('post_management', array('post_id' => $product_id));
							$message = "Post deleted successfully...!";
						}
						$return['result'] = "success";
						$return['message'] = $message;
						echo json_encode($return);exit;
					}
				} else {
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return);
					exit;
				}
			} else {
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return);
				exit;
			}
		}

		function get_product_favourite() {
			$headers = $this->input->get_request_header('Authenticate');
			if (isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')) {
				if (isset($_POST['data']) && !empty($_POST['data'])) {
					$data = json_decode($_POST['data']);
					$num = isset($data->num) ? $data->num : '';
					$user_id = isset($data->user_id) ? $data->user_id : ''; // as a user-id.
	
					if (!empty($user_id)) {
						$rec = array();
	
						/* Filter product */
						$where['product_like.user_id'] = $user_id;
	
						$join[0]['table_name'] = 'post_management';
						$join[0]['column_name'] = 'post_management.post_id = product_like.product_id';
						$join[0]['type'] = 'left';
	
						$join[1]['table_name'] = 'user_register';
						$join[1]['column_name'] = 'user_register.user_id = post_management.user_id';
						$join[1]['type'] = 'left';
		
						$join[2]['table_name'] = 'category';
						$join[2]['column_name'] = 'category.category_id = post_management.category_id';
						$join[2]['type'] = 'left';
	
						$join[3]['table_name'] = 'sub_category';
						$join[3]['column_name'] = 'sub_category.sub_category_id = post_management.sub_category_id';
						$join[3]['type'] = 'left';
	
						$join[4]['table_name'] = 'package_management';
				        $join[4]['column_name'] = 'package_management.id = post_management.package_id';
						$join[4]['type'] = 'left';

						$join[5]['table_name'] = 'country';
						$join[5]['column_name'] = 'country.id = user_register.id_country';
						$join[5]['type'] = 'left';
						
						$join[6]['table_name'] = 'state';
						$join[6]['column_name'] = 'state.id = user_register.id_state';
						$join[6]['type'] = 'left';

						$join[7]['table_name'] = 'city';
						$join[7]['column_name'] = 'city.id = user_register.id_city';
						$join[7]['type'] = 'left';
						
						$product_filter = $this->Production_model->jointable_descending(array('post_management.*', 'category.category_name', 'sub_category.sub_category_name','product_like.like_id','category.cat_image','sub_category.sub_category_name','user_register.name','user_register.user_email','user_register.id_country','user_register.id_state','user_register.id_city','user_register.address','user_register.gender','user_register.mobile_no','user_register.profile_picture','user_register.mobile_no_display','country.country_name','state.state_name','city.city_name','package_management.name as package_name','package_management.price as package_price','package_management.discription as package_description','package_management.day'), 'product_like', '', $join, 'product_like.product_id', 'desc', $where, '', '', 10, $num);
	
						 //echo"<pre>"; echo $this->db->last_query(); print_r($product_filter); exit;
	
						if ($product_filter != null) {
							foreach ($product_filter as $key => $value) {
								$created_date = date('Y-m-d',strtotime($value['create_date']));
                            	$expire_post = date('Y-m-d',strtotime($created_date . + $value['day'] .'days'));

								$rec[$key]['wishlist_id'] = $value['like_id'];
								$rec[$key]['post_id'] = $value['post_id'];
								$rec[$key]['user_id'] = $value['user_id'];
								$rec[$key]['category_id'] = $value['category_id'];
								$rec[$key]['category_name'] = $value['category_name'];
								$rec[$key]['cat_image'] = $value['cat_image'];
								$rec[$key]['sub_category_id'] = $value['sub_category_id'];
								$rec[$key]['sub_category_name'] = $value['sub_category_name'];
								$rec[$key]['i_want'] = $value['i_want'];
								$rec[$key]['post_description'] = $value['post_description'];
								$rec[$key]['price'] = $value['price'];
								$rec[$key]['post_approve_reject_resion'] = $value['post_approve_reject_resion'];
								$rec[$key]['create_date'] = date('d M, Y',strtotime($value['create_date']));

								$rec[$key]['country_id'] = $value['id_country'];
								$rec[$key]['country_name'] = $value['country_name'];
								$rec[$key]['state_id'] = $value['id_state'];
								$rec[$key]['state_name'] = $value['state_name'];
								$rec[$key]['city_id'] = $value['id_city'];
								$rec[$key]['city_name'] = $value['city_name'];

								$rec[$key]['user_name'] = $value['name'];
								$rec[$key]['email'] = $value['user_email'];
								$rec[$key]['mobile_no_display'] = $value['mobile_no_display']; //0=hide , 1=show.
								$rec[$key]['mobile_no'] = $value['mobile_no'];
								$rec[$key]['profile_pic'] = $value['profile_picture']; 
								$rec[$key]['ads_status'] = $value['ads_status'];

								$rec[$key]['package_name'] = $value['package_name'];
								$rec[$key]['package_price'] = $value['package_price'];
								$rec[$key]['package_day'] = $value['day'];
								$rec[$key]['package_expire'] = $expire_post;
								$rec[$key]['is_expire'] = $expire_post > date('Y-m-d') ? '0' : '1';
								$rec[$key]['package_description'] = $value['package_description'];
								$rec[$key]['invoice'] = base_url(POST_INVOICE.$value['invoice']);

								$post_images = $this->Production_model->get_all_with_where('post_similer_image','image_id','desc',array('post_id'=>$value['post_id']));

								$images = array();
								foreach ($post_images as $key1 => $value) {
									$images[] = array(
										'image_id' => $value['image_id'], 
										'post_images' => $value['similar_image'] 
									);
								}				
						       	$rec[$key]['post_images'] = $images !=null ? $images : null;
							}
							$return['result'] = "success";
							$return['message'] = "Record Found Successfully...!";
							$return['data'] = $rec;
							echo json_encode($return);exit;
						} else {
							if ($num > 0) {
								$return['result'] = "fail";
								$return['message'] = "No more result...!";
								echo json_encode($return);
								exit;
							}
							$return['result'] = "fail";
							$return['message'] = "Wishlist Not Available...!";
							echo json_encode($return);
							exit;
						}
					} else {
						$return['result'] = "fail";
						$return['message'] = "Blank Data Received";
						echo json_encode($return);
						exit;
					}
				} else {
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return);
					exit;
				}
			} else {
				$return['result'] = "fail";
				$return['message'] = "Invalid details you entered...!";
				echo json_encode($return);
				exit;
			}
		}

		function add_to_favourite() {
			$headers = $this->input->get_request_header('Authenticate');
			if (isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')) {
	
				if (isset($_POST['data']) && !empty($_POST['data'])) {
					$post_data = json_decode($_POST['data']); // get posted data
					$user_id = $post_data->user_id; // required
					$product_id = $post_data->product_id; // required
					$create_date = date('Y-m-d H:i:s'); // required added from server side time stamp
	
					if (empty($product_id) || empty($user_id)) {
	
						$return['result'] = "fail";
						$return['message'] = "Invalid details you entered...!";
						echo json_encode($return);
						exit;
					} else {
						$data = array(
							'product_id' => $product_id,
							'user_id' => $user_id,
							'create_date' => date('Y-m-d H:i:s')
						);
						// echo"<pre>"; print_r($data); exit;
	
						$get_product = $this->Production_model->get_all_with_where('product_like', '', '', array('user_id' => $user_id, 'product_id' => $product_id));
						if (count($get_product) > 0) {
							$record = $this->Production_model->delete_record('product_like', array('user_id' => $user_id));
							$return['result'] = "success";
							$return['message'] = "Liked remove successfully";
							echo json_encode($return);
							exit;
						}
	
						$record = $this->Production_model->insert_record('product_like', $data);
						if ($record != '') {
							$return['result'] = "success";
							$return['message'] = "Liked successfully";
							echo json_encode($return);
							exit;
						} else {
							$return['result'] = "fail";
							$return['message'] = "Invalid details you entered...!";
							echo json_encode($return);
							exit;
						}
					}
				} else {
					$return['result'] = "fail";
					$return['message'] = "Invalid details you entered...!";
					echo json_encode($return);
					exit;
				}
			} else {
				$return['result'] = "fail";
				$return['message'] = "You are not authorized user!!!";
				echo json_encode($return);
				exit;
			}
		}	
	}
	/* End of file Admin.php */
	/* Location: ./application/controllers/api/Admin.php */
?>
