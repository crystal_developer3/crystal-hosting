<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        // header('Content-Type: application/json');
    }

    //=====================================================================//
    //========================== Call from web ============================//
    //=====================================================================//
    function index(){
        if (!$this->session->userdata('login_id')) {
            redirect(base_url('login'));
        }

        $user_id = $this->session->userdata('login_id');
        /*Notification count*/
        $data = array('read_status'=>1);
        $record = $this->Production_model->update_record('notification',$data,array('user_id'=>$user_id));
        /*End*/

        $tmp_data = get_notifications($user_id);
        $convert_array = json_decode($tmp_data);
        if (isset($convert_array->data) && $convert_array->data !=null) {
            $get_details = json_decode(json_encode($convert_array->data), true); // convert  object to array.
        }
        $data['notification_list'] = isset($get_details) && $get_details !=null ? $get_details : array(); 
        // echo"<pre>"; print_r($data); exit;
        $this->load->view('chat/notification_list',$data);
    }

    function remove_notification($user_id,$notification_id,$type,$status){
        update_notification($user_id,$notification_id,$type,$status);
        $this->session->set_flashdata('Notification removed successfully...!', validation_errors());
        redirect($_SERVER['HTTP_REFERER']);
    }

    function count_notification(){
        $user_id = $this->input->post('user_id');
        $where['notification.user_id'] = $user_id;
        $where['notification.read_status'] = 0;

        $join[0]['table_name'] = 'notification_status';
        $join[0]['column_name'] = 'notification_status.notification_id = notification.notification_id';
        $join[0]['type'] = 'left';

        $join[1]['table_name'] = 'post_management';
        $join[1]['column_name'] = 'post_management.post_id = notification.product_id';
        $join[1]['type'] = 'left';

        $join[2]['table_name'] = 'chat_invitation';
        $join[2]['column_name'] = 'chat_invitation.product_id = notification.product_id';
        $join[2]['type'] = 'left';

        $get_user = $this->Production_model->jointable_descending(array('notification.*','notification_status.read_status','notification_status.remove_status','post_management.*','chat_invitation.seller_status'),'notification','',$join,'notification.notification_id','desc',$where);

        // echo"<pre>"; echo $this->db->last_query(); print_r($get_user); exit;

        if ($get_user !=null) { 
            $rec = array(); 
            $count = 0; 
            foreach ($get_user as $key => $value) {
                if ($value['remove_status'] == null || empty($value['remove_status']) || $value['remove_status'] == 0) {
                    $count++;           
                }
            }   
            
            $return['result'] = "success";
            $return['message'] = "Notification Found Successfully...!";
            $return['data'] = $rec;
            $return['count_notification'] = $count;
            echo json_encode($return);exit;
        }
        else{
            $return['result'] = "fail";
            $return['message'] = "You don't have any notification(s)...!";
            $return['count_notification'] = '0';
            echo json_encode($return);exit;
        }
    } 
         

    //=====================================================================//
    //====================== Call from application ========================//
    //=====================================================================//

    function get_notifications(){
        $headers = $this->input->get_request_header('Authenticate');
        if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')){
            $data  = json_decode($_POST['data']);
            $user_id = isset($data->user_id)?$data->user_id:'';

            if (!empty($user_id))
            {
               echo get_notifications($user_id); exit;
            }	
            else{
                $return['result'] = "fail";
                $return['message'] = "Please All The Fields Required Data...!";
                echo json_encode($return); exit;
            }	
        }	
        else{
            $return['result'] = "fail";
            $return['message'] = "Invalid details you entered...!";
            echo json_encode($return); exit;
        }
    }

    // function notification_list($user_id){

    //    $sql = "SELECT notification.*,notification_status.read_status,notification_status.remove_status,post_management.add_type,post_management.i_want,post_management.post_image,post_management.post_description,post_management.price,post_management.order_id,post_management.package_id,post_management.payment_details,chat_invitation.seller_status,
    //    IF(notification.product_id=chat_invitation.product_id AND chat_invitation.seller_id=notification.user_id , 'Show', 'Hide') as buttons

    //     FROM notification
    //     LEFT JOIN notification_status ON notification_status.notification_id = notification.notification_id
    //     LEFT JOIN post_management ON post_management.post_id = notification.product_id
    //     LEFT JOIN chat_invitation ON chat_invitation.product_id = notification.product_id
    //     WHERE notification.user_id = '".$user_id."' ORDER BY notification.notification_id DESC";

    //     $query = $this->db->query($sql);
    //     $get_user = $query->result_array();

    //     // echo"<pre>"; echo $this->db->last_query(); print_r($get_user); exit; 

    //     if ($get_user !=null) { 
    //         $rec = array(); 
    //         $count = 0; 
    //         foreach ($get_user as $key => $value) {
    //             if ($value['remove_status'] == null || empty($value['remove_status']) || $value['remove_status'] == 0) {
                    
    //                 if ($value['seller_status'] == 0) {
    //                     $status = 'Pending';
    //                 }
    //                 if ($value['seller_status'] == 1) {
    //                     $status = 'Accept';
    //                 }
    //                 if ($value['seller_status'] == 2) {
    //                     $status = 'Reject';
    //                 }
    //                 $rec[$count]['notification_id'] = $value['notification_id'];
    //                 $rec[$count]['title'] = $value['title'];
    //                 $rec[$count]['message'] = $value['description'];
    //                 $rec[$count]['image'] = $value['image'];
    //                 $rec[$count]['type'] = $value['type'];

    //                 $rec[$count]['read_status'] = $value['read_status'];
    //                 $rec[$count]['remove_status'] = $value['remove_status'];
    //                 $rec[$count]['product_id'] = $value['product_id'];
    //                 $rec[$count]['user_id'] = $value['user_id'];
    //                 $rec[$count]['buyer_id'] = $value['buyer_id'];
    //                 $rec[$count]['add_type'] = $value['add_type'];
    //                 $rec[$count]['i_want'] = $value['i_want'];
    //                 $rec[$count]['post_description'] = $value['post_description'];
    //                 $rec[$count]['post_status'] = $status;
    //                 $rec[$count]['buttons'] = $value['buttons'];
    //                 $rec[$count]['created_date'] = date('d-m-Y H:i:s',strtotime($value['create_date']));
    //                 $count++;           
    //             }
    //         }   
            
    //         $return['result'] = "success";
    //         $return['message'] = "Notification Found Successfully...!";
    //         $return['data'] = $rec;
    //         return json_encode($return); 
    //     }
    //     else{
    //         $return['result'] = "fail";
    //         $return['message'] = "You don't have any notification(s)...!";
    //         return json_encode($return);
    //     }
    // } 

    function add_fcm() {

        $headers = $this->input->get_request_header('Authenticate');
        if (isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')) {
            $data = json_decode($_POST['data']);

            $token = isset($data->token) ? $data->token : '';
            $device_id = isset($data->device_id) ? $data->device_id : '';
            $user_id = isset($data->user_id) ? $data->user_id : '';

            if (!empty($token)) {
                $data = array(
                    'token' => $token,
                    'device_id' => $device_id,
                    'user_id' => $user_id
                );

                $get_user_tocken = $this->Production_model->get_all_with_where('notification_token', '', '', array('user_id' => $user_id));
                if ($get_user_tocken != null) {
                    $record = $this->Production_model->update_record('notification_token', $data, array('user_id' => $user_id));
                    $return['result'] = "success";
                    $return['message'] = "Token Updated Successfully...!";
                    echo json_encode($return);exit;
                } else {
                    $add_token = $this->Production_model->insert_record('notification_token', $data);

                    if ($add_token != '') {
                        $return['result'] = "success";
                        $return['message'] = "Token Inserted Successfully...!";
                        echo json_encode($return);exit;
                    } else {
                        $return['result'] = "fail";
                        $return['message'] = "Token Not Inserted...!";
                        echo json_encode($return);exit;
                    }
                }
            } else {
                $return['result'] = "fail";
                $return['message'] = "Please Enter All The Fields Required Data...!";
                echo json_encode($return);exit;
            }
        } else {
            $return['result'] = "fail";
            $return['message'] = "Invalid details you entered...!";
            echo json_encode($return);exit;
        }
    }

    function update_notification() {

        $headers = $this->input->get_request_header('Authenticate');
        if (isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')) {
            $data = json_decode($_POST['data']);
            $user_id = isset($data->user_id) ? $data->user_id : '';
            $notification_id = isset($data->notification_id) ? $data->notification_id : '';
            $type = isset($data->type) ? $data->type : '';
            $status = 1;

            update_notification($user_id,$notification_id,$type,$status);

            $return['result'] = "success";
            $return['message'] = "";
            echo json_encode($return);exit;
        } else {
            $return['result'] = "fail";
            $return['message'] = "Invalid details you entered...!";
            echo json_encode($return);exit;
        }
    }

    function get_unread_count() {

        $headers = $this->input->get_request_header('Authenticate');
        if (isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')) {
            $data = json_decode($_POST['data']);

            $user_id = isset($data->user_id) ? $data->user_id : '';
            $rec = array();

            if (!empty($user_id)) {
                $where['notification.user_id'] = $user_id;
                $where_or['notification.user_id'] = 0;

                $join[0]['table_name'] = 'notification_status';
                $join[0]['column_name'] = 'notification_status.notification_id = notification.notification_id';
                $join[0]['type'] = 'left';

                $get_notification = $this->Production_model->jointable_descending(array('notification.*', 'notification_status.read_status', 'notification_status.remove_status'), 'notification', '', $join, 'notification.notification_id', 'desc', $where, $where_or);

                // echo"<pre>"; echo $this->db->last_query(); print_r($get_notification); exit;
                $counter = 0;
                if ($get_notification != null) {
                    foreach ($get_notification as $key => $value) {
                        if ($value['remove_status'] == null || $value['remove_status'] == '0') {
                            if ($value['read_status'] == null) {
                                $counter++;
                            }
                        }
                    }
                    $return['result'] = "success";
                    $return['message'] = "Notification Found Successfully...!";
                    $return['unread_count'] = $counter;
                    echo json_encode($return);
                    exit;
                } else {
                    $return['result'] = "fail";
                    $return['message'] = "You don't have any notification(s)...!";
                    echo json_encode($return);
                    exit;
                }
            } else {
                $return['result'] = "fail";
                $return['message'] = "Please All The Fields Required Data...!";
                echo json_encode($return);
                exit;
            }
        } else {
            $return['result'] = "fail";
            $return['message'] = "Invalid details you entered...!";
            echo json_encode($return);
            exit;
        }
    }
}
?>