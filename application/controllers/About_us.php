<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class About_us extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();			
		}

		function index()
		{
			$data['about_data'] = $this->Production_model->get_all_with_where('about','','',array('status'=>'1'));
			// echo"<pre>";echo $this->db->last_query(); print_r($data); exit;
			$this->load->view('about_us',$data);	
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>