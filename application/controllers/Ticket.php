<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Ticket extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();	
			if (!$this->session->userdata('login_id')) {
				redirect(base_url('login'));
			}		
		}
		function index()
		{
			$data = array();
			$conditions = array("where"=>array('tickets.user_id'=>$this->session->login_id),"select"=>"tickets.*,user_register.first_name,user_register.id as user_id","join"=>array("table"=>"user_register","join_type"=>"LEFT","join_conditions"=>"tickets.user_id=user_register.id"),"ORDER BY"=>array('id'=>'DESC'));
	        $data = $this->common_model->select_data("tickets", $conditions);
	        $data['tickets_details'] = $data['data'];
			$this->load->view('ticket',$data);	
		}
		function my_tickets()
		{
			$data = array();
			$conditions = array("where"=>array('tickets.user_id'=>$this->session->login_id),"select"=>"tickets.*,user_register.first_name,user_register.id as user_id","join"=>array("table"=>"user_register","join_type"=>"LEFT","join_conditions"=>"tickets.user_id=user_register.id"),"ORDER BY"=>array('id'=>'DESC'));
	        $data = $this->common_model->select_data("tickets", $conditions);
	        $data['tickets_details'] = $data['data'];
			$this->load->view('my_tickets',$data);	
		}
		function generate_ticket(){
			$data = array();
			if($this->input->is_ajax_request()){
				$data = $this->input->post();
				print_r($data);exit;
			}
			$this->load->view('generate_ticket',$data);	
		}
		function ticket_reply($id){
			$data = array();
			if($this->input->is_ajax_request()){
				$data = $this->input->post();
				print_r($data);exit;
			}

			$conditions = array("where"=>array('tickets.id'=>$id),"select"=>"tickets.*,user_register.first_name,user_register.id as user_id","join"=>array("table"=>"user_register","join_type"=>"LEFT","join_conditions"=>"tickets.user_id=user_register.id"),"ORDER BY"=>array('id'=>'DESC'));
	        $data = $this->common_model->select_data("tickets", $conditions);
	        $data['ticket_info'] = $data['data'][0];

	        $where['tickets.id'] = $id;
	        
	        $join1[0]['table_name'] = 'tickets';
	        $join1[0]['column_name'] = 'ticket_reply.ticket_id = tickets.id';
	        $join1[0]['type'] = 'left';

	        $join1[1]['table_name'] = 'user_register';
	        $join1[1]['column_name'] = 'ticket_reply.reply_from = user_register.id or ticket_reply.reply_to = user_register.id';
	        $join1[1]['type'] = 'left';
	        
	        $data['tickets_details'] = $this->Production_model->jointable_descending(array('ticket_reply.*','ticket_reply.ticket_reply','ticket_reply.reply_type','ticket_reply.reply_from','ticket_reply.reply_to','user_register.first_name','ticket_reply.reply_date','ticket_reply.reply_time'), 'ticket_reply', '', $join1, 'ticket_reply.id', 'desc', $where,'',array('ticket_reply.id'));

	        if (isset($this->session->ticket_msg) && $this->session->ticket_msg != '') {
	            $data = array_merge($data, array("success" => $this->session->ticket_msg));
	            $this->session->plan_msg = '';
	        }
	        unset($settings, $conditions);
			$this->load->view('ticket_reply',$data);	
		}

		/*public function changepassword() {
	        // $data = array("form_title" => "Change Password");
	        $this->form_validation->set_rules('current_password', 'current password', 'required|min_length[6]', array('required' => 'Please enter current password'));
	        $this->form_validation->set_rules('new_password', 'new password', 'required|min_length[6]', array('required' => 'Please enter new password'));
	        $this->form_validation->set_rules('confirm_new_password', 'confirm new password', 'required|min_length[6]|matches[new_password]', array('required' => 'Please enter confirm password', "matches" => "Password and confirm password should be same"));

	        if ($this->form_validation->run() === FALSE) {
	            $data = array_merge($data, $_POST);
	        } else {
	            $current_password = $this->input->post("current_password");
	            $get_record = $this->Production_model->get_all_with_where('user_register','','',array('user_id'=>$this->session->userdata('login_id')));
				$check = $this->encryption->decrypt($get_record[0]['password']); 

				if($check != $current_password){           
	                // $data = array_merge($data, array("error" => "Please enter correct old password","current_password_error" => "Please enter correct old password"));
	                $this->session->set_flashdata('error', 'Please enter correct old password...!');
		            redirect($_SERVER['HTTP_REFERER']);
	                // echo"<pre>"; print_r($data); exit;
	            } else {
	                $new_password = $this->input->post("new_password");
	                $records = array(
	                    "password" => $this->encryption->encrypt($new_password),
	                );
	                $conditions = array(
	                    "where" => array("user_id" => $this->session->userdata('login_id')),
	                );
	                $this->common_model->update_data('user_register', $records, $conditions);
	                $this->session->set_flashdata('success', 'Password changed successfully...!');
		            redirect($_SERVER['HTTP_REFERER']);
	                // $_POST = array();
	                // $data = array_merge($data, array("success" => "Password changed successfully"));
	            }
	        }
	        redirect($_SERVER['HTTP_REFERER']);
	    }*/

	   /* public function upload_profile_photo() {
	        $response_array = array('success' => false);
	        $error = false;
	        $error_messages = array();
	        if (isset($_FILES['profile_picture']) && $_FILES['profile_picture']['name'] != "") {
	        	// echo"<pre>"; print_r($_FILES); exit;

	            if ($_FILES["profile_picture"]["size"] >= MAX_FILE_SIZE_IMAGE) {
	                $response_array['message'] = 'Please upload file with size less than ' . (MAX_FILE_SIZE_IMAGE / 1000000) . 'MB';
	                $error = true;
	            }
	            if (!$error) {
	                $config['upload_path'] = './assets/uploads/profile_picture/';                
	                if (!is_dir($config['upload_path'])) {
	                    mkdir($config['upload_path']);
	                    @chmod($config['upload_path'], 0777);

	                    mkdir($config['upload_path'] . 'thumb/');
	                    @chmod($config['upload_path'] . 'thumb/', 0777);    
	                }
	                $config['allowed_types'] = 'gif|jpg|png|jpeg';
	                //$config['max_size'] = MAX_FILE_SIZE_IMAGE;
	                $config['encrypt_name'] = TRUE;
	                $this->load->library('upload', $config);
	                $this->upload->initialize($config);
	                if (!$this->upload->do_upload('profile_picture')) {
	                    $response_array['message'] = $this->upload->display_errors();
	                    $error = true;
	                } else {

	                     // delete old photo 
	                    $get_image = $this->Production_model->get_all_with_where('user_register','','',array('user_id'=>$this->session->userdata('login_id')));

	                    // Old image delete
		                if ($get_image !=null && $get_image[0]['profile_picture'] !=null && !empty($get_image[0]['profile_picture']))
		                {
		                    @unlink(PROFILE_PICTURE.$get_image[0]['profile_picture']);
		                }
	                    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.

	                    // echo"<pre>"; print_r($upload_data); exit;

	                    $file_name = $upload_data['file_name'];
	                    // $this->generate_thumbnail($file_name);
	                    @chmod($config['upload_path'] . $file_name, 0777);

	                    $records = array();
	                    $records["profile_picture"] = $file_name;
	                    $conditions = array(
	                        'where' => array('user_id' => $this->session->userdata('login_id')),
	                    );
	                    $this->common_model->update_data('user_register', $records, $conditions);
	                    // $this->common_functions->update_candidate_session($this->session->front_user_data['id']);
	                    $session = array(
							'profile_picture' => $file_name
						);		
						$this->session->set_userdata($session);

	                    $response_array['success'] = true;
	                    $response_array['message'] = 'Profile Photo updated successfully';
	                }
	            }
	        } else {
	            $response_array['message'] = 'Please select image';
	        }
	        echo json_encode($response_array);
	        exit;
	    } */
	}	
?>