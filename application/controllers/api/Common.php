<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

    function __construct() {
        parent::__construct();
        // header('Content-Type: application/json');
    }
	function getCountries() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					getCountry($is_api_call);            
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function getStates() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			$country_id = isset($data->country_id) ? $data->country_id :'';

			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					if(checkEmpty($country_id)){
						echo json_encode(returnResponse("Fail","Country id is empty",null));exit;
					}else{
						getStates($country_id,$is_api_call);            
					}
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function getCities() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			$state_id = isset($data->state_id) ? $data->state_id :'';
			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					if(checkEmpty($state_id)){
						echo json_encode(returnResponse("Fail","State id is empty",null));exit;
					}else{
						getCities($state_id,$is_api_call);            
					}
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function getCustomerSupport() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					getCustomerSupport($is_api_call);            
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function getHostingDetails() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					getHostingDetails($is_api_call);            
					
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}

	function getDepartments() {
		if(!checkEmpty($_POST['data'])){
			$data = json_decode($_POST['data']);
			$is_api_call = isset($data->is_api_call) ? $data->is_api_call :'';
			if(!checkEmpty($is_api_call)){ // for api
				header('Content-Type: application/json');
				if(checkHeader()){
					getDepartments($is_api_call);            
					
				}else {
					echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
				}
			}else{ //for web

			}
		}else{
			echo json_encode(returnResponse("Fail","You are not authorized user...",null));exit;
		}
	}
	
}
?>
