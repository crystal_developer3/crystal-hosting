<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        // header('Content-Type: application/json');
    }
    //=====================================================================//
    //========================= Call from web =============================//
    //=====================================================================//

    function chat_invitation(){
    	$post_data = json_encode($this->input->post());
    	$data = json_decode($post_data);

    	$product_id = isset($data->product_id) ? $data->product_id :'';
        $buyer_id = isset($data->buyer_id) ? $data->buyer_id :'';
        $seller_id = isset($data->seller_id) ? $data->seller_id :'';

    	send_chat_invitation($product_id,$buyer_id,$seller_id);
    }

    function accept_reject_chat(){
    	$post_data = json_encode($this->input->post());
    	$data = json_decode($post_data);

    	$product_id = isset($data->product_id) ? $data->product_id :'';
        $buyer_id = isset($data->buyer_id) ? $data->buyer_id :'';
        $seller_id = isset($data->seller_id) ? $data->seller_id :'';
        $status = isset($data->status) ? $data->status :'';

    	accept_reject_chat_invitation($product_id,$buyer_id,$seller_id,$status);
    }

    function add(){
    	$post_data = json_encode($this->input->post());
    	$data = json_decode($post_data);

    	$product_id = isset($data->product_id) ? $data->product_id :'';
        $buyer_id = isset($data->buyer_id) ? $data->buyer_id :'';
        $seller_id = isset($data->seller_id) ? $data->seller_id :'';
        $message_send_by = isset($data->message_send_by) ? $data->message_send_by :'';
        $message = isset($data->message) ? $data->message :'';

	    add_chat($product_id,$buyer_id,$seller_id,$message_send_by,$message); 
    }

    function user_chat_list(){ //User chat history.   	      
        $post_data = json_encode($this->input->post());
    	$data = json_decode($post_data);

    	$product_id = isset($data->product_id) ? $data->product_id :'';
        $buyer_id = isset($data->buyer_id) ? $data->buyer_id :'';
        $seller_id = isset($data->seller_id) ? $data->seller_id :'';
        $message_send_by = isset($data->message_send_by) ? $data->message_send_by :'';

        $update_record = array('is_viewed'=>'1');
        $update_chat_details = $this->Production_model->update_record('chat',$update_record,array('product_id' => $product_id,'buyer_id' => $buyer_id,'seller_id' => $seller_id,'message_send_by !='=>$message_send_by));

        $get_chat_details = $this->Production_model->get_all_with_where('chat','id','asc', array('product_id' => $product_id,'buyer_id' => $buyer_id,'seller_id' => $seller_id));

        // echo"<pre>"; echo $this->db->last_query(); print_r($get_chat_details); exit;

        if (isset($get_chat_details) && $get_chat_details != null) {
        	foreach ($get_chat_details as $key => $value) {
        		$get_name = $this->Production_model->get_where_user('user_id,name,profile_picture','user_register','','',array('user_id' => $value['message_send_by']));

        		$datetime = date('d-m-Y h:i:s A',strtotime($value['created_at']));
        		if ($value['message_send_by'] == $message_send_by) { //buyer-messages.
        			echo '<div class="outgoing_msg">
        				<div class="sent_msg"><p>'.$value['message'].'</p>
        					<span class="time_date"> '.$datetime.' </span> </div>
        			</div>';
        		}
        		if ($value['message_send_by'] != $message_send_by) { //seller-messages.
        			echo '<div class="incoming_msg">
                        <div class="incoming_msg_img"> <img src="'.base_url(PROFILE_PICTURE.$get_name[0]['profile_picture']).'" onerror="this.src=https://ptetutorials.com/images/user-profile.png"> </div>
                        <div class="received_msg">
                            <div class="received_withd_msg">
                                <span class="time_date"><b>Name : '.$get_name[0]['name'].' </b> </span>  
                                <p>'.$value['message'].'</p>
                            	<span class="time_date">'.$datetime.'</span>
                            </div>
                        </div>
                    </div>';
        		}
        	}
        }
        else{
        	echo "<center>Chat not available for this post...!</center>";
        }
    }

    function seller_chat(){
    	$post_data = json_encode($this->input->post());
    	$data = json_decode($post_data);
    	// echo"<pre>"; print_r($data); exit;

    	$product_id = isset($data->product_id) ? $data->product_id :'';
        $buyer_id = isset($data->buyer_id) ? $data->buyer_id :'';
        $seller_id = isset($data->seller_id) ? $data->seller_id :'';
        $message_send_by = isset($data->message_send_by) ? $data->message_send_by :'';
        
        $get_chat_details = $this->Production_model->get_all_with_where('chat','id','asc', array('product_id' => $product_id,'buyer_id' => $buyer_id,'seller_id' => $seller_id,'is_viewed'=>'0'));

        $update_record = array('is_viewed'=>'1');
        $update_chat_details = $this->Production_model->update_record('chat',$update_record,array('product_id' => $product_id,'buyer_id' => $buyer_id,'seller_id' => $seller_id,'message_send_by !='=>$message_send_by));

        // echo"<pre>"; echo $this->db->last_query(); print_r($get_chat_details); exit;
        $result = 'fail';
        if (isset($get_chat_details) && $get_chat_details != null) {
        	ob_start();

        	foreach ($get_chat_details as $key => $value) {
        		$get_name = $this->Production_model->get_where_user('user_id,name,profile_picture','user_register','','',array('user_id' => $value['message_send_by']));

        		$datetime = date('d-m-Y h:i:s A',strtotime($value['created_at']));
        		
        		if ($value['message_send_by'] != $message_send_by) { //seller-messages.
        			$result = 'success';
        			?><div class="incoming_msg">                        
                        <div class="incoming_msg_img"> <img src="<?= base_url(PROFILE_PICTURE.$get_name[0]['profile_picture'])?>" onerror="this.src='https://ptetutorials.com/images/user-profile.png'"> </div>
                        <div class="received_msg">
                            <div class="received_withd_msg">
                            	<span class="time_date"><b>Name : <?= $get_name[0]['name']?> </b> </span> 
                                <p><?= $value['message']?></p>
                                <span class="time_date"><?= $datetime?></span>
                            </div>
                        </div>
                    </div>
                    <?php
        		}
        	}
        	$return['html'] = ob_get_clean(); 		
        }
        $return['result'] = $result;
    	echo json_encode($return);exit;
    }
   

    //=====================================================================//
    //====================== Call from application ========================//
    //=====================================================================//

    function checkHeader(){
        $headers = $this->input->get_request_header('Authenticate');
        if (isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')) {
            return true;
        }else{
            return false;
        }
    }

    function send_chat_invitation() {
        //todo send notification which contain 
        //1) product id
        //2) sender id
        if($this->checkHeader()){
            $data = json_decode($_POST['data']);
            $product_id = isset($data->product_id) ? $data->product_id :'';
	        $buyer_id = isset($data->buyer_id) ? $data->buyer_id :'';
	        $seller_id = isset($data->seller_id) ? $data->seller_id :'';

	        send_chat_invitation($product_id,$buyer_id,$seller_id);            
        }else {
            $return['result'] = "fail";
            $return['message'] = "Invalid details you entered...!";
            echo json_encode($return);exit;
        }
    }

    function accept_reject_chat_invitation() 
    {
        if($this->checkHeader()){
            $data = json_decode($_POST['data']);
            $product_id = isset($data->product_id) ? $data->product_id :'';
            $buyer_id = isset($data->buyer_id) ? $data->buyer_id :''; //id who send invitation to login user 
            $seller_id = isset($data->seller_id) ? $data->seller_id :''; //receiver id whose post is this
            $status = isset($data->status) ? $data->status :'';

            accept_reject_chat_invitation($product_id,$buyer_id,$seller_id,$status);
        }else {
            $return['result'] = "fail";
            $return['message'] = "Invalid details you entered...!";
            echo json_encode($return);exit;
        }
    }

    function add_chat(){
    	if($this->checkHeader()){
            $data = json_decode($_POST['data']);
            $product_id = isset($data->product_id) ? $data->product_id :'';
	        $buyer_id = isset($data->buyer_id) ? $data->buyer_id :'';
	        $seller_id = isset($data->seller_id) ? $data->seller_id :'';
	        $message_send_by = isset($data->message_send_by) ? $data->message_send_by :''; //user-loginid.
	        $message = isset($data->message) ? $data->message :''; 
	        $last_chat_id = isset($data->last_chat_id) ? $data->last_chat_id :''; 

	        add_chat($product_id,$buyer_id,$seller_id,$message_send_by,$message,$last_chat_id); 	               
        }else {
            $return['result'] = "fail";
            $return['message'] = "Invalid details you entered...!";
            echo json_encode($return);exit;
        }
    }

    function user_chat_history() {

        if($this->checkHeader()){
            $data = json_decode($_POST['data']);
            $product_id = isset($data->product_id) ? $data->product_id :'';
	        $buyer_id = isset($data->buyer_id) ? $data->buyer_id :'';
	        $seller_id = isset($data->seller_id) ? $data->seller_id :'';
	        $message_send_by = isset($data->message_send_by) ? $data->message_send_by :'';

	        user_chat_history($product_id,$buyer_id,$seller_id,$message_send_by);            
        }else {
            $return['result'] = "fail";
            $return['message'] = "Invalid details you entered...!";
            echo json_encode($return);exit;
        }
    }

    function get_chat_message(){ //call seller chat in one by one.
    	if($this->checkHeader()){
            $data = json_decode($_POST['data']);
            $product_id = isset($data->product_id) ? $data->product_id :'';
	        $buyer_id = isset($data->buyer_id) ? $data->buyer_id :'';
	        $seller_id = isset($data->seller_id) ? $data->seller_id :'';
	        $message_send_by = isset($data->message_send_by) ? $data->message_send_by :'';
	        $chat_id = isset($data->chat_id) ? $data->chat_id :'';

	        get_chat_message($product_id,$buyer_id,$seller_id,$message_send_by,$chat_id);            
        }else {
            $return['result'] = "fail";
            $return['message'] = "Invalid details you entered...!";
            echo json_encode($return);exit;
        }
    }

    function get_chat_list(){ //Used for autoload in timer.
        $headers = $this->input->get_request_header('Authenticate');
        if(isset($headers) && !empty($headers) && $headers == $this->config->item('apikey')){
            $data  = json_decode($_POST['data']);
            $user_id = isset($data->user_id)?$data->user_id:'';

            if (!empty($user_id))
            {
               echo get_chat_list($user_id); exit;
            }   
            else{
                $return['result'] = "fail";
                $return['message'] = "Please All The Fields Required Data...!";
                echo json_encode($return); exit;
            }   
        }   
        else{
            $return['result'] = "fail";
            $return['message'] = "Invalid details you entered...!";
            echo json_encode($return); exit;
        }
    }

    function delete_chat(){
        if($this->checkHeader()){
            $data = json_decode($_POST['data']);
            $chat_id = isset($data->chat_id) ? $data->chat_id :'';
            delete_chat($chat_id);                    
        }else {
            $return['result'] = "fail";
            $return['message'] = "Invalid details you entered...!";
            echo json_encode($return);exit;
        }
    }
}
?>