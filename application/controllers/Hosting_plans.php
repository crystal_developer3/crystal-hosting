<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Hosting_plans extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();			
		}

		function index()
		{
			$data['hosting_data'] = $this->Production_model->get_all_with_where('hosting_plan','','',array('status'=>'1'));
			// echo"<pre>";echo $this->db->last_query(); print_r($data); exit;
			$data['features_details'] = $this->Production_model->get_all_with_where('common_feature','','',array('status'=>'1'));	
			$data['hosting_price_details'] = $this->Production_model->get_all_with_where('hosting_price','','',array('status'=>'1'));	
			$this->load->view('hosting_plans',$data);	
		}
		function hosting_details($id)
		{
			$data['hosting_plan_data'] = $this->Production_model->get_all_with_where('hosting_plan','','',array('status'=>'1','seo_slug'=>$id));
			// echo"<pre>";echo $this->db->last_query(); print_r($data); exit;
			$data['features_details'] = $this->Production_model->get_all_with_where('common_feature','','',array('status'=>'1'));	
			$data['hosting_price_details'] = $this->Production_model->get_all_with_where('hosting_price','','',array('status'=>'1'));	
			$this->load->view('hosting_details',$data);	
		}
	}
	/* End of file Hosting_plans.php */
	/* Location: ./application/controllers/Hosting_plans.php */
?>