<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Dashboard extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();	
			if (!$this->session->userdata('login_id')) {
				redirect(base_url('login'));
			}		
		}
		function index()
		{
			$data = array();
			$settings = array(
	            "url" => site_url() . "dashboard/index/",
	            "per_page" => RECORDS_PER_PAGE,
	        );
	        $conditions = array("select"=>"tickets.*","join"=>array("table"=>"departments","join_type"=>"LEFT","join_conditions"=>"tickets.department=departments.id"),"ORDER BY"=>array('id'=>'DESC'),"where"=>array('tickets.user_id' =>$this->session->login_id));
	        $data = $this->common_model->get_pagination("tickets", $conditions, $settings);
	        if (isset($this->session->ticket_msg) && $this->session->ticket_msg != '') {
	            $data = array_merge($data, array("success" => $this->session->ticket_msg));
	            $this->session->plan_msg = '';
	        }	
	        /*
	        	array("table"=>"departments","join_type"=>"LEFT","join_conditions"=>"tickets.department=departments.id")
	        */
	        // echo "<pre>".$this->db->last_query();print_r($data);exit;		
			$this->load->view('dashboard',$data);	
		}
		function get_user_tickets(){
			$conditions = array("select"=>"tickets.*","join"=>array("table"=>"departments","join_type"=>"LEFT","join_conditions"=>"tickets.department=departments.id"),"ORDER BY"=>array('id'=>'DESC'),"where"=>array('tickets.user_id' =>$this->session->login_id));
	        $data = $this->common_model->select_data("tickets", $conditions);
	        // echo"<pre>";print_r($data['data']);
	        ?>
		        <div class="unpaid_invoice_content2">
		        	<?php 
		        		if(isset($data) && !empty($data) && $data['row_count'] > 0){
			        		foreach ($data['data'] as $key => $value) { ?>
				        		<a href="<?=base_url('ticket/ticket-reply/'.$value['id'])?>">
									<strong>#<?=$value['ticket_no']?> - <?=$value['message']?></strong>
									<label class="label" style="background: #000;">Answered</label>
									<br>
									<small>Last Updated: <?=format_date_Mdy_time($value['modified_date'])?></small>
								</a>
								<div  class="border-bottom"></div>
					        	<?php 
					        }
		        		}else{
				        	echo "<div class='text-center'>No data</div>";
				        }
		        	?>	
           		</div>
	        <?php
		}
		function get_user_unpaid_invoice(){
			$conditions = array("select"=>"invoice.*,currency.currency_name,currency.currency_code","join"=>array("table"=>"currency","join_type"=>"LEFT","join_conditions"=>"invoice.currency_id=currency.id"),"ORDER BY"=>array('id'=>'DESC'),"where"=>array('invoice.user_id' =>$this->session->login_id,'invoice.payment_status'=>'0'),"limit"=>1);
	        $data = $this->common_model->select_data("invoice", $conditions);
	        // echo"<pre>".$this->db->last_query(); print_r($data); exit;
	        if(isset($data) && !empty($data) && $data['row_count'] > 0){
	        	$data = $data['data'][0];
	        	?>
		        <div class="unpaid_invoice_content2">
        			<p>
        				You have 1 overdue invoice(s) with a total balance due of &nbsp;<?=$data['currency_code']?> <b><?=$data['total_amount']?></b> <?=$data['currency_name']?>. 
        			</p>
        			<p>Pay them now to avoid any interruptions in service.</p>
            	</div>
	        <?php
	        }else{
	        	echo "<div class='text-center'>No data</div>";
	        }
		}
	}	
?>