<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	private $_search_array = array('full_name'=>'Full name','user_name'=>'User name');

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function view() {

        // ==================== pagination start ======================== //

        $data = $this->Production_model->pagination_create(base_url('authority/about/view/'),'about','','id','asc',array());

        // ==================== pagination end ======================== //
        
        $this->load->view('authority/about/view',$data);
    }

    function add()
    {
        $data['about_data'] = array();
        $this->load->view('authority/about/add-edit',$data);
    }

    function add_about()
    {
        $data = $this->input->post();
        // echo"<pre>"; print_r($data); exit;
        
        if($_FILES['about_image']['name']==""){ $img_name='';}
        else{
            $imagePath = ABOUT_IMAGE;
            if(!is_dir($imagePath)){
                mkdir($imagePath);
                @chmod($imagePath,0777);
            }
            $img_name = 'ABOUT-'.rand(111,999)."-".str_replace(' ', '_',$_FILES['about_image']['name']);

            $destFile = $imagePath . $img_name; 
            $filename = $_FILES["about_image"]["tmp_name"];       
            move_uploaded_file($filename,  $destFile);

            $data['about_image'] = $img_name;
        }
        // echo "<pre>"; print_r($data); exit;

        $record = $this->Production_model->insert_record('about',$data);
        if ($record !='') {
            $this->session->set_flashdata('success', 'About Add Successfully....!');
            redirect(base_url('authority/about/view')); 
        }
        else
        {
            $this->session->set_flashdata('error', 'About Not Added....!');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function edit($id)
    {
        $data['about_data'] = $this->Production_model->get_all_with_where('about','','',array('id'=>$id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
        $this->load->view('authority/about/add-edit',$data);
    }

    function update_about()
    {
        $about_id = $this->input->post('id');
        $data = $this->input->post();
        // echo"<pre>"; print_r($data); exit;

        if($_FILES['about_image']['name'] !='')
        {
            $get_image = $this->Production_model->get_all_with_where('about','','',array('id'=>$about_id));
            // echo"<pre>"; print_r($get_image); exit;
            if ($get_image !=null && $get_image[0]['about_image'] !=null && !empty($get_image[0]['about_image']))
            {
                @unlink(ABOUT_IMAGE.$get_image[0]['about_image']);
            }

            $imagePath = ABOUT_IMAGE;
            if(!is_dir($imagePath)){
                mkdir($imagePath);
                @chmod($imagePath,0777);
            }
            
            $img_name = 'ABOUT-'.rand(111,999)."-".$_FILES['about_image']['name'];

            $destFile = $imagePath . $img_name; 
            $filename = $_FILES["about_image"]["tmp_name"];       
            move_uploaded_file($filename,  $destFile);
           
            $data['about_image'] = $img_name;
        }
        // echo "<pre>"; print_r($data); exit;

        $record = $this->Production_model->update_record('about',$data,array('id'=>$about_id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;
        if ($record == 1) {
            $this->session->set_flashdata('success', 'About Update Successfully....!');
            redirect(base_url('authority/about/view'));
        }
        else
        {
            $this->session->set_flashdata('error', 'About Not Updated....!');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function delete($id)
    {
        $get_image = $this->Production_model->get_all_with_where('about','','',array('id'=>$id));
        
        if ($get_image !=null && $get_image[0]['about_image'] !=null && !empty($get_image[0]['about_image']))
        {
            @unlink(ABOUT_IMAGE.$get_image[0]['about_image']);
            // @unlink(ABOUT_IMAGE.'thumbnail/medium/'.$get_image[0]['about_image']);
            // @unlink(ABOUT_IMAGE.'thumbnail/thumb/'.$get_image[0]['about_image']);
        }
        $record = $this->Production_model->delete_record('about',array('id'=>$id));

        if ($record == 1) {
            $this->session->set_flashdata('success', 'About Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'About Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
}
?>