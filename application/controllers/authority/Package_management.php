<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Package_management extends CI_Controller {

	private $_search_array = array('full_name'=>'Full name','user_name'=>'User name');

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function view() {

        // ==================== pagination start ======================== //

        $data = $this->Production_model->pagination_create(base_url('authority/package_management/view/'),'package_management','','id','desc',array());

        // ==================== pagination end ======================== //
        
        $this->load->view('authority/package_management/view',$data);
    }

    function add()
    {
        $data['package_data'] = array();
        $this->load->view('authority/package_management/add-edit',$data);
    }

    function add_package()
    {
        $data = $this->input->post();
        if ($data['day_month'] == 'Month') {
            $data['day'] = 30;
        }
        // echo"<pre>"; print_r($data); exit;     

        $record = $this->Production_model->insert_record('package_management',$data);
        if ($record !='') {
            $this->session->set_flashdata('success', 'Package Add Successfully....!');
            redirect(base_url('authority/package_management/view')); 
        }
        else
        {
            $this->session->set_flashdata('error', 'Package Not Added....!');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function edit($id)
    {
        $data['package_data'] = $this->Production_model->get_all_with_where('package_management','','',array('id'=>$id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
        $this->load->view('authority/package_management/add-edit',$data);
    }

    function update_package()
    {
        $package_id = $this->input->post('id');
        $data = $this->input->post();
        if ($data['day_month'] == 'Month') {
            $data['day'] = 30;
        }
        
        // echo "<pre>"; print_r($data); exit;

        $record = $this->Production_model->update_record('package_management',$data,array('id'=>$package_id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;
        if ($record == 1) {
            $this->session->set_flashdata('success', 'Package Update Successfully....!');
            redirect(base_url('authority/package_management/view'));
        }
        else
        {
            $this->session->set_flashdata('error', 'Package Not Updated....!');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function delete($id)
    {
        // $get_image = $this->Production_model->get_all_with_where('package_management','','',array('id'=>$id));
        
        // if ($get_image !=null && $get_image[0]['about_image'] !=null && !empty($get_image[0]['about_image']))
        // {
        //     @unlink(ABOUT_IMAGE.$get_image[0]['about_image']);
        //     // @unlink(ABOUT_IMAGE.'thumbnail/medium/'.$get_image[0]['about_image']);
        //     // @unlink(ABOUT_IMAGE.'thumbnail/thumb/'.$get_image[0]['about_image']);
        // }
        $record = $this->Production_model->delete_record('package_management',array('id'=>$id));

        if ($record == 1) {
            $this->session->set_flashdata('success', 'Package Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Package Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');

        foreach ($chkbox_id as $key => $value) {          
            $record = $this->Production_model->delete_record('package_management',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Package Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Package Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    } 
}
?>