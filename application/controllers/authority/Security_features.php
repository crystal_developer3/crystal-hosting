<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Security_features extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index($page_number='') {
        $settings = array(
            "url" => site_url() . "authority/security_features/index/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data = $this->common_model->get_pagination("security_feature", $conditions, $settings);
        if (isset($this->session->security_feature_msg) && $this->session->security_feature_msg != '') {
            $data = array_merge($data, array("success" => $this->session->security_feature_msg));
            $this->session->plan_msg = '';
        }
        unset($settings, $conditions);        
        $this->load->view('authority/security_features/view', $data);
    }
    function add()
    {
        $data['security_feature_details'] = array();       
        $this->load->view('authority/security_features/add-edit',$data);
    }    
    function insert_security_feature()
    {
        $data = $this->input->post();
        $title = $data['title']; 
        $count_security_feature = count($data['title']); 
        $create_date = date('Y-m-d H:i:s');
        $resultSet = Array();                 
        if(isset($count_security_feature)) {     
            for($i = 0; $i < $count_security_feature; $i++){
                $get_sub_title = $this->Production_model->get_all_with_where('security_feature','','',array('title'=> $title[$i]));

                if(!empty($get_sub_title)) 
                {
                    $resultSet[] = $title[$i];
                }
            }
            if(!empty($resultSet)) {
                $error = implode(', ', $resultSet);
                $this->session->set_flashdata('error',"$error Sucurity feature title is allredy exist...!");
                redirect(base_url('authority/security_features/add'));
            }else{                           
                for($i = 0; $i < $count_security_feature; $i++) {
                    $data = array(
                        'title' => $title[$i],
                        'create_date' => $create_date
                    );
                    if($title[$i] !=null) {
                        $record = $this->Production_model->insert_record('security_feature',$data);
                    }
                }
                if($record !='') {
                    $this->session->set_flashdata('success', 'Sucurity feature Add Successfully....!');
                    redirect(base_url('authority/security_features'));
                }else{
                    $this->session->set_flashdata('error', 'Sucurity feature Not Added....!');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
        }
    }

    function edit($id)
    {
        $data['security_feature_details'] = $this->Production_model->get_all_with_where('security_feature','','',array('id'=>$id));
        $this->load->view('authority/security_features/add-edit',$data);
    }
    function update_security_feature()
    {
        $data = $this->input->post();
        $title = $data['title']; 
        $modified_date = date('Y-m-d H:i:s');
        $id = $this->input->post('id');        
        $get_image = $this->Production_model->get_all_with_where('security_feature','','',array('id'=>$id));
        $data = array(
            'title' => $title[0],
            'modified_date' => $modified_date
        );
        $record = $this->Production_model->update_record('security_feature',$data,array('id'=>$id));
        if ($record == 1) {
            $this->session->set_flashdata('success', 'Sucurity feature Update Successfully....');
            redirect(base_url('authority/security_features'));
        }else{
            $this->session->set_flashdata('error', 'Sucurity feature Not Updated....');
            redirect($_SERVER['HTTP_REFERER']);
        }        
    }
    function delete_security_feature($id)
    {        
        $record = $this->Production_model->delete_record('security_feature',array('id'=>$id));
      
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Sucurity feature Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Sucurity feature Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $record = $this->Production_model->delete_record('security_feature',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Sucurity feature Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Sucurity feature Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>