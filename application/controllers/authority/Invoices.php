<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'html2pdf_vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;
class Invoices extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index($page_number='') {
        $settings = array(
            "url" => site_url() . "authority/invoices/index/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data = $this->common_model->get_pagination("invoice", $conditions, $settings);
        if (isset($this->session->invoice_msg) && $this->session->invoice_msg != '') {
            $data = array_merge($data, array("success" => $this->session->invoice_msg));
            $this->session->invoice_msg = '';
        }
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data['user_details'] = $this->common_model->select_data("user_register", $conditions);
        unset($settings, $conditions);        
        $this->load->view('authority/invoices/view', $data);
    }
    function add()
    {
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data['user_details'] = $this->common_model->select_data("user_register", $conditions);
        if (isset($this->session->invoice_msg) && $this->session->invoice_msg != '') {
            $data = array_merge($data, array("success" => $this->session->invoice_msg));
            $this->session->invoice_msg = '';
        }
        $conditions1 = array("select" => "*",'ORDER BY'=>array('id'=>'ASC'),"where"=>array('status'=>'1'));
        $data['currency_data'] = $this->common_model->select_data("currency", $conditions1);
        
        $data['invoice_details'] = array();
        $data['invoice_item_details'] = array();
        // echo "<pre>";print_r($data);exit;
        unset($settings, $conditions);
        $this->load->view('authority/invoices/add-edit',$data);
    }    
    function insert_invoice()
    {
        if($this->input->post()){    
            $data = $this->input->post();        
            $create_date = date('Y-m-d H:i:s');
            $resultSet = Array(); 
            $item_quantities = count($data['quantity']);
            $quantity = $data['quantity'];
            $price =$data['price'];
            $description = $data['description'];
            $total = $data['total'];

            $data = array(
                'invoice_date' => format_date_ymd($data['invoice_date']),
                'due_date' => format_date_ymd($data['due_date']),
                'user_id' => $data['user_id'],
                'invoice_no' => generate_invoice_no(6),
                'currency_id' => $data['currency_id'],
                'total_amount' => $data['total_amount'],
                'gst' => ($data['gst']!='')?$data['gst']:0,
                'create_date' => $create_date
            );
            $record = $this->Production_model->insert_record('invoice',$data);
            /*$chkbox_id = $this->input->post('feature_id');
            foreach ($chkbox_id as $key => $value) {
                $data = array(
                    "invoice_id"=>$record,
                    "feature_id"=>$value
                );
                $record1 = $this->Production_model->insert_record('invoice_item',$data);
            }*/
            if($record !='') {
                if (isset($item_quantities)) {      
                    for ($i = 0; $i < $item_quantities; $i++) { 
                        $item_data = array(
                            'invoice_id' => $record,                            
                            'quantity' => $quantity[$i],
                            'price' => $price[$i],
                            'description' => $description[$i],
                            'total' => $total[$i]
                        );
                        if ($quantity[$i] !=null) {
                            $invoice_item = $this->Production_model->insert_record('invoice_item',$item_data);
                        }
                    }
                }

                $where['invoice.id'] = $record;
                $join[0]['table_name'] = 'user_register';
                $join[0]['column_name'] = 'user_register.id = invoice.user_id';
                $join[0]['type'] = 'left';
                
                $join[1]['table_name'] = 'currency';
                $join[1]['column_name'] = 'currency.id = invoice.currency_id';
                $join[1]['type'] = 'left';

                $user_details = $this->Production_model->jointable_descending(array('user_register.first_name','user_register.last_name', 'user_register.address', 'invoice.id','currency.currency_name','currency.currency_code'), 'invoice', '', $join, 'invoice.id', 'desc', $where);
                $data = array_merge($user_details[0], $data);
                // echo "<pre>".$this->db->last_query();print_r($data);exit;
                $join1[0]['table_name'] = 'invoice_item';
                $join1[0]['column_name'] = 'invoice_item.invoice_id = invoice.id';
                $join1[0]['type'] = 'left';
                $data['invoice_details'] = $this->Production_model->jointable_descending(array('invoice.*','invoice_item.description','invoice_item.price','invoice_item.quantity','invoice_item.total'), 'invoice', '', $join1, 'invoice.id', 'desc', $where);
                // echo "<pre>".$this->db->last_query();print_r($data);exit;

                require_once 'dompdf/autoload.inc.php';
                $dompdf = new Dompdf\Dompdf(array('enable_remote' => true));
                $html = ($this->load->view('order_genrate/invoice', $data, true));
                // echo $html;
                $dompdf->loadHtml($html);
                $dompdf->setPaper('A4');//'landscape'
                $dompdf->render();
                // $pdf = $dompdf->output();     
                // $dompdf->stream();
                $file_name = str_replace(' ', '', $data['invoice_no']) . '.pdf';
                $update_data['file_name'] = $file_name;
                file_put_contents('generated_invoices/' . $file_name, $dompdf->output()); // file save in folder

                // try {
                
                //     echo $content = $this->load->view('order_genrate/invoice', $data, true);

                //     $html2pdf = new Html2Pdf('L', 'A4', 'fr');
                //     $html2pdf->setDefaultFont('Arial');

                //     $html2pdf->writeHTML($content);
                //     $file_name = str_replace(' ', '', $data['invoice_no']) . '.pdf';
                //     $html2pdf->output('generated_invoices/' . $file_name);
                //     $update_data['file_name'] = $file_name;
                // } catch (Html2PdfException $e) {
                //     $html2pdf->clean();

                //     $formatter = new ExceptionFormatter($e);
                //     echo $formatter->getHtmlMessage();
                // }

                $this->session->set_flashdata('success', 'Invoice Add Successfully....!');
                $this->Production_model->update_record('invoice',$update_data,array('id'=>$record));

                redirect(base_url('authority/invoices'));
            }else{
                $this->session->set_flashdata('error', 'Invoice Not Added....!');
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }
    function edit($id)
    {
        $data['invoice_details'] = $this->Production_model->get_all_with_where('invoice','','',array('id'=>$id));
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data['user_details'] = $this->common_model->select_data("user_register", $conditions);
        if (isset($this->session->invoice_msg) && $this->session->invoice_msg != '') {
            $data = array_merge($data, array("success" => $this->session->invoice_msg));
            $this->session->invoice_msg = '';
        }
        $conditions1 = array("select" => "*",'ORDER BY'=>array('id'=>'ASC'),"where"=>array('status'=>'1'));
        $data['currency_data'] = $this->common_model->select_data("currency", $conditions1);
        $data['invoice_item_details'] = $this->Production_model->get_all_with_where('invoice_item','','',array('status'=> '1','invoice_id'=>$id)); 
        // echo "<pre>";print_r($data);exit;
        $this->load->view('authority/invoices/add-edit',$data);
    }
    function update_invoice()
    {
        $data = $this->input->post();
        $id = $data['id'];
        $modified_date = date('Y-m-d H:i:s');
        $item_quantities = count($data['quantity']);
        $quantity = $data['quantity'];
        $description = $data['description'];
        $price = $data['price'];
        $total = $data['total'];
        $item_id = $this->input->post('edit_item_id');

        $data = array(
            'invoice_date' => format_date_ymd($data['invoice_date']),
            'due_date' => format_date_ymd($data['due_date']),
            'user_id' => $data['user_id'],
            'currency_id' => $data['currency_id'],
            'total_amount' => $data['total_amount'],
            'gst' => $data['gst'],
            'modified_date' => $modified_date
        );
        $record = $this->Production_model->update_record('invoice',$data,array('id'=>$id));
        if ($record) {
            if(isset($item_quantities)) {       
                for($i = 0; $i < $item_quantities; $i++){
                    $item_data = array(
                        'invoice_id' => $id,
                        'quantity' => $quantity[$i],
                        'price' => $price[$i],
                        'description' => $description[$i],
                        'total' => $total[$i]
                    );
                    // echo "<pre>".$item_id[$i];print_r($item_data);exit;

                    if($item_id[$i] !='' && !empty($item_id[$i]) && isset($item_id[$i])) {
                        if($quantity[$i] !=null) {
                            $invoice_item_record = $this->Production_model->update_record('invoice_item',$item_data,array('id'=> $item_id[$i]));
                        }
                    }else{
                        $invoice_item = $this->Production_model->insert_record('invoice_item',$item_data);
                    }
                }
            }

            $where['invoice.id'] = $id;
            $join[0]['table_name'] = 'user_register';
            $join[0]['column_name'] = 'user_register.id = invoice.user_id';
            $join[0]['type'] = 'left';

            $join[1]['table_name'] = 'currency';
            $join[1]['column_name'] = 'currency.id = invoice.currency_id';
            $join[1]['type'] = 'left';

            $user_details = $this->Production_model->jointable_descending(array('user_register.first_name','user_register.last_name', 'user_register.address', 'invoice.id','invoice.invoice_no','currency.currency_name','currency.currency_code'), 'invoice', '', $join, 'invoice.id', 'desc', $where);
            $data = array_merge($user_details[0], $data);
            // echo "<pre>".$this->db->last_query();print_r($data);exit;
            $join1[0]['table_name'] = 'invoice_item';
            $join1[0]['column_name'] = 'invoice_item.invoice_id = invoice.id';
            $join1[0]['type'] = 'left';
            $data['invoice_details'] = $this->Production_model->jointable_descending(array('invoice.*','invoice_item.description','invoice_item.price','invoice_item.quantity','invoice_item.total'), 'invoice', '', $join1, 'invoice.id', 'desc', $where);
            // echo "<pre>".$this->db->last_query();print_r($data);exit;

            require_once 'dompdf/autoload.inc.php';
            $dompdf = new Dompdf\Dompdf(array('enable_remote' => true));
            $html = ($this->load->view('order_genrate/invoice', $data, true));
            // echo $html;
            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'landscape');
            $dompdf->render();
            // $pdf = $dompdf->output();     
            // $dompdf->stream();
            $file_name = str_replace(' ', '', $data['invoice_no']) . '.pdf';
            $update_data['file_name'] = $file_name;
            file_put_contents('generated_invoices/' . $file_name, $dompdf->output()); // file save in folder
            


            // try {
                
            //     $content = $this->load->view('order_genrate/invoice', $data, true);

            //     $html2pdf = new Html2Pdf('L', 'A4', 'fr');
            //     $html2pdf->setDefaultFont('Arial');
            //     $html2pdf->writeHTML($content);
            //     $file_name = str_replace(' ', '', $data['invoice_no']) . '.pdf';
            //     $html2pdf->output('generated_invoices/' . $file_name);
            //     $update_data['file_name'] = $file_name;
            // } catch (Html2PdfException $e) {
            //     $html2pdf->clean();

            //     $formatter = new ExceptionFormatter($e);
            //     echo $formatter->getHtmlMessage();
            // }


            $this->session->set_flashdata('success', 'Invoice Update Successfully....');
            $this->Production_model->update_record('invoice',$update_data,array('id'=>$record));

            redirect(base_url('authority/invoices'));
        }else{
            $this->session->set_flashdata('error', 'Invoice Not Updated....');
            redirect($_SERVER['HTTP_REFERER']);
        }        
    }
    function delete_invoice($id)
    {        
        $record = $this->Production_model->delete_record('invoice',array('id'=>$id));
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Invoice Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Invoice Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $record = $this->Production_model->delete_record('invoice',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Invoice Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Invoice Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>