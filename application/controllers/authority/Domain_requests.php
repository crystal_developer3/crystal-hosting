<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Domain_requests extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index($page_number='') {
        $settings = array(
            "url" => site_url() . "authority/domain_requests/index/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data = $this->common_model->get_pagination("searched_domain", $conditions, $settings);
        if (isset($this->session->request_msg) && $this->session->request_msg != '') {
            $data = array_merge($data, array("success" => $this->session->request_msg));
            $this->session->request_msg = '';
        }
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data['user_details'] = $this->common_model->select_data("user_register", $conditions);
        $conditions2 = array("select" => "*",'ORDER BY'=>array('id'=>'ASC'),"where"=>array('status'=>'1'));
        $data['domain_type_data'] = $this->common_model->select_data("domain", $conditions2);

        $records = array(
            'notification' => '1',
        );
        $conditions = array(
            "where" => array("notification" => '0'),
        );
        $this->common_model->update_data("searched_domain", $records, $conditions); 
        unset($settings, $conditions);        
        $this->load->view('authority/domain_requests/view', $data);
    }   
    
    function delete_searched_domain($id)
    {        
        $record = $this->Production_model->delete_record('searched_domain',array('id'=>$id));
        if ($record != 0) {
            $this->session->set_flashdata('success', 'User Domain Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'User Domain Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $record = $this->Production_model->delete_record('searched_domain',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'User Domain Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'User Domain Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>