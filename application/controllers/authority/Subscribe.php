<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Subscribe extends CI_Controller {

    private $_search_array = array('full_name'=>'Full name','user_name'=>'User name');

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->load->library('form_validation');
        $this->load->library('common_functions');
    }

    public function view() {
      
        // ==================== pagination start ======================== //
        $data = array();
        $total_records = $this->Production_model->get_total('subscribe');

        if ($total_records !=null) 
        {    
            // ==================== pagination start ======================== //
            $tmp_data = $this->Production_model->get_all_with_where('subscribe','','',array());
            $tmp_array['total_record'] = count($tmp_data);
            $tmp_array['url'] = base_url('authority/subscribe/view/');
            $tmp_array['per_page'] = RECORDS_PER_PAGE;
            $record = $this->Production_model->only_pagination($tmp_array);
            
            $data['subscribe_details'] = $this->Production_model->get_all_with_where_limit('subscribe','subscribe_id','desc',array(),$record['limit'],$record['start']); 
           
            $data['pagination'] = $record['pagination']; 
            $data['no'] = $record['no']; 
            // echo"<pre>"; print_r($data); exit;
            // ==================== pagination end ======================== //

        }
        $data['news_details'] = $this->Production_model->get_all_with_where('news_letter','','',array('status'=>'1'));
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;

        // ==================== pagination end ======================== //
        $this->load->view('authority/subscribe/view', $data);
    }

    public function delete($id = "") {
        if ($id != "") {
            $conditions = array("select" => "subscribe_id", "where" => array("subscribe_id" => intval($id)));

            $user = $this->common_model->select_data("subscribe", $conditions);
            // echo"<pre>"; echo $this->db->last_query(); print_r($user); exit;
           
            $conditions = array(
                "where" => array("subscribe_id" => $id),
            );
            $this->common_model->delete_data("subscribe", $conditions);
            // $this->session->user_msg = "Record deleted successfully";
            $this->session->set_flashdata('success','Record deleted successfully!!!');
            redirect("authority/subscribe/view");
        } else {
            redirect("authority/subscribe/view");
        }
    }
    /* new letter send email*/
    function get_details()
    {
        $news_id = $this->input->post('news_id');
        $subscribe_email = $this->input->post('subscribe_id'); 

        if ($subscribe_email !=null) {
            $data = $this->Production_model->get_all_with_where('news_letter','','',array('id'=> $news_id));

            // $send_mail = $this->Production_model->mail_send('News-letter','milan.v@crystalinfoway.com','','mail_form/news_letter/form',$data[0],'');

            if ($data !=null) {
                $mail = new PHPMailer;

                $mail->IsSMTP();         // Set mailer to use SMTP
                $mail->Host = SMTP_HOST; // Specify main and backup server
                $mail->Port = SMTP_PORT; // Set the SMTP port
                $mail->SMTPAuth = true;  // Enable SMTP authentication
                $mail->Username = SMTP_USERNAME; // SMTP username
                $mail->Password = SMTP_PASSWORD; // SMTP password
                //$mail->SMTPSecure = 'tls'; // Enable encryption, 'ssl' also accepted
                $mail->From = $data[0]['email']; // SMTP username
                $mail->FromName = $data[0]['name'];

                $email_addresses = implode(',',$subscribe_email);
                // echo $email_addresses; exit;

                $addr = explode(',',$email_addresses);

                foreach ($addr as $ad) {
                    $mail->AddAddress(trim($ad));       
                }
                $mail->IsHTML(true);       // Set email format to HTML
                $mail->Subject = $data[0]['subject'];

                // echo"<pre>"; print_r($mail); exit;              
                $mail->addAttachment();               
                $body = $this->load->view('mail_form/news_letter/form',$data[0],TRUE);
                $mail->Body = $body;
                //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
               
                if (!$mail->Send()) {
                    echo 'Mailer Error: ' . $mail->ErrorInfo;
                    $this->session->set_flashdata('error', 'Valid new letter name and email must be add to server configuration');
                    redirect($_SERVER['HTTP_REFERER']);
                    return false;
                    //   exit;
                } else {
                    $this->session->set_flashdata('success', 'Mail Send Successfully....!');
                    redirect($_SERVER['HTTP_REFERER']);
                    return true;
                }
            }
            // echo"<pre>"; print_r($send_mail); print_r($subscribe_email); exit;
        }
        else{
            $this->session->set_flashdata('error', 'Select any one email address after submit....!');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    public function details($id = "") {
        $data = array();
        if ($id != "") {
            $conditions = array("where" => array("subscribe_id" => $id));
            $user_info = $this->common_model->select_data("subscribe", $conditions);
            if ($user_info['row_count'] > 0) {
                $data = array_merge($data, $user_info['data'][0]);
            } else {
                redirect("authority/subscribe/view");
            }
        } else {
            redirect("authority/subscribe/view");
        }
        // echo"<pre>"; print_r($data); exit;
        $this->load->view('authority/subscribe/details', $data);
    } 

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {

            $get_record = $this->Production_model->get_all_with_where('subscribe','','',array('subscribe_id'=>$value));
            // if ($get_record !=null && $value['category_image'] !=null && !empty($value['category_image']))   
            // {
            //     unlink(CATEGORY_IMAGE.$value['category_image']);
            // }
            $record = $this->Production_model->delete_record('subscribe',array('subscribe_id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Record deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Record Not deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>