<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Seo_page extends CI_Controller {

	private $_search_array = array('full_name'=>'Full name','user_name'=>'User name');

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function view() {
      
        // ==================== pagination start ======================== //

        $data = $this->Production_model->pagination_create(base_url('authority/seo_page/view/'),'seo_page_manage','','id','asc',array());

        // ==================== pagination end ======================== //
        
        $this->load->view('authority/seo_page/view',$data);
    }

    function add()
    {
        $data['meta_details'] = array();
        $data['menu_details'] = $this->Production_model->get_all_with_where('seo_menu','menu_id','desc',array());
        $this->load->view('authority/seo_page/add-edit',$data);
    }

    function add_meta()
    {
        $data = $this->input->post();
        $data['meta_keywords'] = str_replace(' ', '-', $this->input->post('meta_keywords'));
        $data['meta_description'] = str_replace(' ', '-', $this->input->post('meta_description'));
        $data['meta_title'] = str_replace(' ', '-', $this->input->post('meta_title'));
        $data['slug_name'] = str_replace(' ', '-', $this->input->post('slug_name'));
        // echo"<pre>"; print_r($data); exit;

        $get_menu = $this->Production_model->get_all_with_where('seo_page_manage','','',array('menu_id'=>$data['menu_id']));

        if (count($get_menu) > 0) {
            $this->session->set_flashdata('error', $this->lang->line("menu_allredy_added"));
            redirect(base_url('authority/seo_page/view')); 
        }
        else
        {
            $record = $this->Production_model->insert_record('seo_page_manage',$data);
            if ($record !='') {
                $this->session->set_flashdata('success', $this->lang->line("added"));
                redirect(base_url('authority/seo_page/view')); 
            }
            else
            {
                $this->session->set_flashdata('error', $this->lang->line("not_added"));
                redirect($_SERVER['HTTP_REFERER']);
            }  
        } 
    }

    function edit($id)
    {
        $data['menu_details'] = $this->Production_model->get_all_with_where('seo_menu','menu_id','desc',array());
        $data['meta_details'] = $this->Production_model->get_all_with_where('seo_page_manage','','',array('id'=>$id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
        $this->load->view('authority/seo_page/add-edit',$data);
    }

    function update_meta()
    {
        $id = $this->input->post('id');
        $data = $this->input->post();
        $data['meta_keywords'] = str_replace(' ', '-', $this->input->post('meta_keywords'));
        $data['meta_description'] = str_replace(' ', '-', $this->input->post('meta_description'));
        $data['meta_title'] = str_replace(' ', '-', $this->input->post('meta_title'));
        $data['slug_name'] = str_replace(' ', '-', $this->input->post('slug_name'));

        // echo "<pre>"; print_r($data); exit;

        $record = $this->Production_model->update_record('seo_page_manage',$data,array('id'=>$id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;
        if ($record == 1) {
            $this->session->set_flashdata('success', $this->lang->line("updated"));
            redirect(base_url('authority/seo_page/view'));
        }
        else
        {
            $this->session->set_flashdata('error', $this->lang->line("not_updated"));
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function delete($id)
    {
        $record = $this->Production_model->delete_record('seo_page_manage',array('id'=>$id));

        if ($record == 1) {
            $this->session->set_flashdata('success', $this->lang->line("deleted"));
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', $this->lang->line("not_deleted"));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {            
            $record = $this->Production_model->delete_record('seo_page_manage',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', $this->lang->line("deleted"));
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', $this->lang->line("not_deleted"));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>