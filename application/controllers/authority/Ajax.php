<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* This class is use for all the common ajax request.
*/
class Ajax extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->load->library('form_validation');
        $this->load->library('common_functions');
    }

    public function change_status(){ // use for user register
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$error = false;
		$response_array = array('success'=>false);		
		$required_fields = array(
			'id' => 'missing id in the request',
			'table' => 'missing table in the request',
			'current_status' => 'missing current status in the request',
		);
		foreach($required_fields as $key => $value){
			if(strlen(trim($this->input->post($key))) <= 0){
				$response_array['msg'] = $value;
				$error = true;
				break;
			}
		}
		if(!$error){
			if ($this->input->post('current_status') == '0') {
				$status = '1';
			}
			if ($this->input->post('current_status') == '1') {
				$status = '0';
			}
			$records = array(
				'status' => $status,
			);
			$conditions = array(
				"where" => array("id" => $this->input->post('id')),
			);
			$this->common_model->update_data($this->input->post('table'), $records, $conditions);
			// echo"<pre>"; echo $this->db->last_query(); exit;
			$response_array['success'] = true;
		}
		echo json_encode($response_array);
		exit;
	}

	public function change_common_status(){ // use for user register && feedback && our-expert
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$error = false;
		$response_array = array('success'=>false);		
		$required_fields = array(
			'id' => 'missing id in the request',
			'table' => 'missing table in the request',
			'current_status' => 'missing current status in the request',
		);
		foreach($required_fields as $key => $value){
			if(strlen(trim($this->input->post($key))) <= 0){
				$response_array['msg'] = $value;
				$error = true;
				break;
			}
		}
		if(!$error){
			if ($this->input->post('current_status') == '0') {
				$status = '1';
			}
			if ($this->input->post('current_status') == '1') {
				$status = '0';
			}
			$records = array(
				'status' => $status,
			);
			$conditions = array(
				"where" => array("id" => $this->input->post('id')),
			);
			$this->common_model->update_data($this->input->post('table'), $records, $conditions);
			// echo"<pre>"; echo $this->db->last_query(); exit;
			$response_array['success'] = true;
		}
		echo json_encode($response_array);
		exit;
	}

	public function change_slider_status(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$error = false;
		$response_array = array('success'=>false);		
		$required_fields = array(
			'id' => 'missing id in the request',
			'table' => 'missing table in the request',
			'current_status' => 'missing current status in the request',
		);
		foreach($required_fields as $key => $value){
			if(strlen(trim($this->input->post($key))) <= 0){
				$response_array['msg'] = $value;
				$error = true;
				break;
			}
		}
		if(!$error){
			if ($this->input->post('current_status') == '0') {
				$status = '1';
			}
			if ($this->input->post('current_status') == '1') {
				$status = '0';
			}
			$records = array(
				'status' => $status,
			);
			$conditions = array(
				"where" => array("id" => $this->input->post('id')),
			);
			$this->common_model->update_data($this->input->post('table'), $records, $conditions);		
			$response_array['success'] = true;
		}
		echo json_encode($response_array);
		exit;
	}

	public function change_category_status(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$error = false;
		$response_array = array('success'=>false);		
		$required_fields = array(
			'id' => 'missing id in the request',
			'table' => 'missing table in the request',
			'current_status' => 'missing current status in the request',
		);
		foreach($required_fields as $key => $value){
			if(strlen(trim($this->input->post($key))) <= 0){
				$response_array['msg'] = $value;
				$error = true;
				break;
			}
		}
		if(!$error){
			if ($this->input->post('current_status') == '0') {
				$status = '1';
			}
			if ($this->input->post('current_status') == '1') {
				$status = '0';
			}
			$records = array(
				'status' => $status,
			);
			$conditions = array(
				"where" => array("category_id" => $this->input->post('id')),
			);
			$this->common_model->update_data($this->input->post('table'), $records, $conditions); // category.
			
			$this->Production_model->update_record('sub_category', $records, array("category_id" => $this->input->post('id'))); // subcategory.
			$this->Production_model->update_record('sub_of_sub_category', $records, array("category_id" => $this->input->post('id'))); 

			$response_array['success'] = true;
		}
		echo json_encode($response_array);
		exit;
	}

	public function change_post_status(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$error = false;
		$response_array = array('success'=>false);		
		$required_fields = array(
			'id' => 'missing id in the request',
			'table' => 'missing table in the request',
			'current_status' => 'missing current status in the request',
		);
		foreach($required_fields as $key => $value){
			if(strlen(trim($this->input->post($key))) <= 0){
				$response_array['msg'] = $value;
				$error = true;
				break;
			}
		}
		if(!$error){
			if ($this->input->post('current_status') == '0') {
				$status = '1';
			}
			if ($this->input->post('current_status') == '1') {
				$status = '0';
			}
			$records = array(
				'status' => $status,
			);
			$conditions = array(
				"where" => array("post_id" => $this->input->post('id')),
			);
			// echo"<pre>"; print_r($records); exit;
			$this->common_model->update_data($this->input->post('table'), $records, $conditions); 
			// $this->Production_model->update_record('sub_of_sub_category', $records, array("category_id" => $this->input->post('id'))); 

			$response_array['success'] = true;
		}
		echo json_encode($response_array);
		exit;
	}

	public function change_sub_category_status(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$error = false;
		$response_array = array('success'=>false);		
		$required_fields = array(
			'id' => 'missing id in the request',
			'table' => 'missing table in the request',
			'current_status' => 'missing current status in the request',
		);
		foreach($required_fields as $key => $value){
			if(strlen(trim($this->input->post($key))) <= 0){
				$response_array['msg'] = $value;
				$error = true;
				break;
			}
		}
		if(!$error){
			if ($this->input->post('current_status') == '0') {
				$status = '1';
			}
			if ($this->input->post('current_status') == '1') {
				$status = '0';
			}
			$records = array(
				'status' => $status,
			);
			$conditions = array(
				"where" => array("sub_category_id" => $this->input->post('id')),
			);
			
			// get category record for status active or deactive msg start //
			
			$get_cat_id = $this->Production_model->get_all_with_where('category','','',array("category_id" => $this->input->post('parent_id'))); // get category.

			// echo"<pre>"; echo $this->db->last_query(); print_r($get_cat_id); exit;
			if ($get_cat_id !=null) {
				if ($get_cat_id[0]['status'] == '0') {
					$response_array['error'] = true;
				}
				else{
					$this->common_model->update_data($this->input->post('table'), $records, $conditions); // category.		
					$this->Production_model->update_record('sub_of_sub_category', $records, array("sub_category_id" => $this->input->post('id'))); 			
					$response_array['success'] = true;					
				}
			}
			// end //	
			echo json_encode($response_array);		
		}		
		exit;
	}

	public function change_sub_of_subcat_status(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$error = false;
		$response_array = array('success'=>false);		
		$required_fields = array(
			'id' => 'missing id in the request',
			'table' => 'missing table in the request',
			'current_status' => 'missing current status in the request',
		);
		foreach($required_fields as $key => $value){
			if(strlen(trim($this->input->post($key))) <= 0){
				$response_array['msg'] = $value;
				$error = true;
				break;
			}
		}
		if(!$error){
			if ($this->input->post('current_status') == '0') {
				$status = '1';
			}
			if ($this->input->post('current_status') == '1') {
				$status = '0';
			}
			$records = array(
				'status' => $status,
			);
			$conditions = array(
				"where" => array("sub_of_subcat_id" => $this->input->post('id')),
			);
			// get category record for status active or deactive msg start //
			
			$get_cat_id = $this->Production_model->get_all_with_where('sub_category','','',array("sub_category_id" => $this->input->post('parent_id'))); // get category.

			// echo"<pre>"; echo $this->db->last_query(); print_r($get_cat_id); exit;
			if ($get_cat_id !=null) {
				if ($get_cat_id[0]['status'] == '0') {
					$response_array['error'] = true;
				}
				else{
					$this->common_model->update_data($this->input->post('table'), $records, $conditions);
					// $this->Production_model->update_record('sub_of_subsub_category', $records, array("sub_of_subcat_id" => $this->input->post('id'))); 	
					$response_array['success'] = true;
				}
			}	
			echo json_encode($response_array);
		}
		exit;
	}

	// public function change_product_status(){
	// 	if (!$this->input->is_ajax_request()) {
	// 	   exit('No direct script access allowed');
	// 	}
	// 	$error = false;
	// 	$response_array = array('success'=>false);		
	// 	$required_fields = array(
	// 		'id' => 'missing id in the request',
	// 		'table' => 'missing table in the request',
	// 		'current_status' => 'missing current status in the request',
	// 	);
	// 	foreach($required_fields as $key => $value){
	// 		if(strlen(trim($this->input->post($key))) <= 0){
	// 			$response_array['msg'] = $value;
	// 			$error = true;
	// 			break;
	// 		}
	// 	}
	// 	if(!$error){
	// 		if ($this->input->post('current_status') == '0') {
	// 			$status = '1';
	// 		}
	// 		if ($this->input->post('current_status') == '1') {
	// 			$status = '0';
	// 		}
	// 		$records = array(
	// 			'status' => $status,
	// 		);
	// 		$conditions = array(
	// 			"where" => array("product_id" => $this->input->post('id')),
	// 		);
			
	// 		$this->common_model->update_data($this->input->post('table'), $records, $conditions);
	// 		$response_array['success'] = true;
								
	// 		echo json_encode($response_array);
	// 	}
	// 	exit;
	// }


	//===================== change status all featured/populer/upsell product ========================//

	// public function change_new_arriavl_status(){
	// 	if (!$this->input->is_ajax_request()) {
	// 	   exit('No direct script access allowed');
	// 	}
	// 	$error = false;
	// 	$response_array = array('success'=>false);		
	// 	$required_fields = array(
	// 		'id' => 'missing id in the request',
	// 		'table' => 'missing table in the request',
	// 		'current_status' => 'missing current status in the request',
	// 	);
	// 	foreach($required_fields as $key => $value){
	// 		if(strlen(trim($this->input->post($key))) <= 0){
	// 			$response_array['msg'] = $value;
	// 			$error = true;
	// 			break;
	// 		}
	// 	}
	// 	if(!$error){
	// 		if ($this->input->post('current_status') == '0') {
	// 			$status = '1';
	// 		}
	// 		if ($this->input->post('current_status') == '1') {
	// 			$status = '0';
	// 		}
	// 		$records = array(
	// 			'new_arrivals_product' => $status,
	// 		);
	// 		$conditions = array(
	// 			"where" => array("product_id" => $this->input->post('id')),
	// 		);
			
	// 		$this->common_model->update_data($this->input->post('table'), $records, $conditions);
	// 		$response_array['success'] = true;
	// 		echo json_encode($response_array);
	// 	}
	// 	exit;
	// } 
	
	// public function change_blog_status(){
	// 	if (!$this->input->is_ajax_request()) {
	// 	   exit('No direct script access allowed');
	// 	}
	// 	$error = false;
	// 	$response_array = array('success'=>false);		
	// 	$required_fields = array(
	// 		'id' => 'missing id in the request',
	// 		'table' => 'missing table in the request',
	// 		'current_status' => 'missing current status in the request',
	// 	);
	// 	foreach($required_fields as $key => $value){
	// 		if(strlen(trim($this->input->post($key))) <= 0){
	// 			$response_array['msg'] = $value;
	// 			$error = true;
	// 			break;
	// 		}
	// 	}
	// 	if(!$error){
	// 		if ($this->input->post('current_status') == '0') {
	// 			$status = '1';
	// 		}
	// 		if ($this->input->post('current_status') == '1') {
	// 			$status = '0';
	// 		}
	// 		$records = array(
	// 			'status' => $status,
	// 		);
	// 		$conditions = array(
	// 			"where" => array("id" => $this->input->post('id')),
	// 		);
	// 		$this->common_model->update_data($this->input->post('table'), $records, $conditions);
	// 		// echo"<pre>"; echo $this->db->last_query(); exit;
	// 		$response_array['success'] = true;
	// 	}
	// 	echo json_encode($response_array);
	// 	exit;
	// }

	public function change_news_status(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$error = false;
		$response_array = array('success'=>false);		
		$required_fields = array(
			'id' => 'missing id in the request',
			'table' => 'missing table in the request',
			'current_status' => 'missing current status in the request',
		);
		foreach($required_fields as $key => $value){
			if(strlen(trim($this->input->post($key))) <= 0){
				$response_array['msg'] = $value;
				$error = true;
				break;
			}
		}
		if(!$error){
			if ($this->input->post('current_status') == '0') {
				$status = '1';
			}
			if ($this->input->post('current_status') == '1') {
				$status = '0';
			}
			$records = array(
				'status' => $status,
			);
			$conditions = array(
				"where" => array("id" => $this->input->post('id')),
			);
			$this->common_model->update_data($this->input->post('table'), $records, $conditions);
			// echo"<pre>"; echo $this->db->last_query(); exit;
			$response_array['success'] = true;
		}
		echo json_encode($response_array);
		exit;
	}
	public function change_ticket_status(){ // use for user register
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$error = false;
		$response_array = array('success'=>false);		
		$required_fields = array(
			'id' => 'missing id in the request',
			'table' => 'missing table in the request',
			'current_status' => 'missing current status in the request',
		);
		foreach($required_fields as $key => $value){
			if(strlen(trim($this->input->post($key))) <= 0){
				$response_array['msg'] = $value;
				$error = true;
				break;
			}
		}
		if(!$error){
			if ($this->input->post('current_status') == '0') {
				$status = '1';
			}else if ($this->input->post('current_status') == '1') {
				$status = '0';
			}else if ($this->input->post('current_status') == '2') {
				$status = '2';
			}else{
				$status = '1';
			}
			$records = array(
				'status' => $status,
			);
			$conditions = array(
				"where" => array("id" => $this->input->post('id')),
			);
			$this->common_model->update_data($this->input->post('table'), $records, $conditions);
			// echo"<pre>"; echo $this->db->last_query(); exit;
			$response_array['success'] = true;
		}
		echo json_encode($response_array);
		exit;
	}
}
?>