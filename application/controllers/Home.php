<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Home extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();	
			// if(base_url() == "http://localhost/online-cart/"){
			// 	$this->output->enable_profiler(TRUE);
			// }		
		}

		function index()
		{	
			$data['slider_details'] = $this->Production_model->get_all_with_where('home_slider','slider_position','asc',array('status'=>'1'));
			// $data['announce_details'] = $this->Production_model->get_all_with_where('announce','','',array('status'=>'1'));		
			$data['features_details'] = $this->Production_model->get_all_with_where('common_feature','','',array('status'=>'1'));	
			$data['hosting_price_details'] = $this->Production_model->get_all_with_where('hosting_price','','',array('status'=>'1'));		
			// echo "<pre>";print_r($data);exit;
			$this->load->view('index',$data);	
		}	
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>