<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
        <link rel="stylesheet" href="<?= base_url()?>assets/owl-carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="<?= base_url()?>assets/owl-carousel/owl.theme.default.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/popup_css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/popup_css/demo.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/popup_css/dialog.css" />
        <!-- individual effect -->
        <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/popup_css/dialog-sally.css" />
        <script src="<?= base_url()?>assets/popup_js/modernizr.custom.js"></script>
        <style type="text/css">
          @import url(<?= base_url()?>assets/css/mdb.css);
          input[type=text],input[type=number],input[type=email],.form-control:focus {
              font-size: 2rem;
          }
        </style>
    </head>
    <body>
        <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          
          <!-- header over -->
          <div class="allpage_banner_domain allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'domain.jpg')?>);">
            <h1 class="title_h1">Domains</h1>
            <p><a href="index.html">Home </a> / Domains</p>
          </div>
          
      <div class="all_domain">
        <div class="domain_box">
          <div class="container padding_all">
            <div class="">
              <h3>Find your Perfect Domain Name:</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia aliquam tempora deleniti quibusdam labore quo fuga.</p>
              <div class="col-md-12 col-xs-12 margin_top">
                <form method="post" name="search_domain" id="search_domain">
                  <div class="search_domain_box">
                    <input type="text" name="domain_name">
                      <select id="sel1" class="select_op" name="domain_type">
                         <?php 
                            if(isset($domains_data) && $domains_data !=null){ 
                              $i=1;
                              foreach ($domains_data as $key => $value) { ?>
                                <option value="<?=$value['id']; ?>"><?=$value['title']; ?></option>
                              <?php
                              }
                            }
                          ?>
                      </select>
                    
                    <!-- <button class="btn_order" data-toggle="modal" data-target="#myModal">Search Domain</button> -->
                    <button type="button" class="btn_order trigger" data-dialog="somedialog">Search Domain</button>
                    <label for="domain_name" class="error"></label>
                  </div><hr>
                  <!-- <div id="somedialog" class="dialog">
                    <div class="dialog__overlay"></div>
                    <div class="dialog__content">
                      <form method="post" name="search_domain" id="search_domain">
                        <div class="form-group">
                          <label class="ord_class_label">Name</label>
                          <input type="text" name="name" id="name" class="input_all ord_class_text">
                        </div>
                        <div class="form-group">
                          <label class="ord_class_label">Email</label>
                          <input type="email" name="email" id="email" class="input_all ord_class_text">
                        </div>
                        <div class="form-group">
                          <label class="ord_class_label">Mobile Number</label>
                          <input type="text" name="mobile_no" id="mobile_no" class="input_all ord_class_text">
                        </div>
                        <div class="form-group text-center">
                          <button type="button" class="btn_order check">Submit</button>
                        </div>
                      </form>
                      <hr>
                      <div style="float: right;margin-right: 4%">
                        <button class="btn_order action" data-dialog-close>Close</button>
                      </div>
                    </div>
                  </div> -->
                  <div id="somedialog" class="dialog">
                    <div class="dialog__overlay"></div>
                    <div class="dialog__content" style="padding-right: 30px;padding-left: 30px;">
                        <div class="row">
                          <div class="card-body">
                              <!--Body-->
                              <div class="modal-header">
                                <div class="title">
                                  <h4 class="text-dark">Domain Detail</h4>
                                </div>
                              </div>
                              <div class="modal-logo text-center">
                                <img src="<?=base_url()?>assets/image/logo.png" class="img-responsive mx-auto d-block">
                              </div>
                              <div class="md-form mb-6">
                                  <input type="text" id="name" name="name" class="form-control">
                                  <label for="defaultForm-name">Your Name</label>
                              </div>
                              <div class="md-form mb-6">
                                  <input type="email" id="email" name="email" class="form-control">
                                  <label for="defaultForm-email">Your email</label>
                              </div>
                              <div class="md-form">
                                  <input type="number" id="mobile_no" name="mobile_no" class="form-control">
                                  <label for="defaultForm-number">Conatct Number</label>
                              </div>
                              <div class="text-center">
                                  <button type="button" class="btn_order action check">Submit</button>
                              </div>
                          </div>
                        </div>
                      <hr>
                      <div style="float: right;margin-right: 4%">
                        <button class="btn_order action" data-dialog-close>Close</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="container padding_all">
            <div class="owl-carousel">
            <?php 
              if(isset($domains_data) && $domains_data !=null){ 
                $i=1;
                foreach ($domains_data as $key => $value) { ?>
                    <div class="<?php if($i%2==1){?> color_domain_one <?php }else{?> color_domain_two <?php } ?>text-center">
                      <h3><?=$value['title']; ?></h3>
                      <p>&nbsp;</p>
                      <h2>$ <?=$value['price']; ?></h2>
                      <h6>/year</h6>
                    </div>
                    <?php 
                  $i++;
                }
              } 
            ?>  
          </div>
        </div>
        <div class="container">
          <div class="row ">
          <?php
            if (isset($domain_info_data) && $domain_info_data !=null){ 
                foreach ($domain_info_data as $key => $value1) { ?>
                  <div class="col-md-3 col-xs-12 text_h3" style="margin-top:15px;margin-bottom:25px;">
                    <i class="fa fa-check-square-o"></i>
                    <p><?=$value1['name']; ?></p>
                  </div>
                  <?php 
                } 
            } 
          ?>    
          </div>
        </div>
        
      <!-- domain over -->

      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?> 
      <script type="text/javascript" src="<?= base_url()?>assets/owl-carousel/owl.carousel.min.js"></script>
      <script type="text/javascript">
        $('.owl-carousel').owlCarousel({
            loop:false,
            margin:0,
            
            pagination :false,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:false
                },
                600:{
                    items:3,
                    nav:false
                },
                1000:{
                    items:5,
                    nav:false,
                    loop:false
                }
            }
        })
      </script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.4.1/js/mdb.min.js"></script>
      <script src="<?= base_url()?>assets/popup_js/classie.js"></script>
      <script src="<?= base_url()?>assets/popup_js/dialogFx.js"></script>
      <script>
        (function() {

          var dlgtrigger = document.querySelector( '[data-dialog]' ),
            somedialog = document.getElementById( dlgtrigger.getAttribute( 'data-dialog' ) ),
            dlg = new DialogFx( somedialog );

          dlgtrigger.addEventListener( 'click', dlg.toggle.bind(dlg) );

        })();
      </script>
      <script type="text/javascript">
        $('#search_domain').validate({
            rules: {
                domain_name:{
                    required: true,
                },
                name: {
                    required: true,
                },
                email: {
                    required: true,
                    email:true,
                },
                mobile_no: {
                    required: true,
                    number:true,
                },
            },
            messages: {
                domain_name:{
                    required: "Please enter domain name",
                },
                name: {
                    required: "Please enter name",
                },
                email: {
                  required: "Please enter Email ",
                  email:"Please enter valid Email",                     
                },
                mobile_no: {
                    required: "Please enter mobile number",
                    number: "Please enter only digits",
                },
            }
        });

        var form = $( "#search_domain" );
        form.validate();
        $(document).on('click','.check',function(){
          //alert( "Valid: " + form.valid() );
          if(form.valid()){
            add_search_domain();
          }
     
        });   
        function add_search_domain(){
           var formData = new FormData($('#search_domain')[0]);
            var uurl = BASE_URL+"api/user/add_search_domain";

            $.ajax({
               url: uurl,
               type: 'POST',
               dataType: 'json',
               data: formData,
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){
                if (response.result=="Success") {
                    $.alert({
                        type: 'green',
                        title: 'Search domain accepted',
                        content: response.message,
                    });
                    setTimeout(function() { window.location.reload(); }, 2000);
                }else if(response.result=="Fail"){
                    $.alert({
                        type: 'red',
                        title: 'Search domain failed',
                        content: response.message,
                    });
                }
                // console.log(response);
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
               cache: false,
               contentType: false,
               processData: false
            });
        }
      </script>
   </body>
</html> 