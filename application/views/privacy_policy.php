<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('include/header');
?> 

<!-- About Area Start Here -->
<section class="s-space-bottom-full bg-accent-shadow-body">
    <div class="container">
        <div class="breadcrumbs-area">
            <ul>
                <li><a href="<?= base_url()?>">Home</a> -</li>
                <li class="active"><?= isset($privacy_data) && $privacy_data !=null ? $privacy_data[0]['name'] : '';?></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="gradient-wrapper">
                    <div class="gradient-title">
                        <h2> <?= isset($privacy_data) && $privacy_data !=null ? $privacy_data[0]['name'] : '';?></h2>
                    </div>
                    <div class="about-us gradient-padding">
                        <?= isset($privacy_data) && $privacy_data !=null ? $privacy_data[0]['details'] : '';?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Area End Here -->

<?php $this->load->view('include/copyright');?>

<?php $this->load->view('include/footer');?>