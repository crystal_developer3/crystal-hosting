<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
      <link rel="stylesheet" href="<?=base_url('assets/css/jquery.dataTables.min.css')?>">
    </head>
    <body>
      <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          <!-- about start -->
            <div class="allpage_banner_email allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'email.jpg')?>);">
              <h1 class="title_h1">Register New Domain</h1>
              <p><a href="<?=base_url()?>">Home </a> / Register New Domain</p>
            </div>
            <div class="all_white padding_all">
          <div class="container">
          <div class="row">
            <div class="col-md-10 col-md-offset-1 col-xs-12">
              <form action="#">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><div class="panel-heading">
                        <h4 class="panel-title">
                            I want crystal Hosting to register a new domain for me. 
                        </h4>
                      </div></a>
                      <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="row">
                              <div class="col-md-2 col-lg-2 col-sm-4 col-xs-12">
                                <div class="www">
                                  <h4>www.</h4>
                                </div>
                              </div>
                              <div class="col-md-8 col-lg-8 col-sm-4 col-xs-12">
                                <div class="domain-name">
                                  <div class="form-group">
                                    <input type="text" class="form-control" id="email">
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-2 col-lg-2 col-sm-4 col-xs-12">
                                <div class="domain-name">
                                  <div class="form-group">
                                <select class="form-control domain-dropdown" id="sel1">
                                  <option>.com</option>
                                  <option>.in</option>
                                  <option>.net</option>
                                  <option>.us</option>
                                  <option>.abc</option>
                                  <option>.mobi</option>
                                  <option>.asia</option>
                                </select>
                              </div>
                                </div>
                              </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><div class="panel-heading">
                        <h4 class="panel-title">
                          I want to transfer my domain to Crystal Hosting 
                        </h4>
                      </div></a>
                      <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="row">
                              <div class="col-md-2 col-lg-2 col-sm-4 col-xs-12">
                                <div class="www">
                                  <h4>www.</h4>
                                </div>
                              </div>
                              <div class="col-md-8 col-lg-8 col-sm-4 col-xs-12">
                                <div class="domain-name">
                                  <div class="form-group">
                                    <input type="text" class="form-control" id="email">
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-2 col-lg-2 col-sm-4 col-xs-12">
                                <div class="domain-name">
                                  <div class="form-group">
                                <select class="form-control domain-dropdown" id="sel1">
                                  <option>.com</option>
                                  <option>.in</option>
                                  <option>.net</option>
                                  <option>.us</option>
                                  <option>.abc</option>
                                  <option>.mobi</option>
                                  <option>.asia</option>
                                </select>
                              </div>
                                </div>
                              </div>
                            </div>
                      </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group text-center">
                  </div> 
              </form>
              <div class="text-center" style="margin-bottom: 20px;">
                  <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Continue <i class="fa fa-arrow-circle-right"></i></button>
              </div>
            </div>
          </div>
         </div>
      </div>
      
      <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="width: 70%; margin-left: 10%;border-radius: 15px;">
              <!-- <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Register A New Domain</h4>
              </div> -->
              <div class="modal-body text-center">
                <i class="fa fa-check-circle fa-3x" ariea-hidden="true" style="color: #ff802b;"></i>
                <h3 style="margin-top: 20px; margin-bottom: 20px;">Your Registration Done Successfully</h3>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" style="width: 100%; border-radius: 30px!important;">Done</button>
                  </div>
                  <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div> -->
              </div>
          </div>
      </div>
      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?>       
   </body>
</html> 