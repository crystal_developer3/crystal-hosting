<!doctype html>
<html class="no-js" lang="">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?= SITE_TITLE?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url()?>assets/image/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/bootstrap.min.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/animate.min.css">
    <!-- Font-awesome CSS-->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/font-awesome.min.css">
    <!-- Main Menu CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/meanmenu.min.css">
    <!-- Select2 CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/select2.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/style.css">

    <!--New add-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/developer.css">
    <script>
        var BASE_URL = '<?php echo base_url(); ?>';
    </script>
</head>
<body>
    <!-- Preloader End Here -->
    <div id="wrapper">
        <header>
            <div class="header-top">
                <div class="container">
                    <div class="row no-gutters text-center">
                        <div class="col-12">
                            <div class="logo-area">
                                <a href="<?= base_url()?>" class="img-fluid">
                                    <?php
                                        $profile_photo = $this->Production_model->get_all_with_where('user','','',array());
                                        if ($profile_photo != null) {
                                        ?>
                                            <img src="<?= $profile_photo !=null ? base_url('assets/uploads/profile_photo/').$profile_photo[0]['profile_photo'] : base_url('assets/uploads/profile_photo/default-image.png')?>" alt="logo" class="p-3">
                                        <?php } 
                                    ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Login Area Start Here -->
        <section class="pt-5 pb-5 bg-accent-shadow-body">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-2">
                    </div>
                    <div class="col-lg-6 col-md-8 col-12">  
                        <?php $this->load->view('include/messages');?>

                        <div class="my-account-wrapper gradient-wrapper input-layout1 mt-3">
                            <div class="gradient-title text-center">
                                <h3>Log In</h3>
                            </div>
                            <form id="login-page-form" class="col-md-12 pt-4 pt-sm-5" method="post" action="<?= base_url('login/dologin')?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-lg-12 col-12">
                                                <label class="control-label top-0">Email ID / Mobile no</label>
                                            </div>
                                            <div class="col-lg-12 col-12">
                                                <div class="form-group">
                                                    <input type="text" id="user_email" name="user_email" class="form-control" placeholder="Enter mobile number / email address">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-lg-12 col-12">
                                                <label class="control-label top-0">Password</label>
                                            </div>
                                            <div class="col-lg-12 col-12">
                                                <div class="form-group">
                                                    <input type="password" id="phone" name="password" class="form-control" placeholder="Your Phone Number">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="checkbox checkbox-primary checkbox-circle">
                                                                <input id="checkbox1" type="checkbox" checked="" name="remember_me" value="yes">
                                                                <label for="checkbox1">Always remember</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 text-right">
                                                            <a href="javascript:void(0)" class="pt-2 d-block" data-toggle="modal" data-target="#ForgetPassword">
                                                                Forgot password ?
                                                            </a>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 ml-none--mb">
                                        <div class="form-group">
                                        <button type="submit" class="cp-default-btn-sm-primary border-none w-100">Login</button>
                                        </div>
                                    </div>
                                </div>
                            </form>   

                            <div class="text-center">
                                or
                            </div>         
                            <div class="social-login text-center mt-3 pb-4">
                                <a href="#">
                                    <img src="<?= base_url()?>assets/image/footer/facebook.png">
                                </a>
                                <a href="#" class="ml-2 mr-2">
                                    <img src="<?= base_url()?>assets/image/footer/twitter.png">
                                </a>
                                <a href="#">
                                    <img src="<?= base_url()?>assets/image/footer/google-plus.png">
                                </a>
                            </div>       
                        </div>                       
                    </div>
                </div>
            </div>

            <!-- forgot password start-->
            <div id="ForgetPassword" class="modal fade" role="document" tabindex='-1' style="margin-top: 5%;">
                <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Forgot Passsword</h4>
                      </div>
                      <div class="modal-body">
                        <div class="row">
                          <div class="col-md-12">
                            <label> Enter Your Registered Email-id To Receive Your Password.</label>
                          </div>
                          <div class="col-md-1"></div>
                        </div>

                        <div class="row">
                          <div class="col-md-12">

                            <form method="post">
                              <!-- <label>Email</label> -->
                              <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" id="UserEmail" placeholder="Enter Email" name="UserEmail">
                              </div>
                              <label id="UserEmail-error" class="text-danger"></label>
                              <br>
                              <div class="">      
                                <button type="submit" class="btn btn-success frgt_pwd_check" formaction="<?= base_url('login/forgot_password')?>">Send</button>
                                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
                                <br><br>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <!-- forgot password end -->
        </section>
        <!-- Login Area End Here -->
        <!-- Footer Area Start Here -->
        <footer>
            <div class="footer-area-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12 text-center-mb">
                            <p>Copyright © Crystal Hositing</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12 text-right text-center-mb">
                            <ul>
                                <li>
                                    <img src="<?= base_url()?>assets/image/footer/card1.jpg" alt="card">
                                </li>
                                <li>
                                    <img src="<?= base_url()?>assets/image/footer/card2.jpg" alt="card">
                                </li>
                                <li>
                                    <img src="<?= base_url()?>assets/image/footer/card3.jpg" alt="card">
                                </li>
                                <li>
                                    <img src="<?= base_url()?>assets/image/footer/card4.jpg" alt="card">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Area End Here -->
    </div>
    <!-- jquery-->
    <script src="<?= base_url()?>assets/js/jquery-3.2.1.min.js"></script>
    <!-- Popper js -->
    <script src="<?= base_url()?>assets/js/popper.js"></script>
    <!-- Bootstrap js -->
    <script src="<?= base_url()?>assets/js/bootstrap.min.js"></script>
    <!-- Meanmenu Js -->
    <script src="<?= base_url()?>assets/js/jquery.meanmenu.min.js"></script>
    <!-- Srollup js -->
    <script src="<?= base_url()?>assets/js/jquery.scrollUp.min.js"></script>
    <!-- Select2 Js -->
    <script src="<?= base_url()?>assets/js/select2.min.js"></script>
    <!-- Custom Js -->
    
    <!--new added -->
    <script src="<?php echo base_url(); ?>assets/validation/common.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/address.js"></script>
    <script src="<?php echo base_url(); ?>assets/validation/jquery.validate.js"></script>
    <!-- <script src="<?php echo base_url(); ?>assets/js/price-filter.js"></script> -->
    <!-- Footer Scripts -->
    <script src="<?= base_url()?>assets/js/custom.js"></script>

    <script>
        /*FORM VALIDATION*/
        $("#login-page-form").validate({
            rules: {
                'user_email': {required: true}, 
                'password': {required: true}, 
            },
            messages: {
                'user_email': "Please enter email OR mobile-no", 
                'password': "Please enter password", 
            }
        });

        $('.frgt_pwd_check').click(function(){
            // alert();
            if(isemptyfocus('UserEmail')){
                return false;
            }

            if(isvalidemail('UserEmail')){
                return false;
            }
        });
    </script>
</body>
</html>