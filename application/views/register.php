<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
      <script src='https://www.google.com/recaptcha/api.js'></script>
      <style type="text/css">
        /*.g_captcha .g-recaptcha div{
          width: 100%;
        }*/
      </style>
    </head>
    <body>
        <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          
          <div class="allpage_banner_register allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'register.jpg')?>);">>
            <h1 class="title_h1">Register</h1>
            <p><a href="<?=base_url()?>">Home </a> / Register</p>
          </div>
          <div class="all_white padding_all">
            <div class="container">
              <div class="row">
                <div class="login_box col-md-offset-2 col-md-8 col-xs-12">
                  <h3>Register </h3>
                </div>
              </div>
              <div class="row">
                <div class="col-md-offset-2 col-md-8 col-xs-12 contact_box_input">
                  <p>Create an account with us . . .</p><hr>
                  <div id="signuperrors" class="col-md-12"></div>
                  <div class="row">
                    <form method="post" name="signupform" id="signupform">
                      <div class="col-md-12 text-left">
                        <div class="col-md-6">
                              <label>First Name:</label>
                              <input name="first_name" id="first_name" type="text" class="form-control" placeholder="First Name" value="">
                            <p class="err_p" id="first_name_err"></p>
                        </div>
                        <div class="col-md-6">
                            <label>Last Name:</label>
                            <input name="last_name" id="last_name" type="text" class="form-control" placeholder="Last Name" value="">
                        <p class="err_p" id="last_name_err"></p>
                        </div>
                        
                        <div class="col-md-6">
                            <label>Date Of Birth:</label>
                            <input name="dob" id="dob" type="text" class="form-control" placeholder="Date of Birth" value="">
                          <p class="err_p" id="dob_err"></p>
                        </div>
                        <div class="col-md-6">
                            <label>Email:</label>
                            <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="">
                          <p class="err_p" id="email_err"></p>
                        </div>
                         
                        <div class="col-md-6">
                            <label>Password:</label>
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                          <p class="err_p" id="password_err"></p>
                        </div>
                        <div class="col-md-6">
                            <label>Confirm Password:</label>
                            <input type="password" name="cpassword" id="cpassword" class="form-control" placeholder="Confirm Password">
                          <p class="err_p" id="cpassword_err"></p>
                        </div>
                        <div class="col-md-12">
                            <label>Company Name:</label>
                            <input type="text" name="company_name" id="company_name" class="form-control" placeholder="Company name">
                        </div>
                        
                        <div class="col-md-12">
                            <label>Address:</label>
                            <textarea type="text" name="address" id="address" class="form-control" placeholder="Address" value="">
                            </textarea>
                          <p class="err_p" id="address_err"></p>
                        </div>
                        
                          
                        <div class="col-lg-4" style="margin-bottom: 1rem">
                            <label class="control-label top-0">Country *</label>
                            <?php
                                $country_info = get_country();
                                // echo"<pre>"; print_r($country_info); exit;
                                $options = array();
                                $options[NULL] = 'Select Country';
                                if (count($country_info) > 0) {
                                    foreach ($country_info as $key => $value) {
                                        $options[$value['id']] = $value['country_name'];
                                    }
                                }
                                echo form_dropdown('id_country', $options, isset($user_details[0]['id_country']) ? $user_details[0]['id_country'] : '', 'class="form-control" id="id_country"');
                                echo form_error('id_country', '<label class="error">', '</label>');
                            ?>
                        </div>
                        <div class="col-lg-4" style="margin-bottom: 1rem">
                            <label class="control-label top-0">State *</label>
                            <?php
                                if (isset($user_details[0]['id_country']) && $user_details[0]['id_country'] != '') {
                                    $state_info = get_state($user_details[0]['id_country']);
                                    $options = array();
                                    $options[NULL] = 'Select State';
                                    if (count($state_info) > 0) {
                                        foreach ($state_info as $key => $value) {
                                            $options[$value['id']] = $value['state_name'];
                                        }
                                    }
                                } else {
                                    $options = array(
                                        NULL => 'Select State',
                                    );
                                }
                                echo form_dropdown('id_state', $options, isset($user_details[0]['id_state']) ? $user_details[0]['id_state'] : '', 'class="form-control" id="id_state"');
                                echo form_error('id_state', '<label class="error">', '</label>');
                            ?>
                        </div>
                        <div class="col-lg-4" style="margin-bottom: 1rem">
                            <label class="control-label top-0">City *</label>
                            <?php
                                if (isset($user_details[0]['id_state']) && $user_details[0]['id_state'] != '') {
                                    $city_info = get_city($user_details[0]['id_state']);
                                    $options = array();
                                    $options[NULL] = 'Select City';
                                    if (count($city_info) > 0) {
                                        foreach ($city_info as $key => $value) {
                                            $options[$value['id']] = $value['city_name'];
                                        }
                                    }
                                } else {
                                    $options = array(
                                        NULL => 'Select City',
                                    );
                                }
                                echo form_dropdown('id_city', $options, isset($user_details[0]['id_city']) ? $user_details[0]['id_city'] : '', 'class="form-control" id="id_city"');
                                echo form_error('id_city', '<label class="error">', '</label>');
                            ?>
                        </div>
                                
                        <div class="col-md-6">
                              <label>Zip Code:</label>
                              <input type="text" name="zip_code" id="zip_code" class="form-control" placeholder="Zip Code" value="">
                          <p class="err_p" id="zip_code_err"></p>
                          </div>
                        
                        <div class="col-md-6">
                              <label>Phone Number:</label>
                              <input type="text" name="phone_number" id="phone_number" class="form-control" placeholder="Phone Number" value="">
                          <p class="err_p" id="phone_number_err"></p>
                        </div>
                        <div class="col-md-12">
                            <div class="g-recaptcha" data-sitekey="6Le-wvkSAAAAAPBMRTvw0Q4Muexq9bi0DJwx_mJ-" style="margin-top: 2%;"></div>
                            <span class="error_captcha" style="color: #fc3a3a;"></span>   
                        </div>
                        
                        <div class="text-center btn_margin" style="clear:both;">
                          <!--<button class="btn btn-primary btn_margin">Register</button>-->
                        <input type="submit" value=" Register " class="btn btn-primary btn_margin check" style="background-color: #ff802b;border-color: #ff802b;">
                        </div>
                    </div>
                    
                    </form> 
                    </div>
                    
                  </div>
                  <div class="">
                    <div class="footer_login_box col-md-offset-2 col-md-8 col-xs-12">
                  </div>
                </div>
              </div>
            </div>
          </div>
      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?>

      <!-- <script type="text/javascript">
        $('#login-form').validate({
            rules: {
                email: {
                    required: true,
                    email:true,
                },
                password: {
                    required: true,
                },
                
            },
            messages: {
                email: {
                  required: "Please enter Email ",
                  email:"Please enter valid Email",                     
                },
                password: {
                    required: "Please enter password",
                },
            }
        });

        var form = $( "#login-form" );
        form.validate();
        $(document).on('click','.check',function(){
          //alert( "Valid: " + form.valid() );
          if(form.valid()){
            get_login();
          }
     
        });
   
        function get_login(){
           var formData = new FormData($('#login-form')[0]);
            var uurl = BASE_URL+"api/user/login";

            $.ajax({
               url: uurl,
               type: 'POST',
               dataType: 'json',
               data: formData,
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){
                if (response.result=="Success") {
                    $.alert({
                        type: 'green',
                        title: 'Login success',
                        content: response.message,
                    });
                    setTimeout(function() { window.location.href = BASE_URL; }, 2000);
                }else if(response.result=="Fail"){
                    $.alert({
                        type: 'red',
                        title: 'Login failed',
                        content: response.message,
                    });
                }
                // console.log(response);
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
               cache: false,
               contentType: false,
               processData: false
            });
        }
    </script> -->

    <script type="text/javascript">
        $("#dob").datepicker({
            format: 'd M yyyy',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            todayHighlight: true,
            endDate: "today",
            autoclose: true,            
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            /*Get cities based on state selection*/

            $('#id_country').change(function () {
                var current = $(this);
                if (current.val() != '') {
                    var post_data = {'id_country': current.val()};
                    $.ajax({
                        url: BASE_URL + 'ajax/get_states',
                        method: 'POST',
                        data: post_data,
                        dataType: 'json',
                        success: function (response) {
                            if (response.success) {
                                $('#id_state').html(response.data);
                                $('#id_city').html('');
                            }
                        }
                    });
                }
            });
            /*By default country india selected if any other country is not selected*/
            if ($('#id_country').val() == '') {
                $('#id_country option[value="101"]').attr('selected', 'selected');
                $('#id_country').trigger('change');
            }
            /*Get cities based on state selection*/
            $('#id_state').change(function () {
                var current = $(this);
                if (current.val() != '') {
                    var post_data = {'id_state': current.val()};
                    $.ajax({
                        url: BASE_URL + 'ajax/get_cities',
                        method: 'POST',
                        data: post_data,
                        dataType: 'json',
                        success: function (response) {
                            if (response.success) {
                                $('#id_city').html(response.data);
                            }
                        }
                    });
                }
            });

        });
        
        $('#signupform').validate({
                rules: {
                    first_name: {
                        required: true,
                    },
                    last_name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email:true,
                    },
                    phone_number:{
                        required: true,
                        number:true,
                    },
                    dob:{
                        required: true,
                        date:true,
                    },
                    zip_code:{
                        required: true,
                        number:true,
                    },
                    state:{
                        required: true,
                    },
                    city:{
                        required: true,
                    },
                    password: {
                        required: true,
                    },
                    confirmpassword: {
                        required: true,
                        equalTo: "#password",
                    },
                },
                messages: {
                    
                    first_name: {
                      required: "Please enter your First name",
                      
                    },
                    last_name: {
                      required: "Please enter your Last name",
                      
                    },
                    email: {
                      required: "Please enter your email address",
                      email: "Please Enter valid email address",
                      
                    },
                    phone_number:{
                        required: "Please Enter Moibile Number",
                        number:"Only numbers",
                    },
                    dob:{
                        required: "Please Enter Date of birth",
                        date: "Please Enter valid Date of birth",
                    },
                    zip_code:{
                        required: "Please Enter Pincode",
                        number:"Only digits",
                    },
                    id_state:{
                        required: "Please Enter State",
                    },
                    id_city:{
                        required: "Please Enter City",
                    },
                    password: {
                        required: "Please Enter Password",
                    },
                    confirmpassword: {
                        required: "Please Enter Confirm Password",
                        equalTo: "Confirm Password not matched",
                    },
                }
            });

        // var form = $( "#signupform" );
        // form.validate();
        // $( ".check" ).click(function() {
        //   alert( "Valid: " + form.valid() );
        //   if(form.valid()){

        //     register();
        //   }
        // });
        var form = $( "#signupform" );
        form.validate();
        $(document).on('submit','#signupform',function(e){
        
          if(form.valid()){
            e.preventDefault();
            register();
          }else{
            return false;
          }
     
        });
        function register(){
          errormsg = '';
          var formData = new FormData($('#signupform')[0]);
          
          var uurl = BASE_URL+"api/user/registration";
          var dashboardURL = BASE_URL+"dashboard";
           $.ajax({
               url: uurl,
               type: 'POST',
               data: formData,
               dataType:'json',
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){
                $('#signuperrors').html('');
                 if (response.result=="Success") {
                   setTimeout(function() { window.location.reload(); }, 2000);
                    $.alert({
                        title: 'Message',
                        type: 'green',
                        content: response.message,
                    });
                    setTimeout(function() { window.location.href = dashboardURL; }, 2000);
                 }else if(response.message=="Registration Fail, email already exist"){
                   $.alert({
                        title: 'Message',
                        type: 'red',
                        content: 'Email already exists !!',
                    });
                 }else if(response.message=="Registration Fail, phone number already exist"){
                   $.alert({
                        title: 'Message',
                        type: 'red',
                        content: 'Phone number already exists !!',
                    });
                 }else{
                   errormsg = '<li><i class="fa fa-info-circle"></i>&nbsp;Not Registered.</li>';
                   $('#signuperrors').html('<ul class="form-error">'+errormsg+'</ul>');
                 }
                 $('#signuperrors').show(1000);
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
               cache: false,
               contentType: false,
               processData: false
           });
        }
    </script>
   </body>
</html> 