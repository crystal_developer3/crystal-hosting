<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('include/header');
?>  
<style type="text/css">
    .btn-primary {
        color: #fff !important;
        background-color: #009fff !important;
        border-color: #007bff !important;
    }
</style>
<!-- Login Area Start Here -->
<section class="s-space-bottom-full bg-accent-shadow-body">
    <div class="container-fluid">
        <div class="breadcrumbs-area">
            <ul>
                <li><a href="<?= base_url()?>">Home</a> -</li>
                <li class="active">My Account Page</li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-12">
                <div class="sidebar-item-box">
                    <ul class="nav tab-nav my-account-title">
                        <li class="profile-photo-block-li">
                            <div class="profile-photo-block">
                                <?php
                                    if (isset($user_details) && $user_details != null) {                                    
                                        $path = base_url() . 'assets/uploads/profile_picture/' . $user_details[0]['profile_picture'];
                                    } else {
                                        $path = base_url('assets/uploads/default_img.png');
                                    }
                                ?>
                                <img src="<?php echo $path; ?>">
                            </div>
                            <form name="upload_profile_photo" method="POST" enctype="multipart/form-data">
                                <input type="button" id="get_file" value="+ Upload Photo">
                                <input type="file" id="my_file" name="profile_picture" class="display-file-preview">
                            </form>
                        </li>
                        <li class="nav-item"><a class="active" href="#personal" data-toggle="tab" aria-expanded="false">Personal Information</a></li>
                        <li class="nav-item"><a href="#my-add" data-toggle="tab" aria-expanded="false">My Ads</a></li>
                        <li class="nav-item"><a href="#invoice" data-toggle="tab" aria-expanded="false">Invoice</a></li>
                        <li class="nav-item"><a href="#wishlist" data-toggle="tab" aria-expanded="false">Wishlist</a></li>
                        <li class="nav-item"><a href="#change-password" data-toggle="tab" aria-expanded="false">Change Password</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-9 col-md-8 col-12"> 
                <?php  $this->load->view('include/messages');?>
                
                <div class="tab-content my-account-wrapper gradient-wrapper input-layout1">
                    <div role="tabpanel" class="tab-pane fade active show" id="personal">
                        <div class="gradient-title">
                        <h3>Personal Information</h3>
                    </div>
                    <form id="profile-form" class="col-md-12 p-4" method="post" action="<?= base_url('profile/update-profile')?>">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-lg-12 col-12">
                                        <label class="control-label top-0">Full Name</label>
                                    </div>
                                    <div class="col-lg-12 col-12">
                                        <div class="form-group">
                                            <input type="text" id="name" name="name" class="form-control" placeholder="Enter full name" value="<?= isset($user_details) && $user_details !=null ? $user_details[0]['name'] : '';?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-lg-12 col-12">
                                        <label class="control-label top-0">Email</label>
                                    </div>
                                    <div class="col-lg-12 col-12">
                                        <div class="form-group">
                                            <input type="text" id="user_email" name="user_email" class="form-control" placeholder="Email Address" value="<?= isset($user_details) && $user_details !=null ? $user_details[0]['user_email'] : '';?>">
                                            <div id="valid" style="color: #F66249"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-lg-12 col-12">
                                        <label class="control-label top-0">Mobile no</label>
                                    </div>
                                    <div class="col-lg-12 col-12">
                                        <div class="form-group">
                                            <input type="text" id="mobile_no" name="mobile_no" class="form-control" placeholder="Enter phone number" value="<?= isset($user_details) && $user_details !=null ? $user_details[0]['mobile_no'] : '';?>">
                                            <div class="checkbox checkbox-primary checkbox-circle">
                                                <input id="checkbox1" type="checkbox" name="mobile_no_display" value="0" <?= $user_details[0]['mobile_no_display'] == '0' ? 'checked' : '';?>>
                                                <label for="checkbox1">Hide the phone number on the published ads.</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-lg-12 col-12">
                                        <label class="control-label top-0">Gender</label>
                                    </div>
                                    <div class="col-lg-12 col-12">
                                        <div class="form-group">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="inlineRadio4" value="Male" name="gender" <?= $user_details[0]['gender'] == 'Male' ? 'checked' : '';?>>
                                                <label for="inlineRadio4">Male</label>
                                            </div>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="inlineRadio5" value="Female" name="gender" <?= $user_details[0]['gender'] == 'Female' ? 'checked' : '';?>>
                                                <label for="inlineRadio5">Female</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="row">                 
                                    <div class="col-lg-4 mb-3">
                                        <label class="control-label top-0">Country</label>
                                        <?php
                                            $country_info = get_country();
                                            // echo"<pre>"; print_r($country_info); exit;
                                            $options = array();
                                            $options[NULL] = 'Select Country';
                                            if (count($country_info) > 0) {
                                                foreach ($country_info as $key => $value) {
                                                    $options[$value['id']] = $value['country_name'];
                                                }
                                            }
                                            echo form_dropdown('id_country', $options, isset($user_details[0]['id_country']) ? $user_details[0]['id_country'] : '', 'class="form-control" id="id_country"');
                                            echo form_error('id_country', '<label class="error">', '</label>');
                                        ?>
                                    </div>
                                    <div class="col-lg-4 mb-3">
                                        <label class="control-label top-0">State</label>
                                        <?php
                                            if (isset($user_details[0]['id_country']) && $user_details[0]['id_country'] != '') {
                                                $state_info = get_state($user_details[0]['id_country']);
                                                $options = array();
                                                $options[NULL] = 'Select State';
                                                if (count($state_info) > 0) {
                                                    foreach ($state_info as $key => $value) {
                                                        $options[$value['id']] = $value['state_name'];
                                                    }
                                                }
                                            } else {
                                                $options = array(
                                                    NULL => 'Select State',
                                                );
                                            }
                                            echo form_dropdown('id_state', $options, isset($user_details[0]['id_state']) ? $user_details[0]['id_state'] : '', 'class="form-control" id="id_state"');
                                            echo form_error('id_state', '<label class="error">', '</label>');
                                        ?>
                                    </div>
                                    <div class="col-lg-4 mb-3">
                                        <label class="control-label top-0">City</label>
                                        <?php
                                            if (isset($user_details[0]['id_state']) && $user_details[0]['id_state'] != '') {
                                                $city_info = get_city($user_details[0]['id_state']);
                                                $options = array();
                                                $options[NULL] = 'Select City';
                                                if (count($city_info) > 0) {
                                                    foreach ($city_info as $key => $value) {
                                                        $options[$value['id']] = $value['city_name'];
                                                    }
                                                }
                                            } else {
                                                $options = array(
                                                    NULL => 'Select City',
                                                );
                                            }
                                            echo form_dropdown('id_city', $options, isset($user_details[0]['id_city']) ? $user_details[0]['id_city'] : '', 'class="form-control" id="id_city"');
                                            echo form_error('id_city', '<label class="error">', '</label>');
                                        ?>
                                    </div>
                                    <div class="col-lg-12 col-12">
                                        <label class="control-label top-0">Address</label>
                                        <div class="form-group">
                                            <textarea class="form-control" name="address" placeholder="Address" style="height: 95px"><?= isset($user_details) && $user_details !=null ? $user_details[0]['address'] : '';?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <button type="submit" class="cp-default-btn-sm-primary disabled border-none w-100">Update Details</button>
                                </div>
                            </div>
                        </div>
                    </form> 
                    </div>
                    
                    <div role="tabpanel" class="tab-pane fade" id="my-add">
                       <div class="row">
                           <div class="col-lg-12">
                               <div class="gradient-wrapper item-mb">
                                    <div class="gradient-title">
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="layout-switcher">
                                                    <ul>
                                                        <li>
                                                            <div class="page-controls-sorting">
                                                                <button class="sorting-btn dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">Sort By<i class="fa fa-sort" aria-hidden="true"></i></button>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item" href="#">Date</a>
                                                                    <a class="dropdown-item" href="#">Best Sale</a>
                                                                    <a class="dropdown-item" href="#">Rating</a>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>                                    
                                        </div>
                                    </div>
                                    <div id="category-view" class="category-list-layout3 gradient-padding zoom-gallery">
                                        <div class="row">
                                            <?php
                                                if (isset($post_details) && $post_details !=null) {
                                                    foreach ($post_details as $key => $value) {
                                                        $id = $value['post_id'];
                                                    ?>
                                                        <div class="col-xl-4 col-lg-6 col-md-4 col-sm-6 col-12">
                                                            <div class="product-box item-mb zoom-gallery">
                                                                <div class="item-mask-wrapper">
                                                                    <div class="">
                                                                        <img src="<?= base_url(CAT_IMAGE.$value['cat_image'])?>" alt="categories" class="img-fluid">
                                                                    </div>
                                                                </div>
                                                                <div class="item-content">
                                                                    <!-- <div class="title-ctg">Clothing</div> -->
                                                                    <h3 class="short-title"><a href="<?= base_url('post-management/details/'.$id)?>"><?= $value['i_want']?></a></h3>
                                                                    <h3 class="long-title"><a href="<?= base_url('post-management/details/'.$id)?>"><?= $value['i_want']?></a></h3>

                                                                    <ul class="upload-info">
                                                                        <li class="date"><i class="fa fa-clock-o" aria-hidden="true"></i><?= date('d M, Y',strtotime($value['create_date']))?></li>
                                                                        
                                                                        <li class="place"><i class="fa fa-map-marker" aria-hidden="true"></i><?= $value['country_name'].', '. $value['state_name'].', '. $value['city_name']?></li>
                                                                        
                                                                        <li class="tag-ctg"><i class="fa fa-tag" aria-hidden="true"></i><?= $value['category_name']?></li><br><br>

                                                                        <?php 
                                                                            if (isset($value['sub_category_name']) && $value['sub_category_name'] !=null) {
                                                                            ?>
                                                                                <li class="tag-ctg"><i class="fa fa-tag" aria-hidden="true"></i><?= $value['sub_category_name']?></li>
                                                                            <?php }
                                                                        ?>
                                                                    </ul>
                                                                    <p><?= substr($value['post_description'],0,100)?></p>

                                                                    <a href="<?= base_url('post-management/delete_post/'.$id)?>" title="Remove" class="product-details-btns btn btn-primary" onclick="return confirm('Are you sure you want to delete this post?');">Remove</a> 
                                                                    
                                                                    <?php 
                                                                    if ($value['post_approve_reject'] == '1' && $value['order_status'] !='1') 
                                                                    {    
                                                                        $created_date = date('Y-m-d',strtotime($value['create_date']));
                                                                        $expire_post = date('Y-m-d',strtotime($created_date . + $value['day'] .'days'));
                                                                        
                                                                        if (date('Y-m-d') < $expire_post) {
                                                                            if($value['status'] == '1'){
                                                                                echo '<a href="javascript:void(0)" class="product-details-btns btn btn-primary change-status label-success" data-table="post_management" data-id="'.$id.'" data-current-status="1">'.'Enable advertisement'.'</a>';
                                                                                } else {
                                                                                echo '<a href="javascript:void(0)" class="product-details-btns  btn btn-primary change-status label-danger" data-table="post_management" data-id="'.$id.'" data-current-status="0">'.'Disable advertisement'.'</a>';
                                                                            } 
                                                                            ?>
                                                                                <a href="<?= base_url('post-management/edit/'.$id)?>" title="edit" class="product-details-btna btn btn-primary">Edit</a>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                            <a href="<?= base_url('post-management/close_post/'.$id)?>" title="Close" class="product-details-btn btn btn-primary" onclick="return confirm('Are you sure you want to close this post?');">Close</a>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } 
                                                }
                                                else{
                                                    ?>
                                                        <h6 style="margin-left: 39%;">No post Available...!</h6>
                                                    <?php
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                               <div class="gradient-wrapper mb--xs mb-30 border-none">
                                    <?php
                                        if (isset($pagination) && $pagination !=null) {
                                            echo $pagination;
                                        }
                                    ?>
                                    <!-- <ul class="cp-pagination">                                        
                                       <li class="disabled"><a href="#"><i class="fa fa-angle-double-left" aria-hidden="true"></i>Previous</a></li>
                                       <li class="active"><a href="#">1</a></li>
                                       <li><a href="#">2</a></li>
                                       <li><a href="#">3</a></li>
                                       <li><a href="#">4</a></li>
                                       <li><a href="#">5</a></li>
                                       <li><a href="#">6</a></li>
                                       <li><a href="#">Next<i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
                                    </ul> -->
                               </div>
                           </div>
                       </div>
                    </div>  

                    <div role="tabpanel" class="tab-pane fade" id="invoice">
                       <div class="row">
                           <div class="col-lg-12">
                               <div class="gradient-wrapper item-mb">
                                    <div class="gradient-title">
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="layout-switcher">
                                                    <ul>
                                                        <li>
                                                            <div class="page-controls-sorting">
                                                                <button class="sorting-btn dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">Invoice</button>                
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>                                    
                                        </div>
                                    </div>
                                    <div id="category-view" class="category-list-layout3 gradient-padding zoom-gallery">
                                        <div class="row">
                                            <?php
                                                if (isset($post_details) && $post_details !=null) {
                                                    foreach ($post_details as $key => $value) {
                                                        $created_date = date('Y-m-d',strtotime($value['create_date']));
                                                        $expire_post = date('Y-m-d',strtotime($created_date . + $value['day'] .'days'));

                                                        $id = $value['post_id'];
                                                    ?>
                                                        <div class="col-xl-4 col-lg-6 col-md-4 col-sm-6 col-12">
                                                            <div class="product-box item-mb zoom-gallery">
                                                                <div class="item-mask-wrapper">
                                                                    <div class="">
                                                                        <img src="<?= base_url(CAT_IMAGE.$value['cat_image'])?>" alt="categories" class="img-fluid">
                                                                    </div>
                                                                </div>
                                                                <div class="item-content">
                                                                    <!-- <div class="title-ctg">Clothing</div> -->
                                                                    <h3 class="short-title"><a href="<?= base_url('post-management/details/'.$id)?>"><?= $value['i_want']?></a></h3>
                                                                    <h3 class="long-title"><a href="<?= base_url('post-management/details/'.$id)?>"><?= $value['i_want']?></a></h3>

                                                                    <ul class="upload-info">
                                                                        <li class="date"><i class="fa fa-clock-o" aria-hidden="true"></i><?= date('d M, Y',strtotime($value['create_date']))?></li>
                                                                        
                                                                        <li class="place"><i class="fa fa-map-marker" aria-hidden="true"></i><?= $value['country_name'].', '. $value['state_name'].', '. $value['city_name']?></li>
                                                                        
                                                                        <li class="tag-ctg"><i class="fa fa-tag" aria-hidden="true"></i><?= $value['category_name']?></li><br><br>

                                                                        <?php 
                                                                            if (isset($value['sub_category_name']) && $value['sub_category_name'] !=null) {
                                                                            ?>
                                                                                <li class="tag-ctg"><i class="fa fa-tag" aria-hidden="true"></i><?= $value['sub_category_name']?></li>
                                                                            <?php }
                                                                        ?>
                                                                        <li class="tag-ctg"><i class="fa fa-tag" aria-hidden="true"></i>Expire Date : <b><?= $expire_post?></b></li><br><br>
                                                                    </ul>
                                                                    <p><?= substr($value['post_description'],0,100)?></p>
                                                                        
                                                                    <a href="<?= base_url(POST_INVOICE.$value['invoice'])?>" class="product-details-btn" download target="_blank">Download</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } 
                                                }
                                                else{
                                                    ?>
                                                        <h6 style="margin-left: 39%;">No post Available...!</h6>
                                                    <?php
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                               <div class="gradient-wrapper mb--xs mb-30 border-none">
                                    <?php
                                        if (isset($pagination) && $pagination !=null) {
                                            echo $pagination;
                                        }
                                    ?>
                               </div>
                           </div>
                       </div>
                    </div> 

                    <div role="tabpanel" class="tab-pane fade" id="wishlist">
                       <div class="row">
                           <div class="col-lg-12">
                               <div class="gradient-wrapper item-mb">
                                    <div class="gradient-title">
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="layout-switcher">
                                                    <ul>
                                                        <li>
                                                            <div class="page-controls-sorting">
                                                                <button class="sorting-btn dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">Sort By<i class="fa fa-sort" aria-hidden="true"></i></button>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item" href="#">Date</a>
                                                                    <a class="dropdown-item" href="#">Best Sale</a>
                                                                    <a class="dropdown-item" href="#">Rating</a>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>                                    
                                        </div>
                                    </div>
                                    <div id="category-view" class="category-list-layout3 gradient-padding zoom-gallery">
                                        <div class="row">
                                            <?php
                                                if (isset($wishlist_details) && $wishlist_details !=null) {
                                                    foreach ($wishlist_details as $key => $value) {
                                                        $id = $value['like_id'];
                                                    ?>
                                                        <div class="col-xl-4 col-lg-6 col-md-4 col-sm-6 col-12">
                                                            <div class="product-box item-mb zoom-gallery">
                                                                <div class="item-mask-wrapper">
                                                                    <div class="">
                                                                        <img src="<?= base_url(CAT_IMAGE.$value['cat_image'])?>" alt="categories" class="img-fluid">
                                                                    </div>
                                                                </div>
                                                                <div class="item-content">
                                                                    <!-- <div class="title-ctg">Clothing</div> -->
                                                                    <h3 class="short-title"><a href="<?= base_url('post-management/details/'.$value['product_id'])?>"><?= $value['i_want']?></a></h3>
                                                                    <h3 class="long-title"><a href="<?= base_url('post-management/details/'.$value['product_id'])?>"><?= $value['i_want']?></a></h3>

                                                                    <ul class="upload-info">
                                                                        <li class="date"><i class="fa fa-clock-o" aria-hidden="true"></i><?= date('d M, Y',strtotime($value['create_date']))?></li>
                                                                        
                                                                        <li class="place"><i class="fa fa-map-marker" aria-hidden="true"></i><?= $value['country_name'].', '. $value['state_name'].', '. $value['city_name']?></li>
                                                                        
                                                                        <li class="tag-ctg"><i class="fa fa-tag" aria-hidden="true"></i><?= $value['category_name']?></li><br><br>

                                                                        <?php 
                                                                            if (isset($value['sub_category_name']) && $value['sub_category_name'] !=null) {
                                                                            ?>
                                                                                <li class="tag-ctg"><i class="fa fa-tag" aria-hidden="true"></i><?= $value['sub_category_name']?></li>
                                                                            <?php }
                                                                        ?>
                                                                    </ul>
                                                                    <p><?= substr($value['post_description'],0,100)?></p>
                                                                    <a href="<?= base_url('wishlist/remove_wishlist/'.$id)?>" title="Remove" class="product-details-btn" onclick="return confirm('Are you sure you want to remove wishlist?');">Remove</a> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } 
                                                }
                                                else{
                                                    ?>
                                                        <h6 style="margin-left: 39%;">No Wishlist Available...!</h6>
                                                    <?php
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                               <div class="gradient-wrapper mb--xs mb-30 border-none">
                                    <?php
                                        if (isset($pagination) && $pagination !=null) {
                                            echo $pagination;
                                        }
                                    ?>
                               </div>
                           </div>
                       </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="change-password">
                        <div class="gradient-title">
                            <h3>Change Password</h3>
                        </div>
                        <form id="change_password" class="col-md-12 p-4" method="post" action="<?= base_url('profile/changepassword')?>">  
                            <div class="row">
                                <div class="col-xl-3 col-md-4 col-12">
                                    <label class="control-label">Current Password</label>
                                </div>
                                <div class="col-xl-9 col-md-8 col-12">
                                    <div class="form-group">
                                        <input type="password" id="current_password" name="current_password" class="form-control" placeholder="Type  Your Password">
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-4 col-12">
                                    <label class="control-label">New Password</label>
                                </div>
                                <div class="col-xl-9 col-md-8 col-12">
                                    <div class="form-group">
                                        <input type="password" id="new_password" name="new_password" class="form-control" placeholder="Type  Your Password">
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-4 col-12">
                                    <label class="control-label">Confirm Password</label>
                                </div>
                                <div class="col-xl-9 col-md-8 col-12">
                                    <div class="form-group">
                                        <input type="password" id="confirm_new_password" name="confirm_new_password" class="form-control" placeholder="Type  Your Password">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <button type="submit" class="cp-default-btn-sm-primary disabled border-none w-100">Update Details</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>                   
                </div>                       
            </div>                    
        </div>
    </div>
</section>
<!-- Login Area End Here -->

<?php $this->load->view('include/copyright');?>
<script type="text/javascript">
    document.getElementById('get_file').onclick = function() {
        document.getElementById('my_file').click();
    };
</script>

<script>
    /*FORM VALIDATION*/
    $("#profile-form").validate({
        rules: {
            'name': {required: true}, 
            'user_email': {required: true,email: true}, 
            'mobile_no': {required: true,minlength: 10}, 
            'id_country': {required: true}, 
            'id_state': {required: true}, 
            'id_city': {required: true}, 
            'address': {required: true}, 
            // 'password': {required: true}, 
            // 'confirm_password': {required: true,equalTo: "#n-password"}, 
        },
        messages: {
            'name': "Please enter name",      
            'user_email': {required: 'Please enter email-id',email: 'Please enter valid email'},     
            'mobile_no': {required: 'Please enter mobile no',minlength: 'minimum 10 digits'}, 
            'id_country': "Select country",     
            'id_state': "Select state",     
            'id_city': "Select city",     
            'address': "Please enter address",     
            // 'password': "Please enter password",     
            // 'confirm_password': {required: 'Enter confirm password',equalTo: "Password and confirm password must be same"},
        }
    });

    $(document).ready(function() {
        $(document).on('click','.change-status',function(){
            var current_element = jQuery(this);
            var id = jQuery(this).data('id');
            var table = jQuery(this).data('table');
            var current_status = jQuery(this).attr('data-current-status');
            // alert(current_status);

            var post_data = {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                'action': 'change_status',
                'id': id,
                'table': table,
                'current_status': current_status,
            }
            $.ajax({
                type: "POST",
                url: BASE_URL + 'post_management/change_post_status',
                data: post_data,
                async: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.success) {
                        current_element.toggleClass('label-danger label-success');
                        if(current_element.hasClass('label-success')){
                            current_element.text('Enable advertisement');
                            current_element.attr('data-current-status','1');
                        } else {
                            current_element.text('Disable advertisement');
                            current_element.attr('data-current-status','0');
                        }
                    } 
                    else {
                        window.location = window.location.href;
                    }
                }
            });
        });    
    });

    $("#change_password").validate({
        rules: {
            current_password: {required: true, minlength: 6},
            new_password: {required: true, minlength: 6},
            confirm_new_password: {required: true, minlength: 6, equalTo: "#new_password"},
        },
        messages: {
            current_password: {required: "Please enter current password", minlength: "Please enter more than 6 character"},
            new_password: {required: "Please enter new password", minlength: "Please enter more than 6 character"},
            confirm_new_password: {required: "Please enter confirm password", minlength: "Please enter more than 6 character", equalTo: "Password and confirm password should be same"},
        }
    });

    /*For checking image size*/
    $(document).on('change', '.display-file-preview', function () {
        var current = $(this);
        // alert(current);
        check_image_size(this, current);
    });

    function check_image_size(input, current) {
        if (input.files && input.files[0]) {
            var file = input.files[0];
            //current.closest('.avatar-edit').next('.avatar-preview').next('.message-avatar,label.error').remove();
            if (file && file.size < <?php echo MAX_FILE_SIZE_IMAGE; ?>) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.employer-dashboard-thumb img').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                    $('[name="upload_profile_photo"]').submit();
                }
            } else {
                $(input).val('');
                $('<label class="error">Please upload file with size less than <?php echo (MAX_FILE_SIZE_IMAGE / 1000000) ?> MB</label>').insertAfter(current.closest('div'));
            }
        }
    }

    $('[name="upload_profile_photo"]').submit(function (e) {
        var current = $(this);
        $('label.error').remove();
        $('label.error,label.text-danger,label.text-success').remove();
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: BASE_URL + 'profile/upload_profile_photo',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response);
                if (response.success) {
                    $('<label class="text-small text-success">' + response.message + '</label>').insertAfter(current.closest('div'));
                    window.location.reload();
                } else {
                    $('<label class="text-small text-danger">' + response.message + '</label>').insertAfter(current.closest('div'));
                }
            }
        });
    });
</script>
<?php $this->load->view('include/footer');?>