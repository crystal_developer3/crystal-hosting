<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Domains Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Domains Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php echo site_url() . "authority/domains/view"; ?>">Domains</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $domain_details !=null ? 'Edit' : 'Add'?> Domains</h3>

                        <!-- <form method="post" action="<?//= base_url('authority/domains/save_excel_domain')?>" enctype="multipart/form-data"><br>                            
                            <div class="col-md-4"> 
                                <input type="file" name="xls_file" id="xls_file" class="form-control" accept=".xlsx, .xls">
                            </div>
                            <div class="col-md-2"> 
                                <input type="submit" class="btn btn-sm btn-success check_excel" value="submit">
                            </div>
                            <div class="col-md-4">                            
                                <a href="<?//= base_url('authority/domains/download_excel_domain');?>" class="btn btn-sm btn-warning">Excel Download</a>
                            </div>
                        </form>
                        <div class="col-md-12" style="color: red; font-size: 20px;"><center>(OR)</center></div> -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php $this->load->view('authority/common/messages');?>
                        <?php $action = $domain_details !=null ? base_url('authority/domains/update_domain') : base_url('authority/domains/insert_domain'); ?>

                        <form id="form" method="post" action="<?= $action?>" enctype="multipart/form-data">
                            <div class="clone-section-main">   
                                <div class="clone-section-sub">   
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <input type="hidden" name="id" id="domain_id" value="<?= ($domain_details !=null) ? $domain_details[0]['id'] : ""; ?>">
                                            <label for="title">Title :<span class="required">*</span></label> 
                                            <?php
                                                $input_fields = array(
                                                    'name' => 'title[]',
                                                    'placeholder' => 'Title',
                                                    'class' => 'form-control title txtonly',
                                                    'id' => 'title',
                                                    'value' => (isset($domain_details) && $domain_details !=null ? $domain_details[0]['title'] : ""),
                                                );
                                                echo form_input($input_fields);
                                                echo form_error("title", "<div class='error'>", "</div>");
                                            ?>
                                            <span class="error_title" style="color: #fc3a3a;"></span>
                                        </div>
                                        <div class="col-md-2">
                                            <?php
                                                if ($domain_details == null) {
                                                    ?>   
                                                        <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                        <button type="button" class="btn add-more pull-left" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                        
                                                        <button type="button" class="btn remove-more btn-danger pull-right" title="Delete"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                    <?php 
                                                }
                                            ?>
                                        </div>
                                        <br><br><br><br>
                                        <div class="col-md-10">
                                            <label for="price">Price :<span class="required">*</span></label> 
                                            <?php
                                                $input_fields = array(
                                                    'name' => 'price[]',
                                                    'placeholder' => 'Price',
                                                    'class' => 'form-control price',
                                                    'id' => 'price',
                                                    'value' => (isset($domain_details) && $domain_details !=null ? $domain_details[0]['price'] : ""),
                                                );
                                                echo form_input($input_fields);
                                                echo form_error("price", "<div class='error'>", "</div>");
                                            ?>
                                            <span class="error_price" style="color: #fc3a3a;"></span>
                                        </div>
                                    </div><br><br><br><br>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input class="btn btn-success text-uppercase check" value="Submit" type="submit">
                                    <a href="<?php echo site_url() . 'authority/domain'; ?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </section>
    </div>
    <?php $this->view('authority/common/copyright'); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
    <script>
        /*FORM VALIDATION*/
         $("#price").keyup(function(){
            var price = this.value = this.value.replace(/[^0-9.]/g, '');
          $("#price").val(price);

        });
        var domain_id = $("#domain_id").val();
        $("#form").validate({
            rules: {
                'title[]': {required: true},      
                'price[]': {required: true},      
                // 'cat_image[]': { 
                //     required: function(element) {
                //         if (domain_id == '') {  
                //             return true;
                //         }
                //         else {
                //             return false;
                //         }
                //     }, 
                // },                  
            },
            messages: {
                'title[]': "Please enter domain name",
                'price[]': "Please enter price",
                // 'cat_image[]': "Please select image",       
            }
        });

        // $('.check').click(function(){
        //     var domain_id = $("#domain_id").val();

        //     var title = $(".title_english").val();
        //     var cat_image = $(".cat_image")[0].files.length;

        //     if (title_english =='' && title_arabic ==''){
        //         $('.error_title').text('Please enter english OR arabic domain-name.');
        //         $('.title_arabic').focus();
        //         return false;
        //     }  

        //     if (domain_id == ''){
        //         if(cat_image === 0){
        //             $('.error_file').text("Please select image.");
        //             return false;
        //         }
        //     } 
        // });

        $('.check_excel').click(function(){
            if(isemptyfocus('xls_file'))
            {
                return false;
            }
        });

        $(document).on('click', '.add-more', function() {
            $clone = $('.clone-section-sub:last').clone();
            $('.clone-section-main').append($clone);
            $('.clone-section-sub:last').find('input[type="text"]').val('');
        });

        $(document).on('click', '.remove-more', function () {
            if (check_clone_lang_section()) {
                $(this).closest('.clone-section-sub').remove();
            } else {
                alert('You can not remove the current section');
            }
        });

        function check_clone_lang_section() {
            if ($('.clone-section-main').find('.clone-section-sub').length > 1) {
                return true;
            } else {
                return false;
            }
        } 
    </script>
<?php $this->view('authority/common/footer'); ?>