<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<?php $this->view('authority/common/header'); ?>

<?php $this->view('authority/common/sidebar'); ?>

<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <h1>Feedback Details</h1>

        <ol class="breadcrumb">

            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>

            <li><a href="<?php echo site_url() . "authority/feedback/view"; ?>"> Feedback</a></li>

            <li class="active">Details</li>

        </ol>

    </section>

    <section class="content">

        <!-- /.row -->

        <div class="row">

            <div class="col-xs-12">

                <div class="box">

                    <!-- /.box-header -->

                    <div class="box-body table-responsive no-padding">

                        <table id="mytable" class="table table-bordred">

                            <tbody>

                                <tr>

                                    <td><b>User name:</b></td>

                                    <td><?= $name; ?></td>

                                </tr>

								<tr>

                                    <td><b>Email:</b></td>

                                    <td><?= $email; ?></td>

                                </tr>

                                <tr>

                                    <td><b>Message:</b></td>

                                    <td><?= $message; ?></td>

                                </tr>

                                <!-- <tr>

                                    <td><b>Gender:</b></td>

                                    <td><?= $gender == '0' ? 'Male' : 'Female';?></td>

                                </tr> -->

                                <!-- <tr>

                                    <td><b>Date of birth:</b></td>

                                    <td><?= date('d-m-Y',strtotime($date_of_birth));?></td>

                                </tr>

                                <tr>

                                    <td><b>Address:</b></td>

                                    <td><?= $address; ?></td>

                                </tr> -->

								<!-- <tr>

									<td><b>Profile Photo</b></td>

									<td>

										<img src="<?= base_url(PROFILE_PICTURE).$profile_picture?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="100" width="100">

									

									</td>

								</tr> -->

                            </tbody>

                        </table>

                    </div>

                    <!-- /.box-body -->

                </div>

                <!-- /.box -->

            </div>

        </div>

    </section>

</div>

<?php $this->view('authority/common/copyright'); ?>

<script>

	$('body').on('click','#video-btn',function(e){

		e.preventDefault();

		var vid = $(document).find('.video').get(0);

		if (!vid.paused) {

			vid.pause();

		} else {

			vid.play();

		}

	  $(this).toggleClass('fa-play fa-pause');

	});

</script>

<?php $this->view('authority/common/footer'); ?>