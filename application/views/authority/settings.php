<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>            
            <li class="active">Settings</li>
        </ol>
    </section>

    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">

                <?php
                $attributes = array("id" => "form", "name" => "form", "method" => "POST");
                echo form_open("", $attributes);
                ?>
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $form_title; ?></h3>

                        <div class="box-tools pull-right">
                            <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>-->
                            <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php
                        if (isset($success)) {
                            ?>
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-check"></i> Success</h4>
                                <?php echo $success; ?>
                            </div>
                            <?php
                        }
                        ?>

                        <?php
                        if (isset($site_settings['row_count']) && $site_settings['row_count'] > 0) {
                            foreach ($site_settings['data'] as $value) {
                                ?>
                                <div class="form-group">
                                    <label for="<?php echo $value['setting_key'] ?>"><?php echo $value['setting_title'] ?>: <?php echo ($value['is_required'] == 1) ? "<span class='required'>*</span>" : ""; ?></label>
                                    <input class="form-control" name="<?php echo $value['setting_key'] ?>" id="<?php echo $value['setting_key'] ?>" type="text" value="<?php echo $value['setting_value']; ?>">
                                    <?php echo form_error($value['setting_key'], "<div class='error'>", "</div>"); ?>
                                </div>
                                <?php
                            }
                        }
                        ?>
                        <div class="form-group">
                            <input class="btn btn-success text-uppercase" value="Submit" type="submit">
                            <a href="<?php echo site_url() . 'authority/dashboard'; ?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- /.box -->

    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/authority/plugins/jquery-validate/jquery.validate.js"></script>
<script>
    /*FORM VALIDATION*/
//    $("#form1").validate({
//        rules: {
//            designation: "required",
//        },
//        messages: {
//            designation: "Please enter designation",
//        }
//    });
</script>
<?php $this->view('authority/common/footer'); ?>