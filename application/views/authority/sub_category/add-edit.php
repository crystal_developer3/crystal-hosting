<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Category Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Category Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i><?= $this->lang->line('home');?> Home</a></li>
            <li><a href="javascript:void()">Sub category</a></li>
        </ol>
    </section>
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $sub_category_details !=null ? 'Edit' : 'Add'?> Sub category</h3>

                        <!-- <form method="post" action="<?= base_url('authority/sub_category/save_excel_category')?>" enctype="multipart/form-data"><br>     

                            <div class="col-md-4">                       
                                <label for="category_name">Category Name:<span class="required">*</span></label>
                                <select class="form-control" name="category_id" id="excel_category_id">
                                    <option value="">Select category</option>
                                    <?php
                                        if ($category_details !=null) {
                                            foreach ($category_details as $key => $value) {
                                                ?>
                                                    <option value="<?= $value['category_id']?>">
                                                   <?= $value['category_name_english'],$value['category_name_arabic']?></option>
                                                <?php
                                            }
                                        }
                                    ?>
                                </select>
                            </div>

                            <div class="col-md-4"> 
                                <label for="category_name">Sub Category Excel:<span class="required">*</span></label>
                                <input type="file" name="xls_file" id="xls_file" class="form-control" accept=".xlsx, .xls">
                            </div>
                            <div class="col-md-2" style="margin-top: 4%;"> 
                                <input type="submit" class="btn btn-sm btn-success check_excel" value="submit">
                            </div>
                            <div class="col-md-2" style="margin-top: 4%;">                            
                                <a href="<?= base_url('authority/sub_category/download_excel_category');?>" class="btn btn-sm btn-warning">Excel Download</a>
                            </div>
                        </form>
                        <div class="col-md-12" style="color: red; font-size: 20px;"><center>(OR)</center></div> -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php $this->load->view('authority/common/messages');?>
                        <?php 
                            $action = $sub_category_details !=null ? base_url('authority/subcategory/update_sub_category') : base_url('authority/subcategory/insert_sub_category'); 
                            $attributes = array('class' => 'email','id'=>'form');
                            echo form_open_multipart($action, $attributes);

                            echo form_hidden('sub_category_id',$sub_category_details !=null ? $sub_category_details[0]['sub_category_id'] : "");?>
                            <div class="form-group">
                                <div class="col-md-10">
                                    <label for="category_name">category_name :<span class="required">*</span></label>
                                    <?php 
                                        if ($category_details !=null) {
                                            $option[null] = 'Select'; 
                                            $data = array(
                                                'class' => 'form-control category_id',
                                                'id' => 'category_id',
                                            );
                                            foreach ($category_details as $key => $value) {
                                                $option[$value['category_id']] = $value['category_name'];
                                            }
                                        }
                                        echo form_dropdown('category_id',$option,isset($sub_category_details[0]['category_id']) ? $sub_category_details[0]['category_id'] : '',$data);
                                    ?>
                                    <span class="error_cat_name" style="color: #fc3a3a;"></span>
                                    <?php echo form_error("category_name", "<div class='error'>", "</div>"); ?>
                                </div>
                            </div><br><br><br><br>

                            <div class="form-group">
                                <div class="clone-section-main">   
                                    <div class="clone-section-sub">                                 
                                        <div class="col-md-10">
                                            <label for="category_name">Sub category name :<span class="required">*</span></label>
                                            
                                            <?php
                                                $input_fields = array(
                                                    'name' => 'sub_category_name[]',
                                                    'placeholder' => 'Sub category name',
                                                    'class' => 'form-control sub_category_name txtonly',
                                                    'id' => 'sub_category_name',
                                                    'value' => (isset($sub_category_details) && $sub_category_details !=null ? $sub_category_details[0]['sub_category_name'] : ""),
                                                );
                                                echo form_input($input_fields);
                                                echo form_error("sub_category_name", "<div class='error'>", "</div>");
                                            ?> 
                                            <span class="error_sub_cat_name" style="color: #fc3a3a;"></span>
                                        </div>

                                        <!-- <div class="col-md-10">
                                            <label for="category_name">Sub category icon :<span class="required">*</span>(Upload by 64&#215;64)</label>
                                            
                                            <?php
                                                $input_fields = array(
                                                    'name' => 'sub_category_icon[]',
                                                    'class' => 'form-control',
                                                );
                                                echo form_upload($input_fields);
                                                echo form_error("sub_category_icon", "<div class='error'>", "</div>");
                                            ?> 
                                            <span class="error_sub_cat_img" style="color: #fc3a3a;"></span>
                                        </div> -->
                                        <!-- <?php
                                            if ($sub_category_details != null) {
                                                ?>
                                                <div class="col-md-10">
                                                    <label for="last_name">Current Image</label><br>       
                                                    <div class="col-md-2">
                                                        <img src="<?= base_url(SUB_CAT_ICON).$sub_category_details[0]['sub_category_icon']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'">
                                                    </div>
                                                </div><br><br><br><br>
                                            <?php }
                                        ?> -->

                                        <div class="col-md-2">
                                            <?php
                                                if ($sub_category_details == null) {
                                                    ?>   
                                                        <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                        <button type="button" class="btn add-more pull-left" title="Add"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                        
                                                        <button type="button" class="btn remove-more btn-danger pull-right" title="Delete"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                    <?php 
                                                }
                                            ?>
                                        </div><br><br><br><br>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <input class="btn btn-success text-uppercase check" value="Submit" type="submit">
                                    <a href="<?php echo site_url().'authority/subcategory'; ?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                                </div>
                            </div>
                        <?= form_close()?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>

<script>
    /*FORM VALIDATION*/
    $("#form").validate({
        rules: {
            category_id : {required: true},
            'sub_category_name[]' : {required: true},
            'sub_category_icon[]' : {required: true},
        },
        messages: {
            category_id:  {required: 'Please select category'},
            'sub_category_name[]' : {required: 'Please enter subcategory name'},
            'sub_category_icon[]' : {required: 'Please select icon'},
        }
    });

    // $('.check').click(function(){
    //     var cat_name = $(".category_id").val();
    //     var sub_cat_name = $(".sub_category_name").val();

    //     if (cat_name ==''){
    //         $('.error_cat_name').text('Please select category-name.');
    //         $('.category_name').focus();
    //         return false;
    //     }
    //     if (sub_cat_name ==''){
    //         $('.error_sub_cat_name').text('Please enter subcategory-name.');
    //         $('.sub_category_name').focus();
    //         return false;
    //     }  
    // });

    $(document).on('click','.add-more',function(){
        $clone = $('.clone-section-sub:first').clone();
        $('.clone-section-main').append($clone);
        $('.clone-section-sub:last').find('input[type="text"]').val('');
        $('.clone-section-sub:last').find('input[type="file"]').val('');
    });

    $(document).on('click', '.remove-more', function () {
        if (check_clone_lang_section()) {
            $(this).closest('.clone-section-sub').remove();
        } else {
            alert('You can not remove the current section');
        }
    });

    function check_clone_lang_section() {
        if ($('.clone-section-main').find('.clone-section-sub').length > 1) {
            return true;
        } else {
            return false;
        }
    }
</script>
<?php $this->view('authority/common/footer'); ?>