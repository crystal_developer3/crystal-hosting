<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/css/footable.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/all.css">
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Subscribe</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home </a></li>
            <li class="active">Subscribe</li>
		</ol>
        <?php $this->view('authority/common/messages'); ?>
	</section>

	<section class="content-header">
        <div class="form-group">
            <div class="input-group">
                 <span class="input-group-addon">Search</span>
                 <input type="text" name="search_text" id="search_text" placeholder="Type to Search" class="form-control"  />
            </div>
        </div>
    </section>
	
    <section class="content">
        <!-- /.row -->		
		<div class="clearfix"></div>
        <div class="row">
        	<form method="post" action="<?= base_url('authority/subscribe/get_details')?>" enctype="multipart/form-data">
        		<div class="col-md-12 row">
        			<?php
						if (isset($subscribe_details) && $subscribe_details !=null) {
							?>
								<div class="col-md-1">
									<input type="submit" class="btn btn-md btn-danger chk_submit" value="Delete" formaction="<?= base_url('authority/subscribe/multiple_delete')?>">
								</div>								
							<?php
						}
					?>
        			<div class="col-md-2">
						<select class="form-control" name="news_id" id="news_id">
							<option value="">Select news</option>
							<?php
								if ($subscribe_details !=null) {
									foreach ($news_details as $key => $value) {
									?>
										<option value="<?= $value['id']?>"><?= $value['name']?></option>
									<?php }
								}
							?>
						</select>
					</div>
					<div class="col-md-2">
						<button type="submit" class="btn btn-warning btn-sm check">Send Email</button>
					</div>
        		</div>

	            <div class="col-md-12">	            	
	                <div class="box">
	                    <?php /*
							<div class="box-header">
							<div class="box-tools">
							<div class="input-group input-group-sm">
							<input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
							
							<div class="input-group-btn">
							<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
							</div>
							</div>
							</div>
							</div>
						* * */ ?>
	                    <!-- /.box-header -->
	                    
	                    <div class="box-body table-responsive no-padding">
	                        <table id="myTable" class="table table-bordred table-striped">
	                            <thead>
	                                <tr>
	                                	<th>
											<div class="checkbox" style="margin: -20px 0 0 30px;"><input type="checkbox" name="check_all" id="select_all"></div>
										</th>
	                                    <th>No</th>
										<th>Email</th>
										<th>Send</th>
	                                    <th>Create date</th>
	                                    <th data-hide="phone,medium"  align="center">Action</th>
									</tr>
								</thead>
	                            <tbody>
	                                <?php
										if (isset($subscribe_details) && $subscribe_details !=null):
		                                    foreach ($subscribe_details  as $key => $value) {
		                                    	$id = $value['subscribe_id'];
											?>
												<tr data-expanded="true">
													<td>
														<div class="checkbox" style="margin-left: 30px"><input type="checkbox" name="chk_multi_checkbox[]" class="chk_all" value="<?= $id?>"><div class="checkbox">
													</td>
													<td><?= $no+$key;?></td>
													<td><?= $value['subscribe_email'];?></td>
													<td>
														<input type="checkbox" name="subscribe_id[]" class="subscribe_id" value="<?= $value['subscribe_email']?>">
													</td>

													<td><?= date('d-m-Y h:i:s A',strtotime($value['create_date']));?></td>
													<td class="action" align="center">												
														<p data-placement="top" data-toggle="tooltip" title="Delete"><button type="button" class="btn btn-danger btn-xs delete-btn" data-href="<?php echo site_url(); ?>authority/subscribe/delete/<?php echo $id; ?>" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p>
													</td>
												</tr>
											<?php
										}
									?>
									<?php else: ?>
									<tr data-expanded="true">
										<td colspan="10" align="center">Records not found</td>
									</tr>
									<?php endif; ?>
								</tbody>
							</table>
						</div>
						<div class="row">
	                        <div class="col-md-12" style="padding: 0px 30px 0px 0;">
	                            <ul class="pagination pull-right">
	                                <?php
	                                    if (isset($pagination)) 
	                                    { 
	                                        echo $pagination;
	                                    }
	                                ?>
	                            </ul>
	                        </div>
	                    </div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
			</form>
		</div>
	</section>
</div>

<!-- DELETE POPUP -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
				<h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" value="" name="delete_link" id="delete_link"/>
				<div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
				
			</div>
			<div class="modal-footer ">
				<button type="button" class="btn btn-success btn-confirm-yes" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
				<button type="button" class="btn btn-default btn-confirm-no" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
			</div>
		</div>
		<!-- /.modal-content --> 
	</div>
	<!-- /.modal-dialog --> 
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/js/footable.min.js"></script>

<script type="text/javascript">
	$('.check').click(function() {
        if(isemptyfocus('news_id'))
        {
            return false;
        }

		if($('.subscribe_id:checkbox:checked').length < 1)
	  	{
	  		$.alert({
		        title: 'Error',
		        type:'red',
		        content: 'Please select at least one email address',
		    });
	  		return false;
	  	}
		else{
		  	return true;
		}
	});

	$(document).ready(function () {	

		$(document).on("click", ".delete-btn", function () {
			$("#delete_link").val($(this).data("href"));
		});
		
		$(".btn-confirm-yes").on("click", function () {
			window.location = $("#delete_link").val();
		});
	});

	// multiple delete //
    $('.chk_submit').on('click', function() {
		var boxes = $('.chk_all:checkbox');
        if(boxes.length > 0) {
            if($('.chk_all:checkbox:checked').length < 1) {
                $.alert({
			        title: 'Confirm Delete',
			        content: 'Please select at least one checkbox',
			    });
                return false;
            }
            else{
        		confirm('Are you sure you want to delete this item?');
	        	return true;
            }
        }
	});
</script>
<?php $this->view('authority/common/footer'); ?>									