<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>

<?php $this->view('authority/common/sidebar'); ?>
<!-- User Hostings Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- User Hostings Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php echo site_url() . "authority/user_hostings/view"; ?>">User Hostings</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                    <h3 class="box-title"><?= $user_hostings_details !=null ? 'Edit' : 'Add'?> User Hostings</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php $this->load->view('authority/common/messages');?>
                        <?php $action = $user_hostings_details !=null ? base_url('authority/user_hostings/update_user_hosting') : base_url('authority/user_hostings/insert_user_hosting'); ?>

                        <form id="form" method="post" action="<?= $action?>" enctype="multipart/form-data">
                            <div class="clone-section-main">   
                                <div class="clone-section-sub">   
                                    <div class="form-group">
                                        <input type="hidden" name="id" id="user_hosting_id" value="<?= ($user_hostings_details !=null) ? $user_hostings_details[0]['id'] : ""; ?>">
                                        <div class="col-md-3">
                                            <label for="provider_name">Provider Name :<span class="required">*</span></label> 
                                            <?php
                                                $input_fields = array(
                                                    'name' => 'provider_name',
                                                    'placeholder' => 'Provider Name',
                                                    'class' => 'form-control provider_name txtonly',
                                                    'id' => 'provider_name',
                                                    'value' => (isset($user_hostings_details) && $user_hostings_details !=null ? $user_hostings_details[0]['provider_name'] : ""),
                                                );
                                                echo form_input($input_fields);
                                                echo form_error("provider_name", "<div class='error'>", "</div>");
                                            ?>
                                            <span class="error_provider_name" style="color: #fc3a3a;"></span>
                                            <div class="clearfix" ></div>
                                            <hr>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="book_date">Book Date :<span class="required">*</span></label> 
                                                <?php
                                                    $input_fields = array(
                                                        'name' => 'book_date',
                                                        'placeholder' => 'Book Date',
                                                        'class' => 'form-control book_date',
                                                        'id' => 'book_date',
                                                        'value' => (isset($user_hostings_details) && $user_hostings_details !=null ? format_date_d_m_y($user_hostings_details[0]['book_date']) : ""),
                                                    );
                                                    echo form_input($input_fields);
                                                    echo form_error("book_date", "<div class='error'>", "</div>");
                                                ?>
                                                <span class="error_title" style="color: #fc3a3a;"></span>
                                                <div class="clearfix" ></div>
                                                <hr>  
                                            </div>
                                            <div class="clearfix" ></div>
                                            <div class="col-md-3">
                                                <label for="host_username">Hosting username :<span class="required">*</span></label> 
                                                <?php
                                                    $input_fields = array(
                                                        'name' => 'host_username',
                                                        'placeholder' => 'Hosting username',
                                                        'class' => 'form-control host_username',
                                                        'id' => 'host_username',
                                                        'value' => (isset($user_hostings_details) && $user_hostings_details !=null ? $user_hostings_details[0]['host_username'] : ""),
                                                    );
                                                    echo form_input($input_fields);
                                                    echo form_error("host_username", "<div class='error'>", "</div>");
                                                ?>
                                                <span class="error_host_username" style="color: #fc3a3a;"></span>
                                                <div class="clearfix" ></div>
                                                <hr>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="host_password">Hosting password :<span class="required">*</span></label> 
                                                <?php
                                                    $input_fields = array(
                                                        'name' => 'host_password',
                                                        'type' => 'password',
                                                        'placeholder' => 'Hosting password',
                                                        'class' => 'form-control host_password',
                                                        'id' => 'host_password',
                                                        'value' => (isset($user_hostings_details) && $user_hostings_details !=null ? decrypt_password($user_hostings_details[0]['host_password']) : ""),
                                                    );
                                                    echo form_input($input_fields);
                                                    echo form_error("host_password", "<div class='error'>", "</div>");
                                                ?>
                                                <span class="error_host_password" style="color: #fc3a3a;"></span>
                                                <div class="clearfix" ></div>
                                                <hr>
                                            </div>
                                            <!-- <div class="col-md-3">
                                                <label for="expiry_date">Due Date :<span class="required">*</span></label> 
                                                <?php
                                                    $input_fields = array(
                                                        'name' => 'expiry_date',
                                                        'placeholder' => 'Expiry Date',
                                                        'class' => 'form-control date',
                                                        'id' => 'expiry_date',
                                                        'value' => (isset($user_hostings_details) && $user_hostings_details !=null ? $user_hostings_details[0]['expiry_date'] : ""),
                                                    );
                                                    echo form_input($input_fields);
                                                    echo form_error("expiry_date", "<div class='error'>", "</div>");
                                                ?>
                                                <span class="expiry_date" style="color: #fc3a3a;"></span>
                                                <div class="clearfix" ></div>
                                                <hr>  
                                            </div> -->
                                            <div class="clearfix" ></div>
                                            <div class="col-md-4">
                                                <label for="user_id">Select User :<span class="required">*</span></label>
                                                <?php 
                                                    if ($user_details !=null) {
                                                        $option[null] = 'Select'; 
                                                        $data = array(
                                                            'class' => 'form-control user_id',
                                                            'id' => 'user_id',
                                                        );
                                                        foreach ($user_details['data'] as $key => $value) {
                                                            $option[$value['id']] = $value['first_name']." ".$value['last_name'];
                                                        }
                                                    }
                                                    echo form_dropdown('user_id',$option,isset($user_hostings_details[0]['user_id']) ? $user_hostings_details[0]['user_id'] : '',$data);
                                                ?>
                                                <span class="error_user_id" style="color: #fc3a3a;"></span>
                                                <?php echo form_error("user_id", "<div class='error'>", "</div>"); ?>
                                                <hr>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="currency_id">Currency :<span class="required">*</span></label>
                                                <select class="form-control" id="currency_id" name="currency_id">
                                                    <option value="">Select</option>
                                                    <?php 
                                                        foreach ($currency_data['data'] as $key => $value1) { ?>
                                                            <option value="<?=$value1['id']?>" <?=(isset($user_hostings_details[0]['currency_id']) && $value1['id']==$user_hostings_details[0]['currency_id'])?'selected':''?>><?=$value1['currency_code']?> | <?=$value1['currency_name']?>
                                                                
                                                            </option>
                                                        <?php }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="price">Price :<span class="required">*</span></label>
                                                <input type="number" id="price" name="price" placeholder='Enter Unit Price' class="form-control price" value="<?=(isset($user_hostings_details) && $user_hostings_details !=null ? $user_hostings_details[0]['price'] : "")?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input class="btn btn-success text-uppercase check" value="Submit" type="submit">
                                    <a href="<?php echo site_url() . 'authority/feature'; ?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </section>
    </div>
    <?php $this->view('authority/common/copyright'); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
    <script>
        /*FORM VALIDATION*/
        
        var user_hosting_id = $("#user_hosting_id").val();
        $("#form").validate({
            rules: {
                'provider_name': {required: true},
                'book_date': {required: true},
                // 'expiry_date': {required: true},
                'user_id':{required: true},
                'currency_id':{required: true},
                'price': {required: true},
            },
            messages: {
                'provider_name': "Please enter domain name",
                'book_date': "Please enter book date",
                'user_id': "Please select user",
                'currency_id': "Please select currency",
                // 'expiry_date': "Please enter expiry date",
                'price': "Please enter price",
            }

        });

        $('.check_excel').click(function(){
            if(isemptyfocus('xls_file'))
            {
                return false;
            }
        });

    </script>

    
<?php $this->view('authority/common/footer'); ?>
<script type="text/javascript">
    $(document).ready(function($) {
        $(".book_date").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            todayHighlight: true,
            autoclose: true,            
        });
    });
    $(document).on('focus blur', '.host_password', function(e) {
        if(user_hosting_id != ""){        
            if(this.type == "password"){
                $(this).prop({
                    type: 'text',
                });
            }else{
                $(this).prop({
                    type: 'password',
                });
            }
        }
    });
</script>   