<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Change Password</li>
        </ol>
    </section>

    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?php
                $attributes = array("id" => "form", "name" => "form", "method" => "POST");
                echo form_open("", $attributes);
                ?>
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $form_title; ?></h3>

                        <div class="box-tools pull-right">
                            <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>-->
                            <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php
                        if (isset($success)) {
                            ?>
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-check"></i> Success</h4>
                                <?php echo $success; ?>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="form-group">
                            <label for="current_password">Current Password:<span class="required">*</span></label>
                            <input class="form-control" name="current_password" id="current_password" placeholder="Enter current password" type="password" value="<?php echo (isset($current_password) ? $current_password : ""); ?>">
                            <?php echo form_error("current_password", "<div class='error'>", "</div>"); ?>
                            <?php
                                if (isset($current_password_error) && $current_password_error != "") {
                                    echo "<div class='error'>" . $current_password_error . "</div>";
                                }
                            ?>
                        </div>
                        <div class="form-group">
                            <label for="new_password">New Password:<span class="required">*</span></label>
                            <input class="form-control" name="new_password" id="new_password" placeholder="Enter new password" type="password" value="<?php echo (isset($new_password) ? $new_password : ""); ?>">
                            <?php echo form_error("new_password", "<div class='error'>", "</div>"); ?>
                        </div>
                        <div class="form-group">
                            <label for="confirm_new_password">Confirm New Password:<span class="required">*</span></label>
                            <input class="form-control" name="confirm_new_password" id="confirm_new_password" placeholder="confirm password" type="password" value="<?php echo (isset($confirm_new_password) ? $confirm_new_password : ""); ?>">
                            <?php echo form_error("confirm_new_password", "<div class='error'>", "</div>"); ?>
                        </div>
                        <div class="form-group">
                            <input class="btn btn-success text-uppercase" value="Submit" type="submit">
                            <a href="<?php echo site_url() . 'authority/dashboard'; ?>" class="btn btn-default text-uppercase pull-right">CANCEL</a>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- /.box -->

    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/authority/plugins/jquery-validate/jquery.validate.js"></script>
<script>
    /*FORM VALIDATION*/
    $("#form").validate({
        rules: {
            current_password: {required: true, minlength: 6},
            new_password: {required: true, minlength: 6},
            confirm_new_password: {required: true, minlength: 6, equalTo: "#new_password"},
        },
        messages: {
            current_password: {required: "Please enter current password", minlength: "Please enter more than 6 character"},
            new_password: {required: "Please enter new password", minlength: "Please enter more than 6 character"},
            confirm_new_password: {required: "Please enter confirm password", minlength: "Please enter more than 6 character", equalTo: "Password and confirm password should be same"},
        }
    });
</script>
<?php $this->view('authority/common/footer'); ?>