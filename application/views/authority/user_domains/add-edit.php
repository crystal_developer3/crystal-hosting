<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<style type="text/css">

    .custom_check {
      display: block;
      position: relative;
      padding-left: 35px;
      margin-bottom: 12px;
      cursor: pointer;
      font-size: 22px;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    /* Hide the browser's default checkbox */
    .custom_check input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
      height: 0;
      width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
      position: absolute;
      top: 0;
      left: 0;
      height: 30px;
      width: 30px;
      background-color: #eee;
      border: solid #2196F3 1px;
    }

    /* On mouse-over, add a grey background color */
    .custom_check:hover input ~ .checkmark {
      background-color: #ccc;
      border: solid #2196F3 1px;
    }

    /* When the checkbox is checked, add a blue background */
    .custom_check input:checked ~ .checkmark {
      background-color: #2196F3;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
      content: "";
      position: absolute;
      display: none;
    }

    /* Show the checkmark when checked */
    .custom_check input:checked ~ .checkmark:after {
      display: block;
    }

    /* Style the checkmark/indicator */
    .custom_check .checkmark:after {
        left: 33%;
        top: 7%;
        width: 0.99rem;
        height: 2rem;
        border: solid white;
        border-width: 0 4px 4px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
    
</style>
<?php $this->view('authority/common/sidebar'); ?>
<!-- User Domain Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- User Domain Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php echo site_url() . "authority/user_domains/view"; ?>">User Domain</a></li>
            <li class="active"><?= $user_domains_details !=null ? 'Edit' : 'Add'?></li>
        </ol>
    </section>
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $user_domains_details !=null ? 'Edit' : 'Add'?> User Domain</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php $this->load->view('authority/common/messages');?>
                        <?php $action = $user_domains_details !=null ? base_url('authority/user_domains/update_domain') : base_url('authority/user_domains/insert_domain'); ?>

                        <form id="form" method="post" action="<?= $action?>" enctype="multipart/form-data">
                            <div class="clone-section-main">   
                                <div class="clone-section-sub">   
                                    <div class="form-group">
                                        <input type="hidden" name="id" id="user_domain_id" value="<?= ($user_domains_details !=null) ? $user_domains_details[0]['id'] : ""; ?>">
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <label for="domain_name">Domain Name :<span class="required">*</span></label> 
                                                <?php
                                                    $input_fields = array(
                                                        'name' => 'domain_name',
                                                        'placeholder' => 'Domain Name',
                                                        'class' => 'form-control domain_name ',
                                                        'id' => 'domain_name',
                                                        'value' => (isset($user_domains_details) && $user_domains_details !=null ? $user_domains_details[0]['domain_name'] : ""),
                                                    );
                                                    echo form_input($input_fields);
                                                    echo form_error("domain_name", "<div class='error'>", "</div>");
                                                ?>
                                                <span class="error_domain_name" style="color: #fc3a3a;"></span>
                                                <div class="clearfix" ></div>
                                                <hr>
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <label for="domain_type_id">Domain Type :<span class="required">*</span></label>
                                                <select class="form-control" id="domain_type_id" name="domain_type_id">
                                                    <option value="">Select</option>
                                                    <?php 
                                                        foreach ($domain_type_data['data'] as $key => $value1) { ?>
                                                            <option value="<?=$value1['id']?>" <?=(isset($user_domains_details[0]['domain_type_id']) && $value1['id']==$user_domains_details[0]['domain_type_id'])?'selected':''?>><?=$value1['title']?>
                                                                
                                                            </option>
                                                        <?php }
                                                    ?>
                                                </select>
                                                <?=form_error("domain_type_id", "<div class='error'>", "</div>");?>
                                                <hr>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="book_date">Book Date :<span class="required">*</span></label> 
                                                <?php
                                                    $input_fields = array(
                                                        'name' => 'book_date',
                                                        'placeholder' => 'Book Date',
                                                        'class' => 'form-control book_date',
                                                        'id' => 'book_date',
                                                        'value' => (isset($user_domains_details) && $user_domains_details !=null ? format_date_d_m_y($user_domains_details[0]['book_date']) : ""),
                                                    );
                                                    echo form_input($input_fields);
                                                    echo form_error("book_date", "<div class='error'>", "</div>");
                                                ?>
                                                <span class="error_title" style="color: #fc3a3a;"></span>
                                                <hr>  
                                                
                                            </div>
                                        </div>
                                        <div class="clearfix" ></div>
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <label for="user_id">Select User :<span class="required">*</span></label>
                                                <?php 
                                                    if ($user_details !=null) {
                                                        $option[null] = 'Select'; 
                                                        $data = array(
                                                            'class' => 'form-control user_id',
                                                            'id' => 'user_id',
                                                        );
                                                        foreach ($user_details['data'] as $key => $value) {
                                                            $option[$value['id']] = $value['first_name']." ".$value['last_name'];
                                                        }
                                                    }
                                                    echo form_dropdown('user_id',$option,isset($user_domains_details[0]['user_id']) ? $user_domains_details[0]['user_id'] : '',$data);
                                                ?>
                                                <span class="error_user_id" style="color: #fc3a3a;"></span>
                                                <?php echo form_error("user_id", "<div class='error'>", "</div>"); ?>
                                                <hr>
                                            </div>
                                            
                                        </div>
                                        <div class="clearfix" ></div>
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="currency_id">Client Currency :<span class="required">*</span></label>
                                                <select class="form-control" id="currency_id" name="currency_id">
                                                    <option value="">Select</option>
                                                    <?php 
                                                        foreach ($currency_data['data'] as $key => $value1) { ?>
                                                            <option value="<?=$value1['id']?>" <?=(isset($user_domains_details[0]['currency_id']) && $value1['id']==$user_domains_details[0]['currency_id'])?'selected':''?>><?=$value1['currency_code']?> | <?=$value1['currency_name']?>
                                                                
                                                            </option>
                                                        <?php }
                                                    ?>
                                                </select>
                                                <hr>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="price">Client Price :<span class="required">*</span></label>
                                                <input type="number" id="price" name="price" placeholder='Enter Unit Price' class="form-control price" value="<?=(isset($user_domains_details) && $user_domains_details !=null ? $user_domains_details[0]['price'] : "")?>">
                                                <hr>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-3">
                                                <label for="costing_currency_id">Admin Currency:<span class="required">*</span></label>
                                                <select class="form-control" id="costing_currency_id" name="costing_currency_id">
                                                    <option value="">Select</option>
                                                    <?php 
                                                        foreach ($currency_data['data'] as $key => $value1) { ?>
                                                            <option value="<?=$value1['id']?>" <?=(isset($user_domains_details[0]['costing_currency_id']) && $value1['id']==$user_domains_details[0]['costing_currency_id'])?'selected':''?>><?=$value1['currency_code']?> | <?=$value1['currency_name']?>
                                                                
                                                            </option>
                                                        <?php }
                                                    ?>
                                                </select>
                                                <?=form_error("costing_currency_id", "<div class='error'>", "</div>");?>
                                                <hr>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="costing_price">Admin Price :<span class="required">*</span></label>
                                                <input type="number" id="costing_price" name="costing_price" placeholder='Enter Unit Price' class="form-control costing_price" value="<?=(isset($user_domains_details) && $user_domains_details !=null ? $user_domains_details[0]['costing_price'] : "")?>">
                                                <?= form_error("costing_price", "<div class='error'>", "</div>");?>
                                                <hr>
                                            </div>
                                            <div class="clearfix" ></div>
                                            <div class="col-md-4">
                                                <label for="user_id">Provider Name :<span class="required">*</span></label>
                                                <input type="text" id="provider_name" name="provider_name" placeholder='Enter provider name' class="form-control provider_name" value="<?=(isset($user_domains_details) && $user_domains_details !=null ? $user_domains_details[0]['provider_name'] : "")?>">
                                                <?=form_error("provider_name", "<div class='error'>", "</div>");?>
                                                <hr>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="username">Domain Username :<span class="required">*</span></label>
                                                <input type="text" id="username" name="username" placeholder='Enter username' class="form-control username" value="<?=(isset($user_domains_details) && $user_domains_details !=null ? $user_domains_details[0]['username'] : "")?>">
                                                <?=form_error("username", "<div class='error'>", "</div>");?>
                                                <hr>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="password">Domain Password :<span class="required">*</span></label>
                                                <div class="has-feedback">
                                                    <input type="password" id="password" name="password" placeholder='Enter password' class="form-control password" value="<?=(isset($user_domains_details) && $user_domains_details !=null ? decrypt_password($user_domains_details[0]['password']) : "")?>">
                                                    <!-- <span class="glyphicon glyphicon-eye-open form-control-feedback"></span> -->
                                                </div>
                                                
                                                <?=form_error("password", "<div class='error'>", "</div>");?>
                                                <hr>
                                            </div>
                                            
                                            <?php
                                                if(!$user_domains_details){ ?>
                                                    <div class="col-md-2">
                                                        <label>Inlcude Hosting ?</label>
                                                        <label class="custom_check">
                                                          <input type="checkbox" class="hosting_check" name="hosting" value="0">
                                                          <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                    <?php 
                                                }else{
                                                    ?>
                                                    <input type="hidden" class="hosting_check" name="hosting" value="1">
                                                    <?php
                                                }
                                            ?>
                                        </div>
                                        <div class="clearfix"></div>

                                            <div class="form-group hosting_section ">
                                                <h3 class="col-md-12">Hosting</h3>
                                                <div class="col-md-3">
                                                    <label for="user_id">Provider Name :<span class="required">*</span></label>
                                                    <input type="text" id="host_provider_name" name="host_provider_name" placeholder='Enter provider name' class="form-control host_provider_name" value="<?=(isset($user_hosting) && $user_hosting !=null ? $user_hosting['provider_name'] : "")?>">
                                                    <?=form_error("host_provider_name", "<div class='error'>", "</div>");?>
                                                    <hr>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="user_id">Hosting Username :<span class="required">*</span></label>
                                                    <input type="text" id="host_username" name="host_username" placeholder='Enter provider name' class="form-control host_username" value="<?=(isset($user_hosting) && $user_hosting !=null ? $user_hosting['host_username'] : "")?>">
                                                    <?=form_error("host_username", "<div class='error'>", "</div>");?>
                                                    <hr>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="user_id">Hosting Password :<span class="required">*</span></label>
                                                    <div class="has-feedback">
                                                        <input type="password" id="host_password" name="host_password" placeholder='Enter provider name' class="form-control host_password" value="<?=(isset($user_hosting) && $user_hosting !=null ? decrypt_password($user_hosting['host_password']) : "")?>">
                                                        <!-- <span class="glyphicon glyphicon-eye-open form-control-feedback"></span> -->
                                                    </div>
                                                    
                                                    <?=form_error("host_password", "<div class='error'>", "</div>");?>
                                                    <hr>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="hosting_plan_id">Select Hosting plan :<span class="required">*</span></label>
                                                    <?php 
                                                        if ($hosting_plan_data !=null) {
                                                            $option2[null] = 'Select'; 
                                                            $data = array(
                                                                'class' => 'form-control hosting_plan_id',
                                                                'id' => 'hosting_plan_id',
                                                            );
                                                            foreach ($hosting_plan_data as $key => $value) {
                                                                $option2[$value['id']] = $value['title']." | ".get_currency($value['currency_id']).$value['price'];
                                                            }
                                                        }
                                                        echo form_dropdown('hosting_plan_id',$option2,isset($user_hosting['hosting_plan_id']) ? $user_hosting['hosting_plan_id'] : '',$data);
                                                    ?>
                                                    <span class="error_hosting_plan_id" style="color: #fc3a3a;"></span>
                                                    <?php echo form_error("hosting_plan_id", "<div class='error'>", "</div>"); ?>
                                                    <hr>
                                                </div>
                                                
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input class="btn btn-success text-uppercase check" value="Submit" type="submit">
                                    <a href="<?php echo site_url() . 'authority/user_domains'; ?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </section>
    </div>
    <?php $this->view('authority/common/copyright'); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
    <script>
        /*FORM VALIDATION*/
        
        var user_domain_id = $("#user_domain_id").val();
        $("#form").validate({
            rules: {
                'domain_name': {required: true},
                'provider_name': {required: true},
                'book_date': {required: true},
                'domain_type_id':{required: true},
                // 'expiry_date': {required: true},
                'user_id':{required: true},
                'currency_id':{required: true},
                'costing_currency_id':{required: true},
                'price': {required: true},
                'costing_price': {required: true},
            },
            messages: {
                'domain_name': "Please enter domain name",
                'provider_name': "Please enter provider name",
                'book_date': "Please enter book date",
                'domain_type_id': "Please select domain type",
                'user_id': "Please select user",
                'currency_id': "Please select currency",
                'costing_currency_id': "Please select admin currency",
                // 'expiry_date': "Please enter expiry date",
                'price': "Please enter price",
                'costing_price': "Please enter admin price",
            }

        });

        $('.check_excel').click(function(){
            if(isemptyfocus('xls_file'))
            {
                return false;
            }
        });

    </script>
</script>
    
<?php $this->view('authority/common/footer'); ?>
<script type="text/javascript">
    var user_domain_id = $("#user_domain_id").val();
    var user_hosting = '<?=(isset($user_hosting) && $user_hosting !=null ? $user_hosting['provider_name'] : "")?>';
    $(document).ready(function($) {
        $(".book_date").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            todayHighlight: true,
            
            autoclose: true,            
        });
        if(user_domain_id == "" || user_hosting==""){
            $(".hosting_section").hide();
        }
    });
    $(document).on('click', '.hosting_check', function(e) {
        if ($(this).val() == 1) {
            $(this).val(0);
        }else{
            $(this).val(1);
        }
        $('.hosting_section').slideToggle('medium');
    });
    $(document).on('focus blur', '.host_password', function(e) {
        if(user_domain_id != ""){        
            if(this.type == "password"){
                $(this).prop({
                    type: 'text',
                });
            }else{
                $(this).prop({
                    type: 'password',
                });
            }
        }
    });
    $(document).on('focus blur', '.password', function(e) {
        if(user_domain_id != ""){        
            if(this.type == "password"){
                $(this).prop({
                    type: 'text',
                });
            }else{
                $(this).prop({
                    type: 'password',
                });
            }
        }
    });
    
    
</script>