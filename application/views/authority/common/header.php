<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo SITE_TITLE; ?></title>
        <!-- <link rel="icon" href="<?php echo base_url(); ?>assets/admin/images/favicon.ico" /> -->
        <link rel="icon" href="<?= base_url(PROFILE_PHOTO.'logo1.png'); ?>" />
        <link rel="shortcut icon" href="<?= base_url(PROFILE_PHOTO.'logo1.png'); ?>" />
        <link rel="apple-touch-icon" href="<?= base_url(PROFILE_PHOTO.'logo1.png'); ?>" />
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/dist/css/AdminLTE.min.css">
       
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="<?= base_url()?>assets/admin/fonts/flaticon.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/custom.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/developer.css">
        
        
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

        <link rel="stylesheet" href="<?= base_url()?>assets/admin/css/fullcalendar.min.css">
        <link rel="stylesheet" href="<?= base_url()?>assets/admin/css/jquery.dataTables.min.css">
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

        <link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="Stylesheet"
        type="text/css" />
        
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <style type="text/css">.form-control {height: auto !important;} .label{cursor:pointer;}</style>
        <style>
            .nicEdit-main{
                min-height: 100px !important;
            } 
            .multiselect{
                width: 313px !important;
                overflow: hidden;
                text-overflow: ellipsis;
                text-align: left !important;
            }
            .multiselect-container > li {
                padding: 0;
                min-width: 310px !important;
            }            
        </style>
        <!-- jQuery 3 -->
        <script type="text/javascript">
            var BASE_URL = '<?php echo base_url(); ?>';
        </script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    </head>
    <body class="hold-transition skin-black-light login-page">
        <div class="loading">Loading...</div>
        <div class="loader-overlay"></div>
        <div class="loader-img"><img src="<?php echo base_url(); ?>assets/admin/images/newloader.svg"/></div>
        <div class="wrapper">