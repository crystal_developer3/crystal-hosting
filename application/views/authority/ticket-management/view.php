<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/css/footable.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/all.css">
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="dashboard-content-one">
        <section class="content-header">
            <div class="heading-layout1">
                <div class="item-title">
                    <h3>Ticket List</h3>
                </div>
            </div>
        </section>
        <!-- <section class="content-header">
            <div class="form-group">
                <div class="input-group">
                     <span class="input-group-addon">Search</span>
                     <input type="text" name="search_text" id="search_text" placeholder="Type to Search" class="form-control"  />
                </div>
            </div>
        </section>     -->
        <section class="content">
            <?php $this->view('authority/common/messages');?>
            <!-- /.row -->
            

                <!-- <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Mail send</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <?php
                                if (isset($user_details) && $user_details !=null) {
                                ?>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6">  
                                                <label>Add Message</label>
                                                <textarea class="form-control" name="txt_message" id="txt_message"></textarea>
                                            </div>

                                            <div class="col-md-4">  
                                                <label>Send Image</label>
                                                <input type="file" class="form-control" name="image">
                                            </div>

                                            <div class="col-md-2" style="margin-top: 25px;">
                                                <button type="submit" class="btn btn-warning btn-sm check_mail">Send</button>
                                            </div>
                                        </div>
                                    </div>
                                <?php } 
                            ?>                   
                        </div>
                    </div>
                </div> -->
                <?php /*
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">SEARCH</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <?php
                                if (isset($user_details) && $user_details !=null) {
                                ?>
                                    <div class="box-body">
                                        <div class="filter-section">
                                           <!-- <form action="<?= base_url('authority/userlist/view')?>" method="post" enctype="multipart/form-data"> -->
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="filter_by_day">Start Date:</label>
                                                            <input class="form-control filter_date" id="start_date" name="start_date" placeholder="Start Date" type="text" value="<?php if($start_date==""){}else{ echo $start_date;} ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="filter_by_day">End Date:</label>
                                                            <input class="form-control filter_date" id="end_date" name="end_date" placeholder="End Date" type="text" value="<?php if($end_date==""){}else{ echo $end_date;} ?>">
                                                        </div>
                                                    </div>                                  
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="filter_by_day">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                            <button type="submit" class="btn btn-default form-control check btn-sm" name="submit" value="submit" formaction="<?= base_url('authority/userlist/view')?>">SEARCH</button>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label for="filter_by_day">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                            <button type="submit" class="btn btn-primary form-control btn-sm" formaction="<?= base_url('authority/userlist/excel_genrate_report')?>">Export Excel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            <!-- </form>  -->
                                        </div>
                                    </div> 
                                <?php } 
                            ?>                   
                        </div>
                    </div>
                </div>php */?>

                <div class="clearfix"></div>
                <div class="card height-auto">        
                    <div class="card-body">
                        <div class="row"> 
                            <?php
                            if (isset($type)) {
                                ?>
                                <div class="col-xl-1 col-lg-6 form-group">
                                    <a href="<?= base_url('authority/ticket-management/add') ?>" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Add</a>
                                </div>
                            <?php }
                            ?>
                            <?php
                            if (isset($ticket_details) && $ticket_details != null) {
                                ?>
                                <div class="col-xl-2 col-lg-6 form-group">
                                    <button type="button" class="btn-fill-lg bg-blue-dark btn-hover-yellow chk_submit" data-form-action="<?= base_url('authority/ticket-management/multiple-delete-ticket') ?>">Delete All</button>
                                </div>

                                <div class="<?= isset($type) ? 'col-xl-7' : 'col-xl-8'; ?> col-lg-6 col-12 form-group"></div>
                                <div class="col-xl-2 col-lg-6 col-12 form-group">
                                    <input type="text"  name="search_text" id="search_text" placeholder="Search by Name ..." class="form-control">
                                </div>
                            <?php }
                            ?>
                        </div>

                        <div class="table-responsive">
                            <table class="table display data-table text-nowrap" id="myTable">
                                <thead>
                                    <tr>
                                        <th>
                                            <div class="form-check">
                                                <input type="checkbox" name="check_all" class="form-check-input checkAll">
                                                <label class="form-check-label">&nbsp;</label>
                                            </div>
                                        </th>
                                        <th>User name</th>
                                        <th>Ticket details</th>
                                        <th>Ticket status</th>
                                        <!-- <th>Location</th> -->
                                        <th>View Problem</th>
                                        <th>Details</th>
                                        <th>Create date</th>
                                        <!--<th>Action</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (isset($ticket_details) && $ticket_details != null) {
                                        // echo"<pre>"; print_r($ticket_details);
                                        foreach ($ticket_details as $key => $value) {
                                            $name = array();
                                            $id = $value['id'];
                                            $allocate_user_dtls = $this->Production_model->get_all_with_where('ticket_allocate', 'id', 'desc', array('ticket_id' => $value['id']));
                                            $user_id = isset($allocate_user_dtls) && $allocate_user_dtls != null ? $allocate_user_dtls[0]['user_id'] : '';
                                            // echo"<pre>"; print_r($ticket_allocate_dtls);
                                            $get_name = $this->Production_model->get_all_with_where_in('ticket_problem', 'id', 'desc', 'id', explode(',', $value['ticket_problem_id']), array('status' => '1'));
                                            if (isset($get_name) && $get_name != null) {
                                                foreach ($get_name as $key1 => $name_row) {
                                                    array_push($name, $name_row['problem_name']);
                                                }
                                            }
                                            $ticket_problem_name = implode(' , ', $name);
                                            ?>
                                            <tr>
                                                <td>
                                                    <div class="form-check">
                                                        <input type="checkbox" name="chk_multi_checkbox[]" class="form-check-input chk_all" value="<?= $id ?>">
                                                        <label class="form-check-label"><?= $key+1;?></label>
                                                    </div>
                                                </td>       
                                                <td>
                                                    <?php
                                                    if ($value['type'] == 'user') {
                                                        echo $value['name'];
                                                    }
                                                    if ($value['type'] == 'admin') {
                                                        $get_admin_name = $this->Production_model->get_all_with_where('user', '', '', array());

                                                        echo isset($get_admin_name) && $get_admin_name != null ? $get_admin_name[0]['username'] : '';
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <b>Ticket no : </b><?= $value['ticket_no']; ?><br>
                                                    <?php
                                                    $ticket_allocate_dtls = $this->Production_model->get_all_with_where('ticket_allocate', 'id', 'desc', array('ticket_id' => $value['id']));
                                                    if (isset($ticket_allocate_dtls) && $ticket_allocate_dtls != null) {
                                                        $user_dtls = $this->Production_model->get_where_user('name,user_email', 'user_register', '', '', array('id' => $ticket_allocate_dtls[0]['user_id']));
                                                        if (isset($user_dtls) && $user_dtls != null) {
                                                            ?>
                                                            <b>Allocated by : </b><?= isset($user_dtls) && $user_dtls != null ? $user_dtls[0]['name'] : ''; ?>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                                <td><?= $value['status_name']; ?></td>
                                                <!-- <td><?= $value['location_name']; ?></td> -->
                                                <td>
                                                    <button type="button" class="btn-fill-xs font-normal no-radius text-light btn-gradient-yellow" onclick="view_details('<?= $id ?>', '<?= $ticket_problem_name ?>', '<?=$value['problem'] ?>');">View</button>
                                                </td>
                                                <td>
                                                    <!-- <button type="button" class="btn-fill-xs font-normal no-radius text-light gradient-pastel-green" onclick="ticket_allocate('<?= $id ?>','<?= $user_id ?>');">Allocate</button> -->
                                                    <a href="<?= base_url('authority/ticket-management/allocate/' . $id) ?>" style="color: #fff;"><button class="btn-fill-xs font-normal no-radius text-light gradient-pastel-green">View Details</button></a>
                                                </td>
                                                <td>
                                                    <b>Create Date :</b> <?= date('d-m-Y h:i A', strtotime($value['create_date'])); ?>
                                                </td>
                                                    <!--<td>     
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                            <span class="flaticon-more-button-of-three-dots"></span>
                                                        </a> 
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                             <a class="dropdown-item" href="<?= base_url('authority/ticket-management/edit/' . $id); ?>"><i class="fas fa-cogs text-dark-pastel-green"></i>Edit</a> 

                                                            <a class="dropdown-item" href="<?= base_url('authority/ticket-management/ticket-details/' . $id); ?>"><i class="fas fa-eye text-dark-pastel-green"></i>View</a>

                                                            <a class="dropdown-item delete-btn" href="<?= base_url('authority/ticket-management/delete-ticket/' . $id) ?>" data-title="Delete" data-toggle="modal" data-target="#delete"><i class="fas fa-times text-orange-red"></i>Delete</a>
                                                        </div>
                                                    </div>
                                                </td>-->
                                            </tr> 
                                            <?php
                                        }
                                    }
                                    ?>                      
                                </tbody>
                            </table> 
                        </div>
                    </div>
                </div>
            
        </section>
    </div>
</div>
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" name="delete_link" id="delete_link"/>
                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
            </div>
            <div class="modal-footer ">
                <button type="button" class="btn btn-success btn-confirm-yes" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                <button type="button" class="btn btn-default btn-confirm-no" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>
<!-- DELETE POPUP -->

                                  
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" value="" name="delete_link" id="delete_link"/>
                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-success btn-confirm-yes" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                    <button type="button" class="btn btn-default btn-confirm-no" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                </div>
            </div>
        </div>
    </div>
<!-- DELETE POPUP -->

    <!-- View ticket details -->
    <div class="modal fade" id="view_details" tabindex="-1">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ticket Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <form action="#" method="post" enctype="multipart/form-data">
                                <input class="form-control" id="ticket_id" type="hidden" />

                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label for=""><b>Problem</b></label>
                                        <!--<input type="text" id="problem" class="form-control" value="" disabled/>-->
                                        <p id="problem"></p>
                                    </div>

                                    <div class="form-group">
                                        <label for=""><b>Problem Description</b></label>
                                        <!--<textarea id="problem_desc" class="form-control" value="" disabled placeholder="Problem" style="height: 200px;"></textarea>-->
                                        <p id="problem_desc"></p>
                                    </div>

                                    <div class="form-group">
                                        <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- View ticket details -->

    <!-- Ticket allocate details -->
    <!-- <div class="modal fade" id="ticket_allocate" tabindex="-1">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Allocate Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <form action="<?= base_url('authority/ticket_management/ticket_allocate') ?>" method="post" enctype="multipart/form-data">
                                <input class="form-control" id="ticket_allocate_id" name="ticket_id" type="hidden"/>

                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label for="">User name</label>
                                        <select class="form form-control" name="user_id" id="user_id">
                                            <option value="">Select</option>
                                                <?php
                                                if (isset($user_details) && $user_details != null) {
                                                    foreach ($user_details as $key => $value) {
                                                        ?>
                                                            <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" id="save" class="btn btn-success pull-left check">Allocate</button>

                                        <button type="button" id="Cancel" class="btn btn-danger pull-right" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div> -->
    <!-- Ticket allocate details -->

    

    

<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/js/footable.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () { 
        $(document).on('click','.change-status',function(){
            var current_element = jQuery(this);
            var id = jQuery(this).data('id');
            var table = jQuery(this).data('table');
            var current_status = jQuery(this).attr('data-current-status');
            // alert(current_status);
            var post_data = {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                'action': 'change_status',
                'id': id,
                'table': table,
                'current_status': current_status,
            }
            $.ajax({
                type: "POST",
                url: BASE_URL + 'authority/ajax/change_status',
                data: post_data,
                async: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.success) {
                        current_element.toggleClass('label-danger label-success');
                        if(current_element.hasClass('label-success')){
                            current_element.text('Active');
                            current_element.attr('data-current-status','1');
                            } else {
                            current_element.text('Deactive');
                            current_element.attr('data-current-status','0');
                        }
                        } else {
                        window.location = window.location.href;
                    }
                }
            });
        });
        $(document).on("click", ".delete-btn", function () {
            $("#delete_link").val($(this).data("href"));
        });
        $(".btn-confirm-yes").on("click", function () {
            window.location = $("#delete_link").val();
        });
    });
    $('.check_mail').click(function(){
        if(isemptyfocus('txt_message'))
        {
            return false;
        }
        var email = $('.user_email:checkbox');
        if(email.length > 0) {
            if($('.user_email:checkbox:checked').length < 1) {
                $.alert({
                    type: 'red',
                    title: 'Confirm Delete',
                    content: 'Please select at least one user',
                });
                return false;
            }
        }
    });

    $('.check').click(function(){
        if(isemptyfocus('start_date') || isemptyfocus('end_date'))
        {
            return false;
        }
    });
    // multiple delete //
    $('.chk_submit').on('click', function() {
        var boxes = $('.chk_all:checkbox');
        if(boxes.length > 0) {
            if($('.chk_all:checkbox:checked').length < 1) {
                $.alert({
                    title: 'Confirm Delete',
                    content: 'Please select at least one checkbox',
                });
                return false;
            }
            else{
                confirm('Are you sure you want to delete this item?');
                return true;
            }
        }
    });
</script>

<script type="text/javascript">
        $('.check').click(function () {
            if (isemptyfocus('user_id')) {
                return false;
            }
        });

        $(document).ready(function () {
            $(document).on("click", ".delete-btn", function () {
                $("#delete_link").val($(this).attr("href"));
            });
            $(".btn-confirm-yes").on("click", function () {
                window.location = $("#delete_link").val();
            });
        });

        // multiple delete //
        $('.chk_submit').on('click', function () {
            var current = $(this);
            var boxes = $('.chk_all:checkbox');
            if (boxes.length > 0) {
                if ($('.chk_all:checkbox:checked').length < 1) {
                    $.alert({
                        title: 'Confirm Delete',
                        content: 'Please select at least one checkbox',
                    });
                    return false;
                } else {
                    $.confirm({
                        title: 'Confirm!',
                        content: 'Are you sure you want to delete?',
                        buttons: {
                            confirm: function () {
                                var record = [];
                                $('.chk_all:checkbox:checked').each(function () {
                                    record.push($(this).val());
                                });
                                $.ajax({
                                    url: '<?php echo base_url(); ?>authority/ticket-management/multiple_delete_ticket',
                                    method: 'POST',
                                    data: {'chk_multi_checkbox': record},
                                    dataType: 'json',
                                    success: function (response) {
                                        if (response.success) {
                                            window.location = window.location.href;
                                        }
                                    }
                                });
                            },
                            cancel: function () {
                            },
                        }
                    });
                    return false;
                }
            }
        });

        function view_details(id, problem, problem_desc)
        {
            // alert(type);
            var decodedetails = problem_desc;
            $('#ticket_id').val(id);
            $('#problem').html(problem);
            $('#problem_desc').html(decodedetails);
            $("#view_details").modal('show');
        }
    </script>
    <?php $this->view('authority/common/footer'); ?>                                    