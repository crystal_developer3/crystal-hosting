<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$this->view('authority/common/header');
	$this->view('authority/common/sidebar');
?>

<!-- Sidebar Area End Here -->
<div class="content-wrapper">
    <div class="dashboard-content-one">
        <!-- Breadcubs Area Start Here -->
        <div class="breadcrumbs-area">
            <ul>
                <li>
                    <a href="<?= base_url('authority/dashboard')?>">Home</a>
                </li>
                <li>Problem</li>
            </ul>
        </div>
        <!-- Breadcubs Area End Here -->
        
        <!-- Student Table Area Start Here -->
        <section class="content">
            <div class="card height-auto">
                <?php $this->view('authority/common/messages');?>

                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title">
                            <h3>Ticket Problem </h3>
                        </div>
                    </div>
                    <form class="new-added-form" action="" method="post" enctype="multipart/form-data">
                        <div class="row">                    
                            <div class="col-xl-1 col-lg-6 col-12 form-group mg-t-8">
                                <a href="<?= base_url('authority/ticket-management/add-problem')?>" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Add</a>
                            </div>
                            <?php
                                if (isset($problem_details) && $problem_details !=null) {    
                                ?>
                                    <div class="col-xl-3 col-lg-6 col-12 form-group mg-t-8">
                                        <button type="button" class="btn-fill-lg bg-blue-dark btn-hover-yellow chk_submit" data-form-action="<?= base_url('authority/ticket-management/multiple_delete_problem')?>">Delete All</button>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-6 form-group"></div>
                                    <div class="col-xl-2 col-lg-6 col-12 form-group">
                                        <input type="text"  name="search_text" id="search_text" placeholder="Search by Name ..." class="form-control">
                                    </div>
                                <?php } 
                            ?>
                        </div>
                        <table class="table display data-table text-nowrap" id="myTable">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="form-check">
                                            <input type="checkbox" name="check_all" class="form-check-input checkAll">
                                            <label class="form-check-label">No</label>
                                        </div>
                                    </th>
                                    <th>Name</th>
                                    <th>Create date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if (isset($problem_details) && $problem_details !=null)
                                    {
                                        foreach ($problem_details  as $key => $value) {
                                            $id = $value['id'];
                                        ?>
                                            <tr>
                                                <td>
                                                    <div class="form-check">
                                                        <input type="checkbox" name="chk_multi_checkbox[]" class="form-check-input chk_all" value="<?= $id?>">
                                                        <label class="form-check-label"><?= $key+1;?></label>
                                                    </div>
                                                </td>       
                                                <td><?= $value['problem_name'];?></td>
                                                <td>
                                                    <b>Created date :</b> <?= date('d-m-Y h:i A',strtotime($value['create_date']));?><br>
                                                    <?php
                                                        if ($value['modified_date'] != '0000-00-00') {
                                                        ?>
                                                            <b>Modified date :</b> <?= date('d-m-Y h:i A',strtotime($value['modified_date']));?>
                                                        <?php }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        if($value['status'] == '1'){            
                                                            echo "<a class='change-status' href='javascript:void(0)' data-table='ticket_problem' data-id=".$id." data-current-status='1'><i class='fa fa-check text-dark-pastel-green'></i>Active</a>";
                                                        }
                                                        else {
                                                            echo "<a class='change-status' href='javascript:void(0)' data-table='ticket_problem' data-id=".$id." data-current-status='0'><i class='fa fa-ban text-dark-pastel-red'></i>Deactive</a>";
                                                        } 
                                                    ?>
                                                </td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                            <span class="flaticon-more-button-of-three-dots"></span>
                                                        </a> 
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="<?= base_url('authority/ticket-management/edit-problem/'.$id);?>"><i class="fa fa-cogs text-dark-pastel-green"></i>Edit</a>

                                                            <a class="dropdown-item delete-btn" href="<?= base_url('authority/ticket-management/delete-problem/'.$id)?>" data-title="Delete" data-toggle="modal" data-target="#delete"><i class="fa fa-times text-orange-red"></i>Delete</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr> 
                                        <?php } 
                                    }
                                ?>                      
                            </tbody>
                        </table> 
                    </form>
                </div>
            </div>
        </section>

        <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" value="" name="delete_link" id="delete_link"/>
                        <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-success btn-confirm-yes" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                        <button type="button" class="btn btn-default btn-confirm-no" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                    </div>
                </div>
                <!-- /.modal-content --> 
            </div>
            <!-- /.modal-dialog --> 
        </div>
        <!-- DELETE POPUP -->
    </div>
</div>
<?php $this->view('authority/common/copyright'); ?>

<script type="text/javascript">
	$(document).ready(function () {	
		$(document).on('click','.change-status',function(){
			var current_element = jQuery(this);
			var id = jQuery(this).data('id');
			var table = jQuery(this).data('table');
			var current_status = jQuery(this).attr('data-current-status');
			// alert(current_status);
			var post_data = {
				'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
				'action': 'change_status',
				'id': id,
				'table': table,
				'current_status': current_status,
			}
			$.ajax({
				type: "POST",
				url: BASE_URL + 'authority/ajax/change_common_status',
				data: post_data,
				async: false,
				success: function (response) {
					var response = JSON.parse(response);
					if (response.success) {
						current_element.find('i').toggleClass('fa-check fa-ban');
						if(current_element.find('i').hasClass('fa-check')){
                            current_element.html('');
                            current_element.html("<i class='fa fa-check text-dark-pastel-green'></i>Active");
							current_element.attr('data-current-status','1');
						} else {
							current_element.text('Deactive');
                            current_element.html('');
                            current_element.html("<i class='fa fa-ban text-dark-pastel-red'></i>Deactive");
							current_element.attr('data-current-status','0');
						}
					} 
                    else {
					   window.location = window.location.href;
					}
				}
			});
		});
		$(document).on("click", ".delete-btn", function () {
			$("#delete_link").val($(this).attr("href"));
		});
		$(".btn-confirm-yes").on("click", function () {
			window.location = $("#delete_link").val();
		});
	});
	
	$('.check').click(function(){
        if(isemptyfocus('start_date') || isemptyfocus('end_date'))
        {
            return false;
        }
    });

    // multiple delete //
    $('.chk_submit').on('click', function() {
    	var current = $(this);
		var boxes = $('.chk_all:checkbox');
        if(boxes.length > 0) {
            if($('.chk_all:checkbox:checked').length < 1) {
                $.alert({
			        title: 'Confirm Delete',
			        content: 'Please select at least one checkbox',
			    });
                return false;
            }
            else{
        		$.confirm({
				    title: 'Confirm!',
				    content: 'Are you sure you want to delete?',
				    buttons: {
				        confirm: function () {
				           	current.closest('form').attr('action',current.attr('data-form-action'));
	        				current.closest('form').submit();
				        },
				        cancel: function () {				            
				        },
				    }
				});
                return false;
            }
        }
	});
</script>
<?php $this->view('authority/common/footer'); ?>									