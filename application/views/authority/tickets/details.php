<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/view_ticket.css">
<style type="text/css">
    a:focus, a:hover{
        color: #000;
        text-decoration: none;
    }
    [data-toggle="collapse"].collapsed.card-header:after {
      transform: rotate(0deg) ;
    }
    .collapsed{
        text-align: left!important
    }
    .ticket_repies thead{
        background-color: lightgray;
    }
    .pop_up_message{
        background-color: lightgray;
    }    
</style>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Ticket</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Ticket</li>
        </ol>
    </section>
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-md-3 pull-md-left sidebar">
                <div menuitemname="Client Details" class="panel panel-default">
                  <div class="panel-heading">
                      <h3 class="panel-title"> <i class="fa fa-ticket" style="transform: rotate(45deg);"></i>&nbsp; Ticket Information
                      </h3>
                  </div>
                  <div class="list-group">
                      <div menuitemname="Subject" class="list-group-item ticket-details-children" id="Primary_Sidebar-Ticket_Information-Subject">
                        <div class="truncate" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#<?=$ticket_no?> - <?=$subject?>">#<?=$ticket_no?> - <?=$subject?>
                        </div> 
                        <?=(!empty($tickets_details))?'<span class="label" style="background-color:#000000;">Answered</span>':''?>                        
                      </div>
                      <div menuitemname="Department" class="list-group-item ticket-details-children" id="Primary_Sidebar-Ticket_Information-Department">
                          <span class="title1">Department</span><br><?=getDepartment($department)?>
                        </div>
                      <div menuitemname="Date Opened" class="list-group-item ticket-details-children" id="Primary_Sidebar-Ticket_Information-Date_Opened">
                        <span class="title1">Submitted</span><br><?=format_date_Mdy_time($create_date)?>
                      </div>
                      <div menuitemname="Last Updated" class="list-group-item ticket-details-children" id="Primary_Sidebar-Ticket_Information-Last_Updated">
                            <span class="title1">Last Updated</span><br>
                            <?=(empty($tickets_details))?format_date_Mdy_time($create_date):format_date_Mdy_time($tickets_details[0]['create_date'])?>
                      </div>
                      <div menuitemname="Priority" class="list-group-item ticket-details-children" id="Primary_Sidebar-Ticket_Information-Priority">
                          <?php
                            
                              switch ($priority) {
                                case '1':
                                  $priority = "Low";
                                  break;
                                case '2':
                                  $priority = "Medium";
                                  break;
                                case '3':
                                  $priority = "High";
                                  break;
                              }
                          ?>
                          <span class="title1">Priority</span><br><?=$priority?>
                      </div>
                      <div menuitemname="Status" class="list-group-item ticket-details-children" id="Primary_Sidebar-Ticket_Information-Status">
                          <?php
                              switch ($status) {
                                case '0':
                                  $status = "Pending";
                                  break;
                                case '1':
                                  $status = "Processing";
                                  break;
                                case '2':
                                  $status = "Closed";
                                  break;
                              }
                          ?>
                          <span class="title1">Status</span><br><?=$status?>
                      </div>
                  </div>
                  <div class="panel-footer clearfix">
                    <div class="col-xs-12 col-button-left">
                        <a class="btn btn-success btn-block" style="color: #ffff;" data-toggle="modal" data-target="#reply" >
                            <i class="glyphicon glyphicon-share"></i> Reply
                        </a>
                    </div>
                    <!-- <div class="col-xs-6 col-button-right">
                      <a class="btn btn-danger btn-sm btn-block collapsed" disabled="disabled" href="#"><i class="fa fa-times"></i> Closed</a>
                    </div> -->
                  </div>
                </div>
            </div>
            <div class="col-md-9 ">
              <div class="row">
                    <div class="col-md-12">
                        <?php if (!empty($tickets_details)) { 
                                  foreach ($tickets_details as $key => $value) { ?>
                                    <div class="ticket-reply markdown-content">
                                      <div class="date">
                                          <?=format_date_Mdy_time($value['create_date'])?>
                                      </div>
                                      <div class="user">
                                          <i class="fa fa-user"></i>
                                          <span class="name">
                                            <?=($value['reply_type']==0)?getUserName($value['reply_from']):'Crystal Hosting'?>
                                          </span>
                                          <span class="type"><?=($value['reply_type']==0)?'client':'admin'?></span>
                                      </div>
                                      <div class="message">
                                        <p><?=$value['ticket_reply']?></p><hr>
                                        <?php
                                          if ($value['ticket_document'] != '') {
                                              $path = base_url(TICKET_REPLY_FILE).$value['ticket_document'];
                                              echo '<a href="'.$path.'" target="_blank" class="btn btn-success" download><i class="fa fa-file"></i> Attachment File </a>';
                                          }
                                          ?>
                                      </div>
                                    </div>
                                  <?php 
                                }
                            }
                          ?>
                          <div class="ticket-reply markdown-content">
                            <div class="date">
                                <?=format_date_Mdy_time($create_date)?>
                            </div>
                            <div class="user">
                                <i class="fa fa-user"></i>
                                <span class="name" >
                                    <?=$name?>
                                </span>
                                <span class="type">Client</span>
                            </div>
                            <div class="message">
                              <p><?=$message?></p><hr>
                            </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="reply" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading">Reply on Ticket  <span class="label label-danger" id="ticket_number">#<?=$ticket_no?></span></h4>
            </div>
            <div class="modal-body">
                <form class="row" id="ticket_replies" method="post">
                    <input type="hidden" name="reply_type" value="1">
                    <input type="hidden" name="reply_from"  value="1">
                    <input type="hidden" name="reply_to" id="reply_to" value="<?=$user_id?>">
                    <input type="hidden" name="ticket_id" id="ticket_id" value="<?=$id?>">
                  <div class="form-group col-md-12 col-lg-12 col-sm-12">
                    <label for="ticket_reply">Messege :</label>
                    <textarea class="form-control" id="ticket_reply" name="ticket_reply" rows="3"></textarea>
                  </div>
                  <div class="form-group col-md-12 col-lg-12 col-sm-12">
                      <label for="ticket_document">Ticket Document :</label>
                      <input name="ticket_document" id="ticket_document" type="file" class="input_all">
                      <p class="err_p" id="ticket_document_err"></p>
                  </div>
                  <div class="form-group col-md-12">
                    <input type="button" class="btn btn-primary check" value="Submit">
                  </div>
                </form>

            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>
<?php $this->view('authority/common/copyright'); ?>

<?php $this->view('authority/common/footer'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript">

    // $('#ticket_replies').validate({
    //     rules: {
    //         ticket_reply: {
    //             required: true,
    //         },
    //     },
    //     messages: {
    //         ticket_reply: {
    //           required: "Please enter your message",
    //         },
    //     },
    // });

    var form = $("#ticket_replies");
    form.validate();
    $(document).on('click','.check',function(){
      var formData = new FormData($('#ticket_replies')[0]);
      if(form.valid()){
        add_ticket_reply();
      }
    });
    function add_ticket_reply(){    
      var message = CKEDITOR.instances.ticket_reply.getData();      
      $("#ticket_reply").val(message);
      var formData = new FormData($('#ticket_replies')[0]);
      var uurl = BASE_URL+"api/plans/add_ticket_reply";
      var ticketURL = BASE_URL+"authority/tickets/details/<?=$id?>";

       $.ajax({
           url: uurl,
           type: 'POST',
           data: formData,
           dataType:'json',
           //async: false,
           beforeSend: function(){
             $('.mask').show();
             $('#loader').show();
           },
           success: function(response){
             if (response.result=="Success") {
                $.alert({
                  title: 'Message',
                  type: 'green',
                  content: response.message,
                });
                setTimeout(function() { window.location.href = ticketURL; }, 2000);
             }else{
               $.alert({
                    title: 'Message',
                    type: 'red',
                    content: response.message,
                });
             }
           },
           error: function(xhr) {
           //alert(xhr.responseText);
           },
           complete: function(){
             $('.mask').hide();
             $('#loader').hide();
           },
           cache: false,
           contentType: false,
           processData: false
       });
    }
</script> 
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script>
  CKEDITOR.replace( 'ticket_reply' );
</script>