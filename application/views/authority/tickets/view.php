<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/css/footable.bootstrap.min.css">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/all.css">
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Tickets</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Tickets</li>
        </ol>
    </section>
    <section class="content-header">
        <div class="form-group">
            <div class="input-group">
                 <span class="input-group-addon">Search</span>
                 <input type="text" name="search_text" id="search_text" placeholder="Type to Search" class="form-control"  />
            </div>
        </div>
    </section>
    <section class="content">
        <?php $this->load->view('authority/common/messages');?>
        <!-- /.row -->
        <div class="row">
            <form method="post">
                <div class="col-xs-12">
                    <?php
                        if ($data !=null) {
                            ?>
                                <input type="submit" class="btn btn-md btn-danger chk_submit" value="Delete" formaction="<?= base_url('authority/tickets/multiple_delete')?>">
                            <?php
                        }
                    ?>
                    <!-- <a href="<?= base_url('authority/tickets/add')?>" class="btn btn-md btn-primary">Add</a>  -->

                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table id="myTable" class="table table-bordred table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            <div class="checkbox" style="margin: -20px 0 0 30px;"><input type="checkbox" name="check_all" id="select_all"></div>
                                        </th>
                                        <th>No</th>
                                        <th>Ticket No.</th>
                                        <th>Issue</th>
                                        <th>User </th>
                                        <th>Add Modified Date</th>
                                        <th data-hide="phone,medium">Status<br/><small>(Click to change status)</small></th>
                                        <th data-hide="phone,medium" align="center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($row_count > 0):
                                        foreach ($data as $key=> $value) {
                                            $id = $value['id'];
                                            ?>
                                            <tr data-expanded="true">
                                                <td>
                                                    <div class="checkbox" style="margin-left: 30px"><input type="checkbox" name="chk_multi_checkbox[]" class="chk_all" value="<?= $id?>"><div class="checkbox">
                                                </td>
                                                <td><?= $no+$key;?></td>
                                                <td>
                                                    <a href="<?php echo site_url(); ?>authority/tickets/details/<?php echo $value['id']; ?>" class="btn btn-success">
                                                        #<?=$value['ticket_no'];?>
                                                    </a>
                                                </td>
                                                <td><?= $value['message'];?></td>
                                                <td><?= $value['first_name'];?></td>
                                                <td>
                                                    <b>Added Date :</b> <?= date('d-m-Y h:i:s A',strtotime($value['create_date']));?><br>
                                                    <b>Modified Date :</b> <?= ($value['modified_date'] != '0000-00-00 00:00:00') ? date('d-m-Y h:i:s A',strtotime($value['modified_date'])) : '';?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        
                                                    ?>
                                                    <?php 
                                                        if($value['status'] == '2'){
                                                            echo '<span class="label label-success complete-status" data-table="tickets" data-id="'.$value['id'].'" data-current-status="1">Completed</span>';
                                                        }else{
                                                            if($value['status'] == '1'){
                                                                echo '<span class="label label-warning change-status" data-table="tickets" data-id="'.$value['id'].'" data-current-status="1">'.'Processing'.'</span> ';
                                                            }else{
                                                                echo '<span class="label label-danger change-status" data-table="tickets" data-id="'.$value['id'].'" data-current-status="0">'.'Pending'.'</span> ';
                                                            }
                                                            echo ' <span class="label label-warning complete-status" data-table="tickets" data-id="'.$value['id'].'" data-current-status="2">Complete</span>';
                                                        }
                                                    ?>
                                                    
                                                </td>

                                                <td align="center" class="action">
                                                    <p data-placement="top" data-toggle="tooltip" title="View"><a href="<?php echo site_url(); ?>authority/tickets/details/<?php echo $value['id']; ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-eye-open"></span></a></p>
                                                   
                                                    <p data-placement="top" data-toggle="tooltip" title="Delete"><button type="button" class="btn btn-danger btn-xs delete-btn" data-href="<?php echo site_url(); ?>authority/tickets/delete_ticket/<?php echo $value['id']; ?>" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p>

                                                    <p data-placement="top" data-toggle="tooltip" title="Reply"><button type="button" class="btn btn-success btn-xs reply-btn" data-href="<?php echo site_url(); ?>authority/tickets/details/<?php echo $value['id']; ?>" data-title="Reply" data-toggle="modal" data-target="#reply" data-reply_to="<?php echo $value['user_id']; ?>" data-ticket_id="<?php echo $value['id']; ?>" data-ticket_number="<?= $value['ticket_no'];?>" ><span class="glyphicon glyphicon-share"></span></button></p>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    <?php else: ?>
                                        <tr data-expanded="true">
                                            <td colspan="5" align="center">Records not found</td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <?php 
                            if (isset($pagination) && $pagination != "") { ?>
                                <div class="box-footer clearfix">
                                    <?php echo $pagination; ?>
                                </div>
                                <?php
                            }
                        ?>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </form>
        </div>
    </section>
</div>

<!-- DELETE POPUP -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" name="delete_link" id="delete_link"/>
                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>

            </div>
            <div class="modal-footer ">
                <button type="button" class="btn btn-success btn-confirm-yes" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                <button type="button" class="btn btn-default btn-confirm-no" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>
<div class="modal fade" id="reply" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading">Reply on Ticket  <span class="label label-danger" id="ticket_number"></span></h4>
            </div>
            <div class="modal-body">
                <form class="row" id="ticket_replies" method="post">
                    <input type="hidden" value="" name="reply_link" id="reply_link"/>
                    <input type="hidden" name="reply_type" value="1">
                    <input type="hidden" name="reply_from"  value="1">
                    <input type="hidden" name="reply_to" id="reply_to" value="">
                    <input type="hidden" name="ticket_id" id="ticket_id" value="">
                  <div class="form-group col-md-12 col-lg-12 col-sm-12">
                    <label for="ticket_reply">Messege :</label>
                    <textarea class="form-control" id="ticket_reply" name="ticket_reply" rows="3"></textarea>
                  </div>
                  <div class="form-group col-md-12 col-lg-12 col-sm-12">
                      <label for="ticket_document">Ticket Document :</label>
                      <input name="ticket_document" id="ticket_document" type="file" class="input_all">
                      <p class="err_p" id="ticket_document_err"></p>
                  </div>
                  <div class="form-group col-md-12">
                    <input type="button" class="btn btn-primary check" value="Submit">
                  </div>
                </form>

            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/js/footable.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", ".delete-btn", function () {
            $("#delete_link").val($(this).data("href"));
        });
        $(document).on("click", ".reply-btn", function () {
            $("#reply_link").val($(this).data("href"));

            $("#ticket_number").text("#"+$(this).data("ticket_number"));
            $("#reply_to").val($(this).data("reply_to"));
            $("#ticket_id").val($(this).data("ticket_id"));
        });

        $(".btn-confirm-yes").on("click", function () {
            window.location = $("#delete_link").val();
        });

        $(document).on('click','.change-status',function(){
            var current_element = jQuery(this);
            var id = jQuery(this).data('id');
            var table = jQuery(this).data('table');
            var current_status = jQuery(this).attr('data-current-status');
            // alert(current_status);

            var post_data = {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                'action': 'change_ticket_status',
                'id': id,
                'table': table,
                'current_status': current_status,
            }
            $.ajax({
                type: "POST",
                url: BASE_URL + 'authority/ajax/change_ticket_status',
                data: post_data,
                async: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.success) {
                        current_element.toggleClass('label-danger label-warning');
                        if(current_element.hasClass('label-warning')){
                            current_element.text('Processing');
                            current_element.attr('data-current-status','1');
                        } else {
                            current_element.text('Pending');
                            current_element.attr('data-current-status','0');
                        }
                        } else {
                        window.location = window.location.href;
                    }
                }
            });
        });
        $(document).on('click','.complete-status',function(){
            var current_element = jQuery(this);
            var id = jQuery(this).data('id');
            var table = jQuery(this).data('table');
            var current_status = jQuery(this).attr('data-current-status');
            // alert(current_status);

            var post_data = {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                'action': 'change_ticket_status',
                'id': id,
                'table': table,
                'current_status': current_status,
            }
            $.ajax({
                type: "POST",
                url: BASE_URL + 'authority/ajax/change_ticket_status',
                data: post_data,
                async: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.success) {
                        current_element.toggleClass('label-warning label-success');
                        if(current_element.hasClass('label-success')){
                            current_element.text('Completed');
                            current_element.attr('data-current-status','2');
                        }else {
                            current_element.text('Complete');
                        }
                    } else {
                        window.location = window.location.href;
                    }
                }
            });
        });
    });

    // multiple delete //
    $('.chk_submit').on('click', function() {
        var boxes = $('.chk_all:checkbox');
        if(boxes.length > 0) {
            if($('.chk_all:checkbox:checked').length < 1) {
                $.alert({
                    title: 'Confirm Delete',
                    content: 'Please select at least one checkbox',
                });
                return false;
            }
            else{
                confirm('Are you sure you want to delete this item?');
                return true;
            }
        }
    });


    var form = $("#ticket_replies");
    form.validate();
    $(document).on('click','.check',function(){
      var formData = new FormData($('#ticket_replies')[0]);
      if(form.valid()){
        add_ticket_reply();
      }
    });
    function add_ticket_reply(){    
      var message = CKEDITOR.instances.ticket_reply.getData();      
      $("#ticket_reply").val(message);
      var formData = new FormData($('#ticket_replies')[0]);
      var uurl = BASE_URL+"api/plans/add_ticket_reply";
      var ticketURL = $("#reply_link").val();

       $.ajax({
           url: uurl,
           type: 'POST',
           data: formData,
           dataType:'json',
           //async: false,
           beforeSend: function(){
             $('.mask').show();
             $('#loader').show();
           },
           success: function(response){
             if (response.result=="Success") {
                $.alert({
                  title: 'Message',
                  type: 'green',
                  content: response.message,
                });

                setTimeout(function() { window.location.href = ticketURL; }, 2000);
             }else{
               $.alert({
                    title: 'Message',
                    type: 'red',
                    content: response.message,
                });
             }
           },
           error: function(xhr) {
           //alert(xhr.responseText);
           },
           complete: function(){
             $('.mask').hide();
             $('#loader').hide();
           },
           cache: false,
           contentType: false,
           processData: false
       });
    }
</script>

<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'ticket_reply' );
</script>
<?php $this->view('authority/common/footer'); ?>