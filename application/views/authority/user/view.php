<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/css/footable.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/all.css">
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>User</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User</li>
        </ol>
        <?php
        if (isset($success)) {
            ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                <?php echo $success; ?>
            </div>
            <?php
        }
        ?>
    </section>

    <section class="content">
        <a href="<?= base_url('authority/user/add-edit-sub')?>" class="btn btn-md btn-primary">Add</a> 
        <?php $this->view('authority/common/messages');?>
        <!-- /.row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <?php /*
                      <div class="box-header">
                      <div class="box-tools">
                      <div class="input-group input-group-sm">
                      <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                      <div class="input-group-btn">
                      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                      </div>
                      </div>
                      </div>
                      </div>
                     * * */ ?>
                    <!-- /.box-header -->

                    <div class="box-body table-responsive no-padding">
                        <table id="mytable" class="table table-bordred table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <?php /* <th><input type="checkbox" id="checkall" class="minimal"/></th> */ ?>
                                    <th>Full name</th>
                                    <th data-hide="phone,medium">Email</th>
                                    <th data-hide="phone,medium">Mobile Number</th>
                                    <th data-hide="phone,medium"  align="center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($row_count > 0):
                                    // echo "<pre>";print_r($data);exit;
                                    foreach ($data as $value) {
                                        $id = $value['id'];
                                        ?>
                                        <tr data-expanded="true">
                                            <td></td>
                                            <?php /* <td><input type="checkbox" class="checkthis minimal" value="<?php echo $value['id']; ?>"/></td> */ ?>
                                            <td><?php echo $value['full_name']; ?></td>
                                            <td><?php echo $value['email_address']; ?></td>
                                            <td><?php echo $value['mobile_number_1']; ?></td>
                                            <td class="action" align="center">
                                                <p data-placement="top" data-toggle="tooltip" title="Details"><a href="<?php echo site_url(); ?>authority/user/details/<?php echo $value['id']; ?>" class="btn btn-success btn-xs"><span class="fa fa-eye"></span></a></p>
                                                <p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo site_url(); ?>authority/user/add-edit-sub/<?php echo $value['id']; ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a></p>
                                                <p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs delete-btn" data-href="<?php echo site_url(); ?>authority/user/delete/<?php echo $value['id']; ?>" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p>

                                                <p data-placement="top" data-toggle="tooltip" title="Reset Password"><button type="button" class="btn btn-warning btn-xs reset-btn" data-href="<?=base_url('authority/user/reset_password/'.$value['id']); ?>" data-title="Reset Password" ><i class="fa fa-key" aria-hidden="true"></i></span></button></p>
                                                <p class="resetpassword">
                                                    
                                                </p>
                                            </td>
                                            <td>
                                                <?php 
                                                    if($value['status'] == '1'){
                                                        echo '<span class="label label-success change-status" data-table="user" data-id="'.$id.'" data-current-status="1">Active</span>';
                                                        } else {
                                                        echo '<span class="label label-danger change-status" data-table="user" data-id="'.$id.'" data-current-status="0">Deactive</span>';
                                                    } 
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                <?php else: ?>
                                    <tr data-expanded="true">
                                        <td colspan="5" align="center">Records not found</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <?php if (isset($pagination) && $pagination != "") { ?>
                        <div class="box-footer clearfix">
                            <?php echo $pagination; ?>
                        </div>
                        <?php
                    }
                    ?>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>
<div id="resetpasswordform">
    <form  method="post" id="reset_password_link">
        <input type="text" name="password" id="password" value="">
        <button type="submit" class="btn btn-success btn-xs checkp" data-toggle="tooltip" title="Lock Password"><span class="glyphicon glyphicon-lock"></span> </button>
        <button type="button" class="btn btn-danger btn-xs close-password" data-toggle="tooltip" title="Close"><span class="glyphicon glyphicon-remove"></span> </button>
    </form>
</div>
<!-- DELETE POPUP -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" name="delete_link" id="delete_link"/>
                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>

            </div>
            <div class="modal-footer ">
                <button type="button" class="btn btn-success btn-confirm-yes" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                <button type="button" class="btn btn-default btn-confirm-no" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/js/footable.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click','.change-status',function(){
            var current_element = jQuery(this);
            var id = jQuery(this).data('id');
            var table = jQuery(this).data('table');
            var current_status = jQuery(this).attr('data-current-status');
            // alert(current_status);
            var post_data = {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                'action': 'change_status',
                'id': id,
                'table': table,
                'current_status': current_status,
            }
            $.ajax({
                type: "POST",
                url: BASE_URL + 'authority/ajax/change_status',
                data: post_data,
                async: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.success) {
                        current_element.toggleClass('label-danger label-success');
                        if(current_element.hasClass('label-success')){
                            current_element.text('Active');
                            current_element.attr('data-current-status','1');
                            } else {
                            current_element.text('Deactive');
                            current_element.attr('data-current-status','0');
                        }
                        } else {
                        window.location = window.location.href;
                    }
                }
            });
        });
        $(document).find("#mytable").footable({
            delay: 0,
            breakpoints: {medium: 740, large: 911},
        }).on('beffootable_row_expanded', function () {
            $('tr.footable-detail-show').trigger('footable_toggle_row');
        });
        var triggeredByChild = false;
        $(document).on('ifChecked', "#checkall", function () {
            $(document).find("#mytable input[type=checkbox]").iCheck('check');
        });
        $(document).on('ifUnchecked', "#checkall", function () {
            if (!triggeredByChild) {
                $(document).find("#mytable input[type=checkbox]").iCheck('uncheck');
            }
            triggeredByChild = false;
        });
        // Removed the checked state from "All" if any checkbox is unchecked
        $(document).on('ifUnchecked', "#mytable td input[type=checkbox]", function () {
            triggeredByChild = true;
            $('#checkall').iCheck('uncheck');
        });
        $(document).on('ifChecked', "#mytable td input[type=checkbox]", function () {
            if ($('#mytable td input[type=checkbox]').filter(':checked').length == $('#mytable td input[type=checkbox]').length) {
                $('#checkall').iCheck('check');
            }
        });

        $("[data-toggle=tooltip]").tooltip();

        $(document).find('input[type="checkbox"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });

        $(document).on("click", ".delete-btn", function () {
            $("#delete_link").val($(this).data("href"));
        });

        $(".btn-confirm-yes").on("click", function () {
            window.location = $("#delete_link").val();
        });
    });
</script>

<script type="text/javascript">

    $(document).on("click", ".reset-btn", function () {
        // $(this).parent("p").('#resetpasswordform').remove();
        $(this).parent("p").html($("#resetpasswordform").html());
        $("#resetpasswordform").css('position','absolute');
        $("#reset_password_link").attr('action',$(this).data("href"));
        $('#password').focus();

    });
    $(document).on("submit", "#reset_password_link", function () {
        var password = $("#password").val();          
        if (password ==''){
            $('#password').css('border','1px solid red');
            $('#password').focus();
            return false;
        }
        
    });
    
    $(document).on("click", ".close-password", function () {
        window.location.reload();
    });
 </script>
<?php $this->view('authority/common/footer'); ?>