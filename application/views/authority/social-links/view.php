<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/css/footable.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/all.css">

<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Social-links</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Social-links</li>
		</ol>
        <?php $this->view('authority/common/messages'); ?>
	</section>
	
    <section class="content">
        <!-- /.row -->
		<div class="clearfix"></div>
        <div class="row">
        	<form method="post">
	            <div class="col-xs-12">
	            	<?php
	            		if ($data == null) {
	            		?>
	                		<a href="<?= base_url('authority/social-links/add')?>" class="btn btn-md btn-primary">Add</a>
	            		<?php }
	            	?>
	                <div class="box">	                    
	                    <div class="box-body table-responsive no-padding">
	                        <table id="mytable" class="table table-bordred table-striped">
	                            <thead>
	                                <tr>
	                                    <th>No</th>
										<th>Social-links</th>
	                                    <th>Add modified date</th>
	                                    <th data-hide="phone,medium" align="center">Action</th>
									</tr>
								</thead>
	                            <tbody>
	                                <?php
										if ($data !=null):
		                                    foreach ($data  as $key => $value) {
		                                    	$id = $value['id'];
											?>
												<tr data-expanded="true">
													<td><?= $no+$key;?></td>

													<td>
														<b>Facebook-link</b> : <?= $value['facebook_link'];?><br>
														<b>Instagram-link</b> : <?= $value['instagram_link'];?><br>
														<b>Twiter-link</b> : <?= $value['twiter_link'];?><br>
														<b>Skype-link</b> : <?= $value['skype_link'];?><br>
														<!-- <b>Linkedin</b> : <?= $value['link_in_link'];?><br> -->
														<b>Pinterest-link</b> : <?= $value['pinterest_link'];?><br>
														<b>Youtube-link</b> : <?= $value['youtube_link'];?><br>

													</td>

													<td>
														<b>Added Date : </b> <?= date('d-m-Y h:i:s A',strtotime($value['create_date']));?><br>
														<b>Modified Date : </b> <?= date('d-m-Y h:i:s A',strtotime($value['modified_date']));?>
													</td>

													<td class="action" align="center">
														<p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo site_url(); ?>authority/social-links/edit/<?php echo $id; ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a></p>
													</td>
												</tr>
											<?php
										}
									?>
									<?php else: ?>
									<tr data-expanded="true">
										<td colspan="10" align="center">Records not found</td>
									</tr>
									<?php endif; ?>
								</tbody>
							</table>
						</div>
						<div class="row">
	                        <div class="col-md-12" style="padding: 0px 30px 0px 0;">
	                            <ul class="pagination pull-right">
	                                <?php
	                                    if (isset($links)) 
	                                    { 
	                                        echo $links;
	                                    }
	                                ?>
	                            </ul>
	                        </div>
	                    </div>

						<?php /* if (isset($links) && $links != "") { ?>
							<div class="box-footer clearfix">
								<?php echo $links; ?>
							</div>
							<?php
							} */
						?>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
			</form>
		</div>
	</section>
</div>

<!-- DELETE POPUP -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
				<h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" value="" name="delete_link" id="delete_link"/>
				<div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
				
			</div>
			<div class="modal-footer ">
				<button type="button" class="btn btn-success btn-confirm-yes" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
				<button type="button" class="btn btn-default btn-confirm-no" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
			</div>
		</div>
		<!-- /.modal-content --> 
	</div>
	<!-- /.modal-dialog --> 
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/js/footable.min.js"></script>

<script type="text/javascript">
	$(document).ready(function () {	

		$(document).on("click", ".delete-btn", function () {
			$("#delete_link").val($(this).data("href"));
		});
		
		$(".btn-confirm-yes").on("click", function () {
			window.location = $("#delete_link").val();
		});
	});
</script>
<?php $this->view('authority/common/footer'); ?>									