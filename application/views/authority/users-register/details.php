<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Users Details</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url() . "authority/users/view"; ?>"> Users</a></li>
            <li class="active">Details</li>
        </ol>
    </section>
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table id="mytable" class="table table-bordred">
                            <tbody>
                                <?php
                                    if (isset($first_name) && $first_name != "") { ?>
                                    <tr>
                                        <td><b>Name :</b></td>
                                        <td><?= $first_name." ".$last_name; ?></td>
                                    </tr>
                                <?php } ?>
                                <?php
                                    if (isset($email) && $email != "") { ?>
                                    <tr>
                                        <td><b>Email :</b></td>
                                        <td><?= $email; ?></td>
                                    </tr>                                
                                <?php }?>
                                <?php
                                    if (isset($company_name) && $company_name != "") { ?>
                                    <tr>
                                        <td><b>Company Name :</b></td>
                                        <td><?= $company_name; ?></td>
                                    </tr>                                
                                <?php }?>
                                <?php
                                    if (isset($dob) && $dob != "") { ?>
                                    <tr>
                                        <td><b>Date Of Birth :</b></td>
                                        <td><?= format_date_dmy($dob); ?></td>
                                    </tr>
                                <?php }?>
                                <?php
                                    if (isset($phone_number) && $phone_number != "") { ?>
                                    <tr>
                                        <td><b>Phone no :</b></td>
                                        <td><?= $phone_number; ?></td>
                                    </tr>
                                <?php }?>
                                <?php
                                    if (isset($address) && $address != "") { ?>
                                    <tr>
                                        <td><b>Address :</b></td>
                                        <td><?= nl2br($address); ?></td>
                                    </tr>
                                <?php }?>
                                <?php
                                    if (isset($zip_code) && $zip_code != "") { ?>
                                    <tr>
                                        <td><b>Zip Code :</b></td>
                                        <td><?= $zip_code; ?></td>
                                    </tr>
                                <?php }?>
                                
                                <tr>
                                    <td><b>Country :</b></td>
                                    <td>
                                        <?=get_country_by_id($id_country);?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>State :</b></td>
                                    <td>
                                        <?=get_state_by_id($id_state);?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>City :</b></td>
                                    <td>
                                        <?=get_city_by_id($id_city);?>
                                    </td>
                                </tr>
                                <!-- <tr>
                                    <td><b>Profile Photo</b></td>
                                    <td>
                                        <img src="<?= base_url(PROFILE_PICTURE).$profile_picture?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="100" width="100">
                                    </td>
                                </tr> -->
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<script>
    $('body').on('click','#video-btn',function(e){
        e.preventDefault();
        var vid = $(document).find('.video').get(0);
        if (!vid.paused) {
            vid.pause();
        } else {
            vid.play();
        }
      $(this).toggleClass('fa-play fa-pause');
    });
</script>
<?php $this->view('authority/common/footer'); ?>