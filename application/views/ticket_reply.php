<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
      <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/view_ticket.css">
    </head>
    <body>
      <div class="main aos-all" id="transcroller-body">
        <?php $this->load->view('include/header');?>
        <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
        <!-- about start -->
        <div class="allpage_banner_about allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'about.jpg')?>);">
            <h1 class="title_h1">View Ticket</h1>
            <p><a href="<?=base_url()?>">Home </a> / View Ticket</p>
        </div>
        <!-- domain_style start -->
        <div class="padding_all domain_style">
          <div class="container">
            <h5></h5>
            <div class="row">
              <div class="col-md-3 pull-md-left sidebar">
                <div menuitemname="Client Details" class="panel panel-default">
                  <div class="panel-heading">
                      <h3 class="panel-title"> <i class="fa fa-ticket" style="transform: rotate(45deg);"></i>&nbsp; Ticket Information
                      </h3>
                  </div>
                  <div class="list-group">
                      <div menuitemname="Subject" class="list-group-item ticket-details-children" id="Primary_Sidebar-Ticket_Information-Subject">
                        <div class="truncate" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#<?=$ticket_info['ticket_no']?> - <?=$ticket_info['subject']?>">#<?=$ticket_info['ticket_no']?> - <?=$ticket_info['subject']?>
                        </div> 
                        <?=(!empty($tickets_details))?'<span class="label" style="background-color:#000000;">Answered</span>':''?>
                        
                      </div>
                      <div menuitemname="Department" class="list-group-item ticket-details-children" id="Primary_Sidebar-Ticket_Information-Department">
                          <span class="title1">Department</span><br><?=getDepartment($ticket_info['department'])?>
                        </div>
                      <div menuitemname="Date Opened" class="list-group-item ticket-details-children" id="Primary_Sidebar-Ticket_Information-Date_Opened">
                        <span class="title1">Submitted</span><br><?=format_date_Mdy_time($ticket_info['create_date'])?>
                      </div>
                      <div menuitemname="Last Updated" class="list-group-item ticket-details-children" id="Primary_Sidebar-Ticket_Information-Last_Updated">
                            <span class="title1">Last Updated</span><br>
                            <?=(empty($tickets_details))?format_date_Mdy_time($ticket_info['create_date']):format_date_Mdy_time($tickets_details[0]['create_date'])?>
                      </div>
                      <div menuitemname="Priority" class="list-group-item ticket-details-children" id="Primary_Sidebar-Ticket_Information-Priority">
                          <?php
                            $priority = "";
                              switch ($ticket_info['priority']) {
                                case '1':
                                  $priority = "Low";
                                  break;
                                case '2':
                                  $priority = "Medium";
                                  break;
                                case '3':
                                  $priority = "High";
                                  break;
                              }
                          ?>
                          <span class="title1">Priority</span><br><?=$priority?>
                      </div>
                      <div menuitemname="Status" class="list-group-item ticket-details-children" id="Primary_Sidebar-Ticket_Information-Status">
                          <?php
                              switch ($ticket_info['status']) {
                                case '0':
                                  $status = "Pending";
                                  break;
                                case '1':
                                  $status = "Processing";
                                  break;
                                case '2':
                                  $status = "Closed";
                                  break;
                              }
                          ?>
                          <span class="title1">Status</span><br><?=$status?>
                      </div>
                      <a menuitemname="Open Ticket" href="<?=base_url('ticket/my-tickets')?>" class="list-group-item" id="Secondary_Sidebar-Support-Open_Ticket">
                          <i class="fa fa-ticket fa-fw" style="transform: rotate(45deg);"></i>&nbsp; My Tickets
                        </a>
                  </div>
                  <div class="panel-footer clearfix">
                    <div class="col-xs-12 col-button-left">
                        <a class="btn btn-success btn-sm btn-block" data-toggle="collapse" href="#collapseTwo"  aria-controls="collapseTwo">
                            <i class="fa fa-pencil"></i> Reply
                        </a>
                    </div>
                    <!-- <div class="col-xs-6 col-button-right">
                      <a class="btn btn-danger btn-sm btn-block collapsed" disabled="disabled" href="#"><i class="fa fa-times"></i> Closed</a>
                    </div> -->
                  </div>
                </div>
              </div>
              <!-- <div class="col-md-9 " data-aos="flip-left">
                <h3 class="well">View Ticket</h3>
              </div> -->
              <div class="col-md-9 ">             
                <div>
                    <div id="accordion" role="tablist">
                        <div class="card ">
                          <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <div class="card-header " role="tab" id="headingTwo">
                              <h5 style="margin-left: 1em;">
                                <i class="fa fa-pencil"></i> Reply
                              </h5>
                            </div>
                          </a>
                          <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                              <div class="card-body">
                                <form class="row" id="ticket_replies" method="post">
                                    <input type="hidden" name="reply_type" value="0">
                                    <input type="hidden" name="reply_from" id="reply_from" >
                                    <input type="hidden" name="reply_to" value="1">
                                    <input type="hidden" name="ticket_id" value="<?=$ticket_info['id']?>">
                                    <div class="col-md-6 ">
                                      <label>Name:</label>
                                      <input name="name" id="name" type="text" class="form-control" placeholder="Name" value="" disabled>
                                    </div>
                                      
                                    <div class="col-md-6 ">
                                        <label>Email:</label>
                                        <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="" disabled>
                                    </div>
                                  <div class="form-group col-md-12 col-lg-12 col-sm-12">
                                    <label for="ticket_reply">Messege :</label>
                                    <textarea class="form-control" id="ticket_reply" name="ticket_reply" rows="3"></textarea>
                                  </div>

                                  <div class="form-group col-md-12 col-lg-12 col-sm-12">
                                      <label for="ticket_document">Ticket Document :</label>
                                      <input name="ticket_document" id="ticket_document" type="file" class="input_all">
                                      <p class="err_p" id="ticket_document_err"></p>
                                  </div>
                                  <div class="form-group col-md-12">
                                    <input type="button" class="btn btn-primary check" value="Submit">
                                  </div>
                                </form>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <!-- <div class="table-responsive">
                            <table class="table table-striped ticket_repies">
                                <thead>
                                    <tr>
                                        <th>Reply Date & Time</th>
                                        <th>Replied By</th>
                                        <th>Reply</th>
                                        <th>Document</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    // echo "<pre>";print_r($ticket_info);exit;
                                    if (!empty($tickets_details)) {
                                        foreach ($tickets_details as $key => $value) {
                                            if($value['reply_type'] == '1'){
                                                $reply_type = 'Admin';
                                            }else{
                                                $reply_type = $first_name;
                                            }
                                            if($value['reply_from'] == '1'){
                                                $reply_from = 'Admin';
                                            }else{
                                                $reply_from = $first_name;
                                            }
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo isset($value['reply_date']) ? date('d-m-Y', strtotime($value['reply_date'])) . ' ' : ''; ?>
                                                    <?php echo isset($value['reply_time']) ? date('H:i A', strtotime($value['reply_time'])) : ''; ?>
                                                </td>
                                                <td>
                                                    <?=$reply_type?>
                                                </td>
                                                <td><a href="javascript:void(0);" class="view-reply">View</a></td>
                                                <td>
                                                    <?php
                                                    if ($value['ticket_document'] == '') {
                                                        echo 'Not available';
                                                    } else {
                                                        $path = base_url(TICKET_REPLY_FILE).$value['ticket_document'];
                                                        echo '<a href="'.$path.'" target="_blank" download>Download & View</a>';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr style="display:none;" class="pop_up_message">
                                                <td colspan="6">
                                                    <p><b>Reply</b></p>
                                                    <div class="problem">                                        
                                                        <?php echo isset($value['ticket_reply']) ? closetags($value['ticket_reply']) : ''; ?>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        echo '<tr><td colspan="5" align="center">Records not available</td></tr>';
                                    }
                                    ?>                    
                                </tbody>
                            </table>
                        </div> -->
                        <?php if (!empty($tickets_details)) { 
                                  foreach ($tickets_details as $key => $value) { ?>
                                    <div class="ticket-reply markdown-content">
                                      <div class="date">
                                          <?=format_date_Mdy_time($value['create_date'])?>
                                      </div>
                                      <div class="user">
                                          <i class="fa fa-user"></i>
                                          <span class="name">
                                            <?=($value['reply_type']==0)?getUserName($value['reply_from']):'Crystal Hosting'?>
                                          </span>
                                          <span class="type"><?=($value['reply_type']==0)?'client':'admin'?></span>
                                      </div>
                                      <div class="message">
                                        <p><?=$value['ticket_reply']?></p><hr>
                                        <?php
                                          if ($value['ticket_document'] != '') {
                                              $path = base_url(TICKET_REPLY_FILE).$value['ticket_document'];
                                              echo '<a href="'.$path.'" target="_blank" class="btn btn-success" download><i class="fa fa-file"></i> Attachment File </a>';
                                          }
                                          ?>
                                      </div>
                                    </div>
                                  <?php 
                                }
                            }
                          ?>
                          <div class="ticket-reply markdown-content">
                            <div class="date">
                                <?=format_date_Mdy_time($ticket_info['create_date'])?>
                            </div>
                            <div class="user">
                                <i class="fa fa-user"></i>
                                <span class="name" id="ticket_user" >
                                    
                                </span>
                                <span class="type">Client</span>
                            </div>
                            <div class="message">
                              <p><?=$ticket_info['message']?></p><hr>
                            </div>
                          </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- domain_style over -->
        <!-- help_line start -->
        <div class="padding_all help_line" style="background-image: url(<?=base_url(IMAGES.'mail.jpg')?>);">
          <div class="container text-center">
            <h1 class="h1_title">Need Help?</h1>
              <h4>Let us help you make the right decision!</h4>
            <div class="row margin_top">
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border">
                  <a href="javascript:;"><i class="fa fa-phone"></i></a>
                  <h3>Call Us</h3>
                  <p>Give us a call & ask all of your questions</p>
                </div>
              </div>
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border ">
                  <a href="javascript:;"><i class="fa fa-pencil"></i></a>
                  <h3>Email Us</h3>
                  <p>Send us an email with all of your questions</p>
                </div>
              </div>
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border margin_row">
                  <a href="javascript:;"><i class="fa fa-video-camera"></i></a>
                  <h3>Live Chat</h3>
                  <p>Chat with a member of our support team now</p>
                </div>
              </div>
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border margin_row">
                  <a href="javascript:;"><i class="fa fa-certificate"></i></a>
                  <h3>Real Reviews</h3>
                  <p>Read what real customers have to say</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php $this->load->view('include/footer');?>  
    </div>
    <?php $this->load->view('include/footer_js');?>
    <script type="text/javascript">
      $(document).ready(function () {
        get_user_profile('<?=$this->session->login_id?>');
      });
      function get_user_profile(user_id){
          var uurl = BASE_URL+"api/user/getProfile";
          // var post_data = {id:user_id};
           $.ajax({
               url: uurl,
               method: 'POST',
               dataType:'json',
               data: {id:user_id},
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){
                
                if (response.result=="Success") {
                  var userdata = response.data;
                  console.log(userdata);
                  $('#first_name').text(userdata.first_name);
                  $('#reply_from').val(userdata.id);

                  $('#last_name').text(userdata.last_name);
                  $('#email').text(userdata.email);
                  $('#address').text(userdata.address);
                  $('#country').text(userdata.country_id_info);
                  $('#state').text(userdata.state_id_info);
                  $('#city').text(userdata.city_id_info);
                  $('#name').val(userdata.first_name+" "+userdata.last_name);
                  $('#email').val(userdata.email);
                  $('#ticket_user').text(userdata.first_name+" "+userdata.last_name);
                }
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
           });
          
        }
    </script> 
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
    <script type="text/javascript">

        $('#ticket_replies').validate({
            rules: {
                ticket_reply: {
                    required: true,
                },
            },
            messages: {
                ticket_reply: {
                  required: "Please enter your message",
                },
            },
        });

        var form = $("#ticket_replies");
        form.validate();
        $(document).on('click','.check',function(){
          var formData = new FormData($('#ticket_replies')[0]);
          if(form.valid()){
            add_ticket_reply();
          }
        });
        function add_ticket_reply(){          
          var message = CKEDITOR.instances.ticket_reply.getData();      
          $("#ticket_reply").val(message);
          var formData = new FormData($('#ticket_replies')[0]);
          var uurl = BASE_URL+"api/plans/add_ticket_reply";
          var ticketURL = BASE_URL+"ticket/ticket-reply/<?=$ticket_info['id']?>";

           $.ajax({
               url: uurl,
               type: 'POST',
               data: formData,
               dataType:'json',
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){
                 if (response.result=="Success") {
                    $.alert({
                      title: 'Message',
                      type: 'green',
                      content: response.message,
                    });
                    setTimeout(function() { window.location.href = ticketURL; }, 2000);
                 }else{
                   $.alert({
                        title: 'Message',
                        type: 'red',
                        content: 'Reply not added. !!',
                    });
                 }
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
               cache: false,
               contentType: false,
               processData: false
           });
        }
        $('.view-reply').click(function () {
            $(this).closest('tr').next('tr').fadeToggle();
        });
    </script> 
    <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script>
            CKEDITOR.replace( 'ticket_reply' );
    </script>
   </body>
</html> 