<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
      <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/view_ticket.css">
    </head>
    <body>
      <div class="main aos-all" id="transcroller-body">
        <?php $this->load->view('include/header');?>
        <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
        <!-- about start -->
        <div class="allpage_banner_about allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'about.jpg')?>);">
            <h1 class="title_h1">Ticket</h1>
            <p><a href="<?=base_url()?>">Home </a> / Ticket</p>
        </div>
        <!-- domain_style start -->
        <div class="padding_all domain_style">
          <div class="container">
            <h5></h5>
            <div class="row">
              <!-- <div class="col-md-3 pull-md-left sidebar">
                <div menuitemname="Client Details" class="panel panel-default">
                  <div class="panel-heading">
                      <h3 class="panel-title"> <i class="fa fa-user"></i>&nbsp; Your Info
                      </h3>
                  </div>
                  <div class="panel-body">
                    <p><strong><span id="first_name"></span> <span id="last_name"></span></strong></p><p></p>
                    <p>
                      <span id="address"></span>
                      <span id="country"></span>,<span id="state"></span>,<span id="city"></span>
                    </p>
                  </div>
                  <div class="panel-footer clearfix">
                    <a href="<?=base_url('profile')?>" class="btn btn-success btn-sm btn-block">
                      <i class="fa fa-pencil"></i> Update
                    </a>
                  </div>
                </div>
              </div> -->
              <!-- <?php //$this->load->view('include/user_sidebar');?> -->
              <div class="col-md-3 pull-md-left sidebar">
                <div menuitemname="Client Details" class="panel panel-default">
                  
                  <div class="panel-heading">
                      <h3 class="panel-title"> <i class="fa fa-comments"></i>&nbsp;   Your Recent Tickets
                      </h3>
                  </div>
                  <div class="list-group">
                    <?php
                        if (!empty($tickets_details)) {
                            foreach ($tickets_details as $key1 => $value1) {
                              ?>
                                

                                <a menuitemname="Ticket #<?=$value1['ticket_no']?>" href="<?=base_url('ticket/ticket-reply/'.$value1['id'])?>" class="list-group-item" id="Secondary_Sidebar-Recent_Tickets-Ticket_#751886">
                                  <div class="recent-ticket">
                                    <div class="truncate" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#<?=$value1['ticket_no']?> - <?=$value1['subject']?>">#<?=$value1['ticket_no']?> - <?=$value1['subject']?></div>
                                    <small>
                                      <span class="pull-right"><?=format_date_dmy($value1['create_date'])?></span>
                                      <span class="label" style="background-color:#000000;">Answered</span>
                                    </small>
                                  </div>
                                </a>
                              <?php
                            }
                        }
                    ?>
                  </div>
                  <div class="panel-footer clearfix">
                    
                  </div>
                  
                  <div menuitemname="Client Shortcuts" class="panel panel-default">
                    <div class="list-group">
                      <a menuitemname="Register New Domain" href="<?=base_url('domains')?>" class="list-group-item" id="Secondary_Sidebar-Client_Shortcuts-Register_New_Domain">
                        <i class="fa fa-globe fa-fw"></i>&nbsp; Register a New Domain
                      </a>
                      <a menuitemname="Logout" href="<?=base_url('login/logout')?>" class="list-group-item" id="     Secondary_Sidebar-Client_Shortcuts-Logout">
                        <i class="fa fa-arrow-left fa-fw"></i>&nbsp;  Logout
                      </a>
                      <a menuitemname="Open Ticket" href="<?=base_url('ticket/my-tickets')?>" class="list-group-item" id="Secondary_Sidebar-Support-Open_Ticket">
                          <i class="fa fa-ticket fa-fw" style="transform: rotate(45deg);"></i>&nbsp; My Tickets
                        </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-9 " >
                <h3 class="well">Welcome.. <?=$this->session->username.' !';?></h3>
              </div>

              <!-- <div class="col-md-9" >
                <div id="accordion" role="tablist">
                    <div class="card">
                      <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <div class="card-header " role="tab" id="headingTwo">
                          <h3 class="well">View My Tickets</h3>
                        </div>
                      </a>
                      <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                          <div class="card-body">
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="table-responsive">
                                      <table class="table table-striped ticket_repies">
                                          <thead>
                                              <tr>
                                                 <th>Ticket No.</th>
                                                  <th>Subject</th>
                                                  <th>User </th>
                                                  <th>Generated Date</th>
                                                  <th>Document</th>
                                                  <th>Replay</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              <?php
                                              if (!empty($tickets_details)) {
                                                  foreach ($tickets_details as $key => $value) {
                                                      
                                                      ?>
                                                      <tr>
                                                          <td>
                                                              <?=$value['ticket_no']?>
                                                          </td>
                                                          <td>
                                                              <?=$value['subject']?>
                                                          </td>
                                                          <td><a href="javascript:void(0);" class="view-reply">View</a></td>
                                                          <td>
                                                              <?=format_date_dmy($value['create_date'])?>
                                                          </td>
                                                          <td>
                                                              <?php
                                                              if ($value['user_file'] == '') {
                                                                  echo 'Not available';
                                                              } else {
                                                                  $path = base_url(TICKET_FILE).$value['user_file'];
                                                                  echo '<a href="'.$path.'" target="_blank" download>Download & View</a>';
                                                              }
                                                              ?>
                                                          </td>
                                                          <td>
                                                            <a href="<?=base_url('ticket/ticket-reply/'.$value['id'])?>" class="label label-primary">Reply</a>
                                                          </td>
                                                      </tr>
                                                      <tr style="display:none;" class="pop_up_message">
                                                          <td colspan="6">
                                                              <p><b>Issue</b></p>
                                                              <div class="problem">                                        
                                                                  <?php echo isset($value['message']) ? closetags($value['message']) : ''; ?>
                                                              </div>
                                                          </td>
                                                      </tr>
                                                      <?php
                                                  }
                                              } else {
                                                  echo '<tr><td colspan="5" align="center">Records not available</td></tr>';
                                              }
                                              ?>                    
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                          </div>
                          </div>
                      </div>
                    </div>
                </div>
              </div> -->
              <div class="col-md-9 ">
                <div class="well well-sm col-md-12" style="padding:4em" >
                  <div class="col-md-6 col-sm-3 text-left " style="min-height:100px" >
                  <a href="<?=base_url('ticket/generate-ticket/billing')?>"><span class="fa fa-wpforms fa-2x"></span> <span class="lead">Billing</span></a>
                  <p>Handling Billing related issues</p>  
                  </div>

                  <div class="col-md-6 col-sm-3 text-left" style="min-height:100px">
                  <a href="<?=base_url('ticket/generate-ticket/technical-support')?>"><span class="fa fa-ticket fa-2x"></span> <span class="lead"> Technical Support</span> </a>
                  <p>Handling Technical related issues</p>  
                  </div>
                  
                  <div class="col-md-6 col-sm-3 text-left" style="min-height:100px">
                  <a href="<?=base_url('ticket/generate-ticket/site-down')?>"><span class="fa fa-wheelchair-alt fa-2x"></span> <span class="lead"> Site Down</span></a>
                  <p>For Server</p>   
                  </div>

                  <div class="col-md-6 col-sm-3 text-left" style="min-height:100px">
                  <a href="<?=base_url('ticket/generate-ticket/domain-support')?>"><span class="fa fa-gitlab fa-2x"></span> <span class="lead"> Domain Support</span></a>
                  <p>Handling Domain related issues</p> 
                  </div>
                  
                  <div class="col-md-6 col-sm-3 text-left" style="min-height:100px">
                  <a href="<?=base_url('ticket/generate-ticket/migration')?>"><span class="fa fa-book fa-2x"></span> <span class="lead"> Migration</span></a>
                  <p>Need assostance in migrating your site to out server</p>
                  </div>
      
                  <div class="col-md-6 col-sm-3 text-left" style="min-height:100px">
                  <a href="<?=base_url('ticket/generate-ticket/email-support')?>"><span class="fa fa-envelope fa-2x"></span> <span class="lead"> Email Support</span></a>
                  <p>Handling Email related issues</p>  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- domain_style over -->
        <!-- help_line start -->
        <div class="padding_all help_line" style="background-image: url(<?=base_url(IMAGES.'mail.jpg')?>);">
          <div class="container text-center">
            <h1 class="h1_title">Need Help?</h1>
              <h4>Let us help you make the right decision!</h4>
            <div class="row margin_top">
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border">
                  <a href="javascript:;"><i class="fa fa-phone"></i></a>
                  <h3>Call Us</h3>
                  <p>Give us a call & ask all of your questions</p>
                </div>
              </div>
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border ">
                  <a href="javascript:;"><i class="fa fa-pencil"></i></a>
                  <h3>Email Us</h3>
                  <p>Send us an email with all of your questions</p>
                </div>
              </div>
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border margin_row">
                  <a href="javascript:;"><i class="fa fa-video-camera"></i></a>
                  <h3>Live Chat</h3>
                  <p>Chat with a member of our support team now</p>
                </div>
              </div>
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border margin_row">
                  <a href="javascript:;"><i class="fa fa-certificate"></i></a>
                  <h3>Real Reviews</h3>
                  <p>Read what real customers have to say</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php $this->load->view('include/footer');?>  
    </div>
    <?php $this->load->view('include/footer_js');?>
    <script type="text/javascript">
      $(document).ready(function () {
        get_user_profile('<?=$this->session->login_id?>');
      });
      function get_user_profile(user_id){
          var uurl = BASE_URL+"api/user/getProfile";
          // var post_data = {id:user_id};
           $.ajax({
               url: uurl,
               method: 'POST',
               dataType:'json',
               data: {id:user_id},
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){
                
                if (response.result=="Success") {
                  var userdata = response.data;
                  console.log(userdata);
                  $('#first_name').text(userdata.first_name);
                  $('#last_name').text(userdata.last_name);
                  $('#email').text(userdata.email);
                  $('#address').text(userdata.address);
                  $('#country').text(userdata.country_id_info);
                  $('#state').text(userdata.state_id_info);
                  $('#city').text(userdata.city_id_info);
                }
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
           });
          
        }
        $('.view-reply').click(function () {
            $(this).closest('tr').next('tr').fadeToggle();
        });
    </script> 

   </body>
</html> 