<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
    </head>
    <body>
        <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          <!-- about start -->
            <!-- <div class="allpage_banner_about allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'about.jpg')?>);">
                <h1 class="title_h1">Hosting Plans</h1>
                <p><a href="index.html">Home </a> / Hosting Plans</p>
            </div> -->
          <div class="allpage_banner_hosting allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'banner.jpg')?>);">
              <h1 class="title_h1">Hosting</h1>
              <p><a href="#javascript:;">Home </a> / Hosting</p>
            </div>
            <div class="all_bg padding_all">
              <div class="container">
                <div class="all_maintext">
                  <p>Choose Your</p>
                  <h3>Hosting Plan</h3>
                </div>
              </div>
          </div>
          <div class="host_box row">
            <?php if (isset($hosting_data) && $hosting_data !=null){ 
                    foreach ($hosting_data as $key => $value) { ?>
                      <div class="col-md-3 col-xs-12 host_plan text-center">
                        <!--<img src="upload/hosting_plan/<?php echo $row_host['image']; ?>" width="20%" height="20%">-->
                        <h4><?php echo $value['name']; ?></h4>
                        <?php  echo substr($value['details'],0,270)."..."; ?>
                        <br><br>
                        <a href="<?=base_url('hosting-details/').$value['seo_slug']; ?>" class="btn_order"> Read More </a> 
                      </div>
            <?php } }?> 
          </div>
           <div class="padding_all text-center domain_style">
            <div class="container">
                    <h1 class="h1_title">WE ARE ANNOUNCING PERFECT PACKAGE FOR YOU</h1>
                    <h5>VIPLHOSTING Providing you with a hesitate free web hosting service we take words look the believable.</h5>
                <div class="row">
                    <?php if (isset($hosting_price_details) && $hosting_price_details !=null){ 
                            foreach ($hosting_price_details as $hosting_price_key => $h_value) {
                                ?>
                                <div class="col-md-3 col-xs-12 margin_top" data-aos="flip-left">
                                    <div class="domain_box">
                                        <div class="title">
                                            <h3><?php echo $h_value['title']; ?></h3>
                                        </div>
                                        <div class="domain_box_white">
                                            <h1><?=get_currency($h_value['currency_id'])."".$h_value['price']; ?></h1>
                                            <p>/ Year Regularly</p>
                                        </div>
                                        <ul class="domain_ul">
                                            <?php
                                                if($h_value['id']!="")
                                                {
                                                    foreach ($features_details as $feature_key => $feature_value) { 
                                                        $get_feature = $this->Production_model->get_all_with_where('hosting_related_feature','','',array('hosting_id'=>$h_value['id'],'feature_id'=>$feature_value['id'])); ?>
                                                        <li>
                                                            <?=(isset($get_feature) && $get_feature!=null)?'<span><i class="fa fa-check text-success" aria-hidden="true"></i></span> ':'<span><i class="fa fa-times" aria-hidden="true"></i></span> '?>
                                                            <?php echo $feature_value['title']; ?> 
                                                        </li>
                                                        <?php
                                                    }
                                                }
                                            ?>   
                                        </ul>
                                        <button class="btn_order" data-toggle="modal" data-target="#myModal<?php echo $h_value['id']; ?>">Order Now</button>
                                    </div>
                                </div>
                                <?php 
                            }
                        }   
                    ?>      
                </div>
            </div>
        </div>
      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?> 
   </body>
</html> 