<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
    </head>
    <body>
        <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          <!-- about start -->
            <div class="allpage_banner_about allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'about.jpg')?>);">
                <h1 class="title_h1">Profile</h1>
                <p><a href="<?=base_url()?>">Home </a> / Profile</p>
            </div>
            <div class="all_white padding_all">
              <div class="container">
                <div class="row">
                  <div class="login_box col-md-offset-2 col-md-8 col-xs-12">
                    <h3>Profile</h3>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-offset-2 col-md-8 col-xs-12 contact_box_input">                  
                    <div id="signuperrors" class="col-md-12"></div>
                    <div class="row">
                      <form method="post" name="profile_edit" id="profile_edit">
                        <div class="col-md-12 text-left">
                          <input type="hidden" name="id" value="<?=$this->session->userdata('login_id');?>">
                          <div class="col-md-6">
                                <label>First Name:</label>
                                <input name="first_name" id="first_name" type="text" class="form-control" placeholder="First Name" value="">
                              <p class="err_p" id="first_name_err"></p>
                          </div>
                          <div class="col-md-6">
                              <label>Last Name:</label>
                              <input name="last_name" id="last_name" type="text" class="form-control" placeholder="Last Name" value="">
                            <p class="err_p" id="last_name_err"></p>
                          </div>
                          <div class="clearfix"></div>
                          <div class="col-md-6">
                              <label>Date Of Birth:</label>
                              <input name="dob" id="dob" type="text" class="form-control" placeholder="Date of Birth" value="">
                            <p class="err_p" id="dob_err"></p>
                          </div>
                          <div class="col-md-6">
                              <label>Email:</label>
                              <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="">
                            <p class="err_p" id="email_err"></p>
                          </div>
                          <div class="clearfix"></div>
                          <div class="col-md-12">
                            <label>Company Name:</label>
                              <input type="text" name="company_name" id="company_name" class="form-control" placeholder="Company name">
                          </div>
                          <div class="col-md-12">
                              <label>Address:</label>
                              <textarea type="text" name="address" id="address" class="form-control" placeholder="Address" value="" style="height: 80px;">
                              </textarea>
                            <p class="err_p" id="address_err"></p>
                          </div>
                          
                          <div class="clearfix"></div>
                          
                          <div class="col-lg-4" style="margin-bottom: 1rem">
                              <label class="control-label top-0">Country *</label>
                              <?php
                                  $country_info = get_country();
                                  // echo"<pre>"; print_r($country_info); exit;
                                  $options = array();
                                  $options[NULL] = 'Select Country';
                                  if (count($country_info) > 0) {
                                      foreach ($country_info as $key => $value) {
                                          $options[$value['id']] = $value['country_name'];
                                      }
                                  }
                                  echo form_dropdown('id_country', $options, isset($user_details[0]['id_country']) ? $user_details[0]['id_country'] : '', 'class="form-control" id="id_country"');
                                  echo form_error('id_country', '<label class="error">', '</label>');
                              ?>
                          </div>
                          <div class="col-lg-4" style="margin-bottom: 1rem">
                              <label class="control-label top-0">State *</label>
                              <?php
                                  if (isset($user_details[0]['id_country']) && $user_details[0]['id_country'] != '') {
                                      $state_info = get_state($user_details[0]['id_country']);
                                      $options = array();
                                      $options[NULL] = 'Select State';
                                      if (count($state_info) > 0) {
                                          foreach ($state_info as $key => $value) {
                                              $options[$value['id']] = $value['state_name'];
                                          }
                                      }
                                  } else {
                                      $options = array(
                                          NULL => 'Select State',
                                      );
                                  }
                                  echo form_dropdown('id_state', $options, isset($user_details[0]['id_state']) ? $user_details[0]['id_state'] : '', 'class="form-control" id="id_state"');
                                  echo form_error('id_state', '<label class="error">', '</label>');
                              ?>
                          </div>
                          <div class="col-lg-4" style="margin-bottom: 1rem">
                              <label class="control-label top-0">City *</label>
                              <?php
                                  if (isset($user_details[0]['id_state']) && $user_details[0]['id_state'] != '') {
                                      $city_info = get_city($user_details[0]['id_state']);
                                      $options = array();
                                      $options[NULL] = 'Select City';
                                      if (count($city_info) > 0) {
                                          foreach ($city_info as $key => $value) {
                                              $options[$value['id']] = $value['city_name'];
                                          }
                                      }
                                  } else {
                                      $options = array(
                                          NULL => 'Select City',
                                      );
                                  }
                                  echo form_dropdown('id_city', $options, isset($user_details[0]['id_city']) ? $user_details[0]['id_city'] : '', 'class="form-control" id="id_city"');
                                  echo form_error('id_city', '<label class="error">', '</label>');
                              ?>
                          </div>
                          <div class="clearfix"></div>
                                
                          <div class="col-md-6">
                                <label>Zip Code:</label>
                                <input type="text" name="zip_code" id="zip_code" class="form-control" placeholder="Zip Code" value="">
                            <p class="err_p" id="zip_code_err"></p>
                            </div>
                          
                          <div class="col-md-6">
                                <label>Phone Number:</label>
                                <input type="text" name="phone_number" id="phone_number" class="form-control" placeholder="Phone Number" value="">
                            <p class="err_p" id="phone_number_err"></p>
                            </div>
                          <div class="clearfix"></div>
                          <div class="text-center btn_margin" style="clear:both;">
                            <input type="button" value=" Update " class="btn btn-primary btn_margin check" style="background-color: #ff802b;border-color: #ff802b;">
                          </div>
                        </div>
                      </form> 
                      </div>
                    </div>
                    <div class="">
                      <div class="footer_login_box col-md-offset-2 col-md-8 col-xs-12">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
      <?php $this->load->view('include/footer');?>  
    </div>
    <?php $this->load->view('include/footer_js');?> 
    <script type="text/javascript">
        $("#dob").datepicker({
            format: 'd M yyyy',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            todayHighlight: true,
            // startDate:'+0d',
            endDate: "today",
            autoclose: true,            
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            /*Get cities based on state selection*/
            $('#id_country').change(function () {
                var current = $(this);
                if (current.val() != '') {
                    var post_data = {'id_country': current.val()};
                    $.ajax({
                        url: BASE_URL + 'ajax/get_states',
                        method: 'POST',
                        data: post_data,
                        dataType: 'json',
                        success: function (response) {
                            if (response.success) {
                                $('#id_state').html(response.data);
                                
                            }
                        }
                    });
                }
            });
            /*By default country india selected if any other country is not selected*/
            if ($('#id_country').val() == '') {
                $('#id_country option[value="101"]').attr('selected', 'selected');
                $('#id_country').trigger('change');
            }
            /*Get cities based on state selection*/
            $('#id_state').change(function () {
                var current = $(this);
                if (current.val() != '') {
                    var post_data = {'id_state': current.val()};
                    $.ajax({
                        url: BASE_URL + 'ajax/get_cities',
                        method: 'POST',
                        data: post_data,
                        dataType: 'json',
                        success: function (response) {
                            if (response.success) {
                                $('#id_city').html(response.data);
                            }
                        }
                    });
                }
            });

            get_user_profile('<?=$this->session->userdata('login_id');?>');


        });
        
        $('#profile_edit').validate({
            rules: {
                first_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                email: {
                    required: true,
                    email:true,
                },
                phone_number:{
                    required: true,
                    number:true,
                },
                dob:{
                    required: true,
                    date:true,
                },
                zip_code:{
                    required: true,
                    number:true,
                },
                state:{
                    required: true,
                },
                city:{
                    required: true,
                },
            },
            messages: {
                
                first_name: {
                  required: "Please enter your First name",
                  
                },
                last_name: {
                  required: "Please enter your Last name",
                  
                },
                email: {
                  required: "Please enter your email address",
                  email: "Please Enter valid email address",
                  
                },
                phone_number:{
                    required: "Please Enter Moibile Number",
                    number:"Only numbers",
                },
                dob:{
                    required: "Please Enter Date of birth",
                    date: "Please Enter valid Date of birth",
                },
                zip_code:{
                    required: "Please Enter Pincode",
                    number:"Only digits",
                },
                id_state:{
                    required: "Please Enter State",
                },
                id_city:{
                    required: "Please Enter City",
                },
            }
        });

        // var form = $( "#profile_edit" );
        // form.validate();
        // $( ".check" ).click(function() {
        //   alert( "Valid: " + form.valid() );
        //   if(form.valid()){

        //     register();
        //   }
        // });
        var form = $( "#profile_edit" );
        form.validate();
        $(document).on('click','.check',function(){
        
          //alert( "Valid: " + form.valid() );
          if(form.valid()){
            edit_profile();
          }
     
        });
   
        function edit_profile(){
          errormsg = '';
          var formData = new FormData($('#profile_edit')[0]);
          var uurl = BASE_URL+"api/user/updateProfile";
           $.ajax({
               url: uurl,
               method: 'POST',
               data: formData,
               dataType:'json',
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){
                $('#signuperrors').html('');
                 if (response.result=="Success") {
                   // setTimeout(function() { window.location.reload(); }, 2000);
                    $.alert({
                        title: 'Message',
                        type: 'green',
                        content: 'Profile updated successfully !!',
                    });
                    setTimeout(function() { window.location.reload(); }, 2000);
                 }else if(response.message==""){
                   $.alert({
                        title: 'Message',
                        type: 'red',
                        content: '!',
                    });
                 }else if(response.message==""){
                   $.alert({
                        title: 'Message',
                        type: 'red',
                        content: '!',
                    });
                 }else{
                   errormsg = '<li><i class="fa fa-info-circle"></i>&nbsp;Not Updated.</li>';
                   $('#signuperrors').html('<ul class="form-error">'+errormsg+'</ul>');
                 }
                 $('#signuperrors').show(1000);
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
               cache: false,
               contentType: false,
               processData: false
           });
        }


        function get_user_profile(user_id){
          var uurl = BASE_URL+"api/user/getProfile";
          // var post_data = {id:user_id};
           $.ajax({
               url: uurl,
               method: 'POST',
               dataType:'json',
               data: {id:user_id},
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){
                
                if (response.result=="Success") {
                  var userdata = response.data;
                  // console.log(userdata);
                  $('#first_name').val(userdata.first_name);
                  $('#last_name').val(userdata.last_name);
                  $('#dob').datepicker("setDate", new Date(userdata.dob) );
                  $('#email').val(userdata.email);
                  $('#company_name').val(userdata.company_name);
                  // var address = '<?=nl2br('userdata.address')?>';
                  $('#address').val(userdata.address);
                  $('#zip_code').val(userdata.zip_code);
                  $('#phone_number').val(userdata.phone_number);
                  
                  $("#id_country option").each(function(){
                    if ($(this).val() == userdata.id_country)
                      $(this).attr("selected","selected");
                  });

                  var state_data = {'id_country': userdata.id_country};
                    $.ajax({
                        url: BASE_URL + 'ajax/get_states',
                        method: 'POST',
                        data: state_data,
                        dataType: 'json',
                        success: function (response) {
                            if (response.success) {
                                $('#id_state').html(response.data);
                                $("#id_state option").each(function(){
                                if ($(this).val() == userdata.id_state)
                                  $(this).attr("selected","selected");
                              });
                                
                            }
                        }
                    });
                  var post_data = {'id_state': userdata.id_state};
                      $.ajax({
                          url: BASE_URL + 'ajax/get_cities',
                          method: 'POST',
                          data: post_data,
                          dataType: 'json',
                          success: function (response) {
                              if (response.success) {
                                  $('#id_city').html(response.data);
                                  $("#id_city option").each(function(){
                                    if ($(this).val() == userdata.id_city)
                                      $(this).attr("selected","selected");
                                  });
                              }
                          }
                      });
                  
                  // $('#id_country').val(userdata.id_country);
                  
                    /*$.alert({
                        title: 'Message',
                        type: 'green',
                        content: 'Profile updated successfully !!',
                    });*/
                    // setTimeout(function() { window.location.href = BASE_URL; }, 2000);
                 }else{
                 }
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
           });
          
        }
    </script>
   </body>
</html> 