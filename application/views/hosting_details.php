<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
    </head>
    <body>
        <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          <!-- about start -->
            <!-- <div class="allpage_banner_about allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'about.jpg')?>);">
                <h1 class="title_h1">Hosting Plans</h1>
                <p><a href="index.html">Home </a> / Hosting Plans</p>
            </div> -->
          <div class="allpage_banner_hosting allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'banner.jpg')?>);">
              <h1 class="title_h1">Hosting</h1>
              <p><a href="#javascript:;">Home </a> / Hosting</p>
            </div>
            <div class="about_all padding_all testimonial_all">
            <div class="container">         
              <div class="row text-center">
                <div class=" col-md-12">
                  <?php 
                    if (isset($hosting_plan_data) && $hosting_plan_data !=null){ 
                        $hosting_plan_data = $hosting_plan_data[0];
                      ?>
                      <div class="row">
                        <div class="col-md-4 back_about" style="background:none;"><img src="<?=base_url(HOSTING_IMAGE).$hosting_plan_data['host_image']; ?>" width="100%">
                        </div>
                        <div class="col-md-8 white_about">
                          <h3><?php echo $hosting_plan_data['name']; ?></h3>
                          <?php echo $hosting_plan_data['details']; ?>
                        </div>
                      </div><hr>
                      <?php 
                    }
                  ?> 
                </div>
              </div>
            </div>
      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?> 
   </body>
</html> 