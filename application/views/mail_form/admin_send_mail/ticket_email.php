<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
        <title>Crystal Hosting</title>
        <link rel="icon" type="image/png" sizes="56x56" href="<?= base_url()?>assets/img/favicon.png">
        <style>
            .logo img,header{width:100%}.container,header{display:inline-block}body{text-align:center;font-family:sans-serif;margin:0;padding:0}*{box-sizing:border-box}a,a:hover{color:#fff;text-decoration:none}.logo{float:left;max-width:133px}.view-link{float:right;margin:21px 0}.view-link a{padding:8px 16px;font-size:18px;background-color:#e10a0a}.view-link a:hover{background-color:#272d32}.email-icon,.verify-btn{background-color:#e10a0a}header{padding:20px 0}.container{width:82%;margin:auto}.email-icon{padding:17px 0}.confirmation-box{box-shadow:0 0 10px 1px #848484;padding:0;margin:25px auto 31px;width:80%}.discription{padding:24px 10px}.verify-btn{padding:10px;color:#fff;display:block;width:43%;margin:auto}.verify-btn:hover{background-color:#272d32}.social a{font-size:24px;color:#fff;border-radius:6px;display:inline-block;width:40px;height:40px;margin:0 6px;text-align:center;}.fb{background-color:#3b5998}.twitter{background-color:#55acee}.email{background-color:#fbad1b}@media all and (max-width:736px){.verify-btn{font-size:13px!important}}@media all and (max-width:530px){.logo{display:inline-block;float:none}.view-link{display:block;float:none;margin:36px 0 9px}}

            	/* table */
				table { font-size: 75%; table-layout: fixed; width: 100%; }
				table { border-collapse: separate; border-spacing: 2px; }
				.meta th{ border-width: 1px;
				    padding: 5px 5px;
				    position: relative;
				    text-align: left;
				    font-size: 13px; }
				.inventory th{
					padding: 12px 3px;
				    font-size: 12px;
				}
				td,th{ border-width: 1px;
				    padding: 12px 8px;
				    position: relative;
				    text-align: left;
				    font-size: 15px; 
					color:black;}
				th, td { border-radius: 0.25em; border-style: solid; }
				th { background: #EEE; border-color: #BBB; }
				td { border-color: #DDD; }
				h2{ color: red;margin:0;text-align:center; }
        </style>
    </head>
	<body>
		<header>
			<div class="container">
                <a href="<?= base_url()?>" class="logo">
                    <?php
	                    $profile_photo = $this->Production_model->get_all_with_where('user','','',array());
	                    if ($profile_photo != null) {
	                    ?>
	                        <img src="<?= $profile_photo !=null ? base_url('assets/uploads/profile_photo/').$profile_photo[0]['profile_photo'] : base_url('assets/uploads/profile_photo/default-image.png')?>">
	                    <?php }
	                ?>
                </a>
                <div class="view-link" style="background-color:#999999;color:#FFFFFF;">
                    <a href="<?= base_url()?>" style="background-color:#999999;color:#FFFFFF;">View Website</a>
                </div>
            </div>
		</header>
		<div style="clear:both">
		</div>
		<div class="container">
			<div class="confirmation-box col-md-8 col-md-offset-2">
				<div class="email-icon" style="text-align:center;background-color:#999999;">
                    <img src="<?= base_url()?>assets/img/mail.png" width="55px">
                </div>
				<div class="table-form">
					<div class="discription">
						<h2>Ticket Generated !!</h2>
					</div>
					<table>
						<?php if($name){ ?>
							<tr>
								<td>
									<strong>
										Name :
									</strong>
								</td>
								<td>
									<?= $name?>
								</td>
							</tr>	
						<?php }?>
						<?php if($ticket_no){ ?>
							<tr>
								<td>
									<strong>
										Ticket No. :
									</strong>
								</td>
								<td>
									<?= $ticket_no?>
								</td>
							</tr>	
						<?php }?>
						<?php if($phone_number){ ?>						
							<tr>
								<td>
									<strong>
										Mobile no :
									</strong>
								</td>
								<td>
									<?= $phone_number?>
								</td>
							</tr>
						<?php } ?>	
						<?php if($email){ ?>	
							<tr>
								<td>
									<strong>
										E-mail :
									</strong>
								</td>
								<td>
									<a href="javascript:void(0)" style="color: #000;"><?= $email?></a>
								</td>
							</tr>
						<?php } ?>
						<?php if($message){ ?>
							<tr>
								<td>
									<strong>
										Message :
									</strong>
								</td>
								<td>
									<?= $message?>
								</td>
							</tr>
						<?php } ?>	
					</table>
				</div>	
			</div>
		</div>
		<div class="container">
            <footer style="text-align:center;">
                <div class="col-md-8 col-md-offset-2">
                    <h3>
                        Stay in Touch
                    </h3>
                    <?php
                        $social_media_link = $this->Production_model->get_all_with_where('social_links','','',array());
                        extract($social_media_link[0]);
                    ?>
                    <div class="social">
                        <a href="<?=$facebook_link?>" class="fb">
                            <img src="<?= base_url()?>assets/img/fb.png" width="10px">
                        </a>
                        <a href="<?=$twiter_link?>" class="twitter">
                            <img src="<?= base_url()?>assets/img/tw.png" width="20px">
                        </a>
                        <a href="<?= base_url()?>" class="email">
                            <img src="<?= base_url()?>assets/img/mail.png" width="20px">
                        </a>
                    </div>
                    <h4 style="margin: 32px 0">Copyright &copy; <?= date('Y')?>.All rights are reserved.</h4>
                </div>
            </footer>
        </div>	
	</body>
</html>