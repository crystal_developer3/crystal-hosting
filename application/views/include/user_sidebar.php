<div class="col-md-3 pull-md-left sidebar">
  <div menuitemname="Client Details" class="panel panel-default">
    
    <div class="panel-heading">
        <h3 class="panel-title"> <i class="fa fa-user"></i>&nbsp; Your Info
        </h3>
    </div>
    <div class="panel-body">
      <p><strong><span id="first_name"></span> <span id="last_name"></span></strong></p><p></p>
      <p>
        <span id="address"></span><br>
        <span id="country"></span>,<span id="state"></span>,<span id="city"></span>
      </p>
    </div>
    <div class="panel-footer clearfix">
      <a href="<?=base_url('profile')?>" class="btn btn-success btn-sm btn-block">
        <i class="fa fa-pencil"></i> Update
      </a>
    </div>
    <!-- <div menuitemname="Client Contacts" class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">
        <i class="fa fa-folder-o"></i>&nbsp;Contacts
        </h3>
      </div>
      <div class="list-group">
        <div menuitemname="No Contacts" class="list-group-item" id="Secondary_Sidebar-Client_Contacts-No_Contacts">
          No Contacts Found
        </div>
      </div>
      <div class="panel-footer clearfix">
        <a href="#" class="btn btn-default btn-sm btn-block">
        <i class="fa fa-plus"></i> New Contact...
        </a>
      </div>
    </div> -->
    <div menuitemname="Client Shortcuts" class="panel panel-default">
      <!-- <div class="panel-heading">
        <h3 class="panel-title">
        <i class="fa fa-bookmark"></i>&nbsp;  Shortcuts
        </h3>
      </div> -->
      <div class="list-group">
        <a menuitemname="Register New Domain" href="<?=base_url('domains')?>" class="list-group-item" id="Secondary_Sidebar-Client_Shortcuts-Register_New_Domain">
          <i class="fa fa-globe fa-fw"></i>&nbsp; Register a New Domain
        </a>
        <a menuitemname="Logout" href="<?=base_url('login/logout')?>" class="list-group-item" id="     Secondary_Sidebar-Client_Shortcuts-Logout">
          <i class="fa fa-arrow-left fa-fw"></i>&nbsp;  Logout
        </a>
      </div>
    </div>
  </div>
</div>