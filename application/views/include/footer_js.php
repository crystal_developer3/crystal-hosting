<script type="text/javascript" src="<?= base_url()?>assets/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="<?= base_url()?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url()?>assets/js/modernizr.js"></script> 
<script type="text/javascript" src="<?= base_url()?>assets/js/jquery.animateSlider.js"></script>
<script type="text/javascript" src="<?= base_url()?>assets/js/aos.js"></script>
<script type="text/javascript" type="text/javascript" src="<?= base_url()?>assets/js/script.js"></script>
<script type="text/javascript" type="text/javascript" src="<?= base_url()?>assets/js/validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>  
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>


<script type="text/javascript">
	function nl2br (str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}
function get_user_profile(user_id){
          var uurl = BASE_URL+"api/user/getProfile";
          // var post_data = {id:user_id};
           $.ajax({
               url: uurl,
               method: 'POST',
               dataType:'json',
               data: {id:user_id},
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){
                if (response.result=="Success") {
                  var userdata = response.data;
                  console.log(userdata);
                  $('#first_name').text(userdata.first_name);
                  $('#last_name').text(userdata.last_name);
                  $('#email').text(userdata.email);
                  $('#address').html(nl2br(userdata.address));
                  $('#country').text(userdata.country_id_info);
                  $('#state').text(userdata.state_id_info);
                  $('#city').text(userdata.city_id_info);
                }
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
           });
          
      }
</script>
