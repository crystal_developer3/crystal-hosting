<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
      <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body>
        <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          <!-- about start -->
            

            <div class="allpage_banner_contact allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'contactus.jpg')?>);">
                <h1 class="title_h1">Contact Us</h1>
                <p><a href="<?=base_url()?>">Home </a> / Contact Us</p>
            </div>
            <div class="padding_all all_main text-center">
                <div class="container">
                    <div class="col-md-4 col-xs-12 contact_box">
                        <i class="fa fa-phone"></i>
                        <p><?=$admin_details[0]['mobile_number_1'] ?></p>
                    </div>
                    <div class="col-md-4 col-xs-12 contact_box">
                        <i class="fa fa-envelope"></i>
                        <p><?=$admin_details[0]['email_address'] ?></p>
                    </div>
                    <div class="col-md-4 col-xs-12 contact_box">
                        <i class="fa fa-map-marker"></i>
                        <p><?=nl2br($admin_details[0]['address']) ?></p>
                    </div>
                </div>
            </div>
            <div class="all_white">
                <div class="container">
                    <div class="row">
                        <!-- <div class="col-md-6 contact_box_input"> -->
                        <div class="col-md-6 contact_box_input">
                            <?php $this->load->view('include/messages');?>    
                        <form method="post"  id="contact-form">
                            <div class="form-group">
                                <label for="name">Name *</label>
                                <input type="text" name="name" id="name" class="form-control">
                                <p class="msg_p error" id="err_name"></p>
                            </div>
                            <div class="form-group">
                                <label for="email">Email *</label>
                                <input type="text" name="email" id="email" class="form-control">
                                <p class="msg_p error" id="err_email"></p>
                            </div>
                            <div class="form-group">
                                <label for="phone_number">Phone Number *</label>
                                <input type="number" name="phone_number" id="phone_number" class="form-control">
                                <p class="msg_p error" id="err_phone_number"></p>
                            </div>
                            <div class="form-group">
                                <label for="subject">Subject *</label>
                                <input type="text" name="subject" id="subject" class="form-control">
                                <p class="msg_p error" id="err_subject"></p>
                            </div>
                            <div class="form-group">
                                <label for="message">Message *</label>
                                <textarea type="text" name="message" id="message" rows="5" class="form-control"></textarea>
                                <p class="msg_p error" id="err_message"></p>
                            </div>
                            <div class="form-group">
                                <div class="g-recaptcha" data-sitekey="6Le-wvkSAAAAAPBMRTvw0Q4Muexq9bi0DJwx_mJ-" style="margin-top: 2%;"></div>
                                <span class="error_captcha" style="color: #fc3a3a;"></span>   
                            </div>
                            <input type="button" class="btn_order btn-lg btn_margin check" value=" Send Message ">
                            <!-- <button name="submit" class="btn_order btn-lg btn_margin">Send Message</button> -->
                            </form>
                        </div>
                        <div class="col-md-6 col-xs-12 map_contact">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3691.6887811443976!2d70.80071531441338!3d22.289775985328944!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3959ca0fa7abadb3%3A0xd713430d71c2669f!2sCrystal+Infoway!5e0!3m2!1sen!2sin!4v1482392604752" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <!-- contact over -->
      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?>
    <script type="text/javascript">
        $('#contact-form').validate({
            rules: {
                name: {
                    required: true,
                },
                email: {
                    required: true,
                    email:true,
                },
                phone_number: {
                    required: true,
                    minlength:10,
                    maxlength:15,
                },
                subject: {
                    required: true,
                },
                message: {
                    required: true,
                },

            },
            messages: {
                name: {
                  required: "Please enter Name",                      
                },
                email: {
                  required: "Please enter Email ",
                  email:"Please enter valid Email",                     
                },
                phone_number : {
                    required: "Please enter phone number",
                    minlength: "Please enter minimum 10 digits",
                    maxlength:"Please enter maximum 15 digits",
                },
                subject: {
                    required: "Please enter Subject",
                },
                message: {
                    required: "Please enter Msessage",
                },
            }
        });

        var form = $( "#contact-form" );
        form.validate();
        $(document).on('click','.check',function(){
        
          //alert( "Valid: " + form.valid() );
          if(form.valid()){

            add_contact();
          }
     
        });
   
        function add_contact(){
           var formData = new FormData($('#contact-form')[0]);
            var uurl = BASE_URL+"api/user/add_contact_us";

            $.ajax({
               url: uurl,
               type: 'POST',
               dataType: 'json',
               data: formData,
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){

                if (response.result=="Success") {
                    $.alert({
                        type: 'green',
                        title: 'Added',
                        content: response.message,
                    });
                    setTimeout(function() { window.location.reload(); }, 2000);

                }else if (response.result=="Fail") {
                    $.alert({
                        type: 'red',
                        title: 'Fail',
                        content: response.message,
                    });
                }
                // console.log(response);
                $('#signuperrors').html('');
                    // if(response==1){
                    //     errormsg = '<li><i class="fa fa-check-circle" style="color:#0F8C02"></i>&nbsp;Registered successfully.</li>';
                    //     $('signuperrors').html('<ul class="form-info">'+errormsg+'</ul>');
                    //     // setTimeout(function() { window.location.reload(); }, 2000);
                    //     setTimeout(function() { window.location.href = BASE_URL; }, 2000);
                    // }else if(response==2){
                    //     errormsg += '<li class="error">Email or phone number already exists !</li>';
                    //     $('#email').css({"background-color":"#FFECED","border":"3px solid #FFB9BD"});

                    //     $('#mobile_no').css({"background-color":"#FFECED","border":"3px solid #FFB9BD"});
                    //     $('#signuperrors').html('<ul class="form-error">'+errormsg+'</ul>');
                    // }else{
                    //     errormsg = '<li><i class="fa fa-info-circle"></i>&nbsp;Not Registered.</li>';
                    //     $('#signuperrors').html('<ul class="form-error">'+errormsg+'</ul>');
                    // }
                    
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
               cache: false,
               contentType: false,
               processData: false
            });
        }
    </script>

   </body>
</html> 
<?php /* ?>
<!-- Contact Area Start Here -->
<!-- <section class="s-space-bottom-full bg-accent-shadow-body">
    <div class="container">
        <div class="breadcrumbs-area">
            <ul>
                <li><a href="<?= base_url()?>">Home</a> -</li>
                <li class="active">Contact Page</li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                <?php $this->load->view('include/messages');?>
                <div class="gradient-wrapper mb--sm">
                    <div class="gradient-title">
                        <h2>Contact With us</h2>
                    </div>
                    <div class="contact-layout1 gradient-padding">
                        <p>If you did not find the answer to your question or problem, please get in touch with us using the form below and we will respond to your message as soon as possible.</p>
                        <form id="contact-form" class="contact-form" method="post" action="<?= base_url('contact/add_contact')?>">
                            <fieldset>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" placeholder="Your Name" class="form-control" name="name" id="name" data-error="Name field is required" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="email" placeholder="Your E-mail" class="form-control" name="email" id="user_email" data-error="Email field is required" required>
                                            <div id="valid" style="color: #F66249"></div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" placeholder="Subject" class="form-control" name="subject" id="subject" data-error="Subject field is required" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <textarea placeholder="Message" class="textarea form-control" name="message" id="message" rows="7" cols="20" data-error="Message field is required" required></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <button type="submit" class="cp-default-btn-sm-primary check">Send Message</button>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6 col-sm-12 col-12">
                                        <div class='form-response'></div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
<!-- Contact Area End Here -->
<!-- <?php $this->load->view('include/copyright');?>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script> 
        $(".check").click(function(){
            // var response = grecaptcha.getResponse();
            if(isemptyfocus('name') || isemptyfocus('user_email') || isemptyfocus('subject') || isemptyfocus('message')){
                return false;
            }
            if(isvalidemail('user_email')){
                return false;
            }
        });
    </script>
<?php $this->load->view('include/footer');?> -->
<?php */?>