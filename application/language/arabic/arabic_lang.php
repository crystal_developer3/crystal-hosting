<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	//=================== header menu ===================//
	
	$lang['otg'] = 'Otg';
	$lang['title'] = 'Title';
	$lang['signup'] = 'Signup';
	$lang['welcome'] = 'Welcome to';
	$lang['call_us'] = 'Call Us';
	$lang['success'] = 'Success';
	$lang['error'] = 'Error';
	$lang['save'] = 'Save';
	$lang['cancel'] = 'Cancel';

	$lang['my_account'] = 'My Account';
	$lang['wishlist'] = 'Wishlist';
	$lang['blog'] = 'Blog';
	$lang['todays_deal'] = 'Todays Deal';
	$lang['login'] = 'Login';
	$lang['logout'] = 'Logout';
	$lang['shopping_cart'] = 'Shopping Cart';
	$lang['recent_add_item'] = 'Recently added item(s)';

	$lang['all_categories'] = 'All Categories';
	$lang['search'] = 'Search';
	$lang['item'] = 'Item';
	$lang['categories'] = 'Categories';
	$lang['home'] = 'Home';
	$lang['products'] = 'Products';
	$lang['product'] = 'Product';
	$lang['contact'] = 'Contact';
	$lang['about_us'] = 'About us';
	$lang['best_seller'] = 'Best Seller';
	$lang['sale'] = 'sale';
	$lang['new'] = 'new';
	$lang['add_to_cart'] = 'Add to Cart';

	$lang['featured_product'] = 'Featured products';
	$lang['top_seller'] = 'Top Sellers';
	$lang['hot'] = 'Hot';
	$lang['hot_deal'] = 'Hot deal';
	$lang['news_letter'] = 'News letter';
	$lang['subscribe_msg'] = 'Subscribe to be the first to know about Sales, Events, and Exclusive Offers!';
	$lang['subscribe'] = 'Subscribe';
	$lang['popup_msg'] = 'Don’t show this popup again';

	$lang['login'] = 'Login';
	$lang['login_wc_back'] = 'Welcome back! Sign in to your account';
	$lang['email_address'] = 'Email address';
	$lang['email_or_phone'] = 'Email Or phone no';
	$lang['password'] = 'Password';
	$lang['forgot_password'] = 'Lost your password?';
	$lang['remember'] = 'Remember me';
	$lang['register'] = 'Register';
	$lang['create_account'] = 'Create your very own account';

	$lang['name'] = 'Name';
	$lang['mobile_no'] = 'Mobile no';
	$lang['signup_msg'] = 'Sign up today and you will be able to :';
	$lang['speed_checkout'] = 'Speed your way through checkout';
	$lang['track_your_order'] = 'Track your order';
	$lang['keep_record'] = 'Keep a record of all your purchases';

	$lang['sign_up_newsletter'] = 'Sign up for newsletter';
	$lang['loream_ipsum'] = 'Lorem Ipsum is simply dummy text of the print and typesetting industry.';

	$lang['new_project'] = "We're available for new projects";
	$lang['lets_get'] = "Let's get in touch";
	$lang['lorem_ipsum_txt'] = "Lorem ipsum dolor sit amet onsectetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor dapibus eget. Mauris tincidunt aliquam lectus sed vestibulum. Vestibulum bibendum suscipit mattis.";

	$lang['praesent_nec'] = "Praesent nec tincidunt turpis.";
	$lang['aliquam'] = "Aliquam et nisi risus. Cras ut varius ante.";
	$lang['ut_congue'] = "Ut congue gravida dolor, vitae viverra dolor.";
	$lang['make_enquiry'] = "Make an enquiry";
	$lang['email'] = "Email";
	$lang['send'] = "Send";
	$lang['phone'] = "Phone";
	$lang['subject'] = "Subject";
	$lang['message'] = "Message";
	$lang['website'] = "Website";
	$lang['send_message'] = "Send Message";
	$lang['clear'] = "Clear";

	$lang['our_team'] = "Our Team";
	$lang['shop_by'] = "Shop By";
	$lang['price_range'] = "Price range";
	$lang['shopping_options'] = "Shopping Options";
	$lang['color'] = "Color";
	$lang['price'] = "Price";
	$lang['my_cart'] = "My Cart";
	$lang['cart_subtotal'] = "Cart Subtotal";
	$lang['checkout'] = "Checkout";

	$lang['compare'] = "Compare";
	$lang['clear_all'] = "Clear All";
	$lang['special_products'] = "Special Products";
	$lang['related_products'] = "Related Products";
	$lang['all_products'] = "All Products";
	$lang['popular_tags'] = "Popular Tags";
	$lang['compare_products'] = "Compare Products";

	$lang['value'] = "Value";
	$lang['quality'] = "Quality";
	$lang['rate_this_product'] = "How do you rate this product?";
	$lang['wishlist_not_available'] = "Wish list not available...!";
	$lang['add_to_wishlist'] = "Add to Wishlist";

	$lang['wishlist_login_error'] = "Please login after add wishlist";
	$lang['email_allredy'] = "Email allredy exist...!";
	$lang['subscribe_add'] = "Subscribe Added Successfully...!";
	$lang['subscribe_not_add'] = "Subscribe not added please try again...!";
	$lang['blog_details'] = 'Blog Details';
	$lang['brand'] = 'Brand';
	$lang['select_color'] = 'Please select Color';
	$lang['select_size'] = 'Please select Size';
	$lang['payment_method'] = 'Payment Method';
	$lang['product_not_available'] = 'Product Not Available...!';
	$lang['hot_deal_not_available'] = 'Hot deal Not Available...!';
	$lang['review_not_available'] = 'Review Not Available...!';
	$lang['order_not_available'] = 'Order Not Available...!';
	$lang['out_of_stock'] = 'Product is out-of-stock';

	$lang['top_categories'] = 'Top Categories';
	$lang['daily_deals'] = 'Daily Deals';
	$lang['free_delivery'] = 'Free Delivery';
	$lang['support 24/7'] = 'Support 24/7';
	$lang['free_return'] = 'Free return';
	$lang['big_saving'] = 'Big Saving';
	$lang['our_brands'] = 'Our Brands';
	$lang['sort_by'] = 'Sort By';

	$lang['show'] = 'Show';
	$lang['Name (A to Z)'] = 'Name (A to Z)';
	$lang['Name(Z - A'] = 'Name(Z - A';
	$lang['price(low &gt; high'] = 'price(low &gt; high';
	$lang['price(high &gt; low)'] = 'price(high &gt; low)';



	//===========================================================//
	//================ admin panel keywords strat ===============// 
	//===========================================================//

	$lang['english'] = "English";
	$lang['arabic'] = "Arabic";
	$lang['added_date'] = "Added Date";
	$lang['modified_date'] = "Modified Date";
	$lang['add_modified_date'] = "Added/Modified Date";

	$lang['profile'] = "Profile";
	$lang['signout'] = "Sign out";
	$lang['add'] = "Add";
	$lang['view'] = "View";
	$lang['edit'] = "Edit";
	$lang['submit'] = "Submit";
	$lang['back'] = "Back";
	$lang['action'] = "Action";
	$lang['active'] = "Active";
	$lang['deactive'] = "Deactive";
	$lang['create_date'] = "Create date";
	$lang['user_name'] = "User name";

	$lang['change_password'] = "Change Password";
	$lang['old_password'] = "Old-Password";
	$lang['re_enter_password'] = "Re-enter Password";

	$lang['user_list'] = "User list";
	$lang['home_slider'] = "Home Slider";
	$lang['category'] = "Category";
	$lang['subcategory'] = "Subcategory";
	$lang['subscribe_list'] = "Subscribe list"; 

	$lang['settings'] = "Settings";
	$lang['toggle_navigation'] = "Toggle navigation";

	$lang['no'] = "No";
	$lang['status'] = "Status";
	$lang['click_chanage_status'] = "Click to change status";

	$lang['product_attribute'] = "Product Attribute";
	$lang['attribute'] = "Attribute";
	$lang['product_ad'] = "Product Add";
	$lang['attribute_name'] = "Attribute Name";
	$lang['product_name'] = "Product Name";
	$lang['product_image'] = "Product Image";
	$lang['similar_image'] = "Similar Image";
	$lang['current_image'] = "Current Image";
	$lang['product_discount'] = "Product Discount";
	$lang['product_quantity'] = "Product Quantity";
	$lang['label'] = "Label";

	$lang['meta_keywords'] = "Meta Keywords";
	$lang['meta_description'] = "Meta Description";
	$lang['meta_title'] = "Meta Title";
	$lang['slug_name'] = "Slug Name";
	$lang['seo_page'] = "Seo Page Manage";
	$lang['meta'] = "Meta";
	$lang['page'] = "Page";
	$lang['page'] = "Page";
	$lang['details'] = "Details";
	$lang['menu_allredy_added'] = "This page is allredy added in meta tags";

	$lang['category_name'] = "Category Name";
	$lang['add_category'] = "Add Category";
	$lang['delete'] = "Delete";

	$lang['sub_category'] = "Sub Category";
	$lang['sub_category_name'] = "Sub Category Name";
	$lang['add_category'] = "Add Sub Category";

	$lang['subsubcategory'] = "Sub Sub category";
	$lang['sub_of_subcat_name'] = "Sub Of Subcat Name";

	$lang['slider_image'] = "Slider Image";
	$lang['add_slider_img'] = "Add Slider image";
	$lang['slider_position'] = "Slider Position";
	$lang['description'] = "Description";
	$lang['icon'] = "Icon";	
	$lang['iconimage'] = "Current Icon";	
	$lang['image'] = "Image";	
	$lang['categoryimage'] = "Current Image";	
	$lang['slider_link'] = "Slider Link";	

	$lang['currency_manage'] = "Currency Manage";	
	$lang['currency_name'] = "Currency Name";	
	$lang['currency_unit'] = "Currency Unit";	
	$lang['currency_price'] = "Currency Price(Rate)";	
	$lang['product_color'] = "Product Color";	
	$lang['product_comment'] = "Product Comment";	
	$lang['color_name'] = "Color Name";	

	$lang['slider_add'] = 'Slider Add Successfully....!';
	$lang['slider_not_add'] = 'Slider Not Added....!';
	$lang['slider_update'] = 'Slider Update Successfully....!';
	$lang['slider_not_update'] = 'Slider Not Updated....!';
	$lang['slider_delete'] = 'Slider Deleted Successfully....!';
	$lang['slider_not_delete'] = 'Slider Not Deleted....!';
	$lang['maxfile_upload'] = 'Please upload file with size less than';

	$lang['category_add'] = 'Category Add Successfully....!';
	$lang['category_not_add'] = 'Category Not Added....!';
	$lang['category_update'] = 'Category Update Successfully....!';
	$lang['category_not_update'] = 'Category Not Updated....!';
	$lang['category_delete'] = 'Category Deleted Successfully....!';
	$lang['category_not_delete'] = 'Category Not Deleted....!';

	$lang['product_add'] = 'Product Add Successfully....!';
	$lang['product_not_add'] = 'Product Not Added....!';
	$lang['product_update'] = 'Product Update Successfully....!';
	$lang['product_not_update'] = 'Product Not Updated....!';
	$lang['product_delete'] = 'Product Deleted Successfully....!';
	$lang['product_not_delete'] = 'Product Not Deleted....!';

	$lang['color_add'] = 'Color Add Successfully....!';
	$lang['color_not_add'] = 'Color Not Added....!';
	$lang['color_update'] = 'Color Update Successfully....!';
	$lang['color_not_update'] = 'Color Not Updated....!';
	$lang['color_delete'] = 'Color Deleted Successfully....!';
	$lang['color_not_delete'] = 'Color Not Deleted....!';

	$lang['new_arrivals'] = 'New Arrivals';
	$lang['special_product'] = 'Special Product';
	$lang['related_product'] = 'Related Product';
	$lang['product_ratting'] = 'Product Ratting';

	$lang['added'] = 'Add Successfully....!';
	$lang['not_added'] = 'Not Added....!';
	$lang['updated'] = 'Update Successfully....!';
	$lang['not_updated'] = 'Not Updated....!';
	$lang['deleted'] = 'Delete Successfully....!';
	$lang['not_deleted'] = 'Not Deleted....!';

	$lang['excel_import'] = 'Excel Imported.!!!';
	$lang['file_not_upload'] = 'File Not Uploaded.!!!';

	$lang['about'] = "About";
	$lang['about_add'] = 'About Add Successfully....!';
	$lang['about_not_add'] = 'About Not Added....!';
	$lang['about_update'] = 'About Update Successfully....!';
	$lang['about_not_update'] = 'About Not Updated....!';
	$lang['about_delete'] = 'About Deleted Successfully....!';
	$lang['about_not_delete'] = 'About Not Deleted....!';

	$lang['blog'] = "Blog";
	$lang['blog_type'] = "Blog Type";
	$lang['blog_add'] = 'Blog Add Successfully....!';
	$lang['blog_not_add'] = 'Blog Not Added....!';
	$lang['blog_update'] = 'Blog Update Successfully....!';
	$lang['blog_not_update'] = 'Blog Not Updated....!';
	$lang['blog_delete'] = 'Blog Deleted Successfully....!';
	$lang['blog_not_delete'] = 'Blog Not Deleted....!';

	$lang['social-links'] = "Social links";
	$lang['social-links_add'] = 'Social link Add Successfully....!';
	$lang['social-links_not_add'] = 'Social link Not Added....!';
	$lang['social-links_update'] = 'Social link Update Successfully....!';
	$lang['social-links_not_update'] = 'Social link Not Updated....!';
	$lang['social-links_delete'] = 'Social link Deleted Successfully....!';
	$lang['social-links_not_delete'] = 'Social link Not Deleted....!';

	$lang['website-settings'] = "Website Settings";
	$lang['website-settings_add'] = 'Website Setting Add Successfully....!';
	$lang['website-settings_not_add'] = 'Website Setting Not Added....!';
	$lang['website-settings_update'] = 'Website Setting Update Successfully....!';
	$lang['website-settings_not_update'] = 'Website Setting Not Updated....!';
	$lang['website-settings_delete'] = 'Website Setting Deleted Successfully....!';
	$lang['website-settings_not_delete'] = 'Website Setting Not Deleted....!';	
	
	$lang['facebook_link'] = 'Facebook Link';
	$lang['google_plus_link'] = 'Google+ Link';
	$lang['twiter_link'] = 'Twiter Link';
	$lang['linkedin'] = 'LinkedIn';
	$lang['pinterest_link'] = 'Pinterest Link';

	$lang['testimonial'] = 'Testimonial';
	$lang['testimonial_name'] = 'Testimonial Name';
	$lang['testimonial_title'] = 'Testimonial Title';
	$lang['testimonial_position'] = 'Testimonial Position';
	$lang['brand_icon'] = 'Brand Icon';
	$lang['product_slider'] = 'Product Slider';

	$lang['top_rated'] = 'Top Rated';
	$lang['top_rated_pro'] = 'Top Rated Product';
	$lang['product_sale'] = 'Product Sale';
	$lang['on_sale'] = 'ON SALE';
	$lang['hot_deal'] = 'Hot Deals';
	$lang['order_manage'] = 'Order Manage';
	$lang['order_details'] = 'Order Details';
	$lang['order_no'] = 'Order No';
	$lang['user_details'] = 'User Details';
	$lang['address'] = 'Address';
	$lang['total_price'] = 'Total Price';
	$lang['configure_email_msg'] = 'Configure your server email address add , other wise not send in email';
	$lang['configure_name_msg'] = 'Configure your server name add';

	$lang['add_cart_success'] = 'Product Add to cart successfully...!';
	$lang['add_cart_success'] = 'Product Add to cart successfully...!';
	$lang['order_delete'] = 'Order deleted successfully...!';
	$lang['order_not_delete'] = 'Order not deleted successfully...!';

	//===========================================================//
	//================ admin panel keywords end =================// 
	//===========================================================//
?>