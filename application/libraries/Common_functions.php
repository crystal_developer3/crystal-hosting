<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Common_functions {

    public function __construct($params = array()) {
        $this->CI = & get_instance();
    }

    public function get_users() {
        $conditions = array("select" => "full_name,id", "where" => array("role" => "User", "is_enable" => "1"));
        $info = $this->CI->common_model->select_data("user", $conditions);
        if ($info['row_count'] > 0) {
            return $info;
        } else {
            return array();
        }
    }

    public function get_wallets() {
        $conditions = array("select" => "wallet_name,id", "where" => array("is_enable" => "1"));
        $info = $this->CI->common_model->select_data("wallet", $conditions);
        if ($info['row_count'] > 0) {
            return $info;
        } else {
            return array();
        }
    }

    public function get_payment_types() {
        $conditions = array("select" => "payment_type,id", "where" => array("is_enable" => "1"));
        $info = $this->CI->common_model->select_data("payment_type", $conditions);
        if ($info['row_count'] > 0) {
            return $info;
        } else {
            return array();
        }
    }

    public function get_operators() {
        $conditions = array("select" => "operator_name,id", "where" => array("is_enable" => "1"));
        $info = $this->CI->common_model->select_data("operator", $conditions);
        if ($info['row_count'] > 0) {
            return $info;
        } else {
            return array();
        }
    }

    public function updatesession($id) {
        $conditions = array("where" => array("id" => $id));
        $user_info = $this->CI->common_model->select_data("user", $conditions);
        $info = array();
        if ($user_info['row_count'] > 0) {
            $info = $user_info['data'][0];
        }
        $this->CI->session->user_info = $info;
    }

}
